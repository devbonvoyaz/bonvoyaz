import * as React from "react";
import {
  Button,
  View,
  Text,
  Dimensions,
  TextInput,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
  Image,
} from "react-native";
import RBSheet from "react-native-raw-bottom-sheet";
import Icon from "react-native-vector-icons/Ionicons";
import KeyboardArrowDown from "react-native-vector-icons/MaterialIcons";
import Currencies from "../mockData/currency.json";
import COLOR from "../res/styles/Color";

const deviceheight = Dimensions.get("window").height;
const devicewidth = Dimensions.get("window").width;

const CurrencyPicker = (props) => {
  const {
    setCurrencies,
    setSelectedCurrency,
    currencies,
    refCurrencyModal,
    ...restProps
  } = props;
  const searchCurrency = (searchText = "") => {
    let updatedList = Currencies.filter((curr) => {
      return (
        curr.name.toLowerCase().includes(searchText.toLowerCase()) ||
        curr.name.toLowerCase().includes(searchText.toLowerCase()) ||
        curr.code.toLowerCase().includes(searchText.toLowerCase())
      );
    });
    setCurrencies(updatedList);
  };

  return (
    <RBSheet ref={refCurrencyModal} {...restProps}>
      <View
        style={{
          flex: 1,
          height: "100%",
          backgroundColor: "white",
          alignItems: "center",
        }}
      >
        <SafeAreaView style={{ flex: 1, width: "100%", alignItems: "center" }}>
          <Icon
            name={"close"}
            style={{ fontSize: 24, alignSelf: "flex-end", margin: 10 }}
            onPress={() => refCurrencyModal.current.close()}
          />
          <TextInput
            placeholder={"Search for currency or code"}
            placeholderTextColor={COLOR.PRIMARYGREY}
            // onChangeText={this.searchText.bind(this)}
            keyboardType={"default"}
            style={[styles.search, { marginTop: 0 }]}
            onChangeText={(text) => {
              searchCurrency(text);
            }}
          />
          <FlatList
            style={{ width: "100%" }}
            data={props.currencies}
            renderItem={({ item }) => {
              return (
                <TouchableOpacity
                  key={item.id}
                  style={{
                    // justifyContent: "space-between",
                    flexDirection: "row",
                    marginHorizontal: 40,
                    marginVertical: 10,
                    borderBottomWidth: 2,
                    borderBottomColor: COLOR.GREY,
                    padding: 10,
                  }}
                  onPress={() => {
                    setSelectedCurrency(item);
                    refCurrencyModal.current.close();
                    setCurrencies(Currencies);
                  }}
                >
                  <Image
                    style={{ height: 15, width: 20, marginRight: 10 }}
                    source={{
                      uri: `data:image/png;base64,${item.flag}`,
                    }}
                  />
                  <Text style={{ width: "50%" }}>{item.name}</Text>
                  <Text style={{ width: "20%" }}>{item.code}</Text>
                  <Text style={{ width: "20%" }}>{item.symbol}</Text>
                </TouchableOpacity>
              );
            }}
          />
        </SafeAreaView>
      </View>
    </RBSheet>
  );
};

const styles = StyleSheet.create({
  search: {
    height: 40,
    width: "80%",
    fontSize: 14,
    borderColor: COLOR.PRIMARYGREY,
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    marginTop: 20,
  },
});

export default CurrencyPicker;
