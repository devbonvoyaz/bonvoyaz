import React from "react";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import FontAwesome5Pro from "react-native-vector-icons/FontAwesome5Pro";
import Fontisto from "react-native-vector-icons/Fontisto";
import Foundation from "react-native-vector-icons/Foundation";
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Octicons from "react-native-vector-icons/Octicons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";

export default function RnVectorIcon(props) {
  const Icon = {
    AntDesign: AntDesign,
    Entypo: Entypo,
    EvilIcons: EvilIcons,
    Feather: Feather,
    FontAwesome: FontAwesome,
    FontAwesome5: FontAwesome5,
    FontAwesome5Pro: FontAwesome5Pro,
    Fontisto: Fontisto,
    Foundation: Foundation,
    Ionicons: Ionicons,
    MaterialCommunityIcons: MaterialCommunityIcons,
    MaterialIcons: MaterialIcons,
    Octicons: Octicons,
    SimpleLineIcons: SimpleLineIcons,
  };

  const VectorIcon = () => {
    const { iconFamily, ...restProps } = props;
    if (!props.iconFamily) {
      return;
    }
    if (Object.keys(Icon).indexOf(props.iconFamily) === -1) {
      return;
    }
    const IconFamily = Icon[props.iconFamily];
    return <IconFamily {...restProps} />;
  };

  return VectorIcon();
}
