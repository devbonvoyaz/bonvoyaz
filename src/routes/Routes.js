import React from 'react';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SplashScreen from '../screens/splash/SplashScreen';
import SplashSliderScreen from '../screens/splash/SplashSliderScreen';
import LoginScreen from '../screens/splash/LoginScreen';
import SignUp from '../screens/splash/SignUp';
import Login from '../screens/splash/Login';
import ChatScreen from '../screens/tabs/chat/ChatScreen';
import TripScreen from '../screens/tabs/trip/TripScreen';
import COLOR from '../res/styles/Color';
import ChatBoxScreen from '../screens/tabs/chat/ChatBoxScreen';
import AddTripScreen from '../screens/tabs/trip/AddTripScreen';
import EditTripScreen from '../screens/tabs/trip/EditTrip';
import ViewTripScreen from '../screens/tabs/trip/ViewTripScreen';
import AddPlan from '../screens/tabs/trip/AddPlan';
import MobileVerification from '../screens/splash/MobileVerification';
import ProfileScreen from '../screens/tabs/profile/ProfileScreen';
import UserProfile from '../screens/tabs/profile/UserProfile';
import FriendsScreen from '../screens/tabs/profile/FriendsScreen';
import SettingsScreen from '../screens/tabs/profile/SettingsScreen';
import AddDrive from '../screens/plans/AddDrive';
import AddHotel from '../screens/plans/AddHotel';
import AddRestaurant from '../screens/plans/AddRestaurant';
import UserDetailsScreen from '../screens/tabs/chat/UserDetailsScreen';
import GroupDetailsScreen from '../screens/tabs/chat/GroupDetailsScreen';
import UserChatSettingScreen from '../screens/tabs/profile/UserChatSettingScreen';
import NewParticipants from '../screens/tabs/chat/NewParticipants';
import NewGroupScreen from '../screens/tabs/chat/NewGroupScreen';
import TabBar from '../commonView/TabBar/TabBar';
import SearchScreen from '../screens/tabs/search/SearchScreen';
import CreatePollScreen from "../screens/tabs/chat/CreatePollScreen";
import ChatTripScreen from "../screens/tabs/chat/ChatTripScreen";
import AddExpense from '../screens/tabs/trip/Spending/Expenses/AddExpense';
import ProfileNotifications from "../screens/tabs/profile/ProfileNotifications";
import { setNavigationRef } from '../redux/RootNavigation';
import AddPlace from '../screens/plans/AddPlace';
import AddTrain from '../screens/plans/AddTrain';
import Calendar from '../commonView/globalComponent/Calendar'

const Tab = createBottomTabNavigator();
const ProfileStack = createStackNavigator();
const TripStack = createStackNavigator();
const ChatStack = createStackNavigator();
const Stack = createStackNavigator();

const images = [
  require('../res/assets/message.png'),
  require('../res/assets/camera.png'),
  require('../res/assets/trip.png'),
  require('../res/assets/person.png'),

];

export default function RootStack() {
  return (
    <NavigationContainer ref={ref => {
      setNavigationRef(ref);
    }} >
      <Stack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={'SplashScreen'} headerMode="none">
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="SplashSliderScreen" component={SplashSliderScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="MyTab" component={MyTabs} />
        <Stack.Screen name="ChatBoxScreen" component={ChatBoxScreen} />
        <Stack.Screen name="MobileVerification" component={MobileVerification} />
        <Stack.Screen name="UserDetailsScreen" component={UserDetailsScreen} />
        <Stack.Screen name="GroupDetailsScreen" component={GroupDetailsScreen} />
        <Stack.Screen name="UserChatSettingScreen" component={UserChatSettingScreen} />
        <Stack.Screen name="AddTripScreen" component={AddTripScreen} />
        <Stack.Screen name="CreatePollScreen" component={CreatePollScreen} />
        <Stack.Screen name="ChatTripScreen" component={ChatTripScreen} />
        <Stack.Screen name="AddExpense" component={AddExpense} />
        <Stack.Screen name="calendar" component={Calendar} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}




function MyTabs() {
  return (
    <Tab.Navigator
      style={{ backgroundColor: COLOR.PRIMARYGREY, alignItems: 'center' }}
      tabBar={props => <TabBar {...props} />}>
      <Tab.Screen key={1} name="Chats" component={ChatScreens} />
      <Tab.Screen key={2} name="Search" component={SearchScreen} />
      <Tab.Screen key={3} name="Trips" component={TripScreens} />
      <Tab.Screen key={4} name="Profile" component={ProfileScreens} />
    </Tab.Navigator>
  )
}



function ProfileScreens() {
  return (

    <ProfileStack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={'ProfileScreen'} headerMode="none">
      <ProfileStack.Screen name="ProfileScreen" component={ProfileScreen} />
      <ProfileStack.Screen name="UserProfile" component={UserProfile} />
      <ProfileStack.Screen name="FriendsScreen" component={FriendsScreen} />
      <ProfileStack.Screen name="SettingsScreen" component={SettingsScreen} />
      <ProfileStack.Screen name="UserChatSettingScreen" component={UserChatSettingScreen} />
      <ProfileStack.Screen name="ProfileNotifications" component={ProfileNotifications} />
    </ProfileStack.Navigator>

  );

}

function TripScreens() {
  return (

    <TripStack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={'TripScreen'} headerMode="none">
      <TripStack.Screen name="TripScreen" component={TripScreen} />
      <TripStack.Screen name="AddPlan" component={AddPlan} />
      <TripStack.Screen name="AddTripScreen" component={AddTripScreen} />
      <TripStack.Screen name="EditTripScreen" component={EditTripScreen} />
      <TripStack.Screen name="ViewTripScreen" component={ViewTripScreen} />
      <TripStack.Screen name="AddDrive" component={AddDrive} />
      <TripStack.Screen name="AddTrain" component={AddTrain} />
      <TripStack.Screen name="AddHotel" component={AddHotel} />
      <TripStack.Screen name="AddRestaurant" component={AddRestaurant} />
      <TripStack.Screen name="AddPlace" component={AddPlace} />
    </TripStack.Navigator>

  );

}

function ChatScreens() {
  return (
    <ChatStack.Navigator screenOptions={{ gestureEnabled: false }} initialRouteName={'ChatScreen'} headerMode="none">
      <ChatStack.Screen name="ChatScreen" component={ChatScreen} />
      <ChatStack.Screen name="UserDetailsScreen" component={UserDetailsScreen} />
      <ChatStack.Screen name="GroupDetailsScreen" component={GroupDetailsScreen} />
      <ChatStack.Screen name="NewParticipants" component={NewParticipants} />
      <ChatStack.Screen name="NewGroupScreen" component={NewGroupScreen} />
    </ChatStack.Navigator>
  );
}

