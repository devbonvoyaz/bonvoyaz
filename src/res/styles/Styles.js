'use strict';

import {StyleSheet} from 'react-native';
import COLOR from './Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FONTS from './Fonts';

export default StyleSheet.create ({
  hr: {
    width: wp (90),
    height: 1,
    backgroundColor: COLOR.GREY,
    alignSelf: 'center',
  },
  heading_label: {
    fontSize: 32,
    fontFamily: FONTS.FAMILY_BOLD,
  },
  screen_heading: {
    fontSize: 24,
    fontFamily: FONTS.FAMILY_BOLD,
  },
  text_input: {
    height: 50,
    marginBottom: 20,
    backgroundColor: COLOR.GREY,

    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingStart: 15,
  },
});
