'use strict';
import {Dimensions} from 'react-native';
const {width} = Dimensions.get ('window');

const FONTS = {
  FAMILY_BOLD: 'Quicksand-Bold',
  FAMILY_SEMIBOLD: 'Quicksand-SemiBold',
  FAMILY_MEDIUM: 'Quicksand-Medium',
  FAMILY_REGULAR: 'Quicksand-Regular',
  LARGE: fontSizer (1, width),
  MEDIUM: fontSizer (2, width),
  REGULAR: fontSizer (3, width),
  SMALL: fontSizer (4, width),
};

function fontSizer (type, width) {
  if (width > 400) {
    switch (type) {
      case 1:
        return 22;
        break;
      case 2:
        return 17;
        break;
      case 3:
        return 16;
        break;
      case 4:
        return 12;
        break;
      default:
        break;
    }
  } else if (width > 250) {
    switch (type) {
      case 1:
        return 20;
        break;
      case 2:
        return 16;
        break;
      case 3:
        return 15;
        break;
      case 4:
        return 12;
        break;

      default:
        break;
    }
  } else {
    switch (type) {
      case 1:
        return 20;
        break;
      case 2:
        return 17;
        break;
      case 3:
        return 15;
        break;
      case 4:
        return 12;
        break;
      default:
        break;
    }
  }
}

export default FONTS;
