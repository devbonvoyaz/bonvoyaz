import COLOR from '../styles/Color';

export const CHATS = [
  {
    chat: 0,
    key: 1,
    username: 'Brooklyn Simmons',
    image: require('../assets/user.png'),
  },
  {
    chat: 0,
    key: 2,
    username: 'Brooklyn Simmons',
    image: require('../assets/groupimg.png'),
  },
  {
    chat: 0,
    key: 3,
    username: 'Brooklyn Simmons',
    image: require('../assets/user.png'),
  },
  {
    chat: 0,
    key: 4,
    username: 'Brooklyn Simmons',
    image: require('../assets/groupimg.png'),
  },
  {
    chat: 0,
    key: 5,
    username: 'Brooklyn Simmons',
    image: require('../assets/user.png'),
  },
  {
    chat: 0,
    key: 6,
    username: 'Brooklyn Simmons',
    image: require('../assets/groupimg.png'),
  },
  {
    chat: 0,
    key: 7,
    username: 'Brooklyn Simmons',
    image: require('../assets/user.png'),
  },
];

export const GROUPCHATS = [
  {
    chat: 1,
    key: 1,
    username: 'New York Group',
    image: require('../assets/groupimg.png'),
  },
  {
    chat: 1,
    key: 2,
    username: 'Travel Group 2',
    image: require('../assets/user.png'),
  },
  {
    chat: 1,
    key: 3,
    username: 'Travel Group 3',
    image: require('../assets/groupimg.png'),
  },
  {
    chat: 1,
    key: 4,
    username: 'Travel Group 2',
    image: require('../assets/user.png'),
  },
];

export const GENDERS = [

  { key: 1, label: 'MALE', value: 'Male' },
  { key: 2, label: 'FEMALE', value: 'Female' },
];

export const GROUPBY = [
  { key: 1, label: 'Location', value: 'Location' },
  { key: 2, label: 'Date', value: 'Date' },
];

export const MESSAGE = [
  { user: 0, message: 'It’s ok to dream a little' },
  { user: 1, message: 'It’s ok to dream a little' },
  { user: 0, message: 'It’s ok to dream a little' },
  { user: 1, message: 'It’s ok to dream a little' },
  { user: 0, message: 'It’s ok to dream a little' },
  { user: 1, message: 'Yes ! It’s ok to dream a little' },
];

export const GROUPMESSAGE = [
  { user: 1, username: '', message: 'It’s ok to dream a little' },
  { user: 0, username: 'jane cooper', message: 'It’s ok to dream a little' },
  { user: 0, username: 'Devon Lane', message: 'It’s ok to dream a little' },
  { user: 1, username: '', message: 'It’s ok to dream a little' },
  { user: 0, username: 'jane cooper', message: 'It’s ok to dream a little' },
  { user: 1, username: '', message: 'Yes ! It’s ok to dream a little' },
];

export const TRIPSOBJECTS = [
  {
    name: 'New York Trip',
    duration: 'Jan 2,2021, Sun - Jan 10,2021, Wed',
    image: require('../assets/newyork.png'),
    createdAt: 'Last Month',
  },
  {
    name: 'Tokyo',
    duration: 'Jan 2,2021, Sun - Jan 10,2021, Wed',
    image: require('../assets/tokyo.png'),
    createdAt: '2 Months',
  },
  {
    name: 'Malaysia',
    duration: 'Jan 2,2021, Sun - Jan 10,2021, Wed',
    image: require('../assets/malaysia.png'),
    createdAt: 'Last Month',
  },
];

export const USERLIST = [
  { userId: 'Bell_123', username: 'Bell', image: require('../assets/user.png') },
  {
    userId: 'Savannah Nguyen_458',
    username: 'Savannah Nguyen',
    image: require('../assets/user.png'),
  },
  {
    userId: 'Floyd Miles_87',
    username: 'Floyd Miles',
    image: require('../assets/user.png'),
  },
];

export const GALLERYIMAGE = [
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
  { image: require('../assets/gallery-image3.png') },
];

export const GALLERYIMAGE2 = [
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
  { image: require('../assets/city.png') },
];

export const COUNTRYCODE = [
  { key: 1, value: '+91', label: '+91' },
  { key: 2, value: '+92', label: '+92' },
  { key: 3, value: '+93', label: '+93' },
  { key: 4, value: '+94', label: '+94' },
];

export const ALLPHOTOSCOUNT = [
  { key: 1, value: '5 ', label: '5 ' },
  { key: 2, value: '10', label: '10' },
  { key: 3, value: '20', label: '20' },
  { key: 4, value: '50', label: '50' },
];

export const TRIPNAME = [
  { name: 'New York Trip' },
  { name: 'Sauddi Trip' },
  { name: 'India Trip' },
];

export const PARTICIPANTS = [
  { status: 'Status', username: 'Bell', image: require('../assets/user.png') },
  {
    status: 'Status',
    username: 'Savannah Nguyen',
    image: require('../assets/user.png'),
  },
  {
    status: 'Status',
    username: 'Floyd Miles',
    image: require('../assets/user.png'),
  },
];

export const NOTIFICATIONS = [{
  text: 'This is notification Text',
  time: '12:00 Pm',
  date: '12 APR 22'
},
{
  text: 'This is notification Text',
  time: '12:00 Pm',
  date: '12 APR 22'
},
{
  text: 'This is notification Text',
  time: '12:00 Pm',
  date: '12 APR 22'
}];

export const colors = [
  COLOR.DARK_BLUE,
  COLOR.TEXTCOLOR,
  COLOR.ORANGE,
  COLOR.GREY,
  COLOR.CHERYCOLOR,
  COLOR.RED,
  COLOR.PRIMARTGREEN08,
]

export function getLastMessageLabel(item) {
  return item.details.messageType ? item.details.messageType == 1 ?
    item.details.message : item.details.messageType == 2 ? 'Image' :
      item.details.messageType == 3 ? 'Video' :
        item.details.messageType == 4 ? 'Location' :
          item.details.messageType == 5 ? 'Poll' : '' : ''

}

export function getLastMessageImage(item) {

  return item.details.messageType ? item.details.messageType == 2 ? require('../assets/Gallery.png') :
    item.details.messageType == 3 ? require('../assets/Gallery.png') :
      item.details.messageType == 4 ? require('../assets/location_placeholder.png') :
        item.details.messageType == 5 ? require('../assets/poll.png') : '' : '';
}


export function getLabelByType(val) {
  return val == 1 ? 'Flight' : val == 2 ? 'Hotel' : val == 3 ? 'Restaurant' : val == 4 ? 'Drive' : val == 5 ? 'Train' : val == 6 ? 'Place' : '';
}

export function getColorByType(val, index) {
  if (index == 0) {
    return val == 1 ? COLOR.PURPLE : val == 2 ? COLOR.RED : val == 3 ? COLOR.PRIMARTGREEN : val == 4 ? COLOR.PRIMARYBLUE : val == 5 ? COLOR.ORANGE : val == 6 ? '#cc33ff' : '';

  } else if (index == 1) {
    return val == 1 ? '#E6E6FB' : val == 2 ? '#FEEAEA' : val == 3 ? COLOR.PRIMARTGREEN08 : val == 4 ? '#DDEFFB' : val == 5 ? '#ffe8b3' : val == 6 ? '#f9e6ff' : '';

  } 
}

export function getIconByType(val) {
  return val == 1 ? require('../assets/flight.png') : val == 2 ? require('../assets/hotel.png') : val == 3 ? require('../assets/restaurant.png') : val == 4 ? require('../assets/drive.png') : require('../assets/train.png');
}

export function getItineraryTitle(item) {
  return item.hotelName ?
    item.hotelName : item.address ?
      item.address : item.startPoint ?
        item.startPoint : item.from ?
          item.from : item.placeName;
}

export function getStartTimeAttr(item) {
  switch (item.type) {
    case 1:
      return 'date';
    case 2:
      return 'arrivalTime';
    case 3:
      return 'arrivalTime';
    case 4:
      return 'startTime';
    case 5:
      return 'startTime';
    case 6:
      return 'time';
  }

}