
export let navigationRef;

export const setNavigationRef = (navigator) => {
    navigationRef = navigator;
}

export function navigate(name, params) {

    navigationRef.navigate(name, params);

}

export function goBack() {
    navigationRef.goBack();
}

export function pop(val) {
    navigationRef.pop(val);
    navigationRef.pop
}