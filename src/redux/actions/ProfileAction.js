import API from "../../api/Api";
import types from '../Types';

const api = new API();

const updateProfile = (data)=>{
    return (dispatch,getStore)=>{
        try{
            api.updateProfile(data).then(
                (json)=>{
                    console.log(json.data.response);
                    if(json.status ==200){
                        dispatch({
                            data:json.data.response,
                            type:types.UPDATE_PROFILE
                        });
                    }else if(json.status == 400){
                        console.log(json.data.response);
                    }
                }
            ).catch(function(error){
                console.log(error.response.data.response);
            })
        }catch(error){
            console.log(error);
        }
    }
}

export default {
    updateProfile,
}
