import API from "../../api/Api";
import types from '../Types';
import SimpleToast from "react-native-simple-toast";
import { goBack, navigationRef } from "../RootNavigation";

const api = new API();

const addTrip = (data) => {
    return (dispatch, getStore) => {
        try {
            api.addTrip(data).then(
                (json) => {
                    console.log(json.data.response);
                    if (json.status == 200) {
                        dispatch({
                            data: json.data.response,
                            type: types.TRIP_DATA
                        });
                        navigationRef.goBack();
                    } else if (json.status == 400) {
                        console.log(json.data.response);
                    }
                }
            ).catch(function (error) {
                SimpleToast.show(error.response.data.message);
            })
        } catch (error) {
            console.log(error);
        }
    }
}

const getAllTrips = (val) => {
    return (dispatch, getStore) => {
        api.getAllTrips(val)
            .then(json => {
                if (json.status == 200) {
                    console.log("trips", json.data.response);
                    if (val == 1) {
                        dispatch({
                            type: types.PAST_TRIP_DATA,
                            data: json.data.response,
                        });
                    }
                    if (val == 2) {
                        dispatch({
                            type: types.TRIPS_DATA,
                            data: json.data.response,
                        });
                    }


                }
            }).catch(function (error) {
                console.log(error.response.data.message);
            });
    }
}

const updateTrip = (id, data) => {
    return (dispatch, getStore) => {
        api.updateTrip(id, data).then((json) => {
            if (json.status == 200) {
                console.log(json.data.response);
                if (json.data.response.conflict == false) {
                    SimpleToast.show('Updated Succesfully');
                    console.log(json.data.response);
                    dispatch(getTripItinerary(id));
                    dispatch(getAllTrips(2));
                    dispatch(getAllTrips(1));
                    dispatch({
                        type: types.SINGLE_TRIP,
                        data: json.data.response,
                    });
                } else {
                    let newObject = Object.assign({}, json.data.response, { data: data, id: id });
                    dispatch({
                        type: types.SINGLE_TRIP,
                        data: newObject,
                    });
                }

            }
        }).catch(function (error) {
            console.log(error);
        })
    }
}

const getSingleTrip = (val) => {

    return (dispatch, getStore) => {
        try {
            api.getSingleTrip(val).then(json => {
                console.log(json.data.response);
                if (json.status == 200) {
                    dispatch({
                        type: types.SINGLE_TRIP,
                        data: json.data.response,
                    })
                }
            }).catch(function (error) {
                console.log(error.response.data.message);
            })
        } catch (e) {
            console.log(e);
        }
    }
}

const addTripTraveller = (data) => {
    return (dispatch, getStore) => {
        try {
            api.addTripTraveller(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log("add trveller", json.data.response);
                        dispatch(getSingleTrip(json.data.response.id));
                    }
                }).catch(function (error) {
                    console.log(error.response.data.message);
                    SimpleToast.show(error.response.data.message)
                })
        } catch (e) {
            console.log(e);
        }
    }
}

const removeTripTraveller = (data) => {
    return async (dispatch, getStore) => {
        try {
            api.removeTripTraveller(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log("remove traveller", json.data.response);
                        dispatch(getSingleTrip(json.data.response.id));

                    }
                }).catch(function (error) {
                    console.log(error.response.data.message);
                    SimpleToast.show(error.response.data.message)
                })
        } catch (e) {
            console.log(e);
        }
    }
}

const makeGuide = (data) => {
    return (dispatch, getStore) => {
        try {
            api.makeGuide(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                    }
                }).catch(
                    function (error) {
                        console.log(error.response.data.message);
                        SimpleToast.show(error.response.data.message)
                    })
        } catch (error) {
            console.log(error);
        }
    }
}

const dismissAsGuide = (data) => {
    return (dispatch, getStore) => {
        try {
            api.dismissAsGuide(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data);
                    }
                }).catch(function (error) {
                    console.log(error.response.data.message);
                    SimpleToast.show(error.response.data.message)
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const getBookings = (val) => {
    return (dispatch, getStore) => {
        try {
            api.getBookings(val)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        dispatch({
                            type: types.BOOKINGS,
                            data: json.data.response,
                        })
                    }
                }).catch(function (error) {
                    console.log(error.response.data.message);
                    SimpleToast.show(error.response.data.message)
                })
        } catch (error) {
            console.log(error);
        }
    }
}


const bookFlight = (data) => {
    return (dispatch, getStore) => {
        try {
            api.bookFlight(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        SimpleToast.show('Added successfully');
                        dispatch({
                            type: types.BOOK_FLIGHT,
                            data: json.data.response,
                        });
                        let newData = JSON.parse(data);
                        dispatch(getTripItinerary(newData.trip));
                        goBack();
                    }
                }).catch(function (error) {
                    console.log(error.response.data.message);
                    SimpleToast.show(error.response.data.message)
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const bookCar = (data) => {
    return (dispatch, getStore) => {
        try {
            api.bookCar(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        SimpleToast.show('Added successfully');
                        dispatch({
                            type: types.BOOK_CAR,
                            data: json.data.response,
                        });
                        goBack();
                    }
                }).catch(function (error) {
                    console.log(error.response.data.message);
                    SimpleToast.show(error.response.data.message)
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const bookHotel = (data) => {
    return (dispatch, getStore) => {
        try {
            api.bookHotel(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        SimpleToast.show('Added successfully');
                        dispatch({
                            type: types.BOOK_HOTEL,
                            data: json.data.response,
                        });
                        goBack();


                    }
                }).catch(function (error) {
                    console.log(error.response.data.message);
                    SimpleToast.show(error.response.data.message)
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const getHotelBookingByTrip = (val) => {
    return (dispatch, getStore) => {
        try {
            api.getHotelBookingByTrip(val)
                .then(json => {
                    if (json.status == 200) {
                        console.log('Hotel booking', json.data.response);
                        dispatch({
                            type: types.HOTEL_BOOKINGS,
                            data: json.data.response,
                        })
                    }
                }).catch(function (error) {
                    console.log(error.response.data.message);
                    SimpleToast.show(error.response.data.message)
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const getTripItinerary = (val) => {
    return (dispatch, getStore) => {
        try {
            api.getTripItinerary(val)
                .then(
                    json => {
                        console.log(json.data.response);
                        dispatch({
                            type: types.ITINERARY_DATA,
                            data: json.data.response,
                        });

                    }
                ).catch(function (error) {
                    console.log(error);
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const deleteConflictedPlans = (id, data) => {
    return (dispatch, getStore) => {
        api.deleteConflictedPlans(id, data).then((json) => {
            if (json.status == 200) {
                SimpleToast.show('Updated Succesfully');
                console.log(json.data.response);
                dispatch(getAllTrips(2));
                dispatch(getAllTrips(1));
                dispatch({
                    type: types.SINGLE_TRIP,
                    data: json.data.response,
                });
            }
        }).catch(function (error) {
            SimpleToast.show(error.response.data.response)
        })
    }
}



export default {
    addTrip,
    getAllTrips,
    updateTrip,
    getSingleTrip,
    addTripTraveller,
    removeTripTraveller,
    makeGuide,
    dismissAsGuide,
    bookFlight,
    bookCar,
    bookHotel,
    getBookings,
    getHotelBookingByTrip,
    getTripItinerary,
    deleteConflictedPlans
}
