import API from "../../api/Api";
import types from '../Types';

const api = new API();

const searchUser = (data) => {
    return (dispatch, getStore) => {
        try {
            api.searchUser(data).then(
                (json) => {
                    console.log(json.data.response);
                    if (json.status == 200) {
                        dispatch({
                            data: json.data.response,
                            type: types.SEARCH_DATA
                        });
                    } else if (json.status == 400) {
                        console.log(json.data.response);
                    }
                }
            ).catch(function (error) {
                console.log(error.response.data.response);
            })
        } catch (error) {
            console.log(error);
        }
    }
}

export default {
    searchUser,
}
