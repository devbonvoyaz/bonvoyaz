import API from "../../api/Api";
import Auth from "../../auth";
import types from "../Types";
import Toast from 'react-native-simple-toast';
import SimpleToast from "react-native-simple-toast";



const api = new API();
const auth = new Auth();


const getParticipants = () => {
    return async = (dispatch, getStore) => {
        try {
            api.getParticipants().then(
                (json) => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        dispatch({
                            type: types.PARTICIPANTS,
                            data: json.data.response,
                        })
                    } else {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                            console.log(json.data.response)
                        }, 0);

                    }
                }
            ).catch(function (error) {
                Toast.show(error.response);
                console.log(error);
            });
        } catch (error) {
            console.log(error);
        }

    }
}

const getGroups = () => {
    return async = (dispatch, getStore) => {
        try {
            api.getGroups().then(
                (json) => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        dispatch({
                            type: types.GROUPS,
                            data: json.data.response,
                        })

                    } else {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);

                    }
                }
            ).catch(function (error) {
                Toast.show(error.response.data.response);
                console.log(error);
            });
        } catch (error) {
            console.log(error);
        }

    }
}

const isRegistered = (val) => {
    return async = (dispatch, getStore) => {
        try {
            api.isRegistered(val).then(
                (json) => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        dispatch({
                            type: types.IS_REGISTERED,
                            data: json.data.response,
                        });
                        createThread(json.data.response.id, dispatch, getStore);


                    } else if (json.status == 400) {
                        console.log(json.data.response);
                    } else {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);

                    }
                })
                .catch(function (error) {
                    Toast.show(error.response.data.message);

                });
        } catch (error) {
            console.log(error);

        }
    }
}

const createThread = (val, dispatch, getStore) => {
    try {
        api.createThread(val).then(
            (json) => {
                if (json.status == 200) {
                    console.log(json.data.response);
                    dispatch(
                        {
                            type: types.THREAD_NAVIGATION,
                            data: json.data.response,
                        }
                    )
                } else if (json.status == 400) {
                    setTimeout(() => {
                        Toast.show(json.data.response);
                    }, 0);
                    Toast.show(json.data.response);

                } else {
                    setTimeout(() => {
                        Toast.show(json.data.response);
                    }, 0);

                }
            })
            .catch(function (error) {
                console.log(error);
            });
    } catch (error) {
        console.log(error);
    }
}

const clearThreadNavigation = (val) => {
    return (dispatch, getStore) => {
        dispatch({
            type: types.THREAD_NAVIGATION,
            data: 'undefined',
        });
        console.log('called')
    }
}


const getRegisteredContacts = (data) => {
    return async = (dispatch, getStore) => {
        try {
            api.getRegisteredContacts(data).then(
                (json) => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        dispatch(
                            {
                                type: types.REGISTERD_CONTACTS,
                                data: json.data.response,
                            }
                        )
                    } else if (json.status == 400) {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);
                        Toast.show(json.data.response);

                    } else {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);

                    }
                }
            ).catch(function (error) {

                console.log(error);
            });
        } catch (error) {
            console.log(error);
        }
    }
}

const createGroup = (data) => {
    return async = (dispatch, getStore) => {
        try {
            api.createGroup(data).then(
                (json) => {
                    if (json.status == 200) {
                        console.log(json.data.response);

                    } else if (json.status == 400) {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);
                        Toast.show(json.data.response);

                    } else {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);

                    }
                }
            )
        } catch (error) {
            console.log(error);
        }
    }
}

const updateGroup = (val, data) => {
    return async (dispatch, getStore) => {
        try {
            api.updateGroup(val, data).then((json) => {
                console.log(json.status)
                if (json.status == 200) {
                    console.log(json.data.response);
                } else {
                    console.log(json.data.response);
                }
            })
                .catch(function (error) {
                    console.log(error);
                })
        } catch (error) {
            console.log(error.response.data.message)
        }
    }
}

const removeParticipant = (data) => {
    return async = (dispatch, getStore) => {
        try {
            api.removeParticipant(data).then(
                (json) => {
                    if (json.status == 200) {
                        console.log(json.data.response);

                    } else if (json.status == 400) {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);
                        Toast.show(json.data.response);

                    } else {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);

                    }
                }
            )
        } catch (error) {
            console.log(error);
        }
    }
}

const addParticipants = (data) => {
    return async = (dispatch, getStore) => {
        try {
            api.addParticipants(data).then(
                (json) => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        SimpleToast.show('ADDED');
                        getParticipantsByThread(json.data.response.thread);

                    } else if (json.status == 400) {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);
                        Toast.show(json.data.response);

                    } else {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);

                    }
                }
            )
        } catch (error) {
            console.log(error);
        }
    }
}

const sendMessage = (data) => {
    return async = (dispatch, getStore) => {
        try {
            api.sendMessage(data).then(
                (json) => {
                    if (json.status == 200) {
                        console.log(json.data.response);

                    } else if (json.status == 400) {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);
                        Toast.show(json.data.response);

                    } else {
                        setTimeout(() => {
                            Toast.show(json.data.response);
                        }, 0);

                    }
                }
            ).catch((error) => {
                console.log(error);
            })
        } catch (error) {
            console.log(error);
        }
    }
}

const deleteMessage = (val) => {
    return async (dispatch, getStore) => {
        try {
            api.deleteMessage(val)
                .then(json => {
                    console.log(json.data.message)
                }).catch(function (error) {
                    console.log(error.response.data.message);
                    SimpleToast.show(error.response.data.message);
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const addFriend = (val) => {
    return async (dispatch, getStore) => {
        try {
            api.addFriend(val).then((json) => {
                if (json.status == 200) {
                    console.log(json.data.response);
                    SimpleToast.show('Added to Friends');
                }
            }).catch(function (error) {
                console.log(error.response.data.message)
            })
        } catch (error) {
            console.log(error)
        }
    }
}

const getFriends = (val) => {
    return async (dispatch, getStore) => {
        try {
            api.getFriends(val).then((json) => {
                if (json.status == 200) {
                    console.log(json.data.response)
                    dispatch({
                        type: types.FRIENDS,
                        data: json.data.response,
                    })
                }
            }).catch(function (error) {
                console.log(error.data.response);
            })
        } catch (error) {
            console.log(error);
        }
    }
}

const getParticipantsByThread = (val) => {
    return async (dispatch, getStore) => {
        try {
            api.getParticipantsByThread(val).then(
                (json) => {
                    if (json.status == 200) {
                        console.log('participants by thread', json.data.response);
                        dispatch({
                            type: types.PARTICIPANTS_BY_THREAD,
                            data: json.data.response,
                        })
                    }
                }
            )
                .catch(function (error) {
                    console.log(error.data.message)
                })
        } catch (error) {
            console.log(error)
        }
    }

}

const createPoll = data => {
    return (dispatch, getStore) => {
        try {
            api.createPoll(data)
                .then(json => {
                    console.log(json.data.response);
                    if (json.status == 200) {
                        console.log(json.data.response);
                    }
                })
                .catch(function (error) {
                    console.log(error.data.message)
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const setParticipantAsAdmin = (data) => {
    return (dispatch, getStore) => {
        try {
            api.setParticipantAsAdmin(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                    }
                }).catch(function (params) {
                    console.log(params);
                })
        } catch (e) {
            console.log(e);
        }
    }
}

const voteInPoll = (val) => {
    return (dispatch, getStore) => {
        try {
            api.voteInPoll(val)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data);
                    }
                }).catch(function (error) {
                    SimpleToast.show(error.response.data.message);
                    console.log(error.response.data.message)
                })
        } catch (e) {
            console.log(e)
        }
    }
}

const addTripToThread = (data) => {
    return (dispatch, getStore) => {
        try {
            api.addTripToThread(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        SimpleToast.show('Trip Added');
                    }
                }).catch(function (error) {
                    SimpleToast.show(error.response.data.message);
                });
        } catch (error) {
            console.log(error);
        }
    }
}

const getTripByThread = (val) => {
    return (dispatch, getStore) => {
        try {
            api.getTripByThread(val)
                .then(json => {
                    console.log(json.data.response);
                    if (json.status == 200) {
                        dispatch({
                            type: types.TRIPS_BY_THREAD,
                            data: json.data.response,
                        })
                    }
                }).catch(function (error) {
                    console.log("getTripByThread -- ", error);
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const clearChat = (val) => {
    return (dispatch, getStore) => {
        try {
            api.clearChat(val)
                .then(json => {
                    SimpleToast.show("Cleared")
                }).catch(function (error) {
                    if (error.response) {
                        SimpleToast.show(error.response.data.response);
                    } else {
                        console.log(error);
                    }
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const getMediaByThread = (data) => {
    return (dispatch, getStore) => {
        try {
            api.getMediaByThread(data)
                .then(json => {
                    dispatch({
                        type: types.MEDIA_BY_THREAD,
                        data: json.data.response,
                    })
                }).catch(function (error) {
                    console.log(error.response.data.message);
                })
        } catch (error) {
            console.log(error);
        }
    }
}




export default {
    isRegistered,
    getParticipants,
    getRegisteredContacts,
    createGroup,
    getGroups,
    sendMessage,
    removeParticipant,
    addParticipants,
    addFriend,
    getFriends,
    updateGroup,
    getParticipantsByThread,
    clearThreadNavigation,
    createPoll,
    setParticipantAsAdmin,
    voteInPoll,
    addTripToThread,
    getTripByThread,
    deleteMessage,
    clearChat,
    getMediaByThread
};