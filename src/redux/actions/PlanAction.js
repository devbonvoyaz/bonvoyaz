import API from "../../api/Api";
const api = new API();
import { goBack, navigate, pop } from '../RootNavigation';
import SimpleToast from "react-native-simple-toast";
import TripAction from "./TripAction";

const addDrivePlan = (data) => {
    return async (dispatch, getStore) => {
        try {
            api.addDrivePlan(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data);
                        dispatch(TripAction.getTripItinerary(json.data.response.trip));
                        SimpleToast.show('Added Drive');
                        navigate('ViewTripScreen');
                    }
                }).catch(function (error) {
                    if (error.response) {
                        SimpleToast.show(error.response.data.message);
                    }
                    console.log(error.response.data.message);
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const updateDrivePlan = (val, data) => {
    return async (dispatch, getStore) => {
        try {
            api.updateDrivePlan(val, data)
                .then(json => {
                    SimpleToast.show('Updated');
                    dispatch(TripAction.getTripItinerary(json.data.response.trip));
                    goBack();
                }).catch(error => {
                    if (error.response.data.message) {
                        SimpleToast.show(error.response.data.message)
                    } else {
                        console.log(error);
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }
}

const addFlightPlan = (data) => {
    return async (dispatch, getStore) => {
        try {
            api.addFlightPlan(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data);
                        SimpleToast.show('Added Flight')
                        dispatch(TripAction.getTripItinerary(json.data.response.trip));
                        navigate('ViewTripScreen');
                    }
                }).catch(function (error) {
                    if (error.response) {
                        SimpleToast.show(error.response.data.message);
                    }
                    console.log(error.response.data.message);
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const updateFlightPlan = (val, data) => {
    return async (dispatch, getStore) => {
        try {
            api.updateFlightPlan(val, data)
                .then(json => {
                    SimpleToast.show('Updated');
                    dispatch(TripAction.getTripItinerary(json.data.response.trip));
                    goBack();
                }).catch(error => {
                    if (error.response.data.message) {
                        SimpleToast.show(error.response.data.message)
                    } else {
                        console.log(error);
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }
}

const addRestaurant = (data) => {
    return async (dispatch, getStore) => {
        try {
            api.addRestaurant(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data.response);
                        dispatch(TripAction.getTripItinerary(json.data.response.trip));
                        SimpleToast.show('Added Restaurant')
                        navigate('ViewTripScreen');
                    }
                }).catch(function (error) {
                    if (error.response) {
                        SimpleToast.show(error.response.data.message);
                    }
                    console.log(error.response.data.message);
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const updateRestaurantPlan = (val, data) => {
    return async (dispatch, getStore) => {
        try {
            api.updateRestaurantPlan(val, data)
                .then(json => {
                    SimpleToast.show('Updated');
                    dispatch(TripAction.getTripItinerary(json.data.response.trip));
                    goBack();
                }).catch(error => {
                    if (error.response.data.message) {
                        SimpleToast.show(error.response.data.message)
                    } else {
                        console.log(error);
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }
}

const addHotel = (data) => {
    return async (dispatch, getStore) => {
        try {
            api.addHotel(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data);
                        dispatch(TripAction.getTripItinerary(json.data.response.trip));
                        SimpleToast.show('Added Hotel')
                        navigate('ViewTripScreen');
                    }
                }).catch(function (error) {
                    if (error.response) {
                        SimpleToast.show(error.response.data.message);
                    }
                    console.log(error.response.data.message);
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const updateHotelPlan = (val, data) => {
    return async (dispatch, getStore) => {
        try {
            api.updateHotelPlan(val, data)
                .then(json => {
                    SimpleToast.show('Updated');
                    dispatch(TripAction.getTripItinerary(json.data.response.trip));
                    goBack();
                }).catch(error => {
                    if (error.response.data.message) {
                        SimpleToast.show(error.response.data.message)
                    } else {
                        console.log(error);
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }
}




const addPlace = (data) => {
    return async (dispatch, getStore) => {
        try {
            api.addPlace(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data);
                        dispatch(TripAction.getTripItinerary(json.data.response.trip));
                        SimpleToast.show('Added Place')
                        navigate('ViewTripScreen');
                    }
                }).catch(function (error) {
                    if (error.response) {
                        SimpleToast.show(error.response.data.message);
                    }
                    console.log(error.response.data.message);
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const updatePlacePlan = (val, data) => {
    return async (dispatch, getStore) => {
        try {
            api.updatePlacePlan(val, data)
                .then(json => {
                    SimpleToast.show('Updated');
                    console.log(json.data.response);
                    dispatch(TripAction.getTripItinerary(json.data.response.trip));
                    goBack();
                }).catch(error => {
                    if (error.response.data.message) {
                        SimpleToast.show(error.response.data.message)
                    } else {
                        console.log(error);
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }
}

const addTrain = (data) => {
    return async (dispatch, getStore) => {
        try {
            api.addTrain(data)
                .then(json => {
                    if (json.status == 200) {
                        console.log(json.data);
                        dispatch(TripAction.getTripItinerary(json.data.response.trip));
                        SimpleToast.show('Added Train');
                        navigate('ViewTripScreen');
                    }
                }).catch(function (error) {
                    if (error.response) {
                        SimpleToast.show(error.response.data.message);
                    }
                    console.log(error.response.data.message);
                })
        } catch (error) {
            console.log(error);
        }
    }
}

const updateTrainPlan = (val, data) => {
    return async (dispatch, getStore) => {
        try {
            api.updateTrainPlan(val, data)
                .then(json => {
                    SimpleToast.show('Updated');
                    dispatch(TripAction.getTripItinerary(json.data.response.trip));
                    goBack();
                }).catch(error => {
                    if (error.response.data.message) {
                        SimpleToast.show(error.response.data.message)
                    } else {
                        console.log(error);
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }
}

const removePlan = (val, data) => {
    return async (dispatch, getStore) => {
        try {
            api.removePlan(data)
                .then(json => {
                    SimpleToast.show('Removed');
                    dispatch(TripAction.getTripItinerary(val));
                }).catch(error => {
                    if (error.response.data.message) {
                        SimpleToast.show(error.response.data.message)
                    } else {
                        console.log(error);
                    }
                });
        } catch (error) {
            console.log(error);
        }
    }
}




export default {
    addDrivePlan,
    updateDrivePlan,
    addFlightPlan,
    updateFlightPlan,
    addRestaurant,
    updateRestaurantPlan,
    addHotel,
    updateHotelPlan,
    addPlace,
    updatePlacePlan,
    addTrain,
    updateTrainPlan,
    removePlan
}