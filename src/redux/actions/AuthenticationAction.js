import types from '../Types';
import API from '../../api/Api';
import Auth from '../../auth';
import { AS_USER_TOKEN } from '../../api/Constants';


const api = new API();
const auth = new Auth();
const loginApi = (data) => {
    return async (dispatch, getStore) => {
        let userDetail = getStore().reducer.userDetail;

        try {
            api
                .loginApi(data)
                .then((json) => {
                    console.log('login Responce:-', json.data.response);
                    if (json.status == 200) {
                        auth.setValue(AS_USER_TOKEN, json.data.response.token);
                        console.log('user data:-', data.response);
                        dispatch({
                            type: types.USER_DETAIL,
                            data: json.data.response,
                        });
                    } else {
                        Toast.show(json.data.message);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        } catch (error) {
            console.log(error);
        }

    };
};

const logoutUser = () => {
    return async (dispatch, getStore) => {
      return dispatch({
        type: types.USER_LOGOUT,
        data: null,
      });
    };
  };

const signUpApi = (data) => {
    return async (dispatch, getStore) => {
        let userDetail = getStore().reducer.userDetail;

        try {
            api
                .signUpApi(data)
                .then((json) => {
                    console.log('signUp Responce:-', json.data.response);
                    if (json.status == 200) {
                        auth.setValue(AS_USER_TOKEN, json.data.response.token);
                        console.log('user data:-', data.response);
                        dispatch({
                            type: types.USER_DETAIL,
                            data: json.data.response,
                        });
                    } else if (json.status == 400) {
                        console.log(json.data.message);
                    }
                })
                .catch(function (error) {
                    console.log(error.response);
                });
        } catch (error) {
            console.log(error);
        }

    };
};

const generateOtp = (val)=>{
    return async (dispatch,getStore)=>{
        try {
            api
                .generateOtp(val)
                .then((json) => {
                    console.log('otp Responce:-', json.data.response);
                    if (json.status == 200) {
                        
                        dispatch({
                            type: types.GENERATE_OTP,
                            data: json.data.response,
                        });
                    } else if (json.status == 400) {
                        console.log(json.data.message);
                    }
                })
                .catch(function (error) {
                    console.log(error.response);
                });
        } catch (error) {
            console.log(error);
        }
    }
}

const verifyOtp = (data)=>{
    return async (dispatch,getStore)=>{
        try {
            api
                .verifyOtp(data)
                .then((json) => {
                    console.log('otp Responce:-', json.data.response);
                    if (json.status == 200) {
                        
                        dispatch({
                            type: types.VERIFY_OTP,
                            data: json.data,
                        });
                    } else if (json.status == 400) {
                        console.log(json.data.message);
                        dispatch({
                            type: types.VERIFY_OTP,
                            data: error.response.data,
                        });
                    }
                })
                .catch(function (error) {
                    dispatch({
                        type: types.VERIFY_OTP,
                        data: error.response.data,
                    });
                    console.log(error);
                });
        } catch (error) {
            console.log(error);
            
        }
    }
}

const getSystemPreferences = (val)=>{
    return async (dispatch,getStore)=>{
        try {
            api
                .getSystemPreferences(val)
                .then((json) => {
                    console.log('system pref:-', json.data.response);
                    if (json.status == 200) {
                        dispatch({
                            type: types.SYSTEM_PREFERENCES,
                            data: json.data.response,
                        });
                    } else if (json.status == 400) {
                        console.log(json.data.message);
                    }
                })
                .catch(function (error) {
                    console.log(error.response);
                });
        } catch (error) {
            console.log(error);
        }
    }
}

const setSystemPreferences = (data)=>{
    return async (dispatch,getStore)=>{
        try {
            api
                .setStytemPreference(data)
                .then((json) => {
                    console.log('system pre:-', json.data.response);
                    if (json.status == 200) {
                        dispatch({
                            type: types.SYSTEM_PREFERENCES,
                            data: json.data.response,
                        });
                    } else if (json.status == 400) {
                        console.log(json.data.message);
                    }
                })
                .catch(function (error) {
                    console.log(error.response);
                });
        } catch (error) {
            console.log(error);
        }
    }
}

const setUserOnline =(val)=>{
    return async(dispatch,getStore)=>{
    try{
    api.setUserOnline().then((json)=>{
      if (json.status == 200) {
        console.log(json.data.response);
      
  
    } else {
        setTimeout(() => {
            
            console.log(json.data.response)
        }, 0);
  
    }
    }).catch(function (error) {
      Toast.show(error.response);
      console.log(error);
  });
    }catch(error){
      console.log(error)
    }
    }
  }
  
  const  setUserOffline=(val)=>{
      return async(dispatch,getStore)=>{
    try{
      api.setUserOffline().then((json)=>{
        if (json.status == 200) {
          console.log(json.data.response);
        
    
      } else {
          setTimeout(() => {
              
              console.log(json.data.response)
          }, 0);
    
      }
      }).catch(function (error) {
        Toast.show(error.response);
        console.log(error);
    });
      }catch(error){
        console.log(error)
      }
    }
  }

export default {
    loginApi,
    logoutUser,
    signUpApi,
    generateOtp,
    verifyOtp,
    getSystemPreferences,
    setSystemPreferences,
    setUserOffline,
    setUserOnline
};
