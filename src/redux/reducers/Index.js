import { combineReducers } from 'redux';
import AuthenticationReducer from './AuthenticationReducer';
import ChatReducer from './ChatReducer';
import ProfileReducer from './ProfileReducer';
import SearchReducer from './SearchReducer';
import TripReducer from './TripReducer';
import types from '../Types';

const appReducer = combineReducers({
  reducer: AuthenticationReducer,
  chatReducer: ChatReducer,
  profileReducer: ProfileReducer,
  tripReducer: TripReducer,
  searchReducer: SearchReducer

});

const rootReducer = (state, action) => {
  if (action.type == types.USER_LOGOUT) {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;
