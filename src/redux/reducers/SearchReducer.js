import types from "../Types";

let newState = {
    searchData: null,
}
let cloneObject = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

export default function Reducer(state, action) {
    switch (action.type) {
        case types.SEARCH_DATA:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    searchData: action.data,
                })
            }
            return newState;
        default:
            return state || newState;

    }
}