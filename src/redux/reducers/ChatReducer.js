import types from '../Types';

let newState = {
    isRegistered: null,
    participants: null,
    threadNavigation: null,
    registeredContact: null,
    groups: null,
    friends:null,
    participantsByThread:null,
    tripByThread:[],
};

let cloneObject = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
    switch (action.type) {

        case types.IS_REGISTERED:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    isRegistered: action.data,
                });
            }
            return newState;
        case types.PARTICIPANTS:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    participants: action.data,
                });
            }
            return newState;
        case types.THREAD_NAVIGATION: {
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    threadNavigation: action.data,
                });
            }
            return newState;

        }

        case types.REGISTERD_CONTACTS: {
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    registeredContact: action.data,
                });
            }
            return newState;

        }
        case types.GROUPS: {
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    groups: action.data,
                });
            }
            return newState;

        }
        case types.FRIENDS:{
            newState = cloneObject(state);
            if(action.data){
                newState = Object.assign({},newState,{
                    friends:action.data, 
                })
            }
            return newState;
        }
        case types.PARTICIPANTS_BY_THREAD:{
            newState =cloneObject(state);
            if(action.data){
                newState = Object.assign({},newState,{
                   participantsByThread:action.data
                })
            }
            return newState;
        }
        case types.TRIPS_BY_THREAD:{
            newState = cloneObject(state);
            if (action.data){
                newState = Object.assign({},newState,{
                    tripByThread:action.data,
                })
            }
            return newState;
        }
        default:
            return state || newState;
    }
}
