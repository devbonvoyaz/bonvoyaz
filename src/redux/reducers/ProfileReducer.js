import types from "../Types";

let newState= {
    profileData:null,
}
let cloneObject = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

export default function Reducer(state,action){
    switch(action.type){
        case types.UPDATE_PROFILE:
            newState = cloneObject(state);
            if(action.data){
                newState = Object.assign({},newState,{
                    profileData:action.data,
                })
            }
        return newState;
        default:
            return state || newState;

    }
}