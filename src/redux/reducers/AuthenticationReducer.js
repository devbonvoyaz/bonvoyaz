import types from '../Types';

let newState = {
    userDetail: null,
    otpResponse:null,
    verifyOtpResponse:null,
    systemPreferences:null,
};

let cloneObject = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

export default function reducer(state, action) {
    switch (action.type) {
        case types.USER_DETAIL:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    userDetail: action.data,
                });
            }
            return newState;
        case types.GENERATE_OTP:
            newState = cloneObject(state);
            if(action.data){
                newState = Object.assign({}, newState, {
                    otpResponse: action.data,
                });
            }
            return newState;
        case types.VERIFY_OTP:
            newState = cloneObject(state);
            if(action.data){
                newState = Object.assign({}, newState, {
                    verifyOtpResponse: action.data,
                });
            }
            return newState;    
        case types.SYSTEM_PREFERENCES:
            newState = cloneObject(state);
            if(action.data){
                newState = Object.assign({},newState,{
                    systemPreferences:action.data,
                })
            }
            return newState;    
        default:
            return state || newState;
    }
}
