import types from "../Types";

let newState = {
    tripData: null,
    tripsData: [],
    pastTripData: [],
    singleTrip: null,
    bookFlight: null,
    bookCar: null,
    bookHotel: null,
    booking: null,
    hotelBookings: null,
    itineraryData: [],
}
let cloneObject = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

export default function Reducer(state, action) {
    switch (action.type) {
        case types.TRIP_DATA:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    tripData: action.data,
                });
            }
            return newState;
        case types.TRIPS_DATA:
            newState = cloneObject(state);
            if (action.data) {

                newState = Object.assign({}, newState, {
                    tripsData: action.data,
                });
            }
            return newState;
        case types.PAST_TRIP_DATA:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    pastTripData: action.data,
                });
            }
            return newState;
        case types.SINGLE_TRIP:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    singleTrip: action.data,
                })
            }
            return newState;
        case types.BOOK_FLIGHT:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    bookFlight: action.data,
                })
            }
            return newState;
        case types.BOOK_CAR:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    bookCar: action.data,
                })
            }
            return newState;
        case types.BOOK_HOTEL:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    bookHotel: action.data
                })
            }
            return newState;
        case types.BOOKINGS:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    booking: action.data
                })
            }
            return newState;
        case types.HOTEL_BOOKINGS:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    hotelBookings: action.data,
                })
            }
            return newState;
        case types.ITINERARY_DATA:
            newState = cloneObject(state);
            if (action.data) {
                newState = Object.assign({}, newState, {
                    itineraryData: action.data,
                })
            }
            return newState;
        default:
            return state || newState;

    }
}