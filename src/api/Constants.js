'use-strict'

export const U_BASE = 'http://54.173.17.61:1337/';
export const AS_USER_TOKEN = 'user_access_token';
export const AS_USER_DETAIL = 'user_details';
export const AS_FCM_TOKEN = 'user_fcm_token';

export const U_LOGIN = 'auth/login';
export const U_SINGUP = 'auth/signup';
export const U_LOGOUT_USER = 'logout-user/';
export const U_GENERATE_OTP = 'generate-otp/';
export const U_VERIFY_OTP = 'verify-otp/';

//ONLINE  OFFLINE
export const U_USER_ONLINE = 'user-online';
export const U_USER_OFFLINE = 'user-offline';

//READ RECIPIENT
export const U_GET_SYSTEM_PREFERENCES = 'system-preference';


//CHAT
export const U_GET_PARTICIPANTS = 'participants'
export const U_GET_PARTICIPANTS_BY_THREAD = 'participant/';
export const U_IS_REGISTERED = 'is-registered/';
export const U_SEND_FRIEND_REQUEST = 'friend-request';
export const U_CREATE_THREAD = 'thread/';
export const U_GET_REGISTERED_CONTACTS = 'registered';
export const U_GET_GROUPS = 'thread-groups';
export const U_THREAD_GROUP = 'thread-group';
export const U_MESSAGE = 'message';
export const U_DELETE_MESSAGE = 'message/';
export const U_CREATE_POLL = 'poll';
export const U_VOTE_IN_POLL = 'vote/';
export const U_REMOVE_PARTICIPANT = 'participant';
export const U_ADD_PARTICIPANT = 'participant';
export const U_UPDATE_GROUP = 'thread-group/';
export const U_SET_PARTICIPANT_AS_ADMIN = 'set-admin';
export const U_ADD_TRIP_TO_THREAD = 'add-trip';
export const U_GET_TRIP_BY_THREAD = 'trips-group/';
export const U_CLEAR_CHAT = 'thread-clear/';
export const U_GET_MEDIA_BY_THREAD = 'thread-media';

//FRIEND
export const U_ADD_FRIEND = 'add-friend/';
export const U_GET_FRIENDS = 'friends/';

//PROFILE
export const U_UPDATE_PROFILE = 'update-user';

//TRIP
export const U_ADD_TRIP = 'trip';
export const U_GET_ALL_TRIPS = 'trips-user/';
export const U_UPDATE_TRIP = 'trip/';
export const U_DELETE_CONFLICTED_PLANS = 'trip-conflicted-delete/';
export const U_GET_ITINERARY_DATA = 'itinerary/';
export const U_GET_SINGLE_TRIP = 'trip-single/';
export const U_ADD_TRIP_TRAVELLER = 'add-participant-trip';
export const U_REMOVE_TRIP_TRAVELLER = 'delete-trip-participant';
export const U_MAKE_GUIDE = 'trip-guide';
export const U_DISMISS_AS_GUIDE = 'trip-guide-remove';

//TRIP PLANNER
export const U_GET_BOOKINGS = 'combined-trip-planner/';
export const U_BOOK_FLIGHT = 'book-flight';
export const U_BOOK_CAR = 'book-car-rental';
export const U_BOOK_HOTEL = 'book-hotel';
export const U_GET_HOTEL_BOOKING_BY_TRIP = 'hotels-trip/';
//UPLOAD IMAGE
export const U_UPLOAD_IMAGE = "file";

//PLANS
export const U_ADD_DRIVE_PLAN = "add-drive-plan";
export const U_UPDATE_DRIVE_PLAN = "edit-drive-plan/";
export const U_ADD_FLIGHT_PLAN = "add-flight-plan";
export const U_UPDATE_FLIGHT_PLAN = "edit-flight-plan/";
export const U_ADD_RESTAURANT_PLAN = "add-restaurant-plan";
export const U_UPDATE_RESTAURANT_PLAN = "edit-restaurant-plan/";
export const U_ADD_HOTEL_PLAN = "add-hotel-plan";
export const U_UPDATE_HOTEL_PLAN = "edit-hotel-plan/";
export const U_ADD_PLACE_PLAN = 'add-place-plan';
export const U_UPDATE_PLACE_PLAN = 'edit-place-plan/';
export const U_ADD_TRAIN_PLAN = 'add-train-plan';
export const U_UPDATE_TRAIN_PLAN = 'edit-train-plan/';
export const U_REMOVE_PLAN = 'remove-plan';

//Search
export const U_USER_SEARCH = 'user-search';

//IMAGE
export const GOOGLE_IMAGE = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photo_reference=';

export const GOOGLE_PLACES_API = 'AIzaSyAKhtBwQhmYRoAobEj7lx3jGztFyVzFF8s';