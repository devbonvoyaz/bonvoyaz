import axios from 'axios';
import Auth from '../auth/index';
import {
  AS_USER_TOKEN,
  U_ADD_DRIVE_PLAN,
  U_ADD_FLIGHT_PLAN,
  U_ADD_FRIEND, U_ADD_HOTEL_PLAN, U_ADD_PARTICIPANT,
  U_ADD_PLACE_PLAN,
  U_ADD_RESTAURANT_PLAN,
  U_ADD_TRAIN_PLAN,
  U_ADD_TRIP, U_ADD_TRIP_TO_THREAD, U_ADD_TRIP_TRAVELLER,
  U_BASE, U_BOOK_CAR, U_BOOK_FLIGHT, U_BOOK_HOTEL, U_CREATE_POLL,
  U_CREATE_THREAD,
  U_DELETE_MESSAGE,
  U_DISMISS_AS_GUIDE,
  U_GENERATE_OTP,
  U_GET_ALL_TRIPS,
  U_GET_BOOKINGS,
  U_GET_FRIENDS,
  U_GET_GROUPS,
  U_GET_HOTEL_BOOKING_BY_TRIP,
  U_GET_ITINERARY_DATA,
  U_GET_PARTICIPANTS,
  U_GET_PARTICIPANTS_BY_THREAD,
  U_GET_READ_RECIPIENT,
  U_GET_REGISTERED_CONTACTS, U_GET_SINGLE_TRIP,
  U_GET_SYSTEM_PREFERENCES, U_GET_TRIP_BY_THREAD,
  U_IS_REGISTERED,
  U_LOGIN, U_MAKE_GUIDE,
  U_MESSAGE,
  U_REMOVE_PARTICIPANT, U_REMOVE_TRIP_TRAVELLER,
  U_SEND_FRIEND_REQUEST, U_SET_PARTICIPANT_AS_ADMIN,
  U_SINGUP,
  U_THREAD_GROUP,
  U_UPDATE_GROUP,
  U_UPDATE_PROFILE, U_UPDATE_TRIP,
  U_UPLOAD_IMAGE,
  U_USER_OFFLINE,
  U_USER_ONLINE,
  U_VERIFY_OTP, U_VOTE_IN_POLL,
  U_UPDATE_DRIVE_PLAN,
  U_UPDATE_FLIGHT_PLAN,
  U_UPDATE_HOTEL_PLAN,
  U_UPDATE_PLACE_PLAN,
  U_UPDATE_RESTAURANT_PLAN,
  U_UPDATE_TRAIN_PLAN,
  U_REMOVE_PLAN,
  U_CLEAR_CHAT,
  U_GET_MEDIA_BY_THREAD,
  U_USER_SEARCH,
  U_LOGOUT_USER,
  U_DELETE_CONFLICTED_PLANS
} from './Constants';


const headers = async () => {
  const headers = {
    'Content-Type': 'application/json',
  };

  const auth = new Auth();
  const token = await auth.getValue(AS_USER_TOKEN);

  await console.log('user token is:-', token);
  if (token) {
    headers['Authorization'] = `Bearer ${token}`;
  }
  return headers;
};

const request = async (method, path, body) => {

  const url = `${U_BASE}${path}`;

  const options = { method, url, headers: await headers() };

  if (body) {
    options.data = body;
  }

  console.log('options are:-', options, body);
  return axios(options);
};

export default class API {
  loginApi(data) {
    return request('POST', U_LOGIN, data);
  }
  signUpApi(data) {
    return request('POST', U_SINGUP, data);
  }

  logoutUser(val) {
    return request('POST', U_LOGOUT_USER + val);
  }

  generateOtp(val) {
    return request('GET', U_GENERATE_OTP + val)
  }

  verifyOtp(data) {
    return request('POST', U_VERIFY_OTP, data);
  }

  //ONLINE/OFFLINE
  setUserOnline() {
    return request('GET', U_USER_ONLINE);
  }

  setUserOffline() {
    return request('GET', U_USER_OFFLINE);
  }

  //Read Recipient
  getSystemPreferences(val) {
    return request('GET', U_GET_SYSTEM_PREFERENCES);
  }

  setStytemPreference(data) {
    return request('POST', U_GET_SYSTEM_PREFERENCES, data);
  }

  changeReadRecipient(data) {
    return request('POST', U_GET_READ_RECIPIENT, data);
  }

  //CHAT
  getParticipants() {
    return request('GET', U_GET_PARTICIPANTS);
  }

  getParticipantsByThread(val) {
    return request('GET', U_GET_PARTICIPANTS_BY_THREAD + val);
  }

  getGroups() {
    return request('GET', U_GET_GROUPS);
  }

  updateGroup(val, data) {
    return request('PATCH', U_UPDATE_GROUP + val, data);
  }


  isRegistered(val) {
    return request('GET', U_IS_REGISTERED + val);
  }

  sendFriendRequest(data) {
    return request('POST', U_SEND_FRIEND_REQUEST, data);
  }

  createThread(val) {
    return request('GET', U_CREATE_THREAD + val);
  }

  getRegisteredContacts(data) {
    return request('POST', U_GET_REGISTERED_CONTACTS, data);
  }

  createGroup(data) {
    return request('POST', U_THREAD_GROUP, data);
  }

  removeParticipant(data) {
    return request('DELETE', U_REMOVE_PARTICIPANT, data);
  }

  addParticipants(data) {
    return request('POST', U_ADD_PARTICIPANT, data);
  }

  sendMessage(data) {
    return request('POST', U_MESSAGE, data);
  }

  deleteMessage(val) {
    return request('DELETE', U_DELETE_MESSAGE + val);
  }

  createPoll(data) {
    return request('POST', U_CREATE_POLL, data);
  }

  voteInPoll(val) {
    return request('POST', U_VOTE_IN_POLL + val);
  }

  setParticipantAsAdmin(data) {
    return request('POST', U_SET_PARTICIPANT_AS_ADMIN, data);
  }

  addTripToThread(data) {
    return request('POST', U_ADD_TRIP_TO_THREAD, data);
  }

  getTripByThread(val) {
    return request('GET', U_GET_TRIP_BY_THREAD + val);
  }

  clearChat(val) {
    return request('DELETE', U_CLEAR_CHAT + val);
  }

  getMediaByThread(data) {
    return request('POST', U_GET_MEDIA_BY_THREAD, data);
  }

  //FRIEND
  addFriend(val) {
    return request('GET', U_ADD_FRIEND + val);
  }

  getFriends(val) {
    return request('GET', U_GET_FRIENDS);
  }

  uploadImage(data) {
    return request('POST', U_UPLOAD_IMAGE, data);
  }

  //Profile
  updateProfile(data) {
    return request('PATCH', U_UPDATE_PROFILE, data);
  }

  //Trip
  addTrip(data) {
    return request('POST', U_ADD_TRIP, data);
  }

  getAllTrips(val) {
    return request('GET', U_GET_ALL_TRIPS + val);
  }

  updateTrip(id, data) {
    return request('PATCH', U_UPDATE_TRIP + id, data);
  }

  deleteConflictedPlans(id, data) {
    return request('POST', U_DELETE_CONFLICTED_PLANS + id, data)
  }

  getSingleTrip(val) {
    return request('GET', U_GET_SINGLE_TRIP + val);
  }

  addTripTraveller(data) {
    return request('POST', U_ADD_TRIP_TRAVELLER, data);
  }

  removeTripTraveller(data) {
    return request('POST', U_REMOVE_TRIP_TRAVELLER, data);
  }

  makeGuide(data) {
    return request('POST', U_MAKE_GUIDE, data);
  }

  dismissAsGuide(data) {
    return request('POST', U_DISMISS_AS_GUIDE, data)
  }

  //trip planner
  bookFlight(data) {
    return request('POST', U_BOOK_FLIGHT, data);
  }

  bookCar(data) {
    return request('POST', U_BOOK_CAR, data);
  }

  bookHotel(data) {
    return request('POST', U_BOOK_HOTEL, data);
  }

  getBookings(val) {
    return request('GET', U_GET_BOOKINGS + val);
  }

  getHotelBookingByTrip(val) {
    return request('GET', U_GET_HOTEL_BOOKING_BY_TRIP + val);
  }

  //PLANS


  addDrivePlan(data) {
    return request('POST', U_ADD_DRIVE_PLAN, data);
  }

  updateDrivePlan(val, data) {
    return request('PATCH', U_UPDATE_DRIVE_PLAN + val, data);
  }

  addFlightPlan(data) {
    return request('POST', U_ADD_FLIGHT_PLAN, data);
  }

  updateFlightPlan(val, data) {
    return request('PATCH', U_UPDATE_FLIGHT_PLAN + val, data);
  }

  addRestaurant(data) {
    return request('POST', U_ADD_RESTAURANT_PLAN, data);
  }

  updateRestaurantPlan(val, data) {
    return request('PATCH', U_UPDATE_RESTAURANT_PLAN + val, data);
  }

  addHotel(data) {
    return request('POST', U_ADD_HOTEL_PLAN, data);
  }

  updateHotelPlan(val, data) {
    return request('PATCH', U_UPDATE_HOTEL_PLAN + val, data);
  }

  addPlace(data) {
    return request('POST', U_ADD_PLACE_PLAN, data);
  }

  updatePlacePlan(val, data) {
    return request("PATCH", U_UPDATE_PLACE_PLAN + val, data);
  }

  addTrain(data) {
    return request('POST', U_ADD_TRAIN_PLAN, data);
  }

  updateTrainPlan(val, data) {
    return request('PATCH', U_UPDATE_TRAIN_PLAN + val, data);
  }

  getTripItinerary(val) {
    return request('GET', U_GET_ITINERARY_DATA + val);
  }

  removePlan(data) {
    return request('POST', U_REMOVE_PLAN, data);
  }

  //Search
  searchUser(data) {
    return request('POST', U_USER_SEARCH, data);
  }


}
