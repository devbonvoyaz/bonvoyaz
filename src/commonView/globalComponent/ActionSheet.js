import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Modal,
  ImageBackground,
  SafeAreaView,
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';

import {CustomButton} from './CustomButton';


export default  ActionSheet = (props) => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <SafeAreaView style={{flex: 1}}>
          <View style={styles.white_view}>
            <Text style={{marginVertical: 10}}>
              Choose Option
            </Text>
            <CustomButton
            width={wp(75)}
              text={'Open Camera'}
              labelColor={COLOR.DARK_BLUE}
              onPress={() => props.handleSheet(1)}
            />
            
            <CustomButton
              width={wp(75)}
              text={'Open Gallery'}
              labelColor={COLOR.DARK_BLUE}
              onPress={() => props.handleSheet(2)}
            />
           
            <CustomButton
            width={wp(75)}
              text={'Cancel'}
              labelColor={COLOR.DARK_BLUE}
              onPress={() => props.handleSheet(3)}
            />
          </View>
        </SafeAreaView>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#00000030',
  },
  modalBody: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  appicon_image: {
    height: 90,
    width: 90,
    marginTop: 50,
    marginLeft: 20,
    overflow: 'hidden',
  },
  white_view: {
    width: wp(90),
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    bottom: 20,
    position: 'absolute',
    borderRadius: 20,
  },
});
