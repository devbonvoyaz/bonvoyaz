import React, { Component } from 'react';
import { View, Animated, StyleSheet, TextInput } from 'react-native';
import { string, func, object, number } from 'prop-types';
import COLOR from '../../res/styles/Color';

export class FloatingTitleTextInputField extends Component {
  static propTypes = {
    attrName: string.isRequired,
    title: string.isRequired,
    value: string.isRequired,
    updateMasterState: func.isRequired,
    keyboardType: string,
    isSecureTextEntry: false,
    titleActiveSize: number, // to control size of title when field is active
    titleInActiveSize: number, // to control size of title when field is inactive
    titleActiveColor: string, // to control color of title when field is active
    titleInactiveColor: string, // to control color of title when field is active
    textInputStyles: object,
    otherTextInputProps: object,
  };

  static defaultProps = {
    keyboardType: 'default',
    titleActiveSize: 12,
    titleInActiveSize: 16,
    titleActiveColor: COLOR.BLACK,
    titleInactiveColor: COLOR.LIGHT_TEXT,
    textInputStyles: {},
    otherTextInputAttributes: {},
  };

  constructor(props) {
    super(props);
    const { value } = this.props;
    this.position = new Animated.Value(value ? 1 : 0);
    this.state = {
      isFieldActive: false,
    };
    this.ref = React.createRef();
  }

  componentDidMount() {

    Animated.timing(this.position, {
      toValue: 1,
      duration: 150,
    }).start();

  }


  _handleFocus = () => {
    if (!this.state.isFieldActive) {
      this.setState({ isFieldActive: true });
      Animated.timing(this.position, {
        toValue: 1,
        duration: 150,
      }).start();
    }
  };

  _handleBlur = () => {
    if (this.state.isFieldActive && !this.props.value) {
      this.setState({ isFieldActive: false });
      Animated.timing(this.position, {
        toValue: 0,
        duration: 150,
      }).start();
    }
  };

  _onChangeText = (updatedValue) => {
    const { attrName, updateMasterState } = this.props;
    updateMasterState(attrName, updatedValue);
  };

  _returnAnimatedTitleStyles = () => {
    const { isFieldActive } = this.state;
    const {
      titleActiveColor,
      titleInactiveColor,
      titleActiveSize,
      titleInActiveSize,
    } = this.props;

    return {
      top: this.position.interpolate({
        inputRange: [0, 1],
        outputRange: [14, 0],
      }),
      fontSize: isFieldActive ? titleActiveSize : titleInActiveSize,
      color: isFieldActive ? titleActiveColor : titleInactiveColor,
    };
  };

  render() {
    return (
      <View style={Styles.container}>
        <Animated.Text
          style={[Styles.titleStyles, this._returnAnimatedTitleStyles()]}>
          {this.props.title}
        </Animated.Text>
        <TextInput
          ref={this.ref}
          value={this.props.value}
          style={[Styles.textInput, this.props.textInputStyles]}
          underlineColorAndroid="transparent"
          onFocus={this._handleFocus}
          onBlur={this._handleBlur}
          secureTextEntry={this.props.isSecureTextEntry}
          onChangeText={this._onChangeText}
          keyboardType={this.props.keyboardType}
          placeholderTextColor={COLOR.LIGHT_TEXT_COLOR}
          {...this.props.otherTextInputProps}
        />
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    borderWidth: 0.0,
    height: 45,
    alignSelf: 'center'
  },
  textInput: {
    fontSize: 15,
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 0,
    color: 'black',
    flex: 1,
  },
  titleStyles: {
    position: 'absolute',
    left: 10,
    top: 10,
    marginTop: 0,
    color: COLOR.GREY,

  },
});
