import React, { Component } from 'react';
import {
  View,
  Animated,
  StyleSheet,
  TextInput,
  Platform,
} from 'react-native';
import { string, func, object, number, bool } from 'prop-types';
import COLOR from "../../res/styles/Color";
import FONTS from "../../res/styles/Fonts";
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { subscribeToSignificantLocationUpdates } from 'react-native-location';

export class FloatingTitleTextInputField extends Component {
  static propTypes = {
    attrName: string.isRequired,
    title: string.isRequired,
    updateMasterState: func.isRequired,
    keyboardType: string,
    titleActiveSize: number, // to control size of title when field is active
    titleInActiveSize: number, // to control size of title when field is inactive
    textInputStyles: object,
    otherTextInputProps: object,
    isWhite: bool,
    image: string,
    editable: bool,
    isPasswordVisible: bool,
    handlePasswordVisibility: func,
    handleFieldEmpty: bool,
  };

  static defaultProps = {
    keyboardType: 'default',
    isWhite: true,
    titleActiveSize: 12,
    titleInActiveSize: 12,
    textInputStyles: {},
    otherTextInputAttributes: {},
    image: '',
    editable: true,
    isPasswordVisible: false,
    handleFieldEmpty: false,
    autoCapitalize: 'characters',

  };

  constructor(props) {
    super(props);
    this.state = {
      isFieldActive: false,
      isPasswordVisible: bool,
    };
    const { value } = this.props;
    this.position = new Animated.Value(value ? 1 : 0);
    this.initFields = this.initFields.bind(this);
    this.input = React.createRef();
  }

  componentDidMount() {
    this.setState({
      isPasswordVisible: this.props.isPasswordVisible,
    });
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS,
  ) {
    if (prevProps != this.props) {
      this.initFields();
    }
  }

  initFields = () => {
    if (this.props.value) {
      this.setState({ isFieldActive: true });
      Animated.timing(this.position, {
        toValue: 1,
        duration: 150,
      }).start();
    }
  };

  _handleFocus = () => {
    if (!this.state.isFieldActive) {
      this.setState({ isFieldActive: true });
      Animated.timing(this.position, {
        toValue: 1,
        duration: 150,
      }).start();
    }
  };

  _handleBlur = () => {
    if (this.state.isFieldActive && !this.props.value) {
      this.setState({ isFieldActive: false });
      Animated.timing(this.position, {
        toValue: 0,
        duration: 150,
      }).start();
    }
  };

  _onChangeText = updatedValue => {

    const { attrName, updateMasterState } = this.props;
    updateMasterState(attrName, updatedValue);

  };

  handlePasswordVisibility = updateValue => {
    this.setState({
      isPasswordVisible: !this.state.isPasswordVisible,
    });
  };

  _returnAnimatedTitleStyles = () => {
    const { isFieldActive } = this.state;
    const { titleActiveSize, titleInActiveSize } = this.props;

    return {
      top: this.position.interpolate({
        inputRange: [0, 1],
        outputRange: [14, 0],
      }),
      fontSize: isFieldActive ? titleActiveSize : titleInActiveSize,

    };
  };

  render() {
    return (
      <View style={Styles.container}>
        <Animated.Text
          style={[
            Styles.titleStyles,
            this._returnAnimatedTitleStyles(),
            {
              color: this.props.handleFieldEmpty
                ? COLOR.RED
                : this.props.isWhite == false
                  ? COLOR.PRIMARYGREY
                  : COLOR.WHITE,
            },
          ]}>
          {this.props.title}
        </Animated.Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <TextInput
            ref={this.input}
            value={this.props.value}
            autoCapitalize='words'
            selectionColor={
              this.props.isWhite == false ? COLOR.BLACK : COLOR.WHITE
            }
            style={[
              Styles.textInput,
              this.props.textInputStyles,
              {
                marginRight: -20,
                flex: 1,
                color: this.props.isWhite == false ? COLOR.BLACK : COLOR.WHITE,

              },
            ]}
            underlineColorAndroid="transparent"
            onFocus={this.props.onFocus ? () => {
              this.props.onFocus();
              this.input.current.blur();
            } : this._handleFocus}
            onBlur={this._handleBlur}
            editable={this.props.editable}
            secureTextEntry={this.state.isPasswordVisible}
            onChangeText={this._onChangeText}
            keyboardType={this.props.keyboardType}
            placeholderTextColor={
              this.props.isWhite == false ? COLOR.BLACK : COLOR.WHITE
            }
            {...this.props.otherTextInputProps}
          />
        </View>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    width: '100%',
    borderWidth: 0.0,
    height: 45,
  },
  textInput: {
    fontSize: 16,
    fontFamily: FONTS.FAMILY_MEDIUM,
    marginTop: Platform.OS == 'android' ? 15 : 20,
    marginLeft: 0,
    // borderBottomWidth: 1,
  },
  titleStyles: {
    position: 'absolute',
    margin: 0,
    fontSize: 14,
    fontFamily: FONTS.FAMILY_MEDIUM,
    color: COLOR.PRIMARYGREY,
  },
});
