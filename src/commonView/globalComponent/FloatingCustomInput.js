import React from 'react';
import { Image, StyleSheet, Text, TextInput, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import { FloatingTitleTextInputField } from './FloatingTextField';

export default function FloatingCustomInput(props) {

  return (
    <View
    
      style={[style.text_input_card, { width: props.width, flexDirection: 'row' }]}>
      {props.icon ?
        <View
          style={{
            backgroundColor: props.color,
            width: 40,
            height: 40,
            borderRadius: 20,
            justifyContent: 'center',
            alignSelf:'center'
          }}
        >
          <Image
            source={props.icon}
            style={{
              height: 20,
              width: 20,
              alignSelf: 'center',
            }}
            resizeMode={'contain'}
          />
        </View>
        : null}
    
       
    
        <FloatingTitleTextInputField
        titleActiveColor={COLOR.PRIMARYGREY}
        titleInactiveColor={COLOR.PRIMARYGREY}
        titleActiveSize={15}
        titleInActiveSize={15}
        attrName={props.attrName}
        title={props.title}
        value={props.value}
        updateMasterState={props.updateMasterState}
        />
        
      
    </View>
  );
}

const style = StyleSheet.create({
  text_input_card: {
    alignSelf: 'center',
    borderRadius: 5,
    backgroundColor: COLOR.WHITE,
    elevation: 5,
    padding: 10,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    shadowOffset: { width: 2, height: 2 },
    marginVertical: 8,
  },
});
