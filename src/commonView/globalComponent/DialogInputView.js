import React from 'react';
import { Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';

export default function DialogInputView(props) {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[style.text_input_card, { width: props.width, flexDirection: 'row' }]}>
      {props.icon ?
        <View
          style={{
            backgroundColor: props.color,
            width: 40,
            height: 40,
            borderRadius: 20,
            marginRight: 10,
            justifyContent: 'center',
          }}
        >
          <Image
            source={props.icon}
            style={{
              height: 20,
              width: 20,
              alignSelf: 'center',
            }}
            resizeMode={'contain'}
          />
        </View>
        : null}

      <View style={{justifyContent:'center'}}>
        <Text
          style={{
            fontSize: 14,
            fontFamily: FONTS.FAMILY_MEDIUM,
            color: COLOR.PRIMARYGREY,
          }}
        >
          {props.title}
        </Text>
        <View  >
            {props.placeholder?<Text
                style={{
                    fontSize: 14,
                    fontFamily: FONTS.FAMILY_MEDIUM,
                    marginVertical: 5,
                    color: COLOR.BLACK,
                }}

            >{props.placeholder}</Text>:null}
        </View>
      </View>
    </TouchableOpacity>
  );
}

const style = StyleSheet.create({
  text_input_card: {
    alignSelf: 'center',
    borderRadius: 5,
    backgroundColor: COLOR.WHITE,
    elevation: 5,
    padding: 12,
    height: 60,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    shadowOffset: { width: 2, height: 2 },
    marginVertical: 8,
  },
});
