import React from 'react';
import { Modal, Text, View, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { heightPercentageToDP, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import CalendarPicker from 'react-native-calendar-picker';
import Moment from 'moment';
import moment from 'moment';
import { SafeAreaView } from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import SimpleToast from 'react-native-simple-toast';

export default class SelectDate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            modalVisible: true,
            selectedStartDate: null,
            selectedEndDate: null,
            previousTitle: '',
            nextTitle: '',
            allowRangeSelection: true,
        };
        this.onDateChange = this.onDateChange.bind(this);
    }

    componentDidMount() {
        this.initialiseCalendar(new Date());

    }

    initialiseCalendar(date) {
        let day = date ? Moment(date).toDate() : this.state.selectedStartDate;
        let previousMonth = day.setMonth(day.getMonth() - 1);
        let nextMonth = day.setMonth(day.getMonth() + 2);

        this.setState({
            // selectedStartDate: date,
            previousTitle: '< ' + Moment(previousMonth).format('MMM YYYY'),
            nextTitle: Moment(nextMonth).format('MMM YYYY') + ' >',
        });
    }

    onDateChange(date, type) {
        if (type === 'END_DATE') {
            this.setState({
                selectedEndDate: new Date(date).getTime(),
            });
        } else {
            this.setState({
                selectedStartDate: new Date(date).getTime(),
                selectedEndDate: null,
            });
        }
    }

    TextFieldView = (props) => (
        <View style={{ marginVertical: 5 }}>
            <FloatingTitleTextInputField
                attrName={props.attributeName}
                title={props.title}
                updateMasterState={null}
                textInputStyles={{
                    color: COLOR.BLACK,
                    fontSize: 15,
                }}
                otherTextInputProps={{
                    maxLength: 12,
                }}
            />
        </View>
    );

    SingleLineView = () => (
        <View
            style={{
                borderBottomColor: COLOR.GRAY,
                marginVertical: 5,
                borderBottomWidth: 3,
            }}
        />
    );

    modalBack = () => {
        this.setState({ modalVisible: false }, (val) => {
            this.props.updateMasterState("openCalendar", false);
        });
    }


    render() {
        const { selectedStartDate, previousTitle, nextTitle, packageDetail } = this.state;
        const startDate = selectedStartDate ? selectedStartDate.toString() : '';

        return (
            <RBSheet
                ref={this.props.RBSheet}
                height={heightPercentageToDP(55)}
                openDuration={100}
                closeDuration={100}
                closeOnDragDown="true"
                dragFromTopOnly="true"
                customStyles={{
                    draggableIcon: {
                        backgroundColor: "white"
                    },
                    container: {
                        backgroundColor: COLOR.WHITE,
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                    }
                }}
            >
                <View style={styles.modal}>
                    <View style={styles.modalBody}>

                        <View style={{ flex: 1 }}>
                            <CalendarPicker
                                allowRangeSelection={true}
                                startFromMonday={true}
                                selectedDayColor={COLOR.PRIMARYBLUE}
                                todayBackgroundColor={COLOR.GRAY}
                                showDayStragglers={true}
                                allowBackwardRangeSelect={false}
                                initialDate={new Date()}
                                selectedDayTextColor={COLOR.WHITE}
                                nextTitle={nextTitle}
                                minDate={new Date()}
                                maxDate={null}
                                maxRangeDuration={365}
                                previousTitle={previousTitle}
                                horizontal={true}
                                enableSwipe={true}
                                scaleFactor={375}
                                onDateChange={this.onDateChange}
                                onMonthChange={(date) => this.initialiseCalendar(date)}
                                previousTitleStyle={{ color: COLOR.PRIMARYBLUE }}
                                nextTitleStyle={{ color: COLOR.PRIMARYBLUE }}
                                monthYearHeaderWrapperStyle={{
                                    height: 40,
                                    backgroundColor: 'transparent',
                                }}
                                headerWrapperStyle={{ backgroundColor: COLOR.GREY }}
                            />

                        </View>
                        <View
                            style={{
                                marginVertical: 20,
                                height: 50,
                                flexDirection: 'row',
                                width: wp(90),
                                alignSelf: 'center',
                            }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        selectedStartDate: null,
                                        selectedEndDate: null,
                                    });
                                }}
                                style={{ flex: 1, marginRight: 10 }}>
                                <View
                                    style={{
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        borderRadius: 10,
                                        height: 50,
                                        borderColor: COLOR.GRAY,
                                        borderWidth: 1,
                                        backgroundColor: COLOR.WHITE,
                                    }}>
                                    <Text style={[{ color: COLOR.BLACK }]}>
                                        Clear All
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    if (this.state.selectedStartDate && this.state.selectedEndDate) {
                                        this.props.updateMasterState('startDate', this.state.selectedStartDate);
                                        this.props.updateMasterState('startText', moment(this.state.selectedStartDate).format("DD-MMM-YYYY"));
                                        this.props.updateMasterState('endDate', this.state.selectedEndDate);
                                        this.props.updateMasterState('endText', moment(this.state.selectedEndDate).format("DD-MMM-YYYY"));
                                        this.props.RBSheet.current.close();
                                    } else {
                                        SimpleToast.show('Select both start date and end date')
                                    }

                                }}
                                style={{ flex: 1 }}>
                                <View
                                    style={{
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        borderRadius: 10,
                                        height: 50,
                                        borderColor: COLOR.GRAY,
                                        borderWidth: 1,
                                        backgroundColor: COLOR.WHITE,
                                    }}>
                                    <Text style={[{ color: COLOR.BLACK }]}>
                                        Apply
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </RBSheet>
        );
    }
}

const styles = StyleSheet.create({
    modal: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
    },
    modalBody: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: COLOR.WHITE,
        borderWidth: 1,
        width: wp(100),
        flex: 1,
        borderColor: COLOR.WHITE

    },
    image: {
        height: 120,
        width: 120,
        alignSelf: 'center',
        marginVertical: 20,
    },
    headingLabel: {
        fontFamily: FONTS.FAMILY_BOLD,
        fontSize: FONTS.MEDIUM,
        color: COLOR.BLACK,
    },
    singleLineView: {
        marginVertical: 20,
        borderBottomColor: COLOR.GRAY,
        width: wp(100),
        alignSelf: 'center',
        borderBottomWidth: 1,
    },
    borderView: {
        borderRadius: 10,
        borderWidth: 1,
        width: wp(90),
        borderColor: COLOR.GRAY,
        alignSelf: 'center',
    },
});
