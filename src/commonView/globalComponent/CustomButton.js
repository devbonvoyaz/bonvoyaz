import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';


export const CustomButton = props => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[
        styles.button_content,
        {
          borderRadius: props.borderRadius ? props.borderRadius : 7,
          backgroundColor: props.bg,
          borderColor: props.borderClr ? props.borderClr : props.bg,
          width: props.width == null ? wp(90) : props.width,
        },
      ]}
    >
      <Text style={[{ color: props.labelColor }]}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button_content: {
    height: 50,
    width: wp(90),
    borderRadius: 7,
    borderWidth: 2,
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
