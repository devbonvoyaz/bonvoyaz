import TabItem from "./TabItem";
import React from "react";
import { View } from "react-native";

export default function TabBar({ state, descriptors, navigation }) {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  const images = [
    'comment-alt',
    'search',
    'briefcase',
    'user',

  ];

  return (
    <View style={{ flexDirection: 'row', height: 75, paddingBottom: 10, backgroundColor: '#FAFAFA' }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TabItem
            key={index}
            style={{ flex: 1 }}
            icon={images[index]}
            label={label}
            active={isFocused}
            onPress={() => {
              onPress();
              //if (index == 1) {
              //  navigation.navigate('CameraClassComponent');
              //};
            }}
          />
        );
      })}
    </View>
  );
}