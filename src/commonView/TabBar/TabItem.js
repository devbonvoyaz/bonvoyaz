import React from "react";
import { Image, StyleSheet, Text, TouchableWithoutFeedback, View } from "react-native";
import { useEffect, useRef } from "react";
import { Animated } from "react-native";
import { useMemo } from "react";
import Icon from "react-native-vector-icons/FontAwesome5";


const activeColor = "rgb(30, 30, 110)";
const inactiveColor = "rgba(30, 30, 110, 0.4)";


export default function TabItem({ style, icon, label, active, onPress }){

const animation = useSpring({ to: active ? 1 : 0 }, { stiffness: 50 });
const labelTranslate = animation.interpolate({ inputRange: [0, 1], outputRange: [20, 0] });
const iconTranslate = animation.interpolate({ inputRange: [0, 1], outputRange: [0, -30] });
const labelVisibility = animation;
const iconVisibility = animation.interpolate({ inputRange: [0, 1], outputRange: [1, 0] });
const dotScale = animation;
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[styles.container, style]}>
        <Animated.View
          style={[
            styles.centered,
            { opacity: labelVisibility, transform: [{ translateY: labelTranslate }] },
          ]}
        >
          <Text style={styles.label}>{label}</Text>
        </Animated.View>
        <Animated.View
          style={[
            styles.centered,
            { opacity: iconVisibility, transform: [{ translateY: iconTranslate }] },
          ]}
        >
          
          <Icon size={20} name={icon} />
        </Animated.View>
        <Animated.View style={[styles.dot, { transform: [{ scale: dotScale }] }]} />
      </View>
    </TouchableWithoutFeedback>
  );
};



 function useSpring(value: { to: number }, config?: SpringAnimationConfig) {
  const animatedValue = useMemo(() => new Animated.Value(value.to), []);
  useEffect(() => {
    const animation = Animated.spring(animatedValue, {
      ...config,
      toValue: value.to,
      useNativeDriver: true,
    });
    animation.start();
    return () => animation.stop();
  }, [value.to]);
  return animatedValue;
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
  },
  centered: {
    position: "absolute",
  },
  icon: {
    tintColor: inactiveColor,
    width:25,
    height:25,
    alignSelf:'center'
  },
  label: {
    color: activeColor,
    fontSize: 12,
    fontWeight: "600",
    letterSpacing: -0.2,
  },
  dot: {
    position: "absolute",
    bottom: 8,
    width: 5,
    height: 5,
    borderRadius: 2.5,
    backgroundColor: activeColor,
  },
});
