import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import FONTS from '../../res/styles/Fonts';
import Styles from '../../res/styles/Styles';

export default function SelectFriendsList(props) {


    return (
        <View style={{
            opacity: props.preselected ? 0.5 : 1
        }}>
            <TouchableOpacity
                onPress={() => {
                    if (props.selected) {
                        props.removeParticipant != null ? props.removeParticipant(props.item) : null;
                    } else {
                        props.addParticipant(props.item);
                    }
                }}
            >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            marginVertical: 10,
                        }}
                    >
                        <Image
                            style={{
                                borderRadius: 25,
                                width: 50,
                                height: 50,
                                marginHorizontal: 15,
                            }}
                            source={props.image}
                        />
                        <View style={{ alignSelf: 'center' }}>
                            <Text style={{ fontSize: 14, fontFamily: FONTS.FAMILY_MEDIUM }}>
                                {props.username}
                            </Text>
                        </View>
                    </View>
                    <View style={{ marginRight: 10, alignSelf: 'center' }}>
                        <TouchableOpacity
                            onPress={() => {
                                if (props.selected) {
                                    props.removeParticipant != null ? props.removeParticipant(props.item) : null;
                                } else {
                                    props.addParticipant(props.item);
                                }
                            }}>
                            <Image
                                source={props.selected || props.preselected
                                    ? require('../../res/assets/Checked.png') :
                                    require('../../res/assets/Inactive.png')}
                                resizeMode={'contain'}
                                style={{ width: 25, height: 25, marginHorizontal: 10 }}
                            />
                        </TouchableOpacity>
                    </View>

                </View>
                <View style={Styles.hr} />
            </TouchableOpacity>
        </View>
    );
}
