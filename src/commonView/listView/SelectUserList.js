import React from 'react';
import {Image, Text, View} from 'react-native';
import {Checkbox} from 'react-native-paper';
import FONTS from '../../res/styles/Fonts';
import Styles from '../../res/styles/Styles';

export default function SelectUserList (props) {
  const [checked, setChecked] = React.useState (false);
  return (
    <View>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
          }}
        >
          <Image
            style={{
              borderRadius: 15,
              width: 50,
              height: 50,
              marginHorizontal: 15,
            }}
            source={props.image}
          />
          <View style={{alignSelf: 'center'}}>
            <Text style={{fontSize: 12, fontFamily: FONTS.FAMILY_REGULAR}}>
              {props.userId}
            </Text>
            <Text style={{fontSize: 14, fontFamily: FONTS.FAMILY_MEDIUM}}>
              {props.username}
            </Text>
          </View>
        </View>
        <View style={{marginRight: 10, alignSelf: 'center'}}>
          <Checkbox
            status={checked ? 'checked' : 'unchecked'}
            onPress={() => {
              setChecked (!checked);
            }}
          />
        </View>

      </View>
      <View style={Styles.hr} />
    </View>
  );
}
