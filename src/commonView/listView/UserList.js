import React from 'react';
import { Image, Text, View, TouchableOpacity } from 'react-native';
import FONTS from '../../res/styles/Fonts';
import Styles from '../../res/styles/Styles';
import COLOR from '../../res/styles/Color';


export default function UserList(props) {
  return (
    <TouchableOpacity
      onPress={props.isUserGuide && props.dataLength > 1 ? () => props.onPress(props.item) : null}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
          }}
        >
          <Image
            style={{
              borderRadius: 25,
              width: 50,
              height: 50,
              marginHorizontal: 15,
            }}
            source={props.image}
          />
          <View style={{ alignSelf: 'center' }}>
            <Text style={{ fontSize: 14, fontFamily: FONTS.FAMILY_MEDIUM }}>
              {props.username}
            </Text>

          </View>

        </View>
        {props.isGuide ?
          <View
            style={{
              marginHorizontal: 15,
              alignSelf: 'center',
              justifyContent: 'center',
              width: 80,
              height: 30,
              borderRadius: 5,
              borderWidth: 1,
              borderColor: COLOR.PRIMARYBLUE,
            }}
          >
            <Text
              style={{
                fontSize: 12,
                fontFamily: FONTS.FAMILY_MEDIUM,
                textAlign: 'center',
                alignSelf: 'center',
                color: COLOR.PRIMARYBLUE,
              }}>Guide</Text>
          </View>
          : null
        }
      </View>
      <View style={Styles.hr} />
    </TouchableOpacity>
  );
}
