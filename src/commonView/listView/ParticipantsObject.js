import React, {useState} from 'react';
import {Button, Image, Text, TouchableOpacity, View} from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';

import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import {Dialog, Paragraph} from "react-native-paper";



export default function ParticipantsObject(props) {
  const [removeVisible,setRemoveVisible] = useState(false);
  return (
    <TouchableOpacity 
      style={{width:widthPercentageToDP(90)}}
      onPress={props.onPress ? props.onPress : null}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 10,
          }}
        >
          <Image
            style={{
              borderRadius: 30,
              width: 45,
              height: 45,
              marginHorizontal: 15,
            }}
            source={props.image}
          />
          <View style={{ alignSelf: 'center' }}>
            <Text style={{ fontSize: 14, fontFamily: FONTS.FAMILY_SEMIBOLD, color: COLOR.PRIMARYBLACK }}>
              {props.username}
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontFamily: FONTS.FAMILY_REGULAR,
                color: COLOR.PRIMARYGREY,
              }}
            >
              {props.status}
            </Text>
          </View>
        </View>
        {props.group&&props.isAdmin?
        <TouchableOpacity 
        style={{alignSelf:'center'}}
        onPress={props.removeParticipants}>
        <Image
          style={{alignSelf: 'center', width: 24, height: 24}}
          source={require ('../../res/assets/exit.png')}
          resizeMode={'contain'}
        />
        </TouchableOpacity>
        :
        null}
      </View>


    </TouchableOpacity>
  );
}
