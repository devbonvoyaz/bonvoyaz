import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {Image, Text, TouchableOpacity, View, ImageBackground} from 'react-native';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import Icon from 'react-native-vector-icons/Ionicons';
import RnVectorIcon from '../../reusable/RnVectorIcon';

export default function TripObject (props) {
  const navigation = useNavigation ();
  return (
    <TouchableOpacity
      style={{
        flexDirection: 'row',
        padding: 8,
        borderRadius: 8,
        margin: 5,
        backgroundColor: COLOR.WHITE,
        shadowColor: COLOR.BLACK,
        shadowOpacity: 0.2,
        elevation: 3,
        shadowOffset: {width: 2, height: 2},
      }}
      onPress={props.onPress}
    >
      <Image
        style={{width: 90, height: 90, borderRadius: 8}}
        source={props.image}
      />
      <View style={{marginLeft: 12}}>
        <Text style={{fontSize: 20, fontFamily: FONTS.FAMILY_BOLD, marginTop:5}}>
          {props.name}
        </Text>

        <View style={{flexDirection: 'row', alignItems:'center', marginTop:10}}>
          <RnVectorIcon
            iconFamily="Ionicons"
            name="calendar-outline"
            size={17}
          />

          <Text
            style={{
              fontSize: 13,
              marginLeft: 5,
              color: COLOR.PRIMARYGREY,
              fontFamily: FONTS.FAMILY_SEMIBOLD,
            }}
          >
            {props.duration}
          </Text>
        </View>
        
        <View style={{marginTop: 10, flexDirection: 'row', alignItems:'center'}}>
          <RnVectorIcon
              iconFamily="Ionicons"
              name="time-outline"
              size={18}
          />
          <Text
            style={{
              marginLeft: 5,
              fontSize: 12,
              color: COLOR.PRIMARYGREY,
              fontFamily: FONTS.FAMILY_SEMIBOLD,
            }}
          >
            {props.createdAt}
          </Text>
        </View>
      </View>

    </TouchableOpacity>
  );
}
