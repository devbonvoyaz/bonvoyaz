import React from 'react';
import { Image, Text, View, TouchableOpacity } from 'react-native';
import Styles from '../../res/styles/Styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { useNavigation } from '@react-navigation/core';
import Icon from "react-native-vector-icons/FontAwesome5";
import COLOR from '../../res/styles/Color';

export default function Toolbar(props) {
  const navigation = useNavigation();
  return (
    <View
      style={{
        marginTop: 8,
        alignSelf: 'center',
        width: wp(90),
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 40,
        flexDirection: 'row'
      }}
    >
      <TouchableOpacity
        style={{
          alignSelf: 'center',
          justifyContent: 'center',
          width: 40,
          height: 40,
          borderRadius: 20,

        }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Icon name="chevron-left" style={{ alignSelf: 'center' }} size={18} color={props.color ? props.color : COLOR.BLACK} />
      </TouchableOpacity>

      <Text
        style={[
          Styles.screen_heading,
          { alignSelf: 'center', color: props.color ? props.color : COLOR.BLACK },
        ]}
      >
        {props.title}
      </Text>
      <View
        style={{
          alignSelf: 'center',
          width: 25,
          height: 25
        }}
      />

    </View>
  );
}
