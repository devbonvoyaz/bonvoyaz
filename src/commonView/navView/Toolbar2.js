import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import Styles from '../../res/styles/Styles';
import { useNavigation } from '@react-navigation/core';
import Icon from "react-native-vector-icons/FontAwesome";

export default function Toolbar2(props) {
  const navigation = useNavigation();
  return (
    <View
      style={{
        flexDirection: 'row',
        padding: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
    >
      <TouchableOpacity
        style={{
          width: 30,
          height: 30,
          alignSelf: 'center',
            justifyContent:'center'
        }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Icon style={{alignSelf:'center'}} name="chevron-left" size={18} color={props.color?props.color:COLOR.BLACK}/>
      </TouchableOpacity>

      <Text style={[Styles.screen_heading,{color:props.color?props.color:COLOR.BLACK}]}>
        {props.title}
      </Text>
        {props.imageOption?
            <TouchableOpacity
                style={{
                    width: 30,
                    height: 30,
                    alignSelf: 'center',
                    justifyContent:'center'
                }}
                onPress={props.optionPress}
                //navigation.navigate('ViewTripScreen', { plan: 1 });

            >
                <Icon style={{alignSelf:'center'}} name="pencil" color={COLOR.WHITE} size={18}/>
            </TouchableOpacity>
            :<TouchableOpacity
                onPress={props.optionPress}
                //navigation.navigate('ViewTripScreen', { plan: 1 });

            >
                <Text
                    style={{
                        color: props.color?props.color:COLOR.PRIMARYBLUE,
                        fontFamily: FONTS.FAMILY_SEMIBOLD,
                        fontSize:16,
                    }}
                >
                    {props.option ? props.option : 'Save'}
                </Text>
            </TouchableOpacity>
        }

    </View>
  );
}
