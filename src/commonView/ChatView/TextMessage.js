import React from 'react';
import { Image, Text, View } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLOR from "../../res/styles/Color";
import FONTS from "../../res/styles/Fonts";
import moment from "moment";
import { TouchableWithoutFeedback } from 'react-native';

export default function (props) {
    const createdAt = moment(props.createdAt).format('hh:mm A');
    return (
        <View
            style={{ width: wp(75), alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start', }}
        >
            <TouchableWithoutFeedback
                onLongPress={props.onLongPress}
            >

                <View
                    style={{
                        flexDirection: 'row',
                        alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start',
                        borderColor: props.color ? props.color : COLOR.GREY,
                        borderBottomLeftRadius: 25,
                        borderBottomRightRadius: 25,
                        borderTopLeftRadius: props.user == props.userId ? 25 : 0,
                        borderTopRightRadius: props.user == props.userId ? 0 : 25,
                        borderWidth: 1.5,
                        padding: 12
                    }}

                >

                    <View>
                        <Text style={{ fontFamily: FONTS.FAMILY_REGULAR }}>{props.message}</Text>
                        <Text
                            style={{
                                fontSize: props.user == props.userId ? 8 : 10,
                                color: COLOR.TEXTCOLORSECONDARY,
                                alignSelf: 'flex-end',
                                marginLeft: 7,
                                fontFamily: FONTS.FAMILY_REGULAR,
                                opacity: 0.5,
                            }}
                        >
                            {createdAt}
                        </Text>
                        <Seen user={props.user} />
                    </View>


                </View>
            </TouchableWithoutFeedback>
        </View>

    )
}
const Seen = props => {
    if (props.user == 1) {
        return (
            <Image
                source={require('../../res/assets/Shape.png')}
                style={{
                    width: 11,
                    height: 11,
                    alignSelf: 'flex-end',
                    marginLeft: 5,
                }}
            />
        );
    } else {
        return null;
    }
};