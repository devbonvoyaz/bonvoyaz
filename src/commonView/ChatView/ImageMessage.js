import React, { useState } from 'react';
import { ImageBackground, Text, TouchableWithoutFeedback, View } from "react-native";
import FastImage from "react-native-fast-image";
import { U_BASE } from "../../api/Constants";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLOR from "../../res/styles/Color";
import moment from "moment";
import { ProgressBar } from "react-native-paper";
import FONTS from "../../res/styles/Fonts";

export default function ImageMessage(props) {
    const createdAt = moment(props.createdAt).format('hh:mm A');
    const [loading, setLoading] = useState(false);
    const [image, setImage] = useState(props.image);
    return (
        <TouchableWithoutFeedback
            onLongPress={props.onLongPress}
            style={{ width: wp(75), alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start', }}>
            <View>
                <Text
                    style={{
                        fontSize: 10,
                        color: COLOR.TEXTCOLORSECONDARY,
                        alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start',
                        marginLeft: 7,
                        fontFamily: FONTS.FAMILY_REGULAR,
                        opacity: 0.5,
                    }}
                >
                    {createdAt}
                </Text>
                <ImageBackground
                    source={props.imagePath ? { uri: props.imagePath } : null}
                    imageStyle={{
                        borderBottomLeftRadius: 25,
                        borderBottomRightRadius: 25,
                        borderTopLeftRadius: props.user == props.userId ? 25 : 0,
                        borderTopRightRadius: props.user == props.userId ? 0 : 25,
                    }}
                    resizeMode={'contain'}
                    style={{
                        flexDirection: 'row',
                        alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start',
                        borderColor: props.color ? props.color : COLOR.GREY,
                        borderBottomLeftRadius: 25,
                        borderBottomRightRadius: 25,
                        borderTopLeftRadius: props.user == props.userId ? 25 : 0,
                        borderTopRightRadius: props.user == props.userId ? 0 : 25,
                        borderWidth: 1.5,
                        padding: props.image ? 0 : 12,
                        maxHeight: props.image ? 250 : null,
                        maxWidth: props.image ? 250 : null,
                        minHeight: props.image ? 160 : null,
                        minWidth: props.image ? 240 : null,
                    }}

                >
                    {
                        props.messageId ?
                            <View>
                                <FastImage
                                    style={{
                                        maxHeight: 250,
                                        maxWidth: 250,
                                        height: 160,
                                        width: 240,
                                        borderBottomLeftRadius: 25,
                                        borderBottomRightRadius: 25,
                                        borderTopLeftRadius: props.user == props.userId ? 25 : 0,
                                        borderTopRightRadius: props.user == props.userId ? 0 : 25,
                                        backgroundColor: 'transparent',

                                    }}
                                    onLoadStart={() => setLoading(true)}
                                    onLoadEnd={() => setLoading(false)}
                                    onError={e => {
                                        setLoading(true)
                                        setImage("");
                                        setTimeout(() => {
                                            setImage(image);
                                        }, 1000);
                                    }}
                                    resizeMode={'contain'}
                                    source={{
                                        uri: image,
                                        priority: FastImage.priority.high,
                                    }} />

                                {loading ? <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }}>
                                    <View style={{ width: 150, height: 6, alignSelf: 'center' }}>
                                        <ProgressBar width={150} height={6} indeterminate={true} />
                                    </View>
                                </View> : null}
                            </View>
                            :
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <View style={{ width: 150, height: 6, alignSelf: 'center' }}>
                                    <ProgressBar width={150} height={6} indeterminate={true} />
                                </View>
                            </View>

                    }

                </ImageBackground>
            </View>
        </TouchableWithoutFeedback>

    )
}