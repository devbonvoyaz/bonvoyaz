import moment from 'moment';
import React, { useState } from 'react';
import { View } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import ImageMessage from "./ImageMessage";
import TextMessage from "./TextMessage";
import Video from "react-native-video";
import VideoMessage from "./VideoMessage";
import LocationMessage from "./LocationMessage";

export default function Message(props) {
  const createdAt = moment(props.createdAt).format('hh:mm A');
  const [image, setImage] = useState(props.image);
  const [video, setVideo] = useState(props.video);
  const [loading, setLoading] = useState(false);

  return (

    <View

      style={{ width: wp(95), marginTop: props.marginTop, alignSelf: 'center' }}>
      {image ?
        <ImageMessage
          onLongPress={props.onLongPress}
          createdAt={props.createdAt}
          messageId={props.messageId}
          user={props.user}
          userId={props.userId}
          imagePath={props.imagePath}
          image={props.image}
          color={props.color}
        /> : video ?
          <VideoMessage
            onLongPress={props.onLongPress}
            createdAt={props.createdAt}
            messageId={props.messageId}
            user={props.user}
            userId={props.userId}
            video={props.video}
            color={props.color} /> : props.messageType == 4 ?
            <LocationMessage
              onLongPress={props.onLongPress}
              color={props.color}
              createdAt={props.createdAt}
              messageId={props.messageId}
              user={props.user}
              userId={props.userId}
              message={props.message}
            />


            :
            <TextMessage
              onLongPress={props.onLongPress}
              createdAt={props.createdAt}
              messageId={props.messageId}
              user={props.user}
              userId={props.userId}
              message={props.message}
              color={props.color}
            />

      }
    </View>
  );
}


