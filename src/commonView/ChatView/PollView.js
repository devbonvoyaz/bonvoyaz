import React from 'react';
import { FlatList, Image, Text, View, TouchableOpacity, TouchableWithoutFeedback } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLOR from "../../res/styles/Color";
import FONTS from "../../res/styles/Fonts";
import moment from "moment";
import { connect } from "react-redux";
import ChatAction from "../../redux/actions/ChatAction";


class PollView extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        const createdAt = moment(this.props.createdAt).format('hh:mm A');
        return (

            <TouchableWithoutFeedback
                onLongPress={this.props.onLongPress}
                style={{
                    width: wp(75),
                    alignSelf: this.props.user == this.props.userId ? 'flex-end' : 'flex-start',
                }
                }>

                <View
                    style={{
                        flexDirection: 'row',
                        alignSelf: this.props.user == this.props.userId ? 'flex-end' : 'flex-start',
                        borderColor: this.props.color ? this.props.color : COLOR.GREY,
                        borderBottomLeftRadius: 25,
                        borderBottomRightRadius: 25,
                        borderTopLeftRadius: this.props.user == this.props.userId ? 25 : 0,
                        borderTopRightRadius: this.props.user == this.props.userId ? 0 : 25,
                        borderWidth: 1.5,
                        padding: 12,

                    }}
                >
                    <View style={{ width: 240, }}>
                        <Text style={{
                            fontFamily: FONTS.FAMILY_REGULAR,
                            alignSelf: 'center',
                            width: 240,
                            textAlign: 'center',
                            marginVertical: 10
                        }}>{this.props.message}</Text>
                        <FlatList
                            data={this.props.optons}
                            renderItem={({ item }) => (
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        width: '100%',
                                        marginVertical: 5
                                    }}>
                                    <Text style={{
                                        fontFamily: FONTS.FAMILY_REGULAR,
                                        alignSelf: 'center'
                                    }}>{item.option}</Text>
                                    <TouchableOpacity
                                        onPress={() => this.props.voteInPoll(item.id)}
                                        style={{ width: 80, height: 30, borderRadius: 5, alignSelf: 'center', justifyContent: 'center', backgroundColor: COLOR.PRIMARYBLUE }}
                                    >
                                        <Text style={{
                                            fontFamily: FONTS.FAMILY_REGULAR,
                                            alignSelf: 'center',
                                            color: COLOR.WHITE
                                        }}>{'Vote ' + item.votes}</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                            style={{
                                alignSelf: 'center',
                                width: '100%'
                            }}
                        />
                        <Text
                            style={{
                                fontSize: this.props.user == this.props.userId ? 8 : 10,
                                color: COLOR.TEXTCOLORSECONDARY,
                                alignSelf: 'flex-end',
                                marginLeft: 7,
                                fontFamily: FONTS.FAMILY_REGULAR,
                                opacity: 0.5,
                            }}
                        >
                            {createdAt}
                        </Text>
                        <Seen user={this.props.user} />
                    </View>


                </View>
            </TouchableWithoutFeedback>

        )
    }
}
const Seen = props => {
    if (props.user == 1) {
        return (
            <Image
                source={require('../../res/assets/Shape.png')}
                style={{
                    width: 11,
                    height: 11,
                    alignSelf: 'flex-end',
                    marginLeft: 5,
                }}
            />
        );
    } else {
        return null;
    }
};

const mapDispatchToProps = dispatch => {
    return {
        voteInPoll: (val) => {
            dispatch(ChatAction.voteInPoll(val))
        }
    }
}

export default connect(null, mapDispatchToProps)(PollView);