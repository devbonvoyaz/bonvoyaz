import React, { useState } from 'react';
import { ImageBackground, Text, View, TouchableOpacity, Platform, Linking } from "react-native";
import FastImage from "react-native-fast-image";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLOR from "../../res/styles/Color";
import moment from "moment";
import FONTS from "../../res/styles/Fonts";

export default function LocationMessage(props) {
    const createdAt = moment(props.createdAt).format('hh:mm A');


    const openMap = () => {
        const messageObject = JSON.parse(
            props.message
        );
        const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
        const latLng = `${messageObject.lat},${messageObject.lng}`;
        const label = 'Shared Location';
        const url = Platform.select({
            ios: `${scheme}${label}@${latLng}`,
            android: `${scheme}${latLng}(${label})`
        });

        console.log(messageObject);
        Linking.openURL(url);
    }

    return (
        <View style={{ width: wp(75), alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start', }}>
            <Text
                style={{
                    fontSize: 10,
                    color: COLOR.TEXTCOLORSECONDARY,
                    alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start',
                    marginLeft: 7,
                    fontFamily: FONTS.FAMILY_REGULAR,
                    opacity: 0.5,
                }}
            >
                {createdAt}
            </Text>
            <TouchableOpacity
                onPress={() => openMap()}
                onLongPress={props.onLongPress}
                style={{
                    flexDirection: 'row',
                    alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start',
                    borderColor: props.color ? props.color : COLOR.GREY,
                    borderBottomLeftRadius: 25,
                    borderBottomRightRadius: 25,
                    borderTopLeftRadius: props.user == props.userId ? 25 : 0,
                    borderTopRightRadius: props.user == props.userId ? 0 : 25,
                    borderWidth: 1.5,
                    padding: 0,
                    height: 120,
                    width: 120,
                }}
            >
                <ImageBackground
                    source={require('../../res/assets/location_placeholder.png')}
                    imageStyle={{
                        borderBottomLeftRadius: 25,
                        borderBottomRightRadius: 25,
                        borderTopLeftRadius: props.user == props.userId ? 25 : 0,
                        borderTopRightRadius: props.user == props.userId ? 0 : 25,
                    }}
                    resizeMode={'stretch'}
                    style={{
                        width: '100%',
                        height: '100%'
                    }}

                >


                </ImageBackground>
            </TouchableOpacity>
        </View>



    )
}