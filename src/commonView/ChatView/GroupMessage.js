import moment from 'moment';
import React, { useState } from 'react';
import { ImageBackground, View, Text, Image, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import ImageMessage from "./ImageMessage";
import VideoMessage from "./VideoMessage";
import TextMessage from "./TextMessage";
import LocationMessage from "./LocationMessage";
import PollView from "./PollView";

export default function GroupMessage(props) {
  const createdAt = moment(props.createdAt).format('hh:mm A');
  const [image, setImage] = useState(props.image);
  const [video, setVideo] = useState(props.video);
  return (
    <View style={{ width: wp(95), marginTop: props.sameUser ? 5 : 10, alignSelf: 'center' }}>
      {props.user != props.userId && props.sameUser == false ?
        <Text
          style={[styles.username, { color: props.color }]}
        >
          {props.username}
        </Text> : null}
      {image ?
        <ImageMessage
          onLongPress={props.onLongPress}
          createdAt={props.createdAt}
          messageId={props.messageId}
          user={props.user}
          userId={props.userId}
          imagePath={props.imagePath}
          image={props.image}
          color={props.color}
          username={props.username}
          sameUser={props.sameUser}
        /> : video ?
          <VideoMessage
            onLongPress={props.onLongPress}
            createdAt={props.createdAt}
            messageId={props.messageId}
            user={props.user}
            userId={props.userId}
            video={props.video}
            color={props.color}
            username={props.username}
            sameUser={props.sameUser}
          /> : props.messageType == 4 ?
            <LocationMessage
              onLongPress={props.onLongPress}
              color={props.color}
              createdAt={props.createdAt}
              messageId={props.messageId}
              user={props.user}
              userId={props.userId}
              message={props.message}
            /> : props.messageType == 5 ?
              <PollView
                onLongPress={props.onLongPress}
                createdAt={props.createdAt}
                messageId={props.messageId}
                user={props.user}
                optons={props.options}
                userId={props.userId}
                message={props.message}
                color={props.color}
                username={props.username}
                sameUser={props.sameUser}
              />
              :
              <TextMessage
                onLongPress={props.onLongPress}
                createdAt={props.createdAt}
                messageId={props.messageId}
                user={props.user}
                userId={props.userId}
                message={props.message}
                color={props.color}
                username={props.username}
                sameUser={props.sameUser}
              />

      }
    </View>
  );
}

const Seen = (props) => {
  if (props.user == 1) {
    return (
      <Image
        source={require('../../res/assets/Shape.png')}
        style={styles.seenImage}
      />
    );
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  seenImage: {
    width: 12,
    height: 12,
    alignSelf: 'flex-end',
    marginLeft: 5,
  },
  time: {

    color: COLOR.TEXTCOLORSECONDARY,
    alignSelf: 'flex-end',
    marginLeft: 7,
    fontFamily: FONTS.FAMILY_REGULAR,
    opacity: 0.5,
  },
  username: {
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 10,
    color: COLOR.PRIMARYBLUE,
    marginLeft: 5,
  }
})
