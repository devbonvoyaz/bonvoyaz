import React, { useState } from 'react';
import { ImageBackground, Text, TouchableWithoutFeedback, View } from "react-native";
import FastImage from "react-native-fast-image";
import { U_BASE } from "../../api/Constants";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLOR from "../../res/styles/Color";
import moment from "moment";
import { ProgressBar } from "react-native-paper";
import Video from "react-native-video";
import { error } from "react-native-gifted-chat/lib/utils";
import FONTS from "../../res/styles/Fonts";

export default function VideoMessage(props) {
    const createdAt = moment(props.createdAt).format('hh:mm A');
    const [loading, setLoading] = useState(false);
    const [video, setVideo] = useState(props.video);
    return (
        <TouchableWithoutFeedback
            onLongPress={props.onLongPress}
            style={{ width: wp(75), alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start', }}>
            <View>
                <Text
                    style={{
                        fontSize: 10,
                        color: COLOR.TEXTCOLORSECONDARY,
                        alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start',
                        marginLeft: 7,
                        fontFamily: FONTS.FAMILY_REGULAR,
                        opacity: 0.5,
                    }}
                >
                    {createdAt}
                </Text>
                <View
                    resizeMode={'contain'}
                    style={{
                        flexDirection: 'row',
                        alignSelf: props.user == props.userId ? 'flex-end' : 'flex-start',
                        borderColor: props.color ? props.color : COLOR.GREY,
                        borderBottomLeftRadius: 25,
                        borderBottomRightRadius: 25,
                        borderTopLeftRadius: props.user == props.userId ? 25 : 0,
                        borderTopRightRadius: props.user == props.userId ? 0 : 25,
                        borderWidth: 1.5,
                        padding: props.video ? 0 : 12,
                        maxHeight: 250,
                        maxWidth: 250,
                        minHeight: 160,
                        minWidth: 240,
                    }}

                >
                    <Video
                        controls={true}
                        onError={(error) => {
                            console.log(error);
                            setVideo('');
                            setTimeout(() => {
                                setVideo(props.video);
                            }, 500);
                        }}

                        style={{
                            maxHeight: 250,
                            maxWidth: 250,
                            minHeight: 160,
                            minWidth: 240,
                            borderBottomLeftRadius: 25,
                            borderBottomRightRadius: 25,
                            borderTopLeftRadius: props.user == props.userId ? 25 : 0,
                            borderTopRightRadius: props.user == props.userId ? 0 : 25,
                        }}
                        source={{ uri: video }}
                    />

                </View>
            </View>
        </TouchableWithoutFeedback>



    )
}