import { useNavigation } from '@react-navigation/core';
import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    ScrollView,
    TextInput,
    SafeAreaView,
    Alert,
    ImageBackground,
    Platform
} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { TouchableOpacity } from 'react-native-gesture-handler';
import COLOR from '../../res/styles/Color';
import { CustomButton } from '../../commonView/globalComponent/CustomButton';
import FONTS from '../../res/styles/Fonts';
import { connect } from 'react-redux';
import AuthenticationAction from '../../redux/actions/AuthenticationAction';
import { AS_FCM_TOKEN, AS_USER_DETAIL, AS_USER_TOKEN } from '../../api/Constants';
import API from '../../api/Api';
import Auth from '../../auth';
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/Ionicons';
import Indicator from '../../commonView/globalComponent/ActivityIndicator';

const api = new API();
const auth = new Auth();
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            secure: true,
            isLoading: false,
        }
    }

    validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(text) === false) {
            console.log("Email is Not Correct");

            return false;
        }
        else {
            return true;
            console.log("Email is Correct");
        }
    }

    callLoginApi = async () => {

        if (this.state.username == '' || this.state.username == null || this.validate(this.state.username) == false) {
            Toast.show('Invalid username');
        } else if (this.state.password == '' || this.state.password == null) {
            Toast.show('Please enter password')
        } else {
            const fcm = await auth.getValue(AS_FCM_TOKEN);
            const data = JSON.stringify({
                username: this.state.username,
                password: this.state.password,
                platform: Platform.OS == 'ios' ? 2 : 1,
                token: fcm,
            });
            this.setState({ isLoading: true });
            api
                .loginApi(data)
                .then((json) => {
                    console.log(json)
                    if (json.status == 200) {
                        Toast.show('Logged in Successfully');
                        auth.setValue(AS_USER_TOKEN, json.data.response.token);
                        auth.setValue(AS_USER_DETAIL, json.data.response);
                        this.setState({ isLoading: false });
                        console.log('user data:-', json.data.response.token);
                        if (json.data.response.isVerified) {
                            this.props.navigation.replace('MyTab');
                            this.setUserOnline();
                        } else {
                            this.props.navigation.replace('MobileVerification',
                                { phone: json.data.response.phone });
                        }


                    } else if (json.status == 400) {
                        Toast.show('Wrong cred');
                        this.setState({ isLoading: false });
                    } else {
                        console.log(json);
                        this.setState({ isLoading: false });
                    }
                })
                .catch((error) => {
                    this.setState({ isLoading: false });
                    setTimeout(() => {
                        console.log('json.data.message', error.response.data.message)
                        Toast.show(error.response.data.message);
                    }, 500);
                });
        }
    };

    setUserOnline = () => {
        try {
            api.setUserOnline().then((json) => {
                if (json.status == 200) {
                    console.log(json.data.response);


                } else {
                    setTimeout(() => {
                        Toast.show(json.data.response);
                        console.log(json.data.response)
                    }, 0);

                }
            }).catch(function (error) {
                Toast.show(error.response);
                console.log(error);
            });
        } catch (error) {
            console.log(error)
        }
    }

    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
                <Indicator loading={this.state.isLoading} />
                <Text
                    style={{
                        fontFamily: FONTS.FAMILY_BOLD,
                        fontSize: 24,
                        marginTop: 75,
                        marginLeft: 30,
                    }}
                >
                    Sign in
                </Text>
                <View style={[styles.text_input, { flexDirection: 'row' }]}>
                    <TextInput
                        style={{ width: wp(100), fontSize:17 }}
                        placeholderTextColor= {COLOR.PRIMARYGREY}
                        textContentType='username'
                        placeholder={'Username'}
                        autoCapitalize='none'
                        value={this.state.username}
                        onChangeText={text => this.setState({ username: text })}
                    />
                </View>
                <View style={[styles.text_input, { flexDirection: 'row' }]}>
                    <TextInput
                        style={{ width: wp(70), fontSize:17 }}
                        placeholderTextColor={COLOR.PRIMARYGREY}
                        textContentType='password'
                        placeholder={'Password'}
                        secureTextEntry={this.state.secure}
                        value={this.state.password}
                        onChangeText={text => this.setState({ password: text })}
                    />
                    <TouchableOpacity
                        onPress={() => this.setState({ secure: !this.state.secure })}
                    >
                        <Icon size={25} name={this.state.secure ? "eye-off-outline" : "eye-outline"} />
                    </TouchableOpacity>
                </View>

                <View style={{ marginTop: 30 }}>
                    <CustomButton
                        width={wp(85)}
                        text={'Login'}
                        bg={COLOR.PRIMARYBLUE}
                        labelColor={COLOR.WHITE}
                        borderRadius={10}
                        onPress={this.callLoginApi}
                    />
                </View>
                <TouchableOpacity>
                    <Text
                        style={{
                            textAlign: 'right',
                            color: COLOR.PRIMARYBLUE,
                            paddingRight: 30,
                            fontSize: 16,
                            marginTop: 5,
                        }}
                    >
                        Forgot Password?
                    </Text>
                </TouchableOpacity>

                <Text style={{ color: "#828282", marginTop: 25, alignSelf: 'center'}}>
                    Or Sign In with
                </Text>

                <View style={{flexDirection: 'row', paddingHorizontal:10, marginTop:5 }}>
               
                    <TouchableOpacity style={[styles.social_icon, { flexDirection: 'row', marginRight:20 }]}>                  
                            <Image
                                style={{ width: 35, height: 35, alignSelf: 'center' }}
                                resizeMode={'contain'}
                                source={require('../../res/assets/Facebook.png')}
                            />
                            <Text style={{ alignSelf: 'center', marginHorizontal: 10, fontSize:14, }}>Facebook</Text>                 
                    </TouchableOpacity>
                    <View style={[styles.social_icon, { flexDirection: 'row', marginRight:10 }]}>
                        <TouchableOpacity style={{ flexDirection: 'row' }}>
                            <Image
                                style={{ width: 27, height: 27, alignSelf: 'center' }}
                                resizeMode={'contain'}
                                source={require('../../res/assets/Google.png')}
                            />
                            <Text style={{ alignSelf: 'center', marginHorizontal: 10, fontSize:14 }}>Google</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.social_icon, { flexDirection: 'row'  }]}>
                        <TouchableOpacity style={{ flexDirection: 'row' }}>
                            <Icon size={32} name={'logo-apple'} />
                            <Text style={{ alignSelf: 'center', marginHorizontal: 10, fontSize:14 }}>Apple</Text>
                        </TouchableOpacity>
                    </View>

                </View> 
                <Text style={{ marginTop: 15, marginBottom:41, alignSelf: 'center', color: COLOR.TEXTCOLOR }} >
                    Not a registerd user? 
                    <Text
                        onPress={() => {this.props.navigation.navigate('SignUp');}}
                        style={{ color: COLOR.PRIMARYBLUE }}
                    > <Text>{" "}</Text>
                        Sign Up
                    </Text>
                </Text>

                <ImageBackground 
                    style={{ width: '100%', height: 320}}
                    source={require('../../res/assets/login_bg.png')}>
                </ImageBackground>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    text_input: {
        height: 50,
        marginTop: 15,
        backgroundColor: COLOR.WHITE,
        borderWidth: 1,
        borderColor: COLOR.PRIMARYGREY,
        width: wp(85),
        borderRadius: 10,
        alignItems: 'center',
        alignSelf: 'center',
        paddingStart: 10,
    },
    button_content: {
        height: 50,
        width: wp(90),
        borderRadius: 7,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    social_icon: {
        height: 50,
        backgroundColor: COLOR.WHITE,
        borderRadius: 10,
        alignItems: 'center',
        alignSelf: 'center',
        paddingStart: 10,
    }
});

export default Login;
