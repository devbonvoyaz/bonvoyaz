import {useNavigation} from '@react-navigation/core';
import React, {useState} from 'react';
import {Text, View, StyleSheet, Image, SafeAreaView,TouchableOpacity,Platform} from 'react-native';
import {State} from 'react-native-gesture-handler';
import {Pages} from 'react-native-pages';
import {CustomButton} from '../../commonView/globalComponent/CustomButton';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Dots from 'react-native-dots-pagination';

export default function SplashSliderScreen () {
  const [color, setColor] = useState (COLOR.ORANGE);
  const [buttonText, setButtonText] = useState ('Next');
  const [currentPage, setCurrentPage] = useState (0);
  const [active, setActive] = useState (0);
  const navigation = useNavigation ();
  const pageRef = React.createRef ();

  return (
    <View style={{flex: 1, backgroundColor: COLOR.WHITE}}>

      <View style={{flex:1}}>
        {currentPage==3?null:
            <TouchableOpacity
        style={{
          position: 'absolute',
          alignSelf: 'flex-end',
          marginTop:Platform.OS === 'ios' ? 30 : 10,
          
        }}
        onPress={() => {
          navigation.navigate('LoginScreen');
        }}
        >
        <Text
          style={{
            padding: 15,
            fontSize: 15,
            fontFamily: FONTS.FAMILY_SEMIBOLD,
            color: COLOR.TEXTCOLOR,
          }}
        >
          Skip
        </Text>
        </TouchableOpacity>
}
     
      <View accessible={false} pointerEvents="none" style={{height: hp (75)}}>
        
        <Pages
          ref={pageRef}
          onScrollStart={index => {
            changeColor (index);
            setActive (index);
          }}
          onScrollEnd={index => {
            changeColor (index);
            setActive (index);
          }}
          indicatorColor={COLOR.WHITE}
          indicatorOpacity={0}
          indicatorPosition={'bottom'}
        >
          <Splash
            backgroundColor={COLOR.ORANGE}
            title={'Book your destination'}
            src={require ('../../res/assets/image1.png')}
            width={'100%'}
          />
          <Splash
            backgroundColor={COLOR.PRIMARYBLUE}
            title={'Plan your trip'}
            src={require ('../../res/assets/image2.png')}
            width={'90%'}
          />
          <Splash
            backgroundColor={COLOR.PRIMARTGREEN}
            title={'Capture every moment'}
            src={require ('../../res/assets/image3.png')}
            width={'90%'}
          />
          <Splash
            backgroundColor={COLOR.CHERYCOLOR}
            title={'Communicate your ideas'}
            src={require ('../../res/assets/image4.png')}
            width={'90%'}
          />
        </Pages>
        <View
          style={{justifyContent: 'flex-start', width: 100, marginLeft: 25,marginVertical:10,flexDirection:'row'}}
        >
          <View style={{width:currentPage ==0 ? 20:5 ,height:5,backgroundColor:COLOR.ORANGE,borderRadius:5,marginRight:8}} />
          <View style={{width:currentPage ==1 ? 20:5 ,height:5,backgroundColor:COLOR.PRIMARYBLUE,borderRadius:5,marginRight:8}} />

          <View style={{width:currentPage ==2 ? 20:5 ,height:5,backgroundColor:COLOR.PRIMARTGREEN,borderRadius:5,marginRight:8}} />
          <View style={{width:currentPage ==3 ? 20:5 ,height:5,backgroundColor:COLOR.CHERYCOLOR,borderRadius:5,marginRight:8}} />

        </View>
      </View>
      <View style={{flex: 1, marginBottom:15,justifyContent:'center'}}>

        <CustomButton
          bg={color}
          labelColor={COLOR.WHITE}
          text={buttonText}
          width={wp (87.5)}
          onPress={() => {
            if (currentPage == 3) {
              navigation.navigate ('LoginScreen');
            } else {
              pageRef.current.scrollToPage (currentPage + 1);
              setCurrentPage (currentPage + 1);
            }
          }}
        />
      </View>
      </View>
    </View>
  );

  function changeColor (index) {
    switch (index) {
      case 0: {
        setColor (COLOR.ORANGE);
        setButtonText ('Next');
        setCurrentPage (0);
        break;
      }
      case 1: {
        setColor (COLOR.PRIMARYBLUE);
        setButtonText ('Next');
        setCurrentPage (1);
        break;
      }
      case 2: {
        setColor (COLOR.PRIMARTGREEN);
        setButtonText ('Next');
        setCurrentPage (2);
        break;
      }
      case 3: {
        setColor (COLOR.CHERYCOLOR);
        setButtonText ('Get Started');
        setCurrentPage (3);
        break;
      }
    }
  }
}

const Splash = props => {
  return (
    <View style={{flex: 1}}>
      <View
        style={[
          styles.splashbackground,
          {backgroundColor: props.backgroundColor},
        ]}
      />
      <Image
        source={props.src}
        style={{
          alignSelf: 'center',
          width: props.width,
          height: '60%',
          marginTop: '-70%',
          marginBottom: 30,
        }}
        resizeMode={'stretch'}
      />

      <View>
        <Text style={styles.heading}>{props.title}</Text>
        <Text
          style={{
            fontSize: 12,
            marginHorizontal: 25,
            marginTop: 10,
            fontFamily: FONTS.FAMILY_REGULAR,
          }}
        >
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
        </Text>
      </View>

    </View>
  );
};

const styles = StyleSheet.create ({
  heading: {
    fontSize: 24,
    color: '#3A3A3A',
    marginLeft: 25,
    fontFamily: FONTS.FAMILY_BOLD,
  },
  splashbackground: {
    backgroundColor: COLOR.PRIMARYBLUE,
    opacity: 0.4,
    width: '100%',
    height: '65%',
  },
});
