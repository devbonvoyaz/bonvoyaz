import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TextInput, 
  TouchableOpacity,
  Image
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {COUNTRYCODE} from '../../res/appData';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import Styles from '../../res/styles/Styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { connect } from 'react-redux';
import AuthenticationAction from '../../redux/actions/AuthenticationAction';
import SimpleToast from 'react-native-simple-toast';
import API from '../../api/Api';
import Auth from '../../auth';
import { AS_USER_DETAIL } from '../../api/Constants';
import Indicator from '../../commonView/globalComponent/ActivityIndicator';
import CountryPicker from "react-native-region-country-picker";


const api = new API();
const auth = new Auth();
class MobileVerification extends React.Component {
  constructor(props){
    super(props)
    this.state ={
      screen :0,
      phone:'',
      country_id:'+91',
      num1:'',
      num2:'',
      num3:'',
      num4:'',
      isLoading:false,
      pickerVisible:false

    }
    this.firstInput = React.createRef();
    this.secondInput = React.createRef();
    this.thirdInput = React.createRef();
    this.fourthInput = React.createRef();
    this.countryCodePicker = React.createRef();
  }

  componentWillReceiveProps(props){
    if(props.otpResponse){
      var otp = (props.otpResponse.otp);
      if(otp != null ||otp!='undifined'){
        SimpleToast.show(otp.toString());
      

      }
       
    }
    if(props.verifyOtpResponse){
      if(props.verifyOtpResponse.status ==200){
         this.setState({
           screen:2,
           isLoading:false,
        });
        setTimeout(()=>{
          SimpleToast.show(props.verifyOtpResponse.message);

        },500)
      }else{
        SimpleToast.show(props.verifyOtpResponse.message);
        this.setState({
          isLoading:false
        })
      }
    }
  }

  setUserOnline=()=>{
    try{
    api.setUserOnline().then((json)=>{
      if (json.status == 200) {
        console.log(json.data.response);
      
  
    } else {
        setTimeout(() => {
            
            console.log(json.data.response)
        }, 0);
  
    }
    }).catch(function (error) {
      Toast.show(error.response);
      console.log(error);
  });
    }catch(error){
      console.log(error)
    }
  }

   setUserVerified = async ()=>{
     const userDetails = JSON.parse(await auth.getValue(AS_USER_DETAIL));
     userDetails.isVerified = true;
     console.log('verified ',userDetails);
     auth.setValue(AS_USER_DETAIL , userDetails);

   }

    verifyOtp = ()=>{
        let data = JSON.stringify({
          phone: this.state.phone,
          otp: parseInt(this.state.num1+this.state.num2+this.state.num3+this.state.num4) 
        });
        this.props.verifyOtp(data);
        this.setState({isLoading:true});
    }
     render(){
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLOR.WHITE}}>
       <Indicator loading={this.state.isLoading}/>
        <TouchableOpacity
            onPress={() => {
               navigation.goBack(null)
            }}
            style={{ height:45, width:45, alignItems:'center', justifyContent: 'center',marginLeft:15}}>
            <Icon name="chevron-left" size={18} color={COLOR.WHITE} />
        </TouchableOpacity>
      {this.state.screen == 0
        ? <this.GetOtp/>
        : this.state.screen == 1
            ? <this.VerifyScreen/>
            : <this.SuccessScreen />}

    </SafeAreaView>
  );
      }

       GetOtp = props => {
  
        return (
          <View style={{flex:1}}>
            <Text style={styles.heading}>
              OTP Verification
            </Text>
            <Text
              style={{
                fontFamily: FONTS.FAMILY_REGULAR,
                textAlign: 'center',
                alignSelf: 'center',
                marginHorizontal: 25,
                marginBottom: 25,
                fontSize: 16,
                color: COLOR.TEXTCOLOR,
              }}
            >
              We will send you a one-time password to this mobile number.
            </Text>
                {/*<Text*/}
                {/*  style={{*/}
                {/*    alignSelf: 'center',*/}
                {/*    fontFamily: FONTS.FAMILY_REGULAR,*/}
                {/*    color: COLOR.PRIMARYGREY,*/}
                {/*  }}*/}
                {/*>*/}
                {/*  Enter Mobile Number*/}
                {/*</Text>*/}
            <View
              style={{
                flexDirection: 'row',
                width:wp(90),
                  alignSelf:'center',
                  marginVertical:30,
                  zIndex:3996
              }}
            >
                    <View
                    style={{
                      justifyContent:'center',
                      borderRadius: 10,
                      borderWidth:1,
                      borderColor:COLOR.PRIMARYGREY,
                        width:100,
                        height: 50,
                    }}>
                        <CountryPicker
                            containerStyle={{
                                container: {alignSelf:'center'},
                            }}
                            style={{alignSelf:'center'}}
                            countryPickerRef={(ref: any) => {
                                this.countryCodePicker = ref;
                            }}
                            enable={true}
                            darkMode={false}
                            countryCode={"IND"}
                            containerConfig={{
                                showFlag: true,
                                showCallingCode: true,
                                showCountryName: false,
                                showCountryCode: false,
                            }}

                            modalConfig={{
                                showFlag: true,
                                showCallingCode: true,
                                showCountryName: true,
                                showCountryCode: false,
                            }}
                            onSelectCountry={(data: any) => {
                                this.setState({
                                    country_id:data.callingCode
                                })
                                console.log("DATA", data.callingCode);
                            }}
                            title={"Country"}
                            searchPlaceholder={"Search"}
                            showCloseButton={true}
                            showModalTitle={true}
                        />
                  </View>


      
              <TextInput
                placeholder={'Phone Number'}
                placeholderTextColor={COLOR.PRIMARYGREY}
                value={this.state.phone}
                onChangeText={text=>this.setState({phone:text})}
                style={[Styles.text_input, {flex:1    , marginLeft: 10}]}
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  screen:1,
                });
                this.props.generateOtp(this.state.phone);
              }}
            >
              <LinearGradient
                style={styles.button_content}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={['#2D9CDB', 'rgba(0,69,151,0.8)', 'rgba(6,139,215,0.8)']}
              >
                <Text style={{alignSelf: 'center', color: COLOR.WHITE}}>
                  Get OTP
                </Text>
              </LinearGradient>
            </TouchableOpacity>

      
          </View>
        );
      };

       VerifyScreen = props => {
        return (
          <View>
            <Text style={styles.heading}>
              OTP Verification
            </Text>
            <Text
              style={{
                fontFamily: FONTS.FAMILY_REGULAR,
                textAlign: 'center',
                alignSelf: 'center',
                marginHorizontal: 25,
                marginBottom: 25,
                fontSize: 16,
                color: COLOR.TEXTCOLOR,
              }}
            >
              Enter the OTP sent to
              {' '}
              <Text style={{fontFamily: FONTS.FAMILY_BOLD}}>{this.state.phone}</Text>
            </Text>
      
            <View
              style={{
                flexDirection: 'row',
                marginVertical: 25,
                alignItems: 'center',
                alignSelf:'center'
              }}
            >
      
              <TextInput
              ref={(input) => { this.firstInput = input; }}
                style={[
                  Styles.text_input,
                  {
                    width: 50,
                    height: 50,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: COLOR.PRIMARYBLUE,
                      marginRight:10
                  },
                ]}
                value={this.state.num1}
                maxLength={1}
                keyboardType='number-pad'
                onChangeText={text=>{
                  this.setState({num1:text});
                  if(text.length == 1){
                    this.secondInput.focus();
                  }
                }}
                
              />
              <TextInput
              ref={(input) => { this.secondInput = input; }}
                style={[
                  Styles.text_input,
                  {
                    width: 50,
                    height: 50,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: COLOR.PRIMARYBLUE,
                      marginRight:10
                  },
                ]}
                value={this.state.num2}
                maxLength={1}
                keyboardType='number-pad'
                onChangeText={
                  text=>{
                  this.setState({num2:text});
                  if(text.length == 1){
                    this.thirdInput.focus();
                  }else if(text.length == 0){
                    this.firstInput.focus();
                  }
                }
                }
              />
              <TextInput
              ref={(input) => { this.thirdInput = input; }}
                style={[
                  Styles.text_input,
                  {
                    width: 50,
                    height: 50,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: COLOR.PRIMARYBLUE,
                      marginRight:10
                  },
                ]}
                value={this.state.num3}
                maxLength={1}
                keyboardType='number-pad'
                onChangeText={text=>{
                  this.setState({num3:text})
                  if(text.length == 1){
                    this.fourthInput.focus();
                  }else if(text.length == 0){
                    this.secondInput.focus();
                  }
                }
                }
              />
              <TextInput
                ref={(input) => { this.fourthInput = input; }}
                style={[
                  Styles.text_input,
                  {
                    width: 50,
                    height: 50,
                    borderRadius: 5,
                    borderWidth: 1,
                    borderColor: COLOR.PRIMARYBLUE,
                  },
                ]}
                value={this.state.num4}
                maxLength={1}
                keyboardType='number-pad'
                onChangeText={text=>{
                  this.setState({num4:text});
                  if(text.length == 0){
                    this.thirdInput.focus();
                  }
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: FONTS.FAMILY_REGULAR,
                textAlign: 'center',
                alignSelf: 'center',
                marginHorizontal: 25,
                marginBottom: 25,
                fontSize: 16,
                color: COLOR.PRIMARYGREY,
              }}
            >
              Didn’t you receive the OTP?
              <Text
              onPress={()=>this.props.generateOtp(this.state.phone)}
                style={{fontFamily: FONTS.FAMILY_REGULAR, color: COLOR.PRIMARYBLUE}}
              >
                {' '}Resend OTP
              </Text>
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.verifyOtp();
              }}
            >
              <LinearGradient
                style={styles.button_content}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={['#2D9CDB', 'rgba(0,69,151,0.8)', 'rgba(6,139,215,0.8)']}
              >
                <Text style={{alignSelf: 'center', color: COLOR.WHITE}}>
                  Verify
                </Text>
              </LinearGradient>
            </TouchableOpacity>
      
          </View>
        );
      };

      SuccessScreen = props => {
        
        return (
          <View
            style={{
              flex: 1,
              backgroundColor: COLOR.WHITE,
              justifyContent: 'space-between',
            }}
          >
      
            <Image
              style={{
                width: wp (90),
                height:wp(90),
                aspectRatio: 1,
                alignSelf: 'center',
              }}
              resizeMode={'contain'}
              source={require('../../res/assets/otp-succes.png')}
            />
      
            <Text
              style={{
                fontFamily: FONTS.FAMILY_REGULAR,
                fontSize: 18,
                alignSelf: 'center',
                textAlign: 'center',
                color: COLOR.PRIMARYBLUE,
                margin: 15,
              }}
            >
              You are succesfully verified
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.setUserOnline();
                this.setUserVerified();
                this.props.navigation.replace('MyTab');
              }}
            >
              <LinearGradient
                style={styles.button_content}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={['#2D9CDB', 'rgba(0,69,151,0.8)', 'rgba(6,139,215,0.8)']}
              >
                <Text style={{alignSelf: 'center', color: COLOR.WHITE}}>
                  Next
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        );
      };
}







const styles = StyleSheet.create ({
  heading: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: 24,
    marginTop: 20,
    marginBottom: 25,
    alignSelf: 'center',
    color: COLOR.TEXTCOLOR,
  },
  button_content: {
    height: 50,
    marginTop: 10,
    marginBottom: 15,
    width: wp (90),
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  downArrow:{
    height: 10,
    width: 10,
    alignSelf:'center',
    tintColor: COLOR.VOILET,
    marginHorizontal: 5,
  },
  countryCode:{
    fontFamily:FONTS.FAMILY_REGULAR,
    fontSize:15,
    alignSelf:'center',
  }

});

const mapStateToProps = (state,ownProps)=>{
  return {
  otpResponse:state.reducer.otpResponse,
  verifyOtpResponse:state.reducer.verifyOtpResponse,
  }
}

const mapDispatchToProps = (dispatch)=>{
  return {
    generateOtp:(val)=>{
      dispatch(AuthenticationAction.generateOtp(val))
    },
    verifyOtp:(data)=>{
      dispatch(AuthenticationAction.verifyOtp(data))
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(MobileVerification);