import { useNavigation } from '@react-navigation/core';
import React, { useEffect } from 'react';
import { Text, ToastAndroid, View } from 'react-native';
import COLOR from '../../res/styles/Color';
import { LogBox } from 'react-native';
import Styles from '../../res/styles/Styles';
import Auth from '../../auth';
import { AS_USER_DETAIL, AS_USER_TOKEN } from '../../api/Constants';

const auth = new Auth();
export default function SplashScreen() {
  const navigation = useNavigation();
  LogBox.ignoreLogs(['Warning: ...']);

  useEffect(() => {
    async function checkData() {
      const userDetails = JSON.parse( await auth.getValue(AS_USER_DETAIL));
      if (userDetails == null || userDetails == '') {
        setTimeout(function () {
          navigation.navigate('SplashSliderScreen');
        }, 2000);
      } else {
        if(userDetails.isVerified){
        setTimeout(function () {
          navigation.navigate('MyTab');
        }, 2000);
      }else{
        setTimeout(function () {
          console.log(userDetails.isVerified);
          navigation.navigate('SplashSliderScreen');
          auth.remove(AS_USER_TOKEN);
          auth.remove(AS_USER_DETAIL);
        }, 2000);
      }
      }
    }
    checkData();
  });

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLOR.PRIMARYBLUE,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Text
        style={[
          Styles.heading_label,
          {
            fontSize: 32,
            color: COLOR.WHITE,
            alignSelf: 'center',
          },
        ]}
      >
        Bon Voyaz
      </Text>
    </View>
  );
}
