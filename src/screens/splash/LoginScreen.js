import { useNavigation } from '@react-navigation/core';
import React, { useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
} from 'react-native';

import { CustomButton } from '../../commonView/globalComponent/CustomButton';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import COLOR from '../../res/styles/Color';


export default function LoginScreen() {
  const navigation = useNavigation();
  return (

    <ImageBackground
      style={{ flex: 1 }}
    >


      <Text
        style={{
          fontSize: 30,
          fontWeight: 'bold',
          color: COLOR.PRIMARYBLUE,
          alignSelf: 'center',
          width: wp(90),
          textAlign: 'center',
          marginTop: 100,
        }}
      >
        Bon Voyaz
      </Text>

      <View style={{ alignSelf: 'center', alignItems: 'center', justifyContent: 'center', position: 'absolute', bottom: 0, marginBottom: 25 }}>
        <View style={{ width: wp(90) }}>

          <CustomButton
            bg={COLOR.PRIMARYBLUE}
            borderRadius={10}
            labelColor={COLOR.WHITE}
            text={'Sign In'}
            onPress={() => {
              navigation.navigate('Login');
            }}
          />
          <CustomButton
            bg={COLOR.WHITE}
            borderRadius={10}
            labelColor={COLOR.PRIMARYBLUE}
            text={'Sign Up'}
            onPress={() => {
              navigation.navigate('SignUp');
            }}
          />
        </View>

      </View>
    </ImageBackground>

  );
}
const styles = StyleSheet.create({
  button_content: {
    height: 50,
    width: wp(90),
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
