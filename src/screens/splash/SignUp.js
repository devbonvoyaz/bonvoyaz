import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  ImageBackground, ScrollView,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { CustomButton } from '../../commonView/globalComponent/CustomButton';
import { GENDERS } from '../../res/appData';
import COLOR from '../../res/styles/Color';
import { DatePickerDialog } from 'react-native-datepicker-dialog'
import moment from 'moment';
import FONTS from '../../res/styles/Fonts';
import ReactNativePickerModule from "react-native-picker-module"
import API from '../../api/Api';
import Auth from '../../auth';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { AS_FCM_TOKEN, AS_USER_DETAIL, AS_USER_TOKEN } from '../../api/Constants';
import Toast from 'react-native-simple-toast';
import Icon from "react-native-vector-icons/Ionicons";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Indicator from '../../commonView/globalComponent/ActivityIndicator';
import { Platform } from 'react-native';


const api = new API();
const auth = new Auth();
class SignUp extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      secure: true,
      isLoading: false,
    };
    //this.pickerRef.current.show()
    this.pageRef = React.createRef();
  }

  validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");

      return false;
    }
    else {
      return true;
      console.log("Email is Correct");
    }
  }

  callSignUpAPI = async () => {
    if (this.state.firstName == null || this.state.firstName == '') {
      Toast.show('Invalid First Name')
    } else if (this.state.lastName == null || this.state.lastName == '') {
      Toast.show('Invalid Last Name')
    } else if (this.state.email == '' || this.state.email == null || this.validate(this.state.email) == false) {
      Toast.show('Invalid Email')
    } else if (this.state.password == '' || this.state.password == null) {
      Toast.show('Invalid Password')
    } else {
      const fcm = await auth.getValue(AS_FCM_TOKEN);

      this.setState({
        isLoading: true
      })
      const data = JSON.stringify({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email,
        password: this.state.password,
        platform: Platform.OS == 'ios' ? 2 : 1,
        token: fcm,
      });
      api
        .signUpApi(data)
        .then((json) => {
          if (json.status == 200) {
            Toast.show('Signed up Successfully');
            auth.setValue(AS_USER_TOKEN, json.data.response.token);
            auth.setValue(AS_USER_DETAIL, json.data.response);
            console.log('user data:-', json.data.response.token);
            this.props.navigation.replace('MobileVerification');
          } else if (json.status == 400) {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);
          } else {
            setTimeout(() => {
              Toast.show(json.data.message);
            }, 0);

          }
        })
        .catch((error) => {
          this.setState({ isLoading: false })
          setTimeout(() => {
            console.log('json.data.message', error.response.data)
            //Toast.show(error);
            Toast.show(error.response.data.message);
          }, 500);


        });
    }
  };

  render() {

    return (
      <KeyboardAwareScrollView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
        <Indicator loading={this.state.isLoading} />
        <View style={{ flex: 1 }} >
          <Text
            style={styles.signUpTitle}
          >SignUp</Text>

          <View style={{flexDirection:"row", justifyContent:"space-between", marginHorizontal:20}}>
            <TextInput
              style={[styles.text_input,{width:wp(44)}]}
              placeholderTextColor={COLOR.PRIMARYGREY}
              placeholder={'First Name'}
              value={this.state.firstName}
              onChangeText={text => this.setState({ firstName: text })}
            />
            <TextInput
              style={[styles.text_input,{width:wp(44)}]}
              placeholderTextColor={COLOR.PRIMARYGREY}
              placeholder={'Last Name'}
              value={this.state.lastName}
              onChangeText={text => this.setState({ lastName: text })}
            />
          </View>

          <TextInput
            style={styles.text_input}
            placeholderTextColor={COLOR.PRIMARYGREY}
            placeholder={'Email'}
            autoCapitalize='none'
            value={this.state.email}
            onChangeText={text => this.setState({ email: text })}
          />

          <View style={[styles.text_input, { flexDirection: 'row', justifyContent:"space-between" }]}>
            <TextInput
              style={{width:wp(70)}}
              placeholderTextColor={COLOR.PRIMARYGREY}
              placeholder={'Password'}
              autoCapitalize='none'
              secureTextEntry={this.state.secure}
              value={this.state.password}
              onChangeText={text => this.setState({ password: text })}
            />
            <TouchableOpacity
              onPress={() => this.setState({ secure: !this.state.secure })}
              style={{marginRight:15}}
            >
              <Icon size={25} name={this.state.secure ? "eye-off-outline" : "eye-outline"} />
            </TouchableOpacity>
          </View>

          <View style={{ marginTop: 15 }}>
            <CustomButton
              onPress={this.callSignUpAPI}
              bg={COLOR.PRIMARYBLUE}
              labelColor={COLOR.WHITE}
              text={'Sign Up'}
              borderRadius={10}
            />
          </View>

          <Text style={{ color: COLOR.PRIMARYGREY, marginTop: 25, alignSelf: 'center'}}>
                    Or Sign Up with
          </Text>

          <View style={{flexDirection: 'row', paddingHorizontal:10, marginTop:5 }}>
               
               <TouchableOpacity style={[styles.social_icon, { flexDirection: 'row', marginRight:20 }]}>                  
                       <Image
                           style={{ width: 35, height: 35, alignSelf: 'center' }}
                           resizeMode={'contain'}
                           source={require('../../res/assets/Facebook.png')}
                       />
                       <Text style={{ alignSelf: 'center', marginHorizontal: 10, fontSize:14, }}>Facebook</Text>                 
               </TouchableOpacity>
               <View style={[styles.social_icon, { flexDirection: 'row', marginRight:10 }]}>
                   <TouchableOpacity style={{ flexDirection: 'row' }}>
                       <Image
                           style={{ width: 28, height: 30, alignSelf: 'center' }}
                           resizeMode={'contain'}
                           source={require('../../res/assets/Google.png')}
                       />
                       <Text style={{ alignSelf: 'center', marginHorizontal: 10, fontSize:14 }}>Google</Text>
                   </TouchableOpacity>
               </View>
               <View style={[styles.social_icon, { flexDirection: 'row'  }]}>
                   <TouchableOpacity style={{ flexDirection: 'row' }}>
                       <Icon size={32} name={'logo-apple'} />
                       <Text style={{ alignSelf: 'center', marginHorizontal: 10, fontSize:14 }}>Apple</Text>
                   </TouchableOpacity>
               </View>

           </View> 
          
          <Text
            style={{ marginTop: 15, alignSelf: 'center', marginBottom: 15, color: COLOR.PRIMARYGREY }}
          >
            Are you a registerd user?
            <Text
              onPress={() => { this.props.navigation.navigate('Login'); }}
              style={{ color: COLOR.PRIMARYBLUE }}
            > <Text>{" "}</Text>
              Log in
            </Text>

          </Text>
        </View>
        <ImageBackground 
                    style={{ width: '100%', height: 320}}
                    source={require('../../res/assets/login_bg.png')}
                    >
        </ImageBackground>
      
      </KeyboardAwareScrollView>

    );
  }
}

const styles = StyleSheet.create({
  text_input: {
    minHeight: 50,
    marginBottom: 10,
    backgroundColor: COLOR.WHITE,
    borderWidth: 1,
    borderColor: '#E0E0E0',
    width: wp(90),
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingStart: 15,
    fontSize: 17
  },
  signUpTitle: {
    fontFamily: FONTS.FAMILY_BOLD,
    fontSize: 24,
    marginTop: 75,
    marginBottom: 15,
    marginLeft: 30,
  },
  genderView: {
    flexDirection: 'row',
    width: wp(85),
    alignSelf: 'center',
    alignItems: 'center',
    zIndex: 100,
  },
  social_icon: {
    height: 50,
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    alignItems: 'center',
    alignSelf: 'center',
    paddingStart: 10,
  }
});



export default SignUp;
