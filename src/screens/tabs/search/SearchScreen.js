import React from 'react';
import COLOR from '../../../res/styles/Color';
import { Platform, View, Image, TextInput, StyleSheet, Text, TouchableOpacity, ActivityIndicatorComponent } from 'react-native';
import FONTS from '../../../res/styles/Fonts';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { connect } from 'react-redux';
import SearchAction from '../../../redux/actions/SearchAction';
import { FlatList } from 'react-native';
import ImageProgress from "react-native-image-progress";
import Dialog from "react-native-dialog";
import ChatAction from "../../../redux/actions/ChatAction";



class SearchScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: [],
            addFriendDialog: false,
            friendId: null,
        }
        this.search.bind(this);
        this.renderUser.bind(this);
    }

    componentDidUpdate = (prevProps) => {
        if (prevProps.userData != this.props.userData) {
            this.setState({
                userData: this.props.userData,
            })
        }
        if (
            this.props.threadNavigation !== prevProps.threadNavigation &&
            this.props.threadNavigation !== "undefined"
        ) {
            const threadData = this.props.threadNavigation;

            this.setState({
                addFriendDialog: true,
                friendId: this.props.registered.id,
            });


            this.props.navigation.navigate("ChatBoxScreen", {
                username: threadData.user.firstName + " " + threadData.user.lastName,
                image: require("../../../res/assets/user-image.png"),
                chat: 0,
                thread: threadData.thread.id,
            });
        }
    }

    search = (text) => {
        if (text.length > 2) {
            let data = JSON.stringify({
                query: text
            });
            this.props.searchUser(data);
        } else {
            this.setState({
                userData: [],
            })
        }
    }

    renderUser = ({ item }) => {
        return (
            <View style={{ width: wp(90), alignSelf: 'center', marginTop: 10 }}>
                <TouchableOpacity
                    onPress={() => this.props.isRegistered(item.phone)}
                >
                    <View style={{ flexDirection: 'row', flex: 1 }}>

                        <ImageProgress
                            indicator={ActivityIndicatorComponent}
                            style={{
                                width: 50,
                                height: 50,
                            }}
                            imageStyle={styles.userImage}
                            source={require('../../../res/assets/user-image.png')}
                        />

                        <View style={{ width: '50%', marginHorizontal: 15, alignSelf: 'center' }}>
                            <Text style={styles.username} >
                                {item.firstName + ' ' + item.lastName}
                            </Text>
                        </View>

                    </View>
                    <View style={styles.hr} />
                </TouchableOpacity>
            </View >)
    }
    render() {
        return (
            <View style={{ backgroundColor: COLOR.WHITE, flex: 1 }}>
                <View style={{
                    backgroundColor: COLOR.PRIMARYBLUE,
                    paddingTop: Platform.OS == 'ios' ? 50 : 15,
                    paddingBottom: 20,
                }}>

                    <View>
                        <Text style={{ fontSize: 25, color: COLOR.WHITE, alignSelf: "center", fontFamily: FONTS.FAMILY_BOLD }}>
                            Search
                        </Text>
                    </View>

                </View>
                <View style={styles.searchView}>
                    <Image
                        style={styles.searchIcon}
                        source={require('../../../res/assets/Search.png')}

                    />
                    <TextInput
                        placeholder={'Search People'}
                        placeholderTextColor={COLOR.PRIMARYGREY}
                        style={styles.search}
                        onChangeText={this.search}
                    />
                </View>
                <FlatList
                    style={{ flex: 1 }}
                    data={this.state.userData}
                    renderItem={this.renderUser}

                />
                <Dialog.Container visible={this.state.addFriendDialog}>
                    <Dialog.Title>Add Friend</Dialog.Title>
                    <Dialog.Description>
                        Do you want to add this user as a friend?
                    </Dialog.Description>
                    <Dialog.Button
                        label="Cancel"
                        onPress={() => {
                            this.setState({ addFriendDialog: false });
                        }}
                    />
                    <Dialog.Button
                        label="Add"
                        onPress={() => {
                            this.setState({ addFriendDialog: false });
                            this.props.addFriend(this.state.friendId);
                        }}
                    />
                </Dialog.Container>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    searchView: {
        width: wp(90),
        height: 40,
        alignSelf: 'center',
        flexDirection: 'row',
        backgroundColor: COLOR.GREY,
        borderRadius: 8,
        padding: 5,
        marginVertical: 15,
    },
    search: {
        flex: 1,
        height: 40,
        alignSelf: 'center',
        fontSize: 15,

    },
    searchIcon: {
        width: 14,
        height: 14,
        alignSelf: 'center',
        marginHorizontal: 10
    },
    userImage: {
        width: 50,
        height: 50,
        borderRadius: 30,
    },
    username: {
        fontSize: 14,
        color: COLOR.TEXTCOLOR,
        fontFamily: FONTS.FAMILY_BOLD,
    },
    userStatus: {
        fontSize: 12,
        color: COLOR.TEXTCOLORSECONDARY,
        fontFamily: FONTS.FAMILY_REGULAR,
        marginTop: 3,
    },
})

const mapStateToProps = (state, ownProps) => {
    return {
        userData: state.searchReducer.searchData,
        registered: state.chatReducer.isRegistered,
        threadNavigation: state.chatReducer.threadNavigation,

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        searchUser: (data) => {
            dispatch(SearchAction.searchUser(data));
        },
        isRegistered: (val) => {
            dispatch(ChatAction.isRegistered(val));
        },
        addFriend: (val) => {
            dispatch(ChatAction.addFriend(val));
        },
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen)