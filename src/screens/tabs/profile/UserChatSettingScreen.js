import React from "react";
import { SafeAreaView, View, Text, Switch } from 'react-native';
import Styles from "../../../res/styles/Styles";
import Toolbar from "../../../commonView/navView/Toolbar";
import COLOR from "../../../res/styles/Color";
import FONTS from "../../../res/styles/Fonts";
import { connect } from "react-redux";
import AuthenticationAction from "../../../redux/actions/AuthenticationAction";


class UserChatSettingScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isEnabled: false,
      isOnline: true,
    }
  }

  handleSetSystemPreference = ()=>{
    
    let data = JSON.stringify({
      readRecepient: this.state.isEnabled,
      showOnline: this.state.isOnline
    });

    this.props.setSystemPreferences(data);
    setTimeout(()=>{
        this.state.isOnline?this.props.setUserOnline():this.props.setUserOffline();
    },1000);

   
  }

  componentDidMount(){
    console.log(this.props.systemPreferences)
    if(this.props.systemPreferences){
      
      this.setState({
        isEnabled:this.props.systemPreferences.readRecepient,
        isOnline:this.props.systemPreferences.showOnline
      })
    }
  }

  componentWillReceiveProps(props){
    if(props.systemPreferences){
      this.setState({
        isEnabled:props.systemPreferences.readRecepient,
        isOnline:props.systemPreferences.showOnline
      });
    }
  }

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: COLOR.WHITE, flex: 1 }}>
        <Toolbar title={'Chat Settings'} />
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 25,
            justifyContent: 'space-between',
            marginTop: 30,
            marginBottom: 15,
          }}
        >
          <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, fontSize: 14 }}>
            Online
          </Text>
          <Switch
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            ios_backgroundColor="#3e3e3e"
            onValueChange={()=>this.setState({
              isOnline:!this.state.isOnline
            },()=>{
              this.handleSetSystemPreference();

            })}
            value={this.state.isOnline}
          />

        </View>

        <View style={Styles.hr} />
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 25,
            justifyContent: 'space-between',
            marginVertical: 15,
          }}
        >
          <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, fontSize: 14 }}>
            Read recipients
          </Text>
          <Switch
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            ios_backgroundColor="#3e3e3e"
            onValueChange={()=>this.setState({
              isEnabled:!this.state.isEnabled
            },()=>{
              this.handleSetSystemPreference();
            })}
            value={this.state.isEnabled}
          />

        </View>

      </SafeAreaView>
    );
  }


}

const mapStateToProps = (state,ownProps)=>{
  return {
    systemPreferences:state.reducer.systemPreferences,
  }
}

const mapDispatchToProps = (dispatch)=>{
  return {
    setSystemPreferences:(data)=>{
      dispatch(AuthenticationAction.setSystemPreferences(data));
    },
    setUserOffline:(val)=>{
      dispatch(AuthenticationAction.setUserOffline(val));
    },
    setUserOnline:(val)=>{
      dispatch(AuthenticationAction.setUserOnline(val));
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps) (UserChatSettingScreen);