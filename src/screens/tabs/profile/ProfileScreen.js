import React from 'react';
import { SafeAreaView, View, Text, Image, StyleSheet } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { CustomButton } from '../../../commonView/globalComponent/CustomButton';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Auth from "../../../auth";
import { AS_FCM_TOKEN, AS_USER_DETAIL, AS_USER_TOKEN } from '../../../api/Constants';
import API from '../../../api/Api';
import SimpleToast from 'react-native-simple-toast';
import ProfileAction from '../../../redux/actions/ProfileAction';
import { connect } from 'react-redux';
import AuthenticationAction from '../../../redux/actions/AuthenticationAction';
import Icon from 'react-native-vector-icons/Ionicons';
import { Badge } from 'react-native-elements';

const api = new API();
const auth = new Auth();
class ProfileScreen extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      firstName: '',
      lastName: '',
      numberOfNotifications: 3,
      hasPendingFriendRequests: true,
    }

  }

  async componentDidMount() {
    const userDetails = JSON.parse(await auth.getValue(AS_USER_DETAIL));
    await this.setState({
      firstName: userDetails.firstName,
      lastName: userDetails.lastName,
    });
  }



  callLogout = async () => {
    try {
      api.setUserOffline().then((json) => {
        if (json.status == 200) {
          console.log(json.data.response);
          auth.remove(AS_USER_TOKEN);
          auth.remove(AS_USER_DETAIL);
          auth.remove(AS_FCM_TOKEN);
          this.props.logoutUser();
          this.props.navigation.replace('LoginScreen');



        } else {
          setTimeout(() => {
            Toast.show(json.data.response);
            console.log(json.data.response)
          }, 0);

        }
      }).catch(function (error) {
        SimpleToast.show("Connection lost");
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }
  }



  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
        <View style={{ flex: 1 }}>
          <View style={{ marginTop: 20, marginRight: 30, flexDirection: "row", alignSelf: "flex-end" }}>
            <TouchableOpacity style={{ marginRight: 30 }} onPress={() => this.props.navigation.navigate('ProfileNotifications')}>
              <Icon name="notifications-outline" size={30} color={COLOR.PRIMARYBLUE}></Icon>
              {this.state.numberOfNotifications > 0 ?
                <Badge
                  status="error"
                  containerStyle={{ position: 'absolute', left: 12, bottom: 15 }}
                  badgeStyle={{ size: 40 }}
                  value={this.state.numberOfNotifications} /> : null
              }
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('SettingsScreen')}>
              <Icon name="cog-outline" size={30} color={COLOR.PRIMARYBLUE}></Icon>
            </TouchableOpacity>
          </View>

          <View style={styles.profile_view}>
            <Image
              source={require('../../../res/assets/user-image.png')}
              style={styles.profile_pic}
            />
          </View>
          <Text style={styles.heading}>{this.state.firstName + " " + this.state.lastName}</Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginTop: 10,
            }}
          >
            <TouchableOpacity style={styles.card} onPress={() => this.props.navigation.navigate('UserProfile')}>
              <View style={styles.profileBack}>
                <Image
                  source={require('../../../res/assets/Profile.png')}
                  style={{ width: 25, height: 25, alignSelf: 'center' }}
                  resizeMode={'contain'}
                />
              </View>
              <Text style={{ alignSelf: 'center' }} >Profile</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.card}
              onPress={() => this.props.navigation.navigate('FriendsScreen')}
            >
              {this.state.hasPendingFriendRequests ?
                <View style={styles.tagView} >
                  <Text style={{ color: COLOR.WHITE, alignSelf: 'center' }}>24</Text>
                </View> : null}

              <View style={styles.friendsView}>
                <Image
                  source={require('../../../res/assets/multiuser.png')}
                  style={{ width: 25, height: 25, alignSelf: 'center' }}
                  resizeMode={'contain'}
                />
              </View>
              <Text style={{ alignSelf: 'center', marginBottom: 20 }}>Friends</Text>
            </TouchableOpacity>

          </View>

          <View style={{ position: 'absolute', bottom: 0, alignSelf: 'center' }}>
            <CustomButton
              width={wp(85)}
              text={'Logout'}
              bg={COLOR.PRIMARYBLUE}
              labelColor={COLOR.WHITE}
              borderRadius={10}
              onPress={this.callLogout}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  bookingView: {
    marginVertical: 20,
    height: 55,
    width: 55,
    borderRadius: 30,
    backgroundColor: '#8149FA',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  profile_pic: {
    width: 120,
    height: 120,
    marginTop: 30,
    borderRadius: 60,
    borderWidth: 1,
    borderColor: COLOR.WHITE,
    alignSelf: 'center',
  },
  options: {
    color: COLOR.WHITE,
    fontSize: 16,
    fontFamily: FONTS.FAMILY_MEDIUM,
    marginHorizontal: 15,
  },
  heading: {
    fontFamily: FONTS.FAMILY_MEDIUM,
    fontSize: 26,
    alignSelf: 'center',
    marginTop: 8,
  },
  discription: {
    color: COLOR.TEXTCOLOR,
    fontFamily: FONTS.FAMILY_REGULAR,
    fontSize: 20,
    alignSelf: 'center',
  },
  menu_option: {
    marginHorizontal: 15,
    paddingVertical: 15,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  icon: {
    padding: 2,
    minHeight: 24,
    minWidth: 24,
    marginBottom: 15,
    marginRight: 15,
    alignSelf: 'flex-end',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: COLOR.WHITE,
    justifyContent: 'center',
    backgroundColor: COLOR.PRIMARYBLUE,
    position: 'absolute',
  },
  profile_view: {
    alignSelf: 'center',
    flexDirection: 'row-reverse',
  },
  card: {
    width: 140,
    height: 140,
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: COLOR.WHITE,
    borderRadius: 15,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    elevation: 1,
    shadowOffset: { width: 2, height: 2 },
    marginTop: 15,
  },
  tagView: {
    alignSelf: 'flex-end',
    height: 35,
    width: 35,
    top: 0,
    right: 0,
    position: 'absolute',
    justifyContent: 'center', borderTopEndRadius: 15, borderBottomLeftRadius: 15, backgroundColor: '#8149FA'
  },
  galleryView: {
    marginVertical: 20,
    height: 55,
    width: 55,
    borderRadius: 30,
    backgroundColor: '#01CAE4',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  friendsView: {
    marginVertical: 20,
    height: 55,
    width: 55,
    borderRadius: 30,
    backgroundColor: '#8149FA',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  profileBack: {

    height: 55,
    width: 55,
    borderRadius: 30,
    backgroundColor: '#708DED',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  toolbar: {
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

});

const mapDispatchToProps = (dispatch) => {
  return {
    logoutUser: (val) => {
      dispatch(AuthenticationAction.logoutUser());
    }
  }
}

export default connect(null, mapDispatchToProps)(ProfileScreen);