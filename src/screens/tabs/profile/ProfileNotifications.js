import React from 'react';
import {Image, SafeAreaView, Text, View} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Toolbar from '../../../commonView/navView/Toolbar';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Styles from '../../../res/styles/Styles';

export default function ProfileNotifications (){
        return(
            <View style={{flex: 1, backgroundColor: COLOR.WHITE}}>
                <View style={{
                    backgroundColor:COLOR.PRIMARYBLUE,
                    paddingTop:Platform.OS == 'ios'?50:15,
                    paddingBottom:20,
                    }}>
                    <Toolbar title={'Notifications'} />
                </View>
                
             </View>   
        )
}