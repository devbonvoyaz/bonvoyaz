import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Styles from '../../../res/styles/Styles';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { DatePickerDialog } from 'react-native-datepicker-dialog'
import moment from 'moment';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { GENDERS } from '../../../res/appData';
import ReactNativePickerModule from "react-native-picker-module";
import Icon from "react-native-vector-icons/FontAwesome5";
import Auth from "../../../auth";
import { AS_USER_DETAIL } from '../../../api/Constants';
import FloatingCustomInput from '../../../commonView/globalComponent/FloatingCustomInput';
import { connect } from 'react-redux';
import ProfileAction from '../../../redux/actions/ProfileAction';
import Helper from '../../../commonView/Helper/Helper';
import SimpleToast from 'react-native-simple-toast';
import Animated from 'react-native-reanimated';



const auth = new Auth();
const helper = new Helper();
class UserProfile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      dobDate: null,
      dobText: '',
      gender: null,
      email: '',
      isDatePickerVisible: false,
    };
    this.pageRef = React.createRef();
  }



  async componentDidMount() {
    const userDetails = JSON.parse(await auth.getValue(AS_USER_DETAIL));

    console.log(userDetails);
    await this.setState({
      firstName: userDetails.firstName,
      lastName: userDetails.lastName,
      dobText: userDetails.dob ? moment(userDetails.dob).format('DD-MM-YYYY') : null,
      gender: userDetails.gender,
      email: userDetails.email
    });
  }

  componentDidUpdate(prevState) {
    if (this.props.profileData != prevState.profileData) {
      auth.setValue(AS_USER_DETAIL, this.props.profileData);
    }
  }

  animateText = (value) => {
    return {
      top: value ? 0 : null,
    };
  };

  callUpdateProfile = () => {
    if (this.state.firstName == '') {
      SimpleToast.show('Enter First Name');
    } else if (this.state.lastName == '') {
      SimpleToast.show('Enter Last name');
    } else if (!helper.validate(this.state.email)) {
      SimpleToast.show('Invalid Email')
    } else {
      let data = JSON.stringify({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email,
        gender: this.state.gender,
        dob: this.state.dobDate,
      })
      this.props.updateProfile(data);
    }
  }

  onDOBPress = () => {
    let dobDate = this.state.dobDate;

    if (!dobDate || dobDate == null) {
      dobDate = new Date();
      this.setState({
        dobDate: dobDate
      });
    }

    //To open the dialog
    this.refs.dobDialog.open({
      date: dobDate,
      maxDate: new Date() //To restirct future date
    });

  }

  onDOBDatePicked = (date) => {
    //Here you will get the selected date
    this.setState({
      dobDate: date,
      dobText: moment(date).format('DD-MM-YYYY'),
      isDatePickerVisible: false,
    });
  }

  updateMasterState = (attrName, value) => {
    this.setState({ [attrName]: value });
  };

  render() {
    return (
      <KeyboardAwareScrollView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
        <View>
          <View style={{ backgroundColor: COLOR.PRIMARYBLUE, paddingTop: 30 }} >
            <View style={styles.toolbar} >
              <TouchableOpacity
                style={styles.backButton}
                onPress={() => { this.props.navigation.goBack(); }} >
                <Icon name="chevron-left" style={{ alignSelf: 'center' }} size={18} color={COLOR.WHITE} />
              </TouchableOpacity>

              <Text style={[Styles.screen_heading, { color: COLOR.WHITE }]}>
                Profile
              </Text>
              <TouchableOpacity
                style={{ marginHorizontal: 15 }}
                onPress={() => this.callUpdateProfile()}
              >
                <Text
                  style={{
                    color: COLOR.WHITE,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                  }}
                >
                  Save
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.profile_view}>

              <Image
                source={require('../../../res/assets/user-image.png')}
                style={styles.profile_pic}
              />
              <View style={styles.icon}>
                <Image
                  style={{ alignSelf: 'center', width: 16, height: 16 }}
                  source={require('../../../res/assets/camera.png')}
                  resizeMode={'contain'}
                />
              </View>

            </View>
            <Text
              style={{ fontSize: 16, fontFamily: FONTS.FAMILY_MEDIUM, color: COLOR.WHITE, alignSelf: 'center', marginTop: 10, marginBottom: 20 }}
            >{this.state.firstName + " " + this.state.lastName}
            </Text>

          </View>

          <FloatingCustomInput
            icon={require('../../../res/assets/Profile.png')}
            attrName="firstName"
            title="First Name"
            width={wp(90)}
            color={'#FFB100'}
            value={this.state.firstName}
            updateMasterState={this.updateMasterState}
          />


          <FloatingCustomInput
            icon={require('../../../res/assets/Profile.png')}
            attrName="lastName"
            title="Last Name"
            width={wp(90)}
            color={'#FFB100'}
            value={this.state.lastName}
            updateMasterState={this.updateMasterState}
          />
          <TouchableOpacity
            onPress={() => this.pageRef.current.show()}
            style={[styles.text_input_card, { width: wp(90), flexDirection: 'row' }]}>

            <View style={styles.genderView}>
              <Image
                source={require('../../../res/assets/Gender.png')}
                style={{
                  height: 20,
                  width: 20,
                  alignSelf: 'center',
                }}
                resizeMode={'contain'}
              />
            </View>

            <View style={{ justifyContent: 'center' }}>
              <Animated.Text
                style={[{
                  fontSize: 15,
                  fontFamily: FONTS.FAMILY_MEDIUM,
                  color: COLOR.PRIMARYGREY,
                  width: 100,
                  position: 'absolute',
                }, this.animateText(this.state.gender)]}
              >
                {'Gender'}
              </Animated.Text>
              {this.state.gender ? <Text
                style={{
                  fontSize: 14,
                  fontFamily: FONTS.FAMILY_MEDIUM,
                  padding: 0,
                  marginTop: 15
                }}

              >{this.state.gender}</Text> : null}
            </View>
          </TouchableOpacity>




          <FloatingCustomInput
            icon={require('../../../res/assets/Message2.png')}
            attrName="email"
            title="Email"
            width={wp(90)}
            color={'#8149FA'}
            value={this.state.email}
            updateMasterState={this.updateMasterState}
          />
          <TouchableOpacity
            onPress={() => this.setState({
              isDatePickerVisible: true,
            })}
            style={[styles.text_input_card, { width: wp(90), flexDirection: 'row' }]}>

            <View style={[styles.genderView, { backgroundColor: '#BE63F2' }]}>
              <Image
                source={require('../../../res/assets/Calendar.png')}
                style={{
                  height: 20,
                  width: 20,
                  alignSelf: 'center',
                }}
                resizeMode={'contain'}
              />
            </View>

            <View style={{ justifyContent: 'center' }}>
              <Animated.Text
                style={[{
                  fontSize: 15,
                  fontFamily: FONTS.FAMILY_MEDIUM,
                  color: COLOR.PRIMARYGREY,
                  width: 200,
                  position: 'absolute',
                }, this.animateText(this.state.dobText)]}
              >
                {'Date of birth'}
              </Animated.Text>
              {this.state.dobText ? <Text
                style={{
                  fontSize: 14,
                  fontFamily: FONTS.FAMILY_MEDIUM,
                  padding: 0,
                  marginTop: 15
                }}

              >{this.state.dobText}</Text> : null}
            </View>
          </TouchableOpacity>

          <DatePickerDialog ref="dobDialog" onDatePicked={this.onDOBDatePicked.bind(this)} />
          <DateTimePickerModal
            isVisible={this.state.isDatePickerVisible}
            mode="date"
            maximumDate={new Date()}
            onConfirm={this.onDOBDatePicked.bind(this)}
            onCancel={() => this.setState({ isDatePickerVisible: false })}
          />
        </View>
        <ReactNativePickerModule
          pickerRef={this.pageRef}
          value={this.state.gender}
          title={"Select gender"}
          items={GENDERS}

          onCancel={() => {
            console.log("Cancelled")
          }}
          onValueChange={value => {

            this.setState({
              gender: value,
            }, () => {
              console.log(this.state.gender)
            })


          }}
        />
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  profile_pic: {
    width: 80,
    height: 80,

    borderRadius: 40,
    borderWidth: 1,
    borderColor: COLOR.WHITE,
    alignSelf: 'center',
  },
  green_dot: {
    minHeight: 10,
    minWidth: 10,
    alignSelf: 'center',
    marginTop: 8,
    borderRadius: 8,
    backgroundColor: COLOR.PRIMARTGREEN,
  },
  icon: {
    padding: 2,
    minHeight: 24,
    minWidth: 24,
    alignSelf: 'flex-end',
    borderRadius: 10,
    justifyContent: 'center',
    backgroundColor: COLOR.WHITE,
    position: 'absolute',
  },
  profile_view: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 20,
    flexDirection: 'row-reverse',
  },
  text_input_card: {
    alignSelf: 'center',
    borderRadius: 5,
    backgroundColor: COLOR.WHITE,
    elevation: 5,
    padding: 12,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    shadowOffset: { width: 2, height: 2 },
    marginVertical: 8,
  },
  toolbar: {
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  genderView: {
    backgroundColor: '#01CAE4',
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 10,
    justifyContent: 'center',
  },
  backButton: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignSelf: 'center'
  }
});

mapStateToProps = (state, ownProps) => {
  return {
    profileData: state.profileReducer.profileData,
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    updateProfile: (data) => {
      dispatch(ProfileAction.updateProfile(data));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
