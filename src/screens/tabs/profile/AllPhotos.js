import React, {useState} from 'react';
import {Image, ImageBackground, Platform, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import {FlatList, ScrollView} from 'react-native-gesture-handler';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import Toolbar from '../../../commonView/navView/Toolbar';
import {ALLPHOTOSCOUNT} from '../../../res/appData';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import {GALLERYIMAGE2} from '../../../res/appData/index';
import {useNavigation} from '@react-navigation/native'
import Styles from '../../../res/styles/Styles';
import Icon from "react-native-vector-icons/FontAwesome5";

export default function AllPhotos() {
    const [count, setCount] = useState('10');
    const navigation = useNavigation();
    return (
        <View style={{flex: 1, backgroundColor: COLOR.WHITE}}>
            <ImageBackground
                style={styles.background}
                source={require('../../../res/assets/profileback.png')}
            >
                <View
                    style={styles.toolbar}
                >
                    <TouchableOpacity
                        style={styles.backButton}
                        onPress={() => {
                            navigation.goBack();
                        }}
                    >
                        <Icon name="chevron-left" style={{alignSelf: 'center'}} size={18} color={COLOR.WHITE}/>

                    </TouchableOpacity>

                    <Text style={[Styles.screen_heading, {color: COLOR.WHITE}]}>
                        All Photos
                    </Text>
                    <TouchableOpacity
                        style={styles.backButton}
                    >
                        <Text
                            style={{
                                color: COLOR.WHITE,
                                fontFamily: FONTS.FAMILY_SEMIBOLD,
                            }}
                        >

                        </Text>
                    </TouchableOpacity>
                </View>


            </ImageBackground>

            <ScrollView>
                <View style={{marginHorizontal: 25}}>

                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text
                        style={styles.allphotosTitle}
                    >
                        All Photos
                    </Text>
                    <View style={{width: widthPercentageToDP(20)}}>
                        <DropDownPicker
                            items={ALLPHOTOSCOUNT}
                            defaultValue={count}
                            zIndex={3996}
                            style={{
                                minHeight: 50,
                                borderWidth: 0,
                                color: COLOR.PRIMARYBLUE,
                                fontFamily: FONTS.FAMILY_REGULAR,
                            }}
                            labelStyle={{
                                fontSize: 14,
                                textAlign: 'left',
                                color: COLOR.PRIMARYBLUE,
                                fontFamily: FONTS.FAMILY_REGULAR,
                            }}
                            dropDownStyle={{
                                borderWidth: 0,
                                zIndex: 100,
                            }}
                            itemStyle={{
                                justifyContent: 'flex-start',
                                zIndex: 100,
                            }}
                            onChangeItem={item => setCount(item.value)}
                            showArrow={true}
                        />
                    </View>
                </View>
                <FlatList
                    data={GALLERYIMAGE2}
                    renderItem={({item}) => (
                        <Image
                            style={{flex: 1 / 5, aspectRatio: 1, margin: 1}}
                            source={item.image}
                        />
                    )}
                    numColumns={5}
                />
            </ScrollView>
        </View>
    );


}
const styles = StyleSheet.create({
    background: {
        width: '100%',
        paddingTop: Platform.OS == 'ios' ? 50 : 10,
        marginBottom: 10
    },
    toolbar: {
        flexDirection: 'row',
        padding: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    allphotosTitle: {
        alignSelf: 'center',
        margin: 15,
        fontSize: 18,
        fontFamily: FONTS.FAMILY_REGULAR,
    },
    backButton: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignSelf: 'center'
    }
})
