import React from 'react';
import {Image, SafeAreaView, Text, View} from 'react-native';
import { useNavigation } from '@react-navigation/core';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Toolbar from '../../../commonView/navView/Toolbar';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Styles from '../../../res/styles/Styles';

export default function SettingsScreen () {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLOR.WHITE}}>
     <ScrollView style={{paddingHorizontal: 15}}>
        <Toolbar title={'Settings'} />
        <Text
          style={{
            fontSize: 20,
            marginVertical: 20,
            fontFamily: FONTS.FAMILY_MEDIUM,
            color: COLOR.PRIMARYBLUE,
          }}
        >
          Preferences
        </Text>
        <Options name="Change Password" />
        <Options name="Notifications" />
        <TouchableOpacity onPress={()=> navigation.navigate('UserChatSettingScreen')} 
          style={{paddingTop: 15}}
        >
          <View>
            <Text 
              style={{
                fontSize: 16,
                fontFamily: FONTS.FAMILY_REGULAR,
              }}
            >
              Chat Settings
            </Text>
          </View>
        </TouchableOpacity>
        <Text
          style={{
            fontSize: 20,
            marginVertical: 20,
            fontFamily: FONTS.FAMILY_MEDIUM,
            color: COLOR.PRIMARYBLUE,
          }}
        >
          Support
        </Text>
        <Options name="Help Center" />
        <Options name="Privacy Policy" />
      </ScrollView>
    </SafeAreaView>
  );
}

const Options = props => {
  return (
    <TouchableOpacity
      style={{
        height: 50,
        justifyContent: 'space-between',
        flexDirection: 'row',
      }}
    >
      <Text
        style={{
          alignSelf: 'center',
          fontSize: 16,
          fontFamily: FONTS.FAMILY_REGULAR,
        }}
      >
        {props.name}
      </Text>
      <Image
        style={{alignSelf: 'center',width:15,height:15}}
        source={require ('../../../res/assets/arrow-right.png')}
        resizeMode={'contain'}
      />
    </TouchableOpacity>
  );
};
