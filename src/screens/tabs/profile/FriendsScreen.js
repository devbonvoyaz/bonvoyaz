import React from 'react';
import {
    Image,
    ImageBackground,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import UserList from '../../../commonView/listView/UserList';
import {USERLIST} from '../../../res/appData';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Styles from '../../../res/styles/Styles';
import {useNavigation} from '@react-navigation/native';
import Icon from "react-native-vector-icons/FontAwesome5";
import {connect} from 'react-redux';
import ChatAction from '../../../redux/actions/ChatAction';

class FriendsScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            friends: null,
        }

        this.props.getFriends();
    }

    componentDidMount() {

        if (this.props.friends) {
            this.setState({
                friends: this.props.friends,
            })
        }
    }

    componentWillReceiveProps(props) {
        if (props.friends) {
            this.setState({
                friends: props.friends,
            })
        }
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: COLOR.WHITE}}>
                <ImageBackground
                    style={styles.background}
                    source={require('../../../res/assets/profileback.png')}
                >
                    <View
                        style={styles.toolbar}
                    >
                        <TouchableOpacity
                            style={styles.backButton}
                            onPress={() => {
                                this.props.navigation.goBack();
                            }}
                        >
                            <Icon style={{alignSelf:'center'}} name="chevron-left" size={18} color={COLOR.WHITE}/>
                        </TouchableOpacity>

                        <Text style={[Styles.screen_heading, {color: COLOR.WHITE}]}>
                            Friends
                        </Text>
                        <TouchableOpacity
                            style={styles.backButton}
                        >
                            <Text
                                style={{
                                    color: COLOR.WHITE,
                                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                                }}
                            >

                            </Text>
                        </TouchableOpacity>
                    </View>


                </ImageBackground>

                <View style={{marginHorizontal: 15}}>

                    <View
                        style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                            marginTop: 10,
                        }}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',

                                fontSize: 18,
                                fontFamily: FONTS.FAMILY_REGULAR,
                            }}
                        >
                            Total Friends
                        </Text>
                        <Text
                            style={{
                                alignSelf: 'center',

                                fontSize: 18,
                                fontFamily: FONTS.FAMILY_REGULAR,
                                color: COLOR.PRIMARYBLUE,
                            }}
                        >
                            50
                        </Text>
                    </View>

                    <View style={styles.search_input}>
                        <TextInput
                            placeholderTextColor={COLOR.PRIMARYGREY}
                            placeholder={'Search'}
                        />
                    </View>
                </View>
                <FlatList
                    style={{marginTop: 10}}
                    data={this.state.friends}
                    renderItem={({item}) => (
                        <UserList
                            username={item.user2.firstName + ' ' + item.user2.lastName}
                            userId={''}
                            image={require('../../../res/assets/user-image.png')}
                        />
                    )}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    search_input: {
        height: 40,
        marginTop: 20,
        backgroundColor: COLOR.GREY,
        width: widthPercentageToDP(90),
        borderRadius: 7,
        alignSelf: 'center',
        paddingStart: 15,
        justifyContent: 'center',
    },
    background: {
        width: '100%',
        paddingTop: Platform.OS == 'ios' ? 50 : 10,
        marginBottom: 10
    },
    toolbar: {
        flexDirection: 'row',
        padding: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    backButton: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignSelf: 'center'
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        getFriends: (val) => {
            dispatch(ChatAction.getFriends(val));
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        friends: state.chatReducer.friends,
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(FriendsScreen)