import React from 'react';
import { Image, StyleSheet, View, Text, Platform } from 'react-native';
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import SegmentedControl from '../../../commonView/globalComponent/SegmentControl';
import TripObject from '../../../commonView/TripView/TripObject';
import { TRIPSOBJECTS } from '../../../res/appData';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Icon from "react-native-vector-icons/FontAwesome5";

class UserDetailsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 2,
    };
    this.handleTabsChange = this.handleTabsChange.bind(this);
  }
  handleTabsChange = index => {
    this.setState({
      tabIndex: index,
    });
  };
  render() {
    const { tabIndex } = this.state;

    return (
      <View style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
        <ScrollView style={{ flex: 1 }}>
          <View
            style={styles.background}
          />
          <TouchableOpacity
            style={{ marginVertical: Platform.OS == 'ios' ? 50 : 20, marginHorizontal: 20 }}
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <Icon name="chevron-left" size={18} color={COLOR.WHITE} />

          </TouchableOpacity>
          <Image
            source={require('../../../res/assets/user-image.png')}
            style={styles.profile_pic}
          />
          <Text style={styles.heading}>Victoria Robertson</Text>
          <Text style={styles.discription}>Traveller,Blogger</Text>
          <View style={{ alignSelf: 'center' }}>
            <SegmentedControl
              tabs={['About', 'Media', 'Trips']}
              activeTextColor={COLOR.PRIMARTGREEN}
              paddingVertical={14}
              currentIndex={tabIndex}
              onChange={this.handleTabsChange}
            />
          </View>
          <FlatList
            style={{
              width: widthPercentageToDP(95),
              alignSelf: 'center',
              marginBottom: 10,
            }}
            
            data={TRIPSOBJECTS}
            renderItem={({ item }) => (
              <TripObject
                name={item.name}
                image={item.image}
                duration={item.duration}
                createdAt={item.createdAt}
              />
            )}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  profile_pic: {
    width: 150,
    height: 150,
    marginTop: 25,
    borderRadius: 75,
    borderWidth: 1,
    borderColor: COLOR.WHITE,
    alignSelf: 'center',
  },

  heading: {
    color: COLOR.PRIMARYBLACK,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontSize: 30,
    alignSelf: 'center',
    marginTop: 8,
  },
  discription: {
    color: COLOR.TEXTCOLOR,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    fontSize: 16,
    alignSelf: 'center',
  },
  background:{
    position: 'absolute',
    width: '100%',
    height: 200,
    backgroundColor: COLOR.PRIMARYBLUE,
  }
});

export default UserDetailsScreen;
