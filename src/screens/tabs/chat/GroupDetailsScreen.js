import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Platform, Button, ImageBackground,
} from 'react-native';
import { FlatList, ScrollView, Switch } from 'react-native-gesture-handler';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import SegmentedControl from '../../../commonView/globalComponent/SegmentControl';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Styles from '../../../res/styles/Styles';
import PartcipantObject from '../../../commonView/listView/ParticipantsObject';
import { PARTICIPANTS, TRIPSOBJECTS } from '../../../res/appData';
import TripObject from '../../../commonView/TripView/TripObject';
import { useNavigation } from '@react-navigation/core';
import { connect } from 'react-redux';
import ChatAction from '../../../redux/actions/ChatAction';
import RBSheet from "react-native-raw-bottom-sheet";
import { SearchBar } from "react-native-elements";
import Toolbar2 from "../../../commonView/navView/Toolbar2";
import ImagePicker from "react-native-image-crop-picker";
import API from "../../../api/Api";
import Toast from "react-native-simple-toast";
import ActionSheet from "../../../commonView/globalComponent/ActionSheet";
import ActivityIndicator from "../../../commonView/globalComponent/ActivityIndicator";
import ImageProgress from "react-native-image-progress";
import ProgressBar from 'react-native-progress/Bar';
import moment from 'moment';
import { Dialog, Paragraph } from "react-native-paper";

class GroupDetailsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tabIndex: 0,
            item: null,
            participants: [],
            image: this.props.route.params.image.uri,
            groupName: this.props.route.params.item.details.title,
            groupDescription: this.props.route.params.item.details.description,
            userId: this.props.route.params.userId,
            friendsSearch: '',
            isAdmin: this.props.route.params.item.details.isAdmin,
            groupNameChange: false,
            actionModalVisible: false,
            isLoading: false,
            adminDailog: false,
            selectedParticipantId: null,
            removeParticipant: null,
            removeDailog: false,
            exitDailog: false,
            friends: [],
            trips: [],
            thread: this.props.route.params.item.details.thread,
        };

        this.props.getParticipantsByThread(this.state.thread);
        this.props.getTripByThread(this.state.thread);

        this.handleTabsChange = this.handleTabsChange.bind(this);
        this.props.getFriends();
    }

    componentDidMount() {
        if (this.props.participants) {
            this.setState({
                item: this.props.participants,
                participants: this.props.participants.users,
            })
        }
    }

    componentWillReceiveProps(props) {
        if (props.participants) {
            this.setState({
                item: props.participants,
                participants: props.participants.users,
            })
        }

        if (props.friends) {
            this.setState({
                friends: props.friends
            })
        }

        if (props.tripByThread) {
            this.setState({
                trips: props.tripByThread,
            })
        }
    }

    updateSearch = (e) => {
        this.setState({
            friendsSearch: e,
        })
        let text = e.toLowerCase()
        let trucks = this.props.friends
        let data = [];
        trucks.filter((item) => {
            if ((item.user2.firstName + ' ' + item.user2.lastName).toLowerCase().match(text)) {
                data.push(item)
            }

        })
        if (data.length == 0 || e.length == 0) {
            this.setState({
                friends: this.props.friends,
            })
        } else {
            this.setState({
                friends: data
            })
        }
    };

    handleTabsChange = index => {
        this.setState({
            tabIndex: index,
        });
    };

    addParticipant = (item) => {
        let data = JSON.stringify({
            thread: this.state.item.thread.id,
            user: item.user2.id,
        });
        this.props.addParticipant(data);
        setTimeout(() => {
            this.props.getParticipantsByThread(this.props.route.params.item.details.thread);
        }, 250);

    }

    removeParticipant = (item) => {
        let data = JSON.stringify({
            thread: this.state.item.thread.id,
            user: item.id ? item.id : item,
        });
        let participants = this.state.participants;
        participants.splice(participants.indexOf(item), 1);
        this.setState({ participants: participants });
        this.props.removeParticipant(data);
        this.setState({
            removeParticipant: null,
        });
        item.id ? null : this.props.navigation.pop(2);
        this.props.getGroups();

        console.log(data);
    }

    updateGroup = () => {
        let data = JSON.stringify({
            title: this.state.groupName,
            image: this.state.image,
        });
        this.props.updateGroup(this.state.item.thread.id, data)
    }

    handleSheet = (val) => {
        if (val == 1) {
            this.chooseImage(1);
        } else if (val == 2) {
            this.chooseImage(2);

        } else if (val == 3) {
            this.setState({
                actionModalVisible: false,
            });
        }
    };

    chooseImage = (val) => {
        let options = {
            title: 'Select Image',
            mediaType: 'photo',
        };

        if (val == 1) {
            ImagePicker.openCamera({
                title: 'Select Image',
                width: 1024,
                height: 472,
                compressImageMaxWidth: 1024,
                compressImageMaxHeight: 472,
            }).then((response) => {
                console.log('image local reposnse', response);
                this.setState({
                    actionModalVisible: false,
                });
                this.callUploadImage(response, val);
                this.setState({ path: response.path });

            });
        } else if (val == 2) {
            ImagePicker.openPicker(options).then((response) => {
                console.log('image local reposnse', response);
                this.setState({
                    actionModalVisible: false,
                });

                this.callUploadImage(response, val);

                this.setState({ path: response.path });

            });
        }
    };

    callUploadImage(res, val) {
        var data = new FormData();
        var photo = {
            uri: res.path,
            type: res.type ? res.type : res.mime,
            name: 'imgs.jpg',
        };
        this.setState({
            isLoading: true,
        });

        data.append('file', photo);
        console.log('image uploading data is:-', photo);
        const api = new API();
        api.uploadImage(data)
            .then((json) => {

                this.setState({
                    isLoading: false,
                });
                if (json.status == 200) {
                    console.log("Succes");
                    console.log('upload responce:-', json.data.response.url);
                    this.setState({ image: json.data.response.url });
                    this.updateGroup();
                } else if (json.status == 400) {
                    setTimeout(() => {
                        Toast.show(json.data.message);
                    }, 0);
                } else {
                    setTimeout(() => {
                        Toast.show(json.data.message);
                    }, 0);
                }
            })
            .catch((error) => {
                setTimeout(() => {
                    console.log('json.data.message', error);
                    Toast.show(String(error));
                }, 0);
                this.setState({
                    isLoading: false,
                });
                console.log('error:-', error);

            });


    }

    setParticipantAsAdmin = () => {
        let data = JSON.stringify({
            thread: this.state.item.thread.id,
            user: this.state.selectedParticipantId,
        });
        this.props.setParticipantAsAdmin(data);
        setTimeout(() => this.props.getParticipantsByThread(this.state.item.thread.id), 1000);
    }

    render() {

        const { tabIndex } = this.state;
        return (
            <View style={styles.containerView}>
                <ScrollView>
                    <ImageProgress
                        indicator={ProgressBar}
                        indicatorProps={{
                            color: COLOR.WHITE
                        }}
                        source={{ uri: this.state.image }}
                        style={[styles.image_background, { paddingTop: Platform.OS === 'ios' ? 50 : null }]}>
                        <Toolbar2
                            optionPress={() => this.setState({ actionModalVisible: true })}
                            title={this.state.groupName}
                            imageOption={true}
                            color={COLOR.WHITE} />
                    </ImageProgress>

                    <View style={{ alignSelf: 'center', marginTop: 15 }}>
                        <SegmentedControl
                            tabs={['General', 'Trips', 'Participants']}
                            activeTextColor={COLOR.PRIMARYBLUE}
                            textColor={COLOR.PRIMARYGREY}
                            segmentedControlBackgroundColor={'#F6F6F6'}
                            paddingVertical={14}
                            currentIndex={tabIndex}
                            onChange={this.handleTabsChange}
                        />
                    </View>
                    {tabIndex == 0
                        ? <this.General />
                        : tabIndex == 1 ? <this.TripsView /> : <this.Participants />}


                </ScrollView>
                <ActionSheet
                    modalVisible={this.state.actionModalVisible}
                    handleSheet={this.handleSheet}
                />
                <ActivityIndicator loading={this.state.isLoading} />
                <Dialog visible={this.state.adminDailog}>
                    <Dialog.Title>Make Admin</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Do you want to Make this user Admin?</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => {
                            this.setState({
                                adminDailog: false
                            })
                            this.setParticipantAsAdmin()
                        }} title={"Yes"} />
                        <Button
                            onPress={() => {
                                this.setState({
                                    adminDailog: false,
                                })
                            }}
                            title={"No"} />
                    </Dialog.Actions>
                </Dialog>
                <Dialog visible={this.state.removeDailog}>
                    <Dialog.Title>Remove Participant</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Do you want to remove this Participant?</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => {
                            this.setState({
                                removeDailog: false
                            })
                            this.removeParticipant(this.state.removeParticipant)
                        }} title={"Yes"} />
                        <Button
                            onPress={() => {
                                this.setState({
                                    removeDailog: false
                                })
                            }}
                            title={"No"} />
                    </Dialog.Actions>
                </Dialog>
                <Dialog visible={this.state.exitDailog}>
                    <Dialog.Title>Exit Group</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Do you want to exit group?</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => {
                            this.setState({
                                exitDailog: false
                            })
                            this.removeParticipant(this.state.userId)
                        }} title={"Yes"} />
                        <Button
                            onPress={() => {
                                this.setState({
                                    exitDailog: false
                                })
                            }}
                            title={"No"} />
                    </Dialog.Actions>
                </Dialog>
            </View>
        );
    }

    General = props => {
        var { groupNameChange } = this.state;
        const initialGroupName = this.props.route.params.item.details.title;
        const navigation = useNavigation();
        return (
            <View style={styles.generalView}>

                <View style={[styles.card, { flexDirection: 'column' }]}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.descriptionText}>
                            Group Name
                        </Text>
                        {groupNameChange ?
                            <TouchableOpacity onPress={() => this.updateGroup()}>
                                <Text style={styles.descriptionText}>Save</Text>
                            </TouchableOpacity>

                            : null}
                    </View>
                    <TextInput
                        placeholder={'Group Name'}
                        placeholderTextColor={COLOR.PRIMARYGREY}
                        style={styles.descriptionInput}
                        value={this.state.groupName}
                        onChangeText={text => {
                            if (text == initialGroupName) {
                                this.setState({
                                    groupNameChange: false
                                })
                            } else {
                                this.setState({
                                    groupNameChange: true
                                })
                            }
                            this.setState({ groupName: text })
                        }}
                    />
                </View>
                <View style={[styles.card, { justifyContent: 'space-between', height: 50, }]}>
                    <Text
                        onPress={() => {
                            navigation.navigate('AllPhotos');
                        }}
                        style={{ fontFamily: FONTS.FAMILY_MEDIUM, fontSize: 16 }}
                    >
                        Media, links, photos{' '}
                    </Text>
                    <Image
                        style={{ alignSelf: 'center', width: 18, height: 18 }}
                        resizeMode={'contain'}
                        source={require('../../../res/assets/arrow-right.png')}
                    />
                </View>
                <View
                    style={[styles.card, {
                        height: 50,
                        justifyContent: 'space-between',

                    }]}
                >
                    <Text style={{ fontFamily: FONTS.FAMILY_MEDIUM, fontSize: 16, alignSelf: 'center' }}>
                        Mute notifications
                    </Text>
                    <Switch style={{ alignSelf: 'center' }} />

                </View>

                <View style={{
                    bottom: 0,
                    position: 'absolute', paddingHorizontal: 15
                }}>
                    <TouchableOpacity
                        onPress={() => this.props.clearChat(this.state.thread)}
                        style={styles.clearChatView}>
                        <View style={styles.cardView}>
                            <Image
                                style={styles.binIcon}
                                source={require('../../../res/assets/bin.png')}
                                resizeMode={'contain'}
                            />
                        </View>
                        <Text
                            style={styles.cleartext}
                        >
                            Clear chat
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.setState({ exitDailog: true })}
                        style={styles.clearChatView}>
                        <View style={styles.cardView}>

                            <Image
                                style={styles.exitIcon}
                                source={require('../../../res/assets/exit.png')}
                                resizeMode={'contain'}
                            />
                        </View>
                        <Text
                            style={styles.exitText}
                        >
                            Exit Group
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    Participants = props => {
        return (
            <View style={styles.participantsView}>
                <View
                    style={styles.topView}
                >
                    <View style={{ flexDirection: 'row' }}>
                        {this.state.isAdmin ?
                            <TouchableOpacity
                                onPress={() => this.RBSheet.open()}
                                style={styles.addUserBackground}
                            >
                                <Image
                                    style={{ alignSelf: 'center', width: 25, height: 25 }}
                                    source={require('../../../res/assets/addUser.png')}
                                    resizeMode={'contain'}
                                />
                            </TouchableOpacity> : null}

                        <Text
                            style={styles.addParticipantsText}
                        >
                            {this.state.isAdmin ? 'Add participants' : 'Participants'}
                        </Text>
                    </View>
                    {this.state.participants && this.state.participants.length ?
                        <View style={{
                            backgroundColor: COLOR.PRIMARYBLUE,
                            borderRadius: 5,
                            padding: 5,
                            alignSelf: 'center',
                        }}>
                            <Text
                                style={{
                                    fontSize: 14,
                                    color: COLOR.WHITE,
                                    fontFamily: FONTS.FAMILY_MEDIUM,
                                }}
                            >
                                {this.state.participants.length + ' Participants'}
                            </Text>
                        </View> : null}

                </View>
                <View style={[Styles.hr, { marginTop: 15 }]} />
                <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View
                            style={{
                                flexDirection: 'row',
                                marginVertical: 10,
                            }}
                        >
                            <View
                                style={styles.userBackgroundView}
                            >
                                <Image
                                    source={require('../../../res/assets/plus.png')}
                                    style={{ width: 24, height: 24, alignSelf: 'center' }}
                                    resizeMode={'contain'}
                                />
                            </View>
                            <View style={{ alignSelf: 'center' }}>
                                <Text
                                    style={{ fontSize: 14, fontFamily: FONTS.FAMILY_MEDIUM, color: COLOR.PRIMARYBLACK }}>
                                    You
                                </Text>
                                <Text
                                    style={{
                                        fontSize: 12,
                                        fontFamily: FONTS.FAMILY_REGULAR,
                                        color: COLOR.PRIMARYGREY,
                                    }}
                                >
                                    status
                                </Text>
                            </View>
                        </View>

                    </View>

                </View>

                <FlatList
                    data={this.state.participants}
                    renderItem={({ item }) => (
                        <PartcipantObject
                            onPress={() => {
                                if (this.state.isAdmin && item.isAdmin == false) {
                                    this.setState({
                                        adminDailog: true,
                                        selectedParticipantId: item.id,
                                    })
                                }
                            }}
                            isAdmin={this.state.isAdmin}
                            username={item.firstName + ' ' + item.lastName}
                            image={require('../../../res/assets/user-image.png')}
                            status={item.isAdmin ? 'Admin' : ''}
                            group={true}
                            removeParticipants={() => {
                                this.setState({
                                    removeParticipant: item,
                                    removeDailog: true
                                })
                            }}
                        />
                    )}
                />
                <TouchableOpacity
                    onPress={() => this.setState({ exitDailog: true })}
                    style={styles.clearChatView}
                >
                    <View style={styles.cardView}>
                        <Image
                            style={styles.exitIcon}
                            source={require('../../../res/assets/exit.png')}
                            resizeMode={'contain'}
                        />
                    </View>
                    <Text
                        style={styles.exitText}
                    >
                        Exit Group
                    </Text>
                </TouchableOpacity>
                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    height={heightPercentageToDP(75)}
                    openDuration={100}
                    closeDuration={100}
                    closeOnDragDown="true"
                    dragFromTopOnly="true"
                    customStyles={{
                        draggableIcon: {
                            backgroundColor: "white"
                        }
                    }}
                >
                    <View>
                        <Text style={{ alignSelf: 'center', marginBottom: 20, font: FONTS.FAMILY_SEMIBOLD }}>Add
                            Participants</Text>
                        <SearchBar
                            placeholder="Search a Friend"
                            onChangeText={this.updateSearch}
                            value={this.state.friendsSearch}
                            containerStyle={{ backgroundColor: COLOR.WHITE, }}
                            inputContainerStyle={{ backgroundColor: COLOR.GREY }}
                            inputStyle={{ color: COLOR.BLACK }}
                            round
                        />
                        <FlatList
                            style={{ marginTop: 10 }}
                            data={this.state.friends}
                            renderItem={({ item }) => (
                                <GroupParticipants
                                    item={item}
                                    username={item.user2.firstName + " " + item.user2.lastName}
                                    image={require("../../../res/assets/user-image.png")}
                                    addParticipant={() => this.addParticipant(item)}
                                    selected={
                                        this.state.participants
                                            .map(function (x) {
                                                return x.phone;
                                            })
                                            .indexOf(item.user2.phone) != -1
                                            ? true
                                            : false
                                    }
                                />
                            )}
                            ListEmptyComponent={() => {
                                return (
                                    <Text style={styles.placeholderText}>Select Participants</Text>
                                );
                            }}
                        />


                    </View>
                </RBSheet>


            </View>
        );
    };

    TripsView = props => {
        return (
            <View style={{ backgroundColor: COLOR.WHITE }}>

                <FlatList
                    style={{
                        width: widthPercentageToDP(95),
                        alignSelf: 'center',
                        marginBottom: 10,
                    }}
                    data={this.state.trips}
                    renderItem={({ item }) => (
                        <TripObject
                            onPress={() => {
                                this.props.navigation.navigate('ViewTripScreen', { id: item.id });
                            }}
                            name={item.tripName}
                            image={require('../../../res/assets/city.png')}
                            duration={moment.unix(item.startDate / 1000).format("DD MMM YYYY, ddd") + ' - ' + moment.unix(item.endDate / 1000).format("DD MMM YYYY, ddd")}
                            createdAt={item.time}
                        />
                    )}
                />
            </View>
        );
    };


}

const GroupParticipants = props => {
    return (
        <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View
                    style={{
                        flexDirection: 'row',
                        marginVertical: 10,
                    }}
                >
                    <Image
                        style={{
                            borderRadius: 25,
                            width: 50,
                            height: 50,
                            marginHorizontal: 15,
                        }}
                        source={props.image}
                    />
                    <View style={{ alignSelf: 'center' }}>
                        <Text style={{ fontSize: 14, fontFamily: FONTS.FAMILY_MEDIUM }}>
                            {props.username}
                        </Text>
                    </View>
                </View>
                <View style={{ marginRight: 10, alignSelf: 'center' }}>
                    {props.selected ? null :

                        <Button
                            onPress={props.addParticipant}
                            title='Add'></Button>

                    }
                </View>

            </View>
            <View style={Styles.hr} />
        </View>
    );
}

const styles = StyleSheet.create({
    containerView: {
        flex: 1,
        backgroundColor: COLOR.WHITE
    },
    image_background: {
        backgroundColor: COLOR.PRIMARYBLUE,
        paddingBottom: 15,
        justifyContent: 'center',

    },
    card: {
        width: '90%',
        flexDirection: 'row',
        alignSelf: 'center',
        alignContent: 'center',
        backgroundColor: COLOR.WHITE,
        elevation: 3,
        padding: 15,
        borderRadius: 5,
        shadowColor: COLOR.BLACK,
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 2 },
        marginTop: 15,
    },
    toolbar: {
        marginVertical: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

    },
    backButton: {
        width: 20,
        height: 20,
    },
    editIcon: {
        width: 18,
        height: 18,
        marginRight: 15,
        alignSelf: 'center',
    },
    generalView: {
        backgroundColor: COLOR.WHITE,
        height: '100%'
    },
    descriptionText: {
        fontFamily: FONTS.FAMILY_SEMIBOLD,
        color: COLOR.PRIMARYBLUE,
        fontSize: 16
    },
    descriptionInput: {
        width: '100%',
        height: 40,
        alignSelf: 'center',
        borderWidth: 0.5,
        borderColor: COLOR.PRIMARYGREY,
        borderRadius: 5,
        marginHorizontal: 5,
        padding: 5,
        marginTop: 10,
    },
    clearChatView: {
        width: '90%',
        flexDirection: 'row',
        alignSelf: 'center',
        alignContent: 'center',
        bottom: 0,
        marginTop: 15,
    },
    cardView: {
        alignSelf: 'center', justifyContent: 'center', shadowColor: COLOR.BLACK,
        width: 40,
        height: 40,
        marginRight: 15,
        borderRadius: 25,
        shadowOpacity: 0.2,
        backgroundColor: COLOR.WHITE,
        shadowOffset: { width: 2, height: 2 },
    },
    binIcon: {
        alignSelf: 'center',

        width: 20,
        height: 20,
    },
    exitIcon: {
        alignSelf: 'center',

        width: 20,
        height: 20,
        tintColor: COLOR.RED,
    },
    cleartext: {
        fontFamily: FONTS.FAMILY_REGULAR,
        fontSize: 14,
        alignSelf: 'center',
        color: COLOR.PRIMARYBLUE,
    },
    exitText: {
        fontFamily: FONTS.FAMILY_REGULAR,
        fontSize: 14,
        alignSelf: 'center',
        color: COLOR.RED,
    },
    participantsView: {
        height: '100%',
        backgroundColor: COLOR.WHITE
    },
    topView: {
        flexDirection: 'row',
        paddingHorizontal: 15,
        justifyContent: 'space-between',
    },
    addUserBackground: {
        borderRadius: 25,
        width: 45,
        height: 45,
        alignSelf: 'center',
        justifyContent: 'center',
        backgroundColor: COLOR.PRIMARYBLUE,
    },
    addParticipantsText: {
        marginLeft: 15,
        fontSize: 14,
        fontFamily: FONTS.FAMILY_MEDIUM,
        alignSelf: 'center',
    },
    userBackgroundView: {
        borderRadius: 25,
        width: 45,
        height: 45,
        marginHorizontal: 15,
        backgroundColor: 'rgba(189, 189, 189, 0.5)',
        justifyContent: 'center',
    },
    placeholderText: {
        alignSelf: "center",
        fontSize: 15,
        color: COLOR.SHADOWCOLOR,
        marginVertical: 50,
    },
});
const mapDispatchToProps = (dispatch) => {
    return {
        removeParticipant: (data) => {
            dispatch(ChatAction.removeParticipant(data));
        },
        addParticipant: (data) => {
            dispatch(ChatAction.addParticipants(data))
        },
        getParticipantsByThread: (val) => {
            dispatch(ChatAction.getParticipantsByThread(val));
        },
        getGroups: (val) => {
            dispatch(ChatAction.getGroups(val));
        },
        getFriends: (val) => {
            dispatch(ChatAction.getFriends(val));
        },
        updateGroup: (val, data) => {
            dispatch(ChatAction.updateGroup(val, data))
        },
        setParticipantAsAdmin: (data) => {
            dispatch(ChatAction.setParticipantAsAdmin(data))
        },
        clearChat: (val) => {
            dispatch(ChatAction.clearChat(val));
        },
        getTripByThread: (val) => {
            dispatch(ChatAction.getTripByThread(val))
        },
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        participants: state.chatReducer.participantsByThread,
        friends: state.chatReducer.friends,
        tripByThread: state.chatReducer.tripByThread,
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(GroupDetailsScreen);
