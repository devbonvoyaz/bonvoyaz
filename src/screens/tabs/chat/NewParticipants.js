import React from "react";
import {
  Text,
  FlatList,
  TextInput,
  StyleSheet,
  View,
  Platform,
  Image,
  ScrollView,
} from "react-native";
import Toolbar from "../../../commonView/navView/Toolbar";
import COLOR from "../../../res/styles/Color";
import { widthPercentageToDP } from "react-native-responsive-screen";
import ParticipantsObject from "../../../commonView/listView/ParticipantsObject";
import Contacts from "react-native-contacts";
import API from "../../../api/Api";
import { connect } from "react-redux";
import ChatAction from "../../../redux/actions/ChatAction";
import Auth from "../../../auth";
import { AS_USER_DETAIL } from "../../../api/Constants";
import Dialog from "react-native-dialog";

const auth = new Auth();
const api = new API();
class NewParticipants extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      noData: false,
      user: null,
      navigation: null,
      participant: null,
      friends: [],
      inviteVisibility: false,
      addFriendDialog: false,
      friendId: null,
      theadData: null,
      participantData: null,
      selectFriend: false,
      contactsData:[],
    };
    Contacts.iosEnableNotesUsage(false);
    this.contacts = null;
    this.getUserDetails();
    this.getContacts();
    this.props.getFriends();
  }

  async getUserDetails() {
    const userDetails = await auth.getValue(AS_USER_DETAIL);
    const value = JSON.parse(userDetails);
    this.setState({
      user: value.id,
    });
  }

  getContacts = () => {
    Contacts.getAll()
      .then((contacts) => {
        let object = [];
        for (let i = 0; i < contacts.length; i++) {
          console.log("data is", contacts[i]);
          if (contacts[i].phoneNumbers.length > 0) {
            object.push({
              value: contacts[i].givenName + " " + contacts[i].familyName,
              key: i,
              phone: contacts[i].phoneNumbers[0].number.replace(
                /[^0-9]+/gi,
                ""
              ),
            });
          }
        }
        this.setState({
          data: object,
          contactsData: object,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  componentDidMount() {
    if (this.props.friends) {
      this.setState({
        friends: this.props.friends,
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.threadNavigation !== prevProps.threadNavigation &&
      this.props.threadNavigation !== "undefined"
    ) {
      const threadData = this.props.threadNavigation;
      if (this.state.selectFriend) {
        this.setState({
          selectFriend: false,
        });
      } else {
        this.setState({
          addFriendDialog: true,
          friendId: this.props.registered.id,
        });
      }

      this.props.navigation.navigate("ChatBoxScreen", {
        username: threadData.user.firstName + " " + threadData.user.lastName,
        image: require("../../../res/assets/user-image.png"),
        chat: 0,
        thread: threadData.thread.id,
      });
    }

    if (this.props.friends != prevProps.friends) {
      this.setState({
        friends: this.props.friends,
      });
    }
  }

  searchText = (e) => {
    if (this.state.data != null && this.props.friends != null) {
      let text = e.toLowerCase();
      let contactsTrunk = this.state.data;
      let friendsTrunk = this.state.friends;
      let contactsData = [];
      let friendsData = [];
      contactsTrunk.filter((item) => {
        if (item.value.toLowerCase().match(text)) {
          contactsData.push(item);
        }
      });

      friendsTrunk.filter((item) => {
        if (
          (item.user2.firstName + " " + item.user2.lastName)
            .toLowerCase()
            .match(text)
        ) {
          friendsData.push(item);
        }
      });
      //Contacts
      if (contactsData.length == 0 || e.length == 0) {
        this.setState({
          contactsData: this.state.data,
        });
      } else {
        this.setState({
          contactsData: contactsData,
        });
      }
      //Friends
      if (friendsData.length == 0 || e.length == 0) {
        this.setState({
          friends: this.props.friends,
        });
      } else {
        this.setState({
          friends: friendsData,
        });
      }
    }
  };

  render() {
    return (
      <View style={{ backgroundColor: COLOR.WHITE, flex: 1 }}>
        <View
          style={{
            backgroundColor: COLOR.PRIMARYBLUE,
            paddingTop: Platform.OS == "ios" ? 50 : 15,
            paddingBottom: 20,
          }}
        >
          <Toolbar title={"Add Participant"} color={COLOR.WHITE} />
        </View>
        <View style={styles.searchView}>
          <Image
            style={styles.searchIcon}
            source={require("../../../res/assets/Search.png")}
          />
          <TextInput
            placeholder={"Search Contact"}
            placeholderTextColor={COLOR.PRIMARYGREY}
            onChangeText={this.searchText.bind(this)}
            style={styles.search}
          />
        </View>
        <View style={{ flex: 1 }}>
          <ScrollView>
            <Text style={styles.title}>Friends</Text>
            {this.state.friends == 0 ? (
              <Text style={styles.noFriendsMessage}>
                You have no friends to chat
              </Text>
            ) : (
              <FlatList
                style={{ flex: 1 }}
                data={this.state.friends.sort((a, b) =>(a.user2.firstName+ " "+a.user2.lastName).localeCompare(b.user2.firstName+ " "+b.user2.lastName))}
                renderItem={({ item, index }) => {
                  // console.log(item)
                  return (
                    <ParticipantsObject
                      username={
                        item.user2.firstName + " " + item.user2.lastName
                      }
                      image={require("../../../res/assets/user-image.png")}
                      status={item.user2.phone}
                      onPress={() => {
                        this.setState({ selectFriend: true });
                        this.props.isRegistered(item.user2.phone);
                      }}
                    />
                  );
                }}
              />
            )}
            <Text style={styles.title}>Contacts</Text>
            <FlatList
              data={this.state.contactsData.sort((a, b) => a.value.localeCompare(b.value))}
              style={{ flex: 1 }}
              renderItem={({ item }) => {
                return (
                  <ParticipantsObject
                    username={item.value}
                    image={require("../../../res/assets/user-image.png")}
                    status={item.phone}
                    onPress={() => {
                      this.props.isRegistered(item.phone);
                    }}
                  />
                );
              }}
            />
          </ScrollView>
        </View>
        <Dialog.Container visible={this.state.inviteVisibility}>
          <Dialog.Title>Send Invite</Dialog.Title>
          <Dialog.Description>
            Do you want to send invite to this account?
          </Dialog.Description>
          <Dialog.Button
            label="Cancel"
            onPress={() => this.setState({ inviteVisibility: false })}
          />
          <Dialog.Button
            label="Invite"
            onPress={() => this.setState({ inviteVisibility: false })}
          />
        </Dialog.Container>
        <Dialog.Container visible={this.state.addFriendDialog}>
          <Dialog.Title>Add Friend</Dialog.Title>
          <Dialog.Description>
            Do you want to add this user as a friend?
          </Dialog.Description>
          <Dialog.Button
            label="Cancel"
            onPress={() => {
              this.setState({ addFriendDialog: false });
            }}
          />
          <Dialog.Button
            label="Add"
            onPress={() => {
              this.setState({ addFriendDialog: false });
              this.props.addFriend(this.state.friendId);
            }}
          />
        </Dialog.Container>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchView: {
    width: widthPercentageToDP(90),
    height: 40,
    alignSelf: "center",
    flexDirection: "row",
    backgroundColor: COLOR.GREY,
    borderRadius: 8,
    padding: 5,
    marginVertical: 15,
  },
  search: {
    flex: 1,
    height: 40,
    alignSelf: "center",
    fontSize: 15,
  },
  searchIcon: {
    width: 14,
    height: 14,
    alignSelf: "center",
    marginHorizontal: 10,
  },
  title: {
    color: COLOR.PRIMARYBLUE,
    marginHorizontal: 15,
    marginVertical: 15,
    fontSize: 15,
  },
  noFriendsMessage: {
    color: COLOR.PRIMARYGREY,
    alignSelf: "center",
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    isRegistered: (val) => {
      dispatch(ChatAction.isRegistered(val));
    },
    addFriend: (val) => {
      dispatch(ChatAction.addFriend(val));
    },
    getFriends: (val) => {
      dispatch(ChatAction.getFriends(val));
    },
    clearThreadNavigation: () => {
      dispatch(ChatAction.clearThreadNavigation());
    },
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    registeredContact: state.chatReducer.registeredContact,
    registered: state.chatReducer.isRegistered,
    threadNavigation: state.chatReducer.threadNavigation,
    friends: state.chatReducer.friends,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewParticipants);
