import React from 'react';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import {
  Text,
  View,
  StyleSheet,
  Image,
  FlatList,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AS_USER_DETAIL, U_BASE } from '../../../api/Constants';
import moment from 'moment';
import database from '@react-native-firebase/database';
import Voice, {
  SpeechResultsEvent,
} from '@react-native-voice/voice';
import Auth from '../../../auth';
import AuthenticationAction from '../../../redux/actions/AuthenticationAction';
import ImageProgress from "react-native-image-progress";
import { getLastMessageImage, getLastMessageLabel } from '../../../res/appData';
import { ActivityIndicatorComponent } from 'react-native';

const auth = new Auth();
class ChatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      individual: true,
      group: false,
      participants: [],
      groups: [],
      results: '',
      listening: false,
      initialParticipants: null,
      initialGroups: null,
    }

    Voice.onSpeechResults = this.onSpeechResults;
    this.props.getSystemPreferences();
  }



  async componentDidMount() {
    this.props.setUserOnline();
    //Getting current user id
    const userDetails = JSON.parse(await auth.getValue(AS_USER_DETAIL));
    if (userDetails.user == null) {
      this.setState({ userId: userDetails.id });
      console.log(this.state.userId);
    } else {
      this.setState({ userId: userDetails.user });
      console.log(this.state.userId);
    }


    //Getting threads  
    var threads = [];
    database()
      .ref()
      .child('thread-participants')
      .child(this.state.userId)
      .on('child_added', snapshot => {
        if (snapshot.key != 'id' && snapshot.key != 'threads') {

          threads.unshift(snapshot.val());
          threads.sort((a, b) => {
            let result = (a.details.time > b.details.time) ? -1 : (a.details.time < b.details.time) ? 1 : 0;
            return result;
          });

          this.setState({
            participants: threads,
            initialParticipants: threads,
          });
          console.log('participants ', snapshot.val());
        }

      });


    database()
      .ref()
      .child('thread-participants')
      .child(this.state.userId)
      .on('child_changed', snapshot => {

        if (snapshot.key != 'id' && snapshot.key != 'threads') {
          let participants = this.state.participants;
          let index = participants.map(function (value) {
            return value.details.thread;
          })
            .indexOf(snapshot.val().details.thread);


          participants.splice(index, 1)
          participants.unshift(snapshot.val());
          participants.sort((a, b) => {
            let result = (a.details.time > b.details.time) ? -1 : (a.details.time < b.details.time) ? 1 : 0;
            return result;
          });

          this.setState({
            participants: participants,
            initialParticipants: participants,
          });
        }

      });

    //GROUPS
    var groupThreads = [];
    database()
      .ref()
      .child('thread-participants-group')
      .child(this.state.userId)
      .on('child_added', snapshot => {
        if (snapshot.key != 'id' && snapshot.key != 'threads' && snapshot.key != 'thread') {
          if (snapshot.val().details) {
            groupThreads.unshift(snapshot.val())
            groupThreads.sort((a, b) => {
              let result = (a.details.time > b.details.time) ? -1 : (a.details.time < b.details.time) ? 1 : 0;
              return result;
            });
            this.setState({
              groups: groupThreads,
              initialGroups: groupThreads,
            });
            console.log('groups ', snapshot.val());

          }
        }

      });

    database()
      .ref()
      .child('thread-participants-group')
      .child(this.state.userId)
      .on('child_changed', snapshot => {

        if (snapshot.key != 'id' && snapshot.key != 'thread') {

          let index = this.state.groups.map(function (x) { return x.details ? x.details.thread : null }).indexOf(snapshot.val().details.thread);
          let groups = this.state.groups;
          if (index == -1 && snapshot.val().details) {
            groups.unshift(snapshot.val());
            console.log("CALLED", snapshot.val());
          } else {
            groups[index] = snapshot.val();
          }

          groups.sort((a, b) => {
            let result = (a.details.time > b.details.time) ? -1 : (a.details.time < b.details.time) ? 1 : 0;
            return result;
          });
          this.setState({
            groups: groups,
            initialGroups: groups,
          });
        }

      });

    database()
      .ref()
      .child('thread-participants-group')
      .child(this.state.userId)
      .on('child_removed', snapshot => {

        if (snapshot.key != 'id' && snapshot.key != 'thread') {
          let groups = this.state.groups;
          if (groups != null) {
            let index = groups.map(function (x) { return x.details ? x.details.thread : null }).indexOf(snapshot.val().details.thread);
            groups.splice(index, 1);
            this.setState({
              groups: groups,
              initialGroups: groups,
            });
          } else {
            this.setState({
              groups: groups,
              initialGroups: groups,
            });
          }
        }

      });
  }


  componentWillUnmount() {
    //Participants
    database()
      .ref()
      .child('thread-participants')
      .child(this.state.userId)
      .off('child_added');

    database()
      .ref()
      .child('thread-participants')
      .child(this.state.userId)
      .off('child_changed');

    //Groups
    database()
      .ref()
      .child('thread-participants-group')
      .child(this.state.userId)
      .off('child_added');

    database()
      .ref()
      .child('thread-participants-group')
      .child(this.state.userId)
      .off('child_changed');
    console.log('unmount');

    database()
      .ref()
      .child('thread-participants-group')
      .child(this.state.userId)
      .off('child_removed');



  }

  searchText = (e) => {
    let text = e.toLowerCase()
    let trucks = this.state.individual ? this.state.participants : this.state.groups;
    let data = [];
    trucks.filter((item) => {
      if (this.state.individual) {
        if ((item.details.name).toLowerCase().match(text)) {
          data.push(item)
        }
      } else {
        if (item.details.title) {
          if ((item.details.title).toLowerCase().match(text)) {
            data.push(item)
          }
        }

      }

    })
    if (data.length == 0 || e.length == 0) {
      if (this.state.individual) {
        this.setState({
          participants: this.state.initialParticipants,
        })
      } else {
        this.setState({
          groups: this.state.initialGroups,
        })
      }

    } else {
      if (this.state.individual) {
        this.setState({
          participants: data
        })
      } else {
        this.setState({
          groups: data
        })
      }

    }
  }


  render() {
    return (
      <SafeAreaView style={{ backgroundColor: COLOR.WHITE, flex: 1 }}>

        <View style={styles.toolBar} >
          <View style={styles.toggle_back}>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  group: false,
                  individual: true,
                  results: ''
                })
              }}
              style={{
                flex: 1,
                justifyContent: 'center',
                borderBottomLeftRadius: 2.5,
                borderTopLeftRadius: 2.5,
                backgroundColor:
                  this.state.individual
                    ? COLOR.PRIMARYBLUE
                    : COLOR.WHITE,
              }}
            >

              <Text
                style={{
                  color: this.state.individual ? COLOR.WHITE : COLOR.PRIMARYBLUE,
                  textAlign: 'center',
                  fontFamily: FONTS.FAMILY_REGULAR,
                  fontSize: 14,
                }}
              >
                Individual
              </Text>

            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  group: true,
                  individual: false,
                  results: '',
                })
              }}
              style={{
                flex: 1,
                justifyContent: 'center',
                borderTopRightRadius: 2.5,
                borderBottomRightRadius: 2.5,
                backgroundColor:
                  this.state.group
                    ? COLOR.PRIMARYBLUE
                    : COLOR.WHITE,

              }}
            >
              <Text
                style={{
                  color: this.state.group ? COLOR.WHITE : COLOR.PRIMARYBLUE,
                  textAlign: 'center',
                  fontFamily: FONTS.FAMILY_REGULAR,
                  fontSize: 14,
                }}
              >
                Group
              </Text>
            </TouchableOpacity>

          </View>
          <TouchableOpacity
            style={styles.phoneView}
          >
            <Image
              source={require('../../../res/assets/phone.png')}
              style={styles.phoneIcon}
              resizeMode={'contain'}
            />
          </TouchableOpacity>

        </View>
        <View style={styles.hr} />
        <TouchableOpacity
          onPress={() => {
            this.state.individual ?
              this.props.navigation.navigate('NewParticipants') :
              this.props.navigation.navigate('NewGroupScreen')
          }}
          style={{ alignSelf: 'flex-end', marginRight: 15, marginVertical: 0 }}
        >
          <Text
            style={{ color: COLOR.PRIMARYBLUE, fontFamily: FONTS.FAMILY_MEDIUM }}
          >
            {this.state.individual ? '+ New Participant' : '+ New Group'}
          </Text>
        </TouchableOpacity>
        <View style={styles.search_input}>
          <TextInput
            style={{ width: wp(70) }}
            placeholderTextColor={COLOR.PRIMARYGREY}
            placeholder={'Search'}
            value={this.state.results}
            onChangeText={text => {
              this.setState({ results: text });
              this.searchText(text)
            }}

          />
        </View>
        <View style={styles.hr} />

        <FlatList
          style={{ flex: 1, width: wp(100), alignSelf: 'center' }}
          data={this.state.individual ? this.state.participants : this.state.groups}
          renderItem={({ item }) => {
            return (
              <View style={{ width: wp(90), alignSelf: 'center', marginTop: 10 }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('ChatBoxScreen', {
                      thread: item.details.thread,
                      username:
                        this.state.individual ?
                          item.details.name :
                          item.details.title,
                      image: this.state.individual ? require('../../../res/assets/user-image.png') :
                        item.details.image ? { uri: item.details.image } : require('../../../res/assets/user-image.png'),
                      chat: this.state.individual ? 0 : 1,
                      online: this.state.individual ? item.details.status : null,
                      item: item,
                    });
                  }}
                >
                  <View style={{ flexDirection: 'row', flex: 1 }}>

                    <ImageProgress
                      indicator={ActivityIndicatorComponent}
                      style={{
                        width: 50,
                        height: 50,
                      }}
                      imageStyle={styles.userImage}
                      source={
                        this.state.individual ? require('../../../res/assets/user-image.png') :
                          item.details.image ? { uri: item.details.image } :
                            require('../../../res/assets/user-image.png')
                      }
                    />

                    <View style={{ width: '50%', marginHorizontal: 15 }}>
                      <Text style={styles.username} >
                        {this.state.individual ? item.details.name : item.details.title}
                      </Text>
                      <View style={styles.lastMessageView}>

                        {item.details ? item.details.messageType && item.details.messageType != 1 ?
                          <Image
                            style={styles.lastMessageImage}
                            source={item.details ? item.details.messageType ? getLastMessageImage(item) : null : null}
                          /> : null : null}
                        <Text
                          numberOfLines={1}
                          style={[styles.userStatus, { alignSelf: 'center', textAlign: 'center', height: 20 }]}
                        >
                          {getLastMessageLabel(item)}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text
                        style={styles.time}
                      >
                        {item.details ? moment(item.details.time).isSame(new Date().getTime(), 'day') ? moment(item.details.time).format('hh:mm A') : moment(item.details.time).format('DD/MM/YY') : null}
                      </Text>
                      {this.state.individual
                        ? item.details.status == 1 ?
                          <View
                            style={styles.onlineView}
                          /> : null
                        : null}

                    </View>
                  </View>
                  <View style={styles.hr} />
                </TouchableOpacity>
              </View>
            )
          }}
        />

      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  toggle_back: {
    flex: 1,
    height: 35,
    //width: wp(50),
    flexDirection: 'row',
    borderRadius: 5,
    borderColor: COLOR.PRIMARYBLUE,
    borderWidth: 2,
    alignSelf: 'center',
    marginLeft: 65
  },
  search_input: {
    height: 40,
    marginTop: 12,
    backgroundColor: COLOR.GREY,
    width: wp(90),
    borderRadius: 20,
    alignSelf: 'center',
    paddingStart: 15,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  hr: {
    width: wp(90),
    height: 1,
    backgroundColor: COLOR.GREY,
    marginVertical: 10,
    alignSelf: 'center',
  },
  toolBar: {
    flexDirection: 'row',
    marginVertical: 10,
    backgroundColor: COLOR.WHITE,
    justifyContent: 'space-between',

  },
  chatSettingButton: {
    marginLeft: wp(5),
    marginRight: wp(5),
    color: COLOR.PRIMARYBLUE,
    alignSelf: 'center',
  },
  phoneView: {
    marginLeft: wp(5),
    marginRight: wp(5),
    alignSelf: 'center',
    justifyContent: 'center',
    width: 28,
    height: 28,
    borderRadius: 15,
    backgroundColor: COLOR.PRIMARYBLUE,
  },
  phoneIcon: {
    width: 20,
    height: 20,
    alignSelf: 'center',
  },
  voiceIcon: {
    width: 18,
    height: 20,
    alignSelf: 'center',
    marginHorizontal: 10,
  },
  userImage: {
    width: 50,
    height: 50,
    borderRadius: 30,
  },
  username: {
    fontSize: 14,
    color: COLOR.TEXTCOLOR,
    fontFamily: FONTS.FAMILY_BOLD,
  },
  userStatus: {
    fontSize: 12,
    color: COLOR.TEXTCOLORSECONDARY,
    fontFamily: FONTS.FAMILY_REGULAR,
    marginTop: 3,
  },
  time: {
    textAlign: 'right',
    color: COLOR.TEXTCOLOR,
    marginRight: 8,
    fontSize: 14,
    fontFamily: FONTS.FAMILY_REGULAR,
  },
  onlineView: {
    width: 7,
    height: 7,
    borderRadius: 4,
    backgroundColor: COLOR.PRIMARTGREEN,
    alignSelf: 'flex-end',
    marginVertical: 10,
    marginRight: 30,
  },
  lastMessageImage: {
    width: 15,
    height: 15,
    alignSelf: 'center',
    tintColor: COLOR.PRIMARYGREY,
    marginRight: 10
  },
  lastMessageView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 3
  }
});



const mapDispatchToProps = (dispatch) => {
  return {
    getSystemPreferences: (val) => {
      dispatch(AuthenticationAction.getSystemPreferences(val));
    },
    setUserOnline: (val) => {
      dispatch(AuthenticationAction.setUserOnline(val))
    }
  }
}



export default connect(null, mapDispatchToProps)(ChatScreen);