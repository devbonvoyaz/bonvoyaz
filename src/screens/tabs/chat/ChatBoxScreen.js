import React from 'react';
import {
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {
    Text,
    View,
    Image,
    SafeAreaView,
    TouchableOpacity,
    StyleSheet,
    Platform,
} from 'react-native';
import COLOR from '../../../res/styles/Color';
import Styles from '../../../res/styles/Styles';
import FONTS from '../../../res/styles/Fonts';
import database from '@react-native-firebase/database';
import { connect } from 'react-redux';
import ChatAction from '../../../redux/actions/ChatAction';
import Auth from '../../../auth';
import { AS_USER_DETAIL } from '../../../api/Constants';
import Icon from "react-native-vector-icons/FontAwesome5";
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Message from '../../../commonView/ChatView/Message';
import GroupMessage from '../../../commonView/ChatView/GroupMessage';
import RBSheet from "react-native-raw-bottom-sheet";
import { GiftedChat, Send, Composer } from 'react-native-gifted-chat'
import { hasNotch } from 'react-native-device-info';
import { colors } from '../../../res/appData';
import ImagePicker from "react-native-image-crop-picker";
import API from "../../../api/Api";
import Toast from "react-native-simple-toast";
import { getBottomSpace } from 'react-native-iphone-x-helper';
import ActivityIndicator from "../../../commonView/globalComponent/ActivityIndicator";
import RNLocation from 'react-native-location';
import { error } from "react-native-gifted-chat/lib/utils";
import moment from "moment";
import ActionSheet from 'react-native-actionsheet';
import Clipboard from '@react-native-clipboard/clipboard';
import ImageProgress from "react-native-image-progress";
import { ActivityIndicatorComponent } from 'react-native';



const auth = new Auth();

class ChatBoxScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            chatMessages: [],
            userId: null,
            colorArray: [],
            participantsId: [],
            readRecipient: false,
            online: this.props.route.params.online,
            inputHeight: 35,
            loading: true,
            pollModal: false,
            trips: [],
            tripName: '',
            startDate: '',
            endDate: '',
            selectedText: '',
            selectedMessageId: null,
            selectedMessageType: null,
        }

        this.props.clearThreadNavigation();
        this.username = this.props.route.params.username;
        this.image = this.props.route.params.image;
        this.chat = this.props.route.params.chat;
        this.thread = this.props.route.params.thread;
        this.chatListView = React.createRef();
        this.pollSheet = React.createRef();
        this.props.getTripByThread(this.props.route.params.thread);
        this.ActionSheet = React.createRef();

    }

    componentDidUpdate(prevProps) {
        if (this.props.route.params.online != prevProps.route.params.online) {

            this.setState({
                online: this.props.route.params.online,
            })
        }

        if (this.props.tripByThread != prevProps.tripByThread) {

            if (this.props.tripByThread.length > 0 && this.thread == this.props.tripByThread[0].thread)
                this.setState({
                    trips: this.props.tripByThread,
                    tripName: this.props.tripByThread[0] ? this.props.tripByThread[0].tripName : null,
                    startDate: this.props.tripByThread[0] ? moment.unix(this.props.tripByThread[0].startDate / 1000).format("MMMM DD,YYYY ddd") : null,
                    endDate: this.props.tripByThread[0] ? moment.unix(this.props.tripByThread[0].endDate / 1000).format("MMMM DD,YYYY ddd") : null,
                });

        }
    }


    async componentDidMount() {

        //Read recipient
        if (this.props.systemPreferences) {
            this.setState({
                readRecipient: this.props.systemPreferences.readRecepient
            })
        }

        //Setting up current userID

        const userDetails = JSON.parse(await auth.getValue(AS_USER_DETAIL));
        this.setState({ userId: userDetails.user });
        console.log(this.props.route.params.thread);

        //Online status
        if (this.chat == 0) {
            this.getUserStatus();
        }

        //Set Messsages
        this.getMessages();

        //update Messages
        this.updateMessages();

        database()
            .ref()
            .child('threads')
            .child(this.thread)
            .on('child_removed', snapshot => {
                let messages = this.state.chatMessages;
                if (messages) {
                    let index = messages.map(function (value) {
                        return value._id;
                    }).indexOf(snapshot.key);
                    messages.splice(index, 1);
                    this.setState({
                        chatMessages: messages,
                    });
                }
            })


    }

    componentWillUnmount() {
        database()
            .ref()
            .child('threads')
            .child(this.thread)
            .off('child_added');

        database()
            .ref()
            .child('threads')
            .child(this.thread)
            .off('child_changed');

        database()
            .ref()
            .child('threads')
            .child(this.thread)
            .off('child_removed');

    }

    chooseImage = () => {
        let options = {
            title: 'Select Media',
            mediaType: 'mixed',
        };

        ImagePicker.openPicker(options).then((response) => {

            this.setState({
                actionModalVisible: false,
            });
            if (response.mime == 'video/mp4') {
                this.callUploadMedia(response, 2);
            } else if (response.mime == 'image/jpeg') {
                let iMessage = {
                    user: {
                        _id: this.state.userId,
                        color: COLOR.GREY,
                    },
                    image: response.path,
                    imagePath: response.path,
                }
                let data = this.state.chatMessages;
                data.push(iMessage);
                this.setState({
                    chatMessages: data,
                }, () => {
                    this.chatListView.current.scrollToBottom();
                })

                this.callUploadMedia(response, 1);
            }
            //this.setState({path:response.path});

        });

    };

    callUploadMedia(res, val) {
        var data = new FormData();
        if (val == 1) {
            var media = {
                uri: res.path,
                type: res.type ? res.type : res.mime,
                name: 'imgs.jpg',
            };
        } else {
            var media = {
                uri: res.path,
                type: res.type ? res.type : res.mime,
                name: 'video.mp4',
            };
        }


        data.append('file', media);
        console.log('image uploading data is:-', media);
        const api = new API();
        api.uploadImage(data)
            .then((json) => {


                if (json.status == 200) {
                    console.log("Succes");
                    console.log('upload responce:-', json.data.response.url);
                    if (val == 1) {
                        let index = this.state.chatMessages.map(function (message) {
                            return message.image;
                        }).indexOf(media.uri);
                        let message = this.state.chatMessages[index];
                        message.imageUrl = json.data.response.url;
                        let data = this.state.chatMessages;
                        data[index] = message;
                        this.setState({
                            chatMessages: data,
                        });
                    }

                    this.sendMedia(json.data.response.url, val + 1);

                } else if (json.status == 400) {
                    setTimeout(() => {
                        Toast.show(json.data.message);
                    }, 0);
                } else {
                    setTimeout(() => {
                        Toast.show(json.data.message);
                    }, 0);
                }
            })
            .catch((error) => {
                setTimeout(() => {
                    console.log('json.data.message', error);
                    Toast.show(String(error));
                }, 0);
                console.log('error:-', error);

            });


    }

    permissionHandle = async () => {

        console.log('here')
        let location;

        let permission = await RNLocation.checkPermission({
            ios: 'whenInUse', // or 'always'
            android: {
                detail: 'coarse' // or 'fine'
            }
        });

        console.log('here2')
        console.log(permission)
        if (permission == false) {
            permission = await RNLocation.requestPermission({
                ios: "whenInUse",
                android: {
                    detail: "coarse",
                    rationale: {
                        title: "We need to access your location",
                        message: "We use your location to show where you are on the map",
                        buttonPositive: "OK",
                        buttonNegative: "Cancel"
                    }
                }
            })
            if (permission) {
                try {
                    location = await RNLocation.getLatestLocation({ timeout: 100 })
                    console.log(location, location.longitude, location.latitude,
                        location.timestamp);
                    let message = JSON.stringify({
                        lat: location.latitude,
                        lng: location.longitude,
                    });

                    this.sendLocation(message);
                } catch (e) {
                    console.log(error)
                }

            }
        } else {
            location = await RNLocation.getLatestLocation({ timeout: 100 })
            console.log(location, location.longitude, location.latitude,
                location.timestamp);
            let message = JSON.stringify({
                lat: location.latitude,
                lng: location.longitude,
            });

            this.sendLocation(message);
        }

    }


    getUserStatus() {

        database()
            .ref()
            .child('thread-participants')
            .child(this.state.userId)
            .child(this.thread)
            .on('value', snapshot => {
                this.setState({
                    online: snapshot.val().details.status,
                })
                console.log(snapshot.val())

            })
    }

    getMessages() {
        let array = [];
        database()
            .ref()
            .child('threads')
            .child(this.thread)
            .limitToLast(50)
            .on('child_added', snapshot => {
                let messages = snapshot.val();
                if (
                    snapshot.key != 'createdAt' &&
                    snapshot.key != 'participants' &&
                    snapshot.key != 'id' &&
                    snapshot.key != 'description' &&
                    snapshot.key != 'title' &&
                    snapshot.key != 'message-status'
                ) {
                    var iMessage;
                    if (messages.fields) {
                        var ids = Object.keys(messages.fields).map(function (key) {
                            return key;
                        });
                        console.log(ids);
                        var data = Object.keys(messages.fields).map(function (key) {
                            return messages.fields[key];
                        });

                        data.sort((a, b) => a.order - b.order);
                        console.log(data);

                        var options = [];
                        for (var i = 0; i < data.length; i++) {
                            let object = { option: data[i].label, votes: data[i].value, order: data[i].order, id: ids[i] }
                            options.push(object);
                        }
                        iMessage = {
                            _id: snapshot.key,
                            text: messages.question,
                            createdAt: messages.createdAt,
                            user: {
                                _id: messages.participant,
                                name: messages.participantName,
                            },
                            options: options,
                            image: null,
                            video: null,
                            messageType: 5,
                        };
                    } else {
                        iMessage = {
                            _id: snapshot.key,
                            text: messages.messageType == 1 || messages.messageType == 4 ? messages.message : null,
                            createdAt: messages.createdAt,
                            user: {
                                _id: messages.participant,
                                name: messages.participantName,
                            },
                            image: messages.messageType == 2 ? messages.message : null,
                            video: messages.messageType == 3 ? messages.message : null,
                            messageType: messages.messageType,
                        }
                    }

                    //check if the message is seen or not
                    if (messages.participant == this.state.userId && iMessage.messageType != 5 && iMessage.messageType != 'undefined') {

                        for (var key in messages.status) {
                            console.log("status check", messages.status[key])
                            var value = messages.status[key];
                            if (value == 1) {
                                iMessage.user.color = COLOR.PRIMARYGREY;
                                iMessage.pending = true;
                                break;
                            } else if (value == 2) {
                                iMessage.user.color = COLOR.PRIMARYBLUE;
                                iMessage.sent = true;
                            } else if (value == 3) {
                                iMessage.user.color = COLOR.PRIMARTGREEN;
                                iMessage.received = true;
                            }
                        }
                    } else if (iMessage.messageType != 'undefined' && iMessage.messageType != 5) {
                        console.log('CALLED CALLED Called', iMessage.messageType);
                        const userId = this.state.userId;
                        if (this.state.readRecipient) {
                            database()
                                .ref('threads')
                                .child(this.thread)
                                .child(snapshot.key)
                                .child('status')
                                .update({
                                    [userId]: 3,
                                })
                                .then(() => {
                                });
                        }
                    }


                    //setting color for participant message
                    if (array.length == 0 && messages.participant != this.state.userId) {
                        //If participant array is null and message is not from user
                        iMessage.user.color = colors[0];
                        this.setState({
                            participantsId: [messages.participant],
                        })

                    } else if (this.state.participantsId.indexOf(messages.participant) == -1 && messages.participant != this.state.userId) {
                        //If message is from different user and message is not from user
                        const participantsId = this.state.participantsId;
                        participantsId.push(messages.participant);
                        iMessage.user.color = colors[participantsId.length - 1] ? colors[participantsId.length - 1] : colors[0];
                    } else if (this.state.participantsId.indexOf(messages.participant) != -1 && messages.participant != this.state.userId) {
                        //If message is from same user and message is not from user
                        iMessage.user.color = colors[this.state.participantsId.indexOf(messages.participant)];
                    }

                    if (messages.messageType == 2) {
                        let index = this.state.chatMessages.map(function (value) {
                            return value.imageUrl ? value.imageUrl : null
                        }).indexOf(iMessage.image);
                        if (index != -1) {
                            let data = this.state.chatMessages;
                            let message = data[index];
                            message.image = iMessage.image;
                            message._id = iMessage._id;
                            data[index] = message;
                            this.setState({
                                chatMessages: data,
                            }, () => setTimeout(() => {
                                this.chatListView.current.scrollToBottom()
                            }, 120));
                        } else {
                            if (array.length > 0) {
                                if (array[array.length - 1].createdAt > iMessage.createdAt) {
                                    array.unshift(iMessage);
                                } else {
                                    array.push(iMessage);
                                }
                            } else {
                                array.push(iMessage);
                            }

                            this.setState({
                                chatMessages: array
                            })
                            console.log(iMessage)
                            setTimeout(() => {
                                this.chatListView.current.scrollToBottom()
                            }, 500);
                        }
                    } else {
                        if (array.length > 0) {
                            if (array[array.length - 1].createdAt > iMessage.createdAt) {
                                array.unshift(iMessage);
                            } else {
                                array.push(iMessage);
                            }
                        } else {
                            array.push(iMessage);
                        }
                        this.setState({
                            chatMessages: array
                        })
                        setTimeout(() => {
                            this.chatListView.current.scrollToBottom()
                        }, 500);
                    }
                }
            });
        this.setState({ chatMessages: array, loading: false }, () => {
            if (Platform.OS == 'android') {
                setTimeout(() => {
                    this.chatListView.current.scrollToBottom()
                }, 2500);
            } else {
                setTimeout(() => {
                    this.chatListView.current.scrollToBottom()
                }, 1000)
            }

        });


    }

    updateMessages() {
        database()
            .ref()
            .child('threads')
            .child(this.thread)
            .on('child_changed', snapshot => {
                let messages = snapshot.val();
                var index = -1;
                if (this.state.chatMessages && this.state.chatMessages.length > 0) {
                    index = this.state.chatMessages.map(function (x) {
                        return x._id;
                    }).indexOf(snapshot.key);
                }

                if (index != -1) {
                    console.log("Index called  ", index);
                    if (messages.fields) {

                        var data = Object.keys(messages.fields).map(function (key) {
                            return messages.fields[key];
                        });
                        data.sort((a, b) => a.order - b.order);

                        var options = [];
                        for (var i = 0; i < data.length; i++) {
                            let object = { option: data[i].label, votes: data[i].value, id: data[i].id }
                            options.push(object);
                        }
                        let iMessage = {
                            _id: snapshot.key,
                            text: messages.question,
                            createdAt: messages.createdAt,
                            user: {
                                _id: messages.participant,
                                name: messages.participantName,
                            },
                            options: options,
                            image: null,
                            video: null,
                            messageType: 5,
                        };
                        let chat = this.state.chatMessages;
                        const prevMessage = chat[index];
                        iMessage.user.color = prevMessage.user.color;
                        chat[index] = iMessage;
                        console.log("Update called");
                        this.setState({ chatMessages: chat });
                    } else if (messages.participant == this.state.userId) {

                        for (var key in messages.status) {
                            var value = messages.status[key];
                            let iMessage = {
                                _id: snapshot.key,
                                text: messages.messageType == 1 || messages.messageType == 4 ? messages.message : null,
                                createdAt: messages.createdAt,
                                user: {
                                    _id: messages.participant,
                                    name: messages.participantName,
                                    color: value == 1 ? COLOR.PRIMARYGREY : value == 2 ? COLOR.PRIMARYBLUE : COLOR.PRIMARTGREEN,

                                },
                                image: messages.messageType == 2 ? messages.message : null,
                                video: messages.messageType == 3 ? messages.message : null,
                                messageType: messages.messageType,

                            };

                            if (value == 1) {
                                iMessage.pending = true;
                            } else if (value == 2) {
                                iMessage.sent = true;
                                iMessage.pending = false;
                            } else if (value == 3) {
                                iMessage.sent = false;
                                iMessage.received = true;
                            }

                            if (messages.messageType == 2) {
                                let data = this.state.chatMessages;
                                let message = this.state.chatMessages[index];
                                message.color = iMessage.color;
                                message.pending = iMessage.pending;
                                message.sent = iMessage.sent;
                                message.received = iMessage.received;
                                message.image = iMessage.image;
                                data[index] = message;
                                this.setState({ chatMessages: data });
                            }
                            let data = this.state.chatMessages;
                            data[index] = iMessage;
                            this.setState({ chatMessages: data });
                            if (value == 1) {
                                break;
                            }
                        }
                    }
                }
            });
    }

    sendLocation = (location) => {
        let data = JSON.stringify({
            message: location,
            thread: this.thread,
            messageType: 4,
        });
        this.setState({ message: '' });
        this.props.sendMessage(data);
    }
    sendMessage = (message) => {

        let data = JSON.stringify({
            message: message,
            thread: this.thread,
            messageType: 1,
        });
        this.setState({ message: '' });
        this.props.sendMessage(data);
    }

    sendMedia = (message, type) => {

        let data = JSON.stringify({
            message: message,
            thread: this.thread,
            messageType: type,
        });
        this.setState({ message: '' });
        this.props.sendMessage(data);
    }

    renderSend = (props) => {
        return (
            <Send {...props}>
                <TouchableOpacity style={[styles.circleBackground]}>
                    <Icon name="paper-plane" size={16} style={styles.plusIcon} />
                </TouchableOpacity>
            </Send>
        )
    }

    renderInputToolbar = props => {
        return (
            <View style={styles.bottomBar}>

                <TouchableOpacity onPress={() => this.RBSheet.open()}
                    style={[styles.circleBackground, { marginRight: 10 }]}>
                    <Icon name="plus" size={18} style={styles.plusIcon} />
                </TouchableOpacity>


                <Composer
                    placeholder={'Type a message'}
                    placeholderTextColor={COLOR.PRIMARYGREY}
                    multiline={true}
                    composerHeight={this.state.inputHeight}
                    text={this.state.message}
                    onTextChanged={text => this.setState({ message: text })}
                    textInputStyle={{
                        textAlignVertical: 'center',
                        borderRadius: 10,
                        fontSize: 16,
                        backgroundColor: COLOR.WHITE,
                        paddingVertical: 10,
                        paddingHorizontal: 10,
                        alignSelf: 'center',
                        marginHorizontal: 10,
                        marginVertical: 15,
                    }}
                    onInputSizeChanged={(event) => {
                        if (event.height > 30 && event.height < 60) {
                            this.setState({ inputHeight: event.height })
                        } else if (event.height < 30) {
                            this.setState({
                                inputHeight: 35,
                            })
                        }
                    }}

                />


                <TouchableOpacity onPress={() => this.sendMessage(this.state.message)}
                    style={[styles.circleBackground, { marginLeft: 10 }]}>
                    <Icon name="paper-plane" size={16} style={styles.plusIcon} />
                </TouchableOpacity>

                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    openDuration={100}
                    closeDuration={10}
                    closeOnDragDown="true"
                    dragFromTopOnly="true"
                    customStyles={{
                        draggableIcon: {
                            backgroundColor: "white"
                        }
                    }}
                >
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate('ChatTripScreen', { thread: this.thread });
                                this.RBSheet.close();
                            }}
                            style={styles.multimedia}>
                            <MaterialIcon name="flight" size={30} style={{ marginLeft: 10, marginRight: 30 }} />
                            <Text>Add a Trip</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                this.chooseImage();
                            }}
                            style={styles.multimedia}>
                            <Icon name="image" size={30} style={{ marginLeft: 10, marginRight: 30 }} />
                            <Text>Gallery</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                this.permissionHandle();
                                this.RBSheet.close();
                            }}
                            style={styles.multimedia}>
                            <MaterialIcon name="place" size={30} style={{ marginLeft: 10, marginRight: 30 }} />
                            <Text>Location</Text>
                        </TouchableOpacity>
                        {
                            this.chat == 0 ?
                                null :
                                <TouchableOpacity
                                    onPress={() => {
                                        this.RBSheet.close();
                                        this.props.navigation.navigate('CreatePollScreen', { thread: this.thread })
                                    }}
                                    style={styles.multimedia}>
                                    <MaterialIcon name="equalizer" size={30} style={{ marginLeft: 10, marginRight: 30 }} />
                                    <Text>Create a Poll</Text>
                                </TouchableOpacity>
                        }

                    </View>
                </RBSheet>

            </View>

        )
    }


    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
                <View style={styles.tabView}>
                    <TouchableOpacity style={styles.backButton} onPress={() => {
                        this.props.navigation.goBack();
                    }}>
                        <Icon name="chevron-left" size={18} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            this.chat == 0
                                ? this.props.navigation.navigate('UserDetailsScreen')
                                : this.props.navigation.navigate('GroupDetailsScreen', {
                                    item: this.props.route.params.item,
                                    image: this.props.route.params.image,
                                    userId: this.state.userId
                                });
                        }}
                        style={styles.detailView}
                    >
                        <ImageProgress
                            iindicator={ActivityIndicatorComponent}
                            style={styles.userImage}
                            imageStyle={styles.userImage}
                            source={this.image} />
                        <Text style={styles.username}>
                            {this.username}
                        </Text>
                        {this.chat == 0 && this.state.online == 1
                            ? <View
                                style={styles.onlineView}
                            />
                            : null}

                    </TouchableOpacity>
                </View>

                <View style={Styles.hr} />
                {this.state.trips.length > 0 ?
                    <View style={styles.tripView}>
                        <Text style={styles.tripName}>{this.state.tripName}</Text>
                        <Text style={styles.tripDate}>{this.state.startDate + ' - ' + this.state.endDate}</Text>
                    </View> : null
                }
                <View style={{ flex: 1, marginBottom: 10 }}>
                    <GiftedChat
                        inverted={false}
                        ref={this.chatListView}
                        messages={this.state.chatMessages}
                        onSend={message => this.sendMessage(message)}
                        renderInputToolbar={this.renderInputToolbar}
                        bottomOffset={getBottomSpace()}
                        renderAvatar={null}
                        minInputToolbarHeight={50}
                        scrollToBottom={false}
                        shouldUpdateMessage={(props, nextProps) =>
                            props.currentMessage !== nextProps.currentMessage
                        }
                        renderBubble={(message) => {
                            return (
                                this.chat == 0 ?
                                    <Message
                                        onLongPress={() => {
                                            this.setState({
                                                selectedText: message.currentMessage.text,
                                                selectedMessageId: message.currentMessage._id,
                                                selectedMessageType: message.currentMessage.messageType,
                                            }, () => this.ActionSheet.show())
                                        }}
                                        createdAt={message.currentMessage.createdAt}
                                        messageId={message.currentMessage._id}
                                        user={message.currentMessage.user._id}
                                        userId={this.state.userId}
                                        imagePath={message.currentMessage.imagePath}
                                        image={message.currentMessage.image}
                                        video={message.currentMessage.video}
                                        message={message.currentMessage.text}
                                        color={message.currentMessage.user.color}
                                        messageType={message.currentMessage.messageType}
                                        marginTop={message.previousMessage.user ? message.previousMessage.user._id == message.currentMessage.user._id ? 5 : 20 : 5}
                                    />
                                    :
                                    <GroupMessage
                                        onLongPress={() => {
                                            this.setState({
                                                selectedText: message.currentMessage.text,
                                                selectedMessageId: message.currentMessage._id,
                                                selectedMessageType: message.currentMessage.messageType
                                            }, () => this.ActionSheet.show())
                                        }}
                                        message={message.currentMessage.text}
                                        messageId={message.currentMessage._id}
                                        user={message.currentMessage.user._id}
                                        messageType={message.currentMessage.messageType}
                                        username={message.currentMessage.user.name}
                                        createdAt={message.currentMessage.createdAt}
                                        userId={this.state.userId}
                                        options={message.currentMessage.options ? message.currentMessage.options : null}
                                        messageType={message.currentMessage.messageType}
                                        color={message.currentMessage.user.color}
                                        sameUser={message.previousMessage.user ? message.previousMessage.user._id == message.currentMessage.user._id ? true : false : false}
                                        imagePath={message.currentMessage.imagePath}
                                        image={message.currentMessage.image}
                                        video={message.currentMessage.video}
                                    />
                            )
                        }
                        }
                    /></View>
                <ActivityIndicator loading={this.state.loading} />
                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'Text'}
                    options={
                        this.state.selectedMessageType == 1 ?
                            ['Copy', 'Delete', 'cancel'] : ['Delete', 'cancel']
                    }
                    cancelButtonIndex={2}
                    destructiveButtonIndex={1}
                    onPress={(index) => {
                        switch (index) {
                            case 0:
                                this.state.selectedMessageType == 1 ? Clipboard.setString(this.state.selectedText) : this.props.deleteMessage(this.state.selectedMessageId);
                                break;
                            case 1:
                                this.state.selectedMessageType == 1 ? this.props.deleteMessage(this.state.selectedMessageId) : null;
                                break;
                            case 2:
                                break;
                            default:
                                break;

                        }
                    }}
                />
            </SafeAreaView>
        );
    }

}


const styles = StyleSheet.create({
    tabView: {
        flexDirection: 'row',
        height: 60,
        alignItems: 'center',
        backgroundColor: COLOR.WHITE,

    },
    containerView: {
        flex: 1,
        backgroundColor: COLOR.WHITE,
    },
    backButton: {
        alignSelf: 'center',
        marginLeft: 15
    },
    detailView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 35,

    },
    userImage: {
        width: 40,
        height: 40,
        alignSelf: 'center',
        borderRadius: 30,
        marginRight: 15,
    },
    username: {
        fontSize: 14,
        alignSelf: 'center',
        color: COLOR.TEXTCOLOR,
        fontFamily: FONTS.FAMILY_BOLD,
    },
    onlineView: {
        width: 7,
        height: 7,
        borderRadius: 4,
        backgroundColor: COLOR.PRIMARTGREEN,
        alignSelf: 'center',
        marginLeft: 10,
        marginTop: 2,
    },
    touchableHighlight: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: 'transparent',
    },
    buttonImageBackground: {
        width: 35,
        height: 35,
        marginVertical: 5,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    buttonImage: {
        alignSelf: 'center',
        width: 20,
        height: 20
    },
    textInputView: {
        width: wp(70),
        marginVertical: 15,
        borderColor: COLOR.GREY,
        borderRadius: 10,
        borderWidth: 0.5,
        justifyContent: 'center',


    },
    sendButtonBackground: {
        width: 30,
        height: 30,
        marginHorizontal: 10,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    sendIcon: { alignSelf: 'center', width: 15, height: 15 },
    bottomBar: {
        width: wp(100),
        minHeight: 50,
        backgroundColor: COLOR.GREY,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: Platform.OS == "ios" && hasNotch ? 40 : 0,


    },
    circleBackground: {
        backgroundColor: COLOR.PRIMARYBLUE,
        borderRadius: 30,
        height: 30,
        width: 30,
        alignSelf: 'center',
        justifyContent: "center",
        marginHorizontal: 15,
        marginVertical: 10,

    },
    plusIcon: {
        alignSelf: "center",
        color: COLOR.WHITE,
    },
    multimedia: {
        padding: 10,
        flexDirection: "row",
        alignContent: "center",
        alignItems: "center"
    },
    tripView: {
        backgroundColor: COLOR.PRIMARTGREEN,
        padding: 5,
        width: wp(90),
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    tripName: {
        fontSize: 12,
        color: COLOR.WHITE,
        alignSelf: 'center',
        flex: 1,
        textAlign: 'center'
    },
    tripDate: {
        fontSize: 12,
        color: COLOR.WHITE,
        alignSelf: 'center',
        textAlign: 'center',
        marginLeft: 5
    }

})

const mapDispatchToProps = (dispatch) => {
    return {
        sendMessage: (data) => {
            dispatch(ChatAction.sendMessage(data))
        },
        clearThreadNavigation: (val) => {
            dispatch(ChatAction.clearThreadNavigation(val));
        },
        getTripByThread: (val) => {
            dispatch(ChatAction.getTripByThread(val))
        },
        deleteMessage: (val) => {
            dispatch(ChatAction.deleteMessage(val))
        }

    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        systemPreferences: state.reducer.systemPreferences,
        tripByThread: state.chatReducer.tripByThread,
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatBoxScreen);