import React from 'react';
import { TouchableOpacity, View, Image, Text, SafeAreaView, TextInput, StyleSheet, FlatList, ScrollView, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { connect } from 'react-redux';
import SelectFriendsList from '../../../commonView/listView/SelectFriendsList';
import ChatAction from '../../../redux/actions/ChatAction';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Styles from '../../../res/styles/Styles';
import Toast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-crop-picker';
import Icon from "react-native-vector-icons/FontAwesome5";
import ActionSheet from '../../../commonView/globalComponent/ActionSheet';
import API from '../../../api/Api';
import ActivityIndicator from '../../../commonView/globalComponent/ActivityIndicator';

class NewGroupScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      friends: null,
      selectedParticipants: [],
      name: '',
      image: null,
      actionModalVisible: false,
      path: null,
      groupDetails: null,
      loading: false,
    }

    this.chooseImage = this.chooseImage.bind(this);
    this.handleSheet = this.handleSheet.bind(this);
    this.props.getFriends();

  }

  handleSheet = (val) => {
    if (val == 1) {
      this.chooseImage(1);
    } else if (val == 2) {
      this.chooseImage(2);
    } else if (val == 3) {
      this.setState({
        actionModalVisible: false,
      });
    }
  };

  chooseImage = (val) => {
    let options = {
      title: 'Select Image',
      mediaType: 'photo',
    };

    if (val == 1) {
      ImagePicker.openCamera({
        title: 'Select Image',
        width: 1024,
        height: 472,
        compressImageMaxWidth: 1024,
        compressImageMaxHeight: 472,
      }).then((response) => {
        console.log('image local reposnse', response);
        this.setState({
          actionModalVisible: false,
        });
        this.callUploadImage(response, val);
        this.setState({ path: response.path });

      });
    } else if (val == 2) {
      ImagePicker.openPicker(options).then((response) => {
        console.log('image local reposnse', response);
        this.setState({
          actionModalVisible: false,
        });

        this.callUploadImage(response, val);

        this.setState({ path: response.path });

      });
    }
  };

  callUploadImage(res, val) {
    this.setState({
      loading: true,
    })
    var data = new FormData();
    var photo = {
      uri: res.path,
      type: res.type ? res.type : res.mime,
      name: 'imgs.jpg',
    };

    data.append('file', photo);
    console.log('image uploading data is:-', photo);
    const api = new API();
    api.uploadImage(data)
      .then((json) => {


        if (json.status == 200) {
          console.log("Succes");
          console.log('upload responce:-', json.data.response.url);
          this.setState({
            image: json.data.response.url,
            loading: false,
          });


        } else if (json.status == 400) {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        } else {
          setTimeout(() => {
            Toast.show(json.data.message);
          }, 0);
        }
      })
      .catch((error) => {
        setTimeout(() => {
          console.log('json.data.message', error);
          Toast.show(String(error));
        }, 0);
        console.log('error:-', error);

      });


  }

  addParticipant = (val) => {
    console.log("add ", val);
    const array = this.state.selectedParticipants;
    array.push(val);
    this.setState({
      selectedParticipants: array,
    })
  }

  removeParticipant = (val) => {
    console.log("remove ", val);
    const array = this.state.selectedParticipants;

    const index = array.map(function (x) { return x.user2.phone }).indexOf(val.user2.phone);
    array.splice(index, 1);
    this.setState({
      selectedParticipants: array,
    })
  }



  handleCreateGroup = () => {
    let userIds = [];
    const selectedParticipants = this.state.selectedParticipants;
    if (this.state.name != '' || this.state.name != null) {
      for (var i = 0; i < selectedParticipants.length; i++) {
        userIds.push(selectedParticipants[i].user2.id);
      }

      let data = JSON.stringify({
        users: userIds,
        title: this.state.name,
        description: 'New Group',
        image: this.state.image,
      });

      this.props.createGroup(data);
      this.props.navigation.goBack();

    } else {
      Toast.show('Enter Group Name');
    }
  }


  componentDidMount(props) {
    if (this.props.friends) {
      this.setState({
        friends: this.props.friends
      })
    }


  }

  componentWillReceiveProps(props) {
    if (this.props.friends != props.friends) {
      this.setState({
        friends: props.friends,
      });


    }
  }

  searchText = (e) => {
    let text = e.toLowerCase()
    let trucks = this.props.friends
    let data = [];
    trucks.filter((item) => {
      if ((item.user2.firstName + ' ' + item.user2.lastName).toLowerCase().match(text)) {
        data.push(item)
      }

    })
    if (data.length == 0 || e.length == 0) {
      this.setState({
        friends: this.props.friends,
      })
    } else {
      this.setState({
        friends: data
      })
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.WHITE }}>

        <ScrollView
          showsVerticalScrollIndicator={false}
        >
          <LinearGradient
            style={{ width: '100%', paddingTop: Platform.OS === 'ios' ? 50 : null }}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={['rgba(45, 156, 219, 1)',
              'rgba(6, 139, 215, 0.89)',
              'rgba(0, 69, 151, 0.8)',
            ]}
          >
            <View
              style={{
                flexDirection: 'row',
                padding: 15,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.goBack();
                }}
              >
                <Icon name="chevron-left" size={18} color={COLOR.WHITE} />

              </TouchableOpacity>

              <Text style={[Styles.screen_heading, { color: COLOR.WHITE }]}>
                {this.state.groupDetails ? 'Edit Group' : 'New Group'}
              </Text>
              <TouchableOpacity
                onPress={this.handleCreateGroup}
              >
                <Text
                  style={{
                    color: COLOR.WHITE,
                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                  }}
                >
                  Save
                </Text>
              </TouchableOpacity>
            </View>
          </LinearGradient>
          <View style={{ backgroundColor: COLOR.GREY }}>
            <TouchableOpacity
              style={styles.cameraTouchable}
              onPress={() => this.setState({ actionModalVisible: true })}
            >
              <Image
                style={{
                  width: this.state.path ? 65 : 28,
                  height: this.state.path ? 65 : 28,
                  alignSelf: 'center',
                  borderRadius: this.state.path ? 40 : null
                }}
                source={this.state.path == null ? require('../../../res/assets/cameraLinear.png') : { uri: this.state.path }}
                resizeMode={'contain'}
              />
            </TouchableOpacity>
            <TextInput
              style={styles.textInput}
              placeholder={'Group name'}
              placeholderTextColor={COLOR.PRIMARYGREY}
              value={this.state.name}
              onChangeText={text => this.setState({ name: text })}
            />
          </View>
          <View style={{ backgroundColor: COLOR.WHITE, width: '100%', height: '100%', flex: 1 }}>
            <View style={styles.card}>
              <Text style={{ fontFamily: FONTS.FAMILY_MEDIUM, color: COLOR.PRIMARYBLUE }}>
                {'Selected Participants (' + this.state.selectedParticipants.length + ')'}
              </Text>
              <FlatList
                style={{ marginTop: 15 }}
                horizontal={true}
                data={this.state.selectedParticipants}
                renderItem={({ item }) => (
                  <SelectedFriends
                    username={item.user2.firstName + ' ' + item.user2.lastName}
                    image={require('../../../res/assets/user-image.png')}
                  />
                )}
              />
            </View>
            <View style={styles.search_input}>
              <Image
                style={{ width: 14, height: 14, alignSelf: 'center' }}
                source={require('../../../res/assets/Search.png')}

              />
              <TextInput
                placeholderTextColor={COLOR.PRIMARYGREY}
                placeholder={'Search'}
                onChangeText={this.searchText.bind(this)}
              />
            </View>
            <Text
              style={{
                fontFamily: FONTS.FAMILY_MEDIUM, color: COLOR.PRIMARYBLUE, marginVertical: 15, marginLeft: 20
              }}
            >Friends</Text>
            <FlatList
              data={this.state.friends}
              renderItem={({ item, index }) => {

                return (
                  <SelectFriendsList
                    item={item}
                    username={item.user2.firstName + ' ' + item.user2.lastName}
                    image={require('../../../res/assets/user-image.png')}
                    addParticipant={() => this.addParticipant(item)}
                    removeParticipant={() => this.removeParticipant(item)}
                    selected={this.state.selectedParticipants.map(function (x) { return x.user2.phone }).indexOf(item.user2.phone) != -1 ? true : false}
                  />
                )
              }}
            />
          </View>
        </ScrollView>
        <ActivityIndicator loading={this.state.loading} />
        <ActionSheet
          modalVisible={this.state.actionModalVisible}
          handleSheet={this.handleSheet}
        />
      </View>
    );
  }
}

const SelectedFriends = props => {
  return (
    <View style={{ width: 55, marginHorizontal: 10 }}>
      <View style={styles.profile_view}>

        <Image
          source={props.image}
          style={styles.profile_pic}
        />
        <View style={styles.icon}>
          <Image
            style={{ alignSelf: 'center', width: 5, height: 5 }}
            source={require('../../../res/assets/removeicon.png')}
            resizeMode={'contain'}
          />
        </View>

      </View>
      <Text style={{ fontSize: 10, alignSelf: 'center', marginTop: 5 }}>{props.username}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    borderRadius: 5,
    backgroundColor: COLOR.WHITE,
    elevation: 5,
    padding: 10,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    shadowOffset: { width: 2, height: 2 },
    marginTop: 15,
  },
  search_input: {
    height: 40,
    marginTop: 20,
    flexDirection: 'row',
    backgroundColor: COLOR.GREY,
    width: widthPercentageToDP(90),
    borderRadius: 7,
    alignSelf: 'center',
    paddingStart: 15,
  },
  icon: {
    padding: 2,
    minHeight: 12,
    minWidth: 12,
    alignSelf: 'flex-end',
    borderRadius: 6,
    justifyContent: 'center',
    backgroundColor: COLOR.PRIMARTGREEN,
    position: 'absolute',
  },
  profile_pic: {
    width: 50,
    height: 50,
    borderRadius: 25,
    alignSelf: 'center',
  },
  profile_view: {
    alignSelf: 'center',
    flexDirection: 'row-reverse',
  },
  textInput: {
    width: widthPercentageToDP(70),
    borderRadius: 5,
    borderWidth: 1,
    paddingLeft: 10,
    height: 40,
    borderColor: COLOR.PRIMARYBLUE,
    backgroundColor: COLOR.WHITE,
    alignSelf: 'center',
    marginVertical: 10,
  },
  cameraTouchable: {
    width: 65
    , height: 65
    , borderRadius: 35, alignSelf: 'center'
    , marginVertical: 15
    , backgroundColor: 'rgba(0, 0, 0, 0.04)',
    justifyContent: 'center'
  },

});

const mapDispatchToProps = (dispatch) => {
  return {
    getFriends: (val) => {
      dispatch(ChatAction.getFriends(val));
    },
    createGroup: (data) => {
      dispatch(ChatAction.createGroup(data))
    },
    updateGroup: (val, data) => {
      dispatch(ChatAction.updateGroup(val, data))
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {

    friends: state.chatReducer.friends,
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewGroupScreen);