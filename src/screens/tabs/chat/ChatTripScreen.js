import React from 'react';
import {  View, SafeAreaView, StyleSheet } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import COLOR from '../../../res/styles/Color';
import Styles from '../../../res/styles/Styles';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import TripObject from '../../../commonView/TripView/TripObject';
import {connect} from "react-redux";
import TripAction from "../../../redux/actions/TripAction";
import moment from "moment";
import Toolbar from "../../../commonView/navView/Toolbar";
import ChatAction from "../../../redux/actions/ChatAction";


class ChatTripScreen extends React.Component{
    constructor(props) {
        super(props);
        this.state ={
            tabIndex:0,
            tripsData:[],
            thread:this.props.route.params.thread,
        }
        this.props.getAllTrips(2);
    }

    componentDidMount() {
        if (this.props.tripsData){
            this.setState({
                tripsData:this.props.tripsData,
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.tripsData !=this.props.tripsData){
            this.setState({
                tripsData:this.props.tripsData,
            })
        }

    }

    addTripToThread=(tripId)=>{
        let data = JSON.stringify({
            thread:this.state.thread,
            trip: tripId,
        });
        this.props.addTripToThread(data);
        this.props.navigation.goBack();
        setTimeout(()=>{
            this.props.getTripByThread(this.state.thread);
        },500);
    }


    handleTabsChange = index => {
        this.setState({
            tabIndex:index,
        })
    };

    render() {
        return (
            <SafeAreaView style={styles.containerView}>
                <Toolbar title={'Add Trip'}/>
                <View style={Styles.hr}/>

                <FlatList
                    style={{width: widthPercentageToDP(95), alignSelf: 'center',marginTop:20}}
                    data={this.state.tripsData}
                    renderItem={({item}) => (
                        <TripObject
                            onPress={() => {
                                this.addTripToThread(item.id);
                            }}
                            name={item.tripName}
                            image={require('../../../res/assets/city.png')}
                            duration={moment.unix(item.startDate/1000).format("MMMM DD,YYYY ddd") + ' - '+moment.unix(item.endDate/1000).format("MMMM DD,YYYY ddd")}
                            createdAt={moment.unix(item.createdAt/1000).format("MMMM DD,YYYY")}
                        />
                    )}
                />


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    toolbar:{
        flexDirection: 'row',
        marginVertical: 12,
        alignItems: 'center',
    },
    containerView:{
        flex: 1,
        backgroundColor: COLOR.WHITE },
    titleView:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    }
})

const mapStateToProps = (state,ownProps)=>{
    return {
        tripsData:state.tripReducer.tripsData,

    }
}

const mapDispatchToProps = (dispatch)=>{
    return{
        getAllTrips:(val)=>{
            dispatch(TripAction.getAllTrips(val));
        },
        addTripToThread:(data)=>{
            dispatch(ChatAction.addTripToThread(data));
        },
        getTripByThread:(val)=>{
            dispatch(ChatAction.getTripByThread(val))
        }
    }
}



export default connect(mapStateToProps,mapDispatchToProps)(ChatTripScreen);