import React from 'react';
import {FlatList, Image, SafeAreaView, StyleSheet, Text, TextInput, View, TouchableOpacity, Modal} from "react-native";
import Toolbar from "../../../commonView/navView/Toolbar";
import Styles from "../../../res/styles/Styles";
import COLOR from "../../../res/styles/Color";
import {widthPercentageToDP as wp, widthPercentageToDP} from "react-native-responsive-screen";
import SimpleToast from "react-native-simple-toast";
import Toolbar2 from "../../../commonView/navView/Toolbar2";
import {connect} from "react-redux";
import ChatAction from "../../../redux/actions/ChatAction";

class CreatePollScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            question: '',
            options: [{option: ''}, {option: ''},],
            thread: this.props.route.params.thread
        }
    }

    createPoll() {
        if (this.state.options.length < 2) {
            SimpleToast.show("Options should be more than 2")
        }else if(this.state.question == ''){
            SimpleToast.show("Please Enter the question")
        }else{
            let options = this.state.options.map(function (x) {
                return x.option;
            });
            let data = JSON.stringify({
                thread: this.state.thread,
                messageType:5,
                message: this.state.question,
                fields: options,
            });
            this.props.sendMessage(data);
            this.props.navigation.goBack();
        }

    }

    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: COLOR.WHITE}}>

                <Toolbar title={"Create Poll"}/>
                <View style={Styles.hr}/>
                <Text style={styles.title}>Ask Your Question</Text>
                <TextInput
                    style={styles.textInput}
                    multiline={true}
                    value={this.state.question}
                    onChangeText={text => this.setState({question:text})}
                    placeholder={'Not more than 190 words'}
                    maxLength={190}
                    placeholderTextColor={COLOR.PRIMARYGREY}
                />
                <Text style={styles.title}>Add Options</Text>
                <FlatList
                    style={{alignSelf: 'center', maxHeight: 250,}}
                    data={this.state.options}
                    renderItem={({item, index}) => (
                        <View style={styles.optionView}>
                            <View style={styles.deleteOption}/>
                            <TextInput
                                value={item.option}
                                placeholder={'Option '+(index+1)}
                                placeholderTextColor={COLOR.PRIMARYGREY}
                                onChangeText={text => {
                                    let data = this.state.options;
                                    data[index].option = text;
                                    this.setState({options: data})

                                }

                                }
                                style={styles.optionText}/>
                            <TouchableOpacity
                                style={styles.deleteOption}
                                onPress={() => {
                                    let data = this.state.options;
                                    data.splice(index, 1);
                                    this.setState({
                                        options: data
                                    })
                                }}
                            >
                                <Image
                                    style={{width: 15, height: 15}}
                                    source={require('../../../res/assets/deleteoption.png')}/>
                            </TouchableOpacity>
                        </View>
                    )
                    }
                />
                <TouchableOpacity
                    onPress={() => {
                        let data = this.state.options;
                        if (data.length < 4) {
                            let element = {option: ""};
                            data.push(element);
                            this.setState({options: data});
                        } else {
                            SimpleToast.show('Cannot add options more than 4')
                        }

                    }}
                    style={styles.addOptionButton}>
                    <Text style={{color: COLOR.WHITE, alignSelf: 'center'}}>+ Add Option</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.postButton}
                    onPress={() => this.createPoll()}
                >
                    <Text style={{color: COLOR.WHITE, alignSelf: 'center'}}>POST</Text>
                </TouchableOpacity>
            </SafeAreaView>


        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.WHITE,
    },
    toolbar: {
        flexDirection: 'row',
        marginVertical: 12,
        alignItems: 'center',
    },
    titleView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 15,
        margin: 20,
    },
    textInput: {
        width: widthPercentageToDP(90),
        alignSelf: 'center',
        padding: 5,
        borderWidth: 1,
        borderColor: COLOR.GREY,
        borderRadius: 3,
        height: 80,
    },
    optionView: {
        width: widthPercentageToDP(90),
        height: 40,
        borderRadius: 3,
        marginVertical: 5,
        borderWidth: 1,
        borderColor: COLOR.PRIMARYGREY,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    deleteOption: {
        width: 15,
        height: 15,
        alignSelf: 'center',
        marginHorizontal: 10,
    },
    optionText: {
        fontSize: 15,
        alignSelf: 'center',
        flex:1,
        textAlign:'center'
    },
    addOptionButton: {
        alignSelf: 'center',
        width: 100,
        height: 30,
        backgroundColor: COLOR.PRIMARYBLUE,
        borderRadius: 5,
        justifyContent: 'center'
    },
    modal: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
    },
    modalBody: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: COLOR.WHITE,
        borderWidth: 1,
        width: wp(100),
        marginTop: 50,
        borderColor: COLOR.BORDER,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,

    },
    postButton: {
        justifyContent: 'center',
        width: wp(90),
        alignSelf: 'center',
        backgroundColor: COLOR.PRIMARYBLUE,
        borderRadius: 5,
        height: 40,
        position:'absolute',
        bottom:40,
    }
})

const mapDispatchToProps = dispatch => {
    return {
        sendMessage: (data) => {
            dispatch(ChatAction.sendMessage(data))
        }
    }
}

export default connect(null, mapDispatchToProps)(CreatePollScreen);