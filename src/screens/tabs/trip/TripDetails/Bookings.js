import React from 'react';
import {
    FlatList,
    Image,
    StyleSheet,
    Text,
    View, TouchableOpacity, ScrollView
} from 'react-native';

import { widthPercentageToDP, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import COLOR from '../../../../res/styles/Color';
import FONTS from '../../../../res/styles/Fonts';
import ActionButton from 'react-native-action-button';
import { connect } from 'react-redux';
import TripAction from '../../../../redux/actions/TripAction';
import moment from 'moment';
import RBSheet from 'react-native-raw-bottom-sheet';
import FlightBookingModal from '../bookingModals/FlightBookingModal';
import HotelBookingModal from '../bookingModals/HotelBookingModal';
import CarBookingModal from '../bookingModals/CarBookingModal';

class Bookings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hotelBookings: [],
            carRentals: [],
            flightBooking: [],
            noBooking: true
        }
        this.flightRBSheet = React.createRef();
        this.hotelRBSheet = React.createRef();
        this.carRBSheet = React.createRef();
        this.props.getBooking(this.props.trip);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.booking != this.props.booking) {
            this.setState({
                hotelBookings: this.props.booking.hotelBookings,
                carRentals: this.props.booking.carRentals,
                flightBooking: this.props.booking.FlightBooking,
            }, () => {
                if (this.state.hotelBookings.length > 0 || this.state.carRentals.length > 0 && this.state.flightBooking.length > 0) {
                    this.setState({
                        noBooking: false,
                    })
                }
            });
            console.log('Boooking');
        }

    }



    render() {
        return (
            <View style={{ flexGrow: 1, flex: 1, backgroundColor: COLOR.WHITE }}>
                {this.state.noBooking ?
                    <this.noBooking />
                    :
                    <View style={{ flex: 1 }}>
                        <ScrollView>
                            <View style={styles.titleView}>
                                <Text style={styles.titleText}>
                                    Flight Booking
                                </Text>

                            </View>
                            <FlatList
                                data={this.state.flightBooking}
                                style={styles.flatList}
                                renderItem={({ item }) => (
                                    <View style={styles.objectView}>
                                        <Image
                                            source={require('../../../../res/assets/airlines.png')}
                                            style={styles.imageView}
                                            resizeMode={'contain'}
                                        />
                                        <View style={{ flex: 1, marginHorizontal: 10, justifyContent: 'space-evenly' }}>
                                            <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row' }}>

                                                <Text>{item.name}</Text>
                                                <Text>{moment.unix(item.date / 1000).format("MMMM DD,YY")}</Text>

                                            </View>
                                            <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row' }}>
                                                <View style={{ alignSelf: 'center', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>

                                                    <Text>{item.destination}</Text>
                                                    <Image
                                                        source={require('../../../../res/assets/plane-line.png')}
                                                        style={{
                                                            width: 15,
                                                            height: 15,
                                                            marginHorizontal: 10,
                                                        }}
                                                        resizeMode={'contain'}
                                                    />
                                                    <Text>{item.arrival}</Text>
                                                </View>
                                                <Text>{item.confirmationNumber}</Text>

                                            </View>
                                        </View>
                                    </View>
                                )}
                            />
                            <View style={styles.titleView}>
                                <Text style={styles.titleText}>
                                    Car Booking
                                </Text>
                            </View>
                            <FlatList
                                data={this.state.carRentals}
                                style={styles.flatList}
                                renderItem={({ item }) => (
                                    <View style={styles.objectView}>
                                        <Image
                                            source={require('../../../../res/assets/airlines.png')}
                                            style={styles.imageView}
                                            resizeMode={'contain'}
                                        />
                                        <View style={{ flex: 1, marginHorizontal: 10, justifyContent: 'space-evenly' }}>
                                            <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row' }}>

                                                <Text>{item.pickupLocation}</Text>
                                                <Text>{moment.unix(item.startDate / 1000).format("MMMM DD,YY")}</Text>

                                            </View>
                                            <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row' }}>

                                                <Text>{item.dropLocation}</Text>

                                                <Text>{item.confirmationNumber}</Text>

                                            </View>
                                        </View>
                                    </View>
                                )}
                            />
                            <View style={styles.titleView}>
                                <Text style={styles.titleText}>
                                    Hotel Booking
                                </Text>
                            </View>
                            <FlatList
                                data={this.state.hotelBookings}
                                style={styles.flatList}
                                renderItem={({ item }) => (
                                    <View style={styles.objectView}>
                                        <Image
                                            source={require('../../../../res/assets/tokyo.png')}
                                            style={styles.imageView}
                                            resizeMode={'contain'}
                                        />
                                        <View style={{ flex: 1, marginHorizontal: 10, marginVertical: 5, justifyContent: 'space-evenly' }}>

                                            <Text>{item.name}</Text>
                                            <Text>{''}</Text>


                                            <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row' }}>

                                                <Text>{moment.unix(item.checkIn / 1000).format("MMMM DD,YY")}</Text>

                                                <Text>{moment.unix(item.checkOut / 1000).format("MMMM DD,YY")}</Text>

                                            </View>
                                        </View>
                                    </View>
                                )}
                            />
                        </ScrollView>
                        <ActionButton
                            buttonColor={COLOR.PRIMARYBLUE}
                            onPress={() => {
                                this.RBSheet.open();
                            }}
                        />
                    </View>
                }
                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    openDuration={100}
                    closeDuration={10}
                    closeOnDragDown="true"
                    dragFromTopOnly="true"
                    customStyles={{
                        draggableIcon: {
                            backgroundColor: COLOR.BLACK
                        }
                    }}
                >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>

                        <TouchableOpacity onPress={() => this.flightRBSheet.current.open()} style={{ flex: 1 }}>
                            <View style={[styles.option_view, { backgroundColor: '#6869E5', }]}>
                                <Image
                                    source={require('../../../../res/assets/flight.png')}
                                    style={{ alignSelf: 'center', width: 24, height: 24 }}
                                />
                            </View>
                            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center' }} >
                                Flight
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.hotelRBSheet.current.open()} style={{ flex: 1 }} >
                            <View style={[styles.option_view, { backgroundColor: '#FF585D' }]}>
                                <Image
                                    source={require('../../../../res/assets/hotel.png')}
                                    style={{ alignSelf: 'center', width: 24, height: 24 }}
                                />
                            </View>
                            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center' }} >
                                Lodging
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.carRBSheet.current.open()} style={{ flex: 1 }}>
                            <View style={[styles.option_view, { backgroundColor: '#01CAE4' }]}>
                                <Image
                                    source={require('../../../../res/assets/drive.png')}
                                    style={{ alignSelf: 'center', width: 24, height: 24 }}
                                />
                            </View>
                            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center' }}  >
                                Car Rental
                            </Text>
                        </TouchableOpacity>

                        <FlightBookingModal
                            trip={this.props.trip}
                            navigation={this.props.navigation}
                            flightRBSheet={this.flightRBSheet}
                        />
                        <HotelBookingModal
                            trip={this.props.trip}
                            navigation={this.props.navigation}
                            hotelRBSheet={this.hotelRBSheet} />
                        <CarBookingModal
                            trip={this.props.trip}
                            navigation={this.props.navigation}
                            carRBSheet={this.carRBSheet} />
                    </View>

                </RBSheet>

            </View>
        );
    }
    noBooking = () => {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                    source={require('../../../../res/assets/nobooking.png')}
                    resizeMode='contain'
                    style={{
                        width: widthPercentageToDP(80),
                        alignSelf: 'center',
                        height: 150,
                    }} />
                <Text
                    style={{
                        fontSize: 16,
                        fontFamily: FONTS.FAMILY_BOLD,
                        marginVertical: 5,
                    }}
                >
                    You have no bookings to show</Text>
                <TouchableOpacity
                    onPress={() => this.RBSheet.open()}
                    style={{
                        width: widthPercentageToDP(65),
                        height: 40,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: COLOR.PRIMARYBLUE,
                        borderRadius: 5,
                        marginVertical: 5
                    }}>
                    <Text style={{ color: COLOR.WHITE }}>Add Booking</Text>
                </TouchableOpacity>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    titleView: {
        width: wp(90),
        backgroundColor: COLOR.PRIMARYBLUE,
        height: 30,
        borderRadius: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        marginVertical: 5,
    },
    titleText: {
        color: COLOR.WHITE,
        marginHorizontal: 5
    },
    flatList: {
        alignSelf: 'center',
        marginVertical: 5,
        flex: 1,
        width: '100%'
    },
    objectView: {
        width: wp(90),
        backgroundColor: COLOR.WHITE,
        elevation: 5,
        borderRadius: 5,
        marginVertical: 5,
        alignSelf: 'center',
        flexDirection: 'row',
        padding: 5,
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 2 },
    },
    imageView: {
        width: 70,
        height: 70,
        alignSelf: 'center'
    },
    option_view: {
        width: 60,
        height: 60,
        borderRadius: 35,
        alignContent: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        marginBottom: 5
    }
});

mapStateToProps = (state, ownProps) => {
    return {
        booking: state.tripReducer.booking
    }
}

mapDispatchToProps = dispatch => {
    return {
        getBooking: (val) => {
            dispatch(TripAction.getBookings(val));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Bookings);