import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text, Image } from 'react-native';
import { getColorByType, getIconByType, getItineraryTitle, getLabelByType, getStartTimeAttr } from '../../../../res/appData/index';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import COLOR from '../../../../../res/styles/Color';
import moment from 'moment';

export default function HotelCard(props) {
    const {
        item
    } = props;


    return (
        <View
            style={styles.planObject}
        >
            <View style={styles.dateView}>
                <Text style={{ color: COLOR.PRIMARYBLACK }}>{moment(item['arrivalTime']).format("hh:mm A")}</Text>
            </View>
            <View style={{ marginLeft: 0 }}>
                <View style={[styles.icon, { backgroundColor: COLOR.RED }]}>
                    <Image
                        source={require('../../../../../res/assets/hotel.png')}
                        resizeMode='contain'
                        style={styles.typeIcon}
                    />

                </View>
                <View style={{ width: 3, backgroundColor: COLOR.RED, flex: 1, justifyContent: 'center', alignSelf: 'center' }} />
            </View>
            <TouchableOpacity
                onPress={props.onPress}
                style={[styles.card, { backgroundColor: '#FEEAEA', marginBottom: 15, marginLeft: 60 }]}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}
                >
                    <Text style={{ fontSize: 16, color: COLOR.RED }}>{'Hotel'}</Text>
                    <Text style={styles.planCommonText}>{item.participants ? item.participants.length + ' Travellers' : ''}</Text>
                </View>
                <Text style={styles.planCommonText}>
                    {item.hotelName}
                </Text>
                <Text style={styles.planCommonText}>
                    {item.endPoint ? item.endPoint : item.to ? item.to : ''}
                </Text>
            </TouchableOpacity>
        </View>

    )
}

const styles = StyleSheet.create({
    icon: {
        justifyContent: 'center',
        width: 40,
        height: 40,
        alignItems: 'center',
        borderRadius: 25,
    },
    card: {
        borderRadius: 8,
        padding: 12,
        width: widthPercentageToDP(45),
        marginBottom: 15
    },
    planObject: {
        paddingHorizontal: 10,
        flexDirection: 'row',
    },
    planCommonText: {
        fontSize: 8,
        marginTop: 5,
        color: COLOR.PRIMARYBLACK
    },
    dateText: {
        color: COLOR.WHITE,
        marginHorizontal: 15,
        paddingVertical: 5,
    },
    dateView: {
        marginTop: 8,
        width: widthPercentageToDP(20),
    },
    typeIcon: {
        width: 25,
        height: 25,
        alignSelf: 'center'
    }
})