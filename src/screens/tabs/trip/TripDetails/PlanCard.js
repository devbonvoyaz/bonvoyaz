import React from 'react';
import { StyleSheet } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import COLOR from '../../../../res/styles/Color';
import FlightCard from './planCards/FlightCard';
import HotelCard from './planCards/HotelCard';
import RestaurantCard from './planCards/RestaurantCard';
import DriveCard from './planCards/DriveCard';
import TrainCard from './planCards/TrainCard';
import PlaceCard from './planCards/PlaceCard';

export default function PlanCard(props) {
    const {
        item
    } = props;


    switch (item.type) {
        case 1:
            return <FlightCard onPress={props.onPress} item={item} />
        case 2:
            return <HotelCard onPress={props.onPress} item={item} />
        case 3:
            return <RestaurantCard onPress={props.onPress} item={item} />
        case 4:
            return <DriveCard onPress={props.onPress} item={item} />
        case 5:
            return <TrainCard onPress={props.onPress} item={item} />
        case 6:
            return <PlaceCard onPress={props.onPress} item={item} />
    }
}

const styles = StyleSheet.create({
    icon: {
        justifyContent: 'center',
        width: 40,
        height: 40,
        alignItems: 'center',
        borderRadius: 25,
    },
    card: {
        borderRadius: 8,
        padding: 12,
        width: widthPercentageToDP(45),
        marginBottom: 15
    },
    planObject: {
        paddingHorizontal: 10,
        flexDirection: 'row',
    },
    planCommonText: {
        fontSize: 8,
        marginTop: 5,
        color: COLOR.PRIMARYBLACK
    },
    dateText: {
        color: COLOR.WHITE,
        marginHorizontal: 15,
        paddingVertical: 5,
    },
    dateView: {
        marginTop: 8,
        width: widthPercentageToDP(20),
    },
    typeIcon: {
        width: 25,
        height: 25,
        alignSelf: 'center'
    }
})