import React from 'react';
import {
  StyleSheet,
  Text,
  FlatList,
  View,
} from 'react-native';
import ActionButton from 'react-native-action-button';
import ActionSheet from 'react-native-actionsheet';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { connect } from 'react-redux';
import PlanAction from '../../../../redux/actions/PlanAction';
import TripAction from '../../../../redux/actions/TripAction';
import COLOR from '../../../../res/styles/Color';
import Styles from '../../../../res/styles/Styles';
import FONTS from '../../../../res/styles/Fonts';
import PlanCard from './PlanCard';


class Itinerary extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      itineraryData: [],
      selectedItinerary: null,
    }
    this.props.getTripItinerary(this.props.trip);
    this.ActionSheet = React.createRef();
  }


  componentDidUpdate(prevProps) {
    if (prevProps.itineraryData != this.props.itineraryData) {

      this.setState({
        itineraryData: this.props.itineraryData,
      })
    }
  }


  editPlan = () => {
    let val = this.state.selectedItinerary.type;
    let route = val == 1 ? 'AddFlightScreen' : val == 2 ? 'AddHotel' : val == 3 ? 'AddRestaurant' : val == 4 ? 'AddDrive' : val == 5 ? 'AddTrain' : 'AddPlace';
    this.props.navigation.navigate(route, { item: this.state.selectedItinerary });
  }

  removePlan = () => {
    let data = JSON.stringify({
      plan: this.state.selectedItinerary.id,
      type: this.state.selectedItinerary.type
    });
    this.props.removePlan(this.props.trip, data);

  }

  render() {
    return (
      <View style={{ flex: 1, paddingBottom: 10 }}>
        <FlatList
          data={this.state.itineraryData}
          style={{ flex: 1 }}
          renderItem={({ item }) => {
            return (
              <View style={{ marginTop: 10, backgroundColor: COLOR.WHITE }}>
                <Text style={{ marginVertical: 10, fontFamily: FONTS.FAMILY_SEMIBOLD, fontSize: 20 }}> {item.date}</Text>
                <View style={[Styles.hr, { width: widthPercentageToDP(98), height: 2, backgroundColor: COLOR.PRIMARYGREY }]} />
                <FlatList
                  style={{ flex: 1, marginTop: 10 }}
                  data={item.plan}
                  keyExtractor={(item, index) => index}
                  renderItem={({ item, index }) => {
                    return (
                      <PlanCard item={item} onPress={() => {
                        this.ActionSheet.show();
                        this.setState({
                          selectedItinerary: item,
                        })
                      }} />
                    )
                  }}
                  ListEmptyComponent={() => {
                    return (
                      <Text style={{ alignSelf: "center", marginVertical: 40 }}>No Plans for the day</Text>
                    );
                  }}
                />

              </View>

            )
          }}
        />
        <ActionSheet
          ref={o => this.ActionSheet = o}
          title={''}
          options={
            ['Edit', 'Delete', 'cancel']
          }
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
          onPress={(index) => {
            switch (index) {
              case 0:
                this.editPlan();
                break;
              case 1:
                this.removePlan();
                break;
              default:
                break;

            }
          }}
        />
        <ActionButton
          buttonColor='#01CAE4'
          onPress={() => {
            this.props.navigation.navigate('AddPlan', { trip: this.props.trip })
          }}
        />

      </View>
    );
  }
};

const style = StyleSheet.create({
  icon: {
    justifyContent: 'center',
    width: 40,
    height: 40,
    alignItems: 'center',
    borderRadius: 25,
  },
  card: {
    borderRadius: 8,
    padding: 12,
    width: widthPercentageToDP(45),
    marginBottom: 15
  },
  planObject: {
    paddingHorizontal: 15,
    flexDirection: 'row',
  },
  planCommonText: {
    fontSize: 8,
    marginTop: 5,
    color: COLOR.PRIMARYBLACK
  },
  dateText: {
    color: COLOR.WHITE,
    marginHorizontal: 15,
    paddingVertical: 5,
  },
  typeIcon: {
    width: 25,
    height: 25,
    alignSelf: 'center'
  }
});

const mapStateToProps = (state, ownProps) => {
  return {
    itineraryData: state.tripReducer.itineraryData,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getTripItinerary: (val) => {
      dispatch(TripAction.getTripItinerary(val))
    },
    removePlan: (val, data) => {
      dispatch(PlanAction.removePlan(val, data))
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Itinerary)