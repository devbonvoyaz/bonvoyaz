import React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Budget from "../Spending/Budget/BudgetView";
import Expenses from "../Spending/Expenses/ExpensesView";
import COLOR from "../../../../res/styles/Color";

const SpendingTabs = createMaterialTopTabNavigator();

export default function Spending(props) {
  return (
    <SpendingTabs.Navigator
      initialRouteName="Budget"
      style={{
        flex: 1,
        flexGrow: 1,
        backgroundColor: COLOR.WHITE
      }}
      sceneContainerStyle={{
        flex: 1,
        flexGrow: 1
      }}
      tabBarOptions={{
        activeTintColor: COLOR.WHITE,
        inactiveTintColor: COLOR.PRIMARTGREEN,
        pressOpacity: 1,
        indicatorStyle: {
          backgroundColor: COLOR.PRIMARTGREEN,
          height: "100%",
          width: "50%",
          borderRadius: 3,
        },
        labelStyle: {
          fontWeight: "bold",
          position: "absolute",
          left: -30,
          top: -5,
        },
        tabStyle: {
          justifyContent: "flex-start",
        },
        style: {
          marginHorizontal: 60,
          marginVertical: 10,
          borderStyle: "solid",
          height: 35,
          borderWidth: 1,
          borderColor: COLOR.PRIMARTGREEN,
          borderRadius: 5,
        },
      }}
    >
      <SpendingTabs.Screen
        name="Budget"
        children={() => <Budget {...props} />}
        options={{ tabBarLabel: "Budget" }}
      />
      <SpendingTabs.Screen
        name="Expenses"
        children={() => <Expenses {...props} />}
        options={{ tabBarLabel: "Expenses" }}
      />
    </SpendingTabs.Navigator>
  );
}
