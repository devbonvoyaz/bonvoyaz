import { Animated, View, TouchableOpacity, Image, Text } from 'react-native';
import React from 'react';
import COLOR from '../../../res/styles/Color';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { TabBarIndicator } from 'react-native-tab-view';



export default function TripTabBar({ state, descriptors, navigation, position }) {
    return (
        <View style={{
            width: widthPercentageToDP(90),
            flexDirection: 'row',
            shadowColor: COLOR.BLACK,
            shadowOpacity: 0.2,
            shadowOffset: { width: 2, height: 2 },
            borderRadius: 10,
            paddingHorizontal: 5,
            paddingTop: 15,
            marginVertical: 10,
            alignSelf: 'center',
            backgroundColor: COLOR.WHITE,
            justifyContent: 'space-between'
        }}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        // The `merge: true` option makes sure that the params inside the tab screen are preserved
                        navigation.navigate({ name: route.name, merge: true });
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                const inputRange = state.routes.map((_, i) => i);



                return (
                    <TouchableOpacity
                        accessibilityRole="button"
                        accessibilityState={isFocused ? { selected: true } : {}}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={onPress}
                        onLongPress={onLongPress}
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}
                    >
                        <View
                            style={{
                                width: 50,
                                height: 50,
                                borderRadius: 25,
                                marginHorizontal: 5,
                                justifyContent: 'center',
                                backgroundColor: index == 0 ? '#01CAE4' : index == 1 ? '#8149FA' : COLOR.PRIMARTGREEN,
                            }}>
                            {index == 2 ?
                                <Ionicon size={23} name="wallet-outline" style={{ color: COLOR.WHITE, alignSelf: 'center' }} />

                                : <Image
                                    style={{ width: 20, height: 20, alignSelf: 'center' }}
                                    source={index == 0 ? require('../../../res/assets/itinerary.png') : index == 1 ? require('../../../res/assets/ticket2.png') : null}
                                    resizeMode={'contain'}
                                />}
                        </View>
                        <Text style={{
                            fontSize: 12,
                            alignSelf: 'center',
                            marginTop: 5,
                            marginBottom: 15,
                        }}>
                            {label}
                        </Text>
                        <TabBarIndicator
                            width={'100%'}
                            layout={{
                                width: 1
                            }}
                            position={position}
                            style={{
                                backgroundColor: isFocused ? index == 0 ? '#01CAE4' : index == 1 ? '#8149FA' : COLOR.PRIMARTGREEN : COLOR.WHITE,
                                height: 3,
                            }}
                            navigationState={state}
                            getTabWidth={() => 0}

                        />
                    </TouchableOpacity>
                );
            })}
        </View>
    );
}

