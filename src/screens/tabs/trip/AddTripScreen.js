import React from "react";
import {
    Text,
    View,
    Image,
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { widthPercentageToDP, heightPercentageToDP, widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLOR from "../../../res/styles/Color";
import FONTS from "../../../res/styles/Fonts";
import Styles from "../../../res/styles/Styles";
import moment from "moment";
import Icon from "react-native-vector-icons/FontAwesome5";
import { connect } from "react-redux";
import TripAction from "../../../redux/actions/TripAction";
import SimpleToast from "react-native-simple-toast";
import ChatAction from "../../../redux/actions/ChatAction";
import SelectFriendsList from "../../../commonView/listView/SelectFriendsList";
import RBSheet from "react-native-raw-bottom-sheet";
import { SearchBar } from 'react-native-elements';
import { FloatingTitleTextInputField } from "../../../commonView/globalComponent/FloatingTitleTextInputField";
import { TouchableWithoutFeedback } from "react-native";
import { Keyboard } from 'react-native'
import Calendar from '../../../commonView/globalComponent/Calendar';
import { Chip } from 'react-native-elements';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

class AddTripScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            startText: "Select Start Date",
            endDate: null,
            endText: "End Date",
            tripName: "",
            selectedTravelers: [],
            startDatePicker: false,
            endDatePicker: false,
            friendsSearch: '',
            friends: [],
            places: [{ placeId: null, place: 'add' }],

        };
        this.calRef = React.createRef();
        this.props.getFriends();
        this.searchRef = React.createRef();
        this.Address = React.createRef();

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.friends != this.props.friends) {
            this.setState({
                friends: nextProps.friends,
            })
        }
    }

    handleAddTrip = () => {

        if (this.state.tripName == "") {
            SimpleToast.show("Enter trip name");
        } else if (this.state.endDate == null || this.state.startDate == "") {
            SimpleToast.show("Invalid dates");
        } else if (this.state.places.length <= 1) {
            SimpleToast.show("Selet places ");

        } else {
            const travellers = this.state.selectedTravelers.map(function (value) {
                return value.user2.id
            });
            var startDate = moment(new Date(this.state.startDate)).format("x");
            var endDate = moment(new Date(this.state.endDate)).format("x");
            var places = this.state.places.filter(function (value) {
                if (value.placeId) {
                    return value
                }
            })
            let data = JSON.stringify({
                tripName: this.state.tripName,
                startDate: startDate,
                endDate: endDate,
                participants: travellers,
                places: places,
            });
            this.props.addTrip(data);
            setTimeout(() => {
                this.props.getAllTrips(2);
            }, 500)
        }

    };


    onStartDatePicked = (date) => {
        this.setState({
            startDate: date,
            startText: moment(date).format("DD-MMM-YYYY"),
            startDatePicker: false,
        });
    };

    onEndDatePicked = (date) => {
        this.setState({
            endDate: date,
            endText: moment(date).format("DD-MMM-YYYY"),
            endDatePicker: false,
        });
    };

    addParticipant = (val) => {
        const array = this.state.selectedTravelers;
        array.push(val);
        this.setState({
            selectedTravelers: array,
        });
    };

    removeParticipant = (val) => {
        const array = this.state.selectedTravelers;
        const index = array
            .map(function (x) {
                return x.user2.phone;
            })
            .indexOf(val.user2.phone);
        array.splice(index, 1);
        this.setState({
            selectedTravelers: array,
        });
    };

    updateSearch = (friendsSearch) => {
        if (this.props.friends != null) {
            let text = friendsSearch.toLowerCase();
            let friendsTrunk = this.state.friends;
            let friendsData = [];
            friendsTrunk.filter((item) => {
                if (
                    (item.user2.firstName + " " + item.user2.lastName)
                        .toLowerCase()
                        .match(text)
                ) {
                    friendsData.push(item);
                }
            });
            if (friendsData.length == 0 || friendsSearch.length == 0) {
                this.setState({
                    friends: this.props.friends,
                });
            } else {
                this.setState({
                    friends: friendsData,
                });
            }
        }

    };

    updateMasterState = (attrName, value) => {
        this.setState({ [attrName]: value });
    };

    renderChip = ({ item }) => {

        return (
            <View style={{ marginRight: 5, marginVertical: 5 }}>
                <Chip
                    onPress={() => { item.placeId ? this.removeChip(item) : this.Address.current.open() }}
                    title={item.place}
                    icon={{
                        name: item.placeId ? "close" : "plus",
                        type: "font-awesome",
                        size: 15,
                        color: "white",
                    }}
                    iconRight
                />
            </View>
        )
    }

    removeChip = (item) => {
        let state = this.state.places;
        let index = state.map(value => value.placeId).indexOf(item.placeId);
        if (index != -1) {
            state.splice(index, 1);
            state.length == 2 && state[state.length - 1].placeId != null ? state.push({ placeId: null, place: 'add' }) : null;
            this.setState({ places: state });
        }
    }

    render() {
        const { friendsSearch } = this.state;
        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <SafeAreaView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>

                    <View
                        style={{
                            flexDirection: "row",
                            marginVertical: 12,
                            alignItems: "center",
                        }}
                    >
                        <TouchableOpacity
                            style={style.backButton}
                            onPress={() => {
                                this.props.navigation.goBack();
                            }}
                        >
                            <Icon name="chevron-left" style={{ alignSelf: 'center' }} size={18} />
                        </TouchableOpacity>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                alignItems: "center",
                                justifyContent: "center",
                            }}
                        >
                            <Text style={Styles.screen_heading}>Trips Details</Text>
                        </View>
                        <TouchableOpacity
                            style={style.backButton}
                            onPress={() => {
                                this.handleAddTrip();
                            }}
                        >
                            <Text style={style.saveText}>Save</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={style.text_input_card}>
                        <FloatingTitleTextInputField
                            attrName="tripName"
                            title="Trip Name"
                            isWhite={false}
                            textInputStyles={{
                                padding: 0
                            }}
                            color={'#FFB100'}
                            value={this.state.tripName}
                            updateMasterState={this.updateMasterState}
                        />
                    </View>


                    <View style={style.text_input_card}>
                        <Text style={style.inputLabel}>Date</Text>
                        <TouchableOpacity
                            onPress={() => this.calRef.current.open()}>
                            <Text style={style.inputText}>{this.state.startText + '  -  ' + this.state.endText}</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <FlatList
                            scrollEnabled={false}
                            columnWrapperStyle={{ flexWrap: 'wrap', flex: 1 }}
                            data={this.state.places}
                            renderItem={this.renderChip}
                            numColumns={3}
                            style={{
                                width: widthPercentageToDP(90),
                                alignSelf: 'center',
                                marginTop: 10,
                            }}

                        />
                    </View>


                    <View>
                        <View style={[style.topView, { marginTop: 40 }]}>
                            <TouchableOpacity onPress={() => this.RBSheet.open()} style={{ flexDirection: "row" }}>
                                <View style={style.addUserBackground}>
                                    <Image
                                        style={{ alignSelf: "center", width: 25, height: 25 }}
                                        source={require("../../../res/assets/addUser.png")}
                                        resizeMode={"contain"}
                                    />
                                </View>

                                <Text style={style.addParticipantsText}>Add Travellers</Text>
                            </TouchableOpacity>
                            <View
                                style={{
                                    backgroundColor: COLOR.PRIMARYBLUE,
                                    borderRadius: 5,
                                    padding: 5,
                                    alignSelf: "center",
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 14,
                                        color: COLOR.WHITE,
                                        width: 100,
                                        textAlign: "center",
                                        fontFamily: FONTS.FAMILY_MEDIUM,
                                    }}
                                >
                                    {this.state.selectedTravelers &&
                                        this.state.selectedTravelers.length
                                        ? this.state.selectedTravelers.length + " Travellers"
                                        : " 0 Travellers"}
                                </Text>
                            </View>
                        </View>
                        <View style={Styles.hr} />
                        <RBSheet
                            ref={ref => {
                                this.RBSheet = ref
                            }}
                            height={heightPercentageToDP(85)}
                            openDuration={100}
                            closeDuration={100}
                            closeOnDragDown="true"
                            dragFromTopOnly="true"
                            animationType='slide'
                            keyboardShouldPersistTaps="handled"
                            keyboardAvoidingViewEnabled={true}
                            customStyles={{
                                draggableIcon: {
                                    backgroundColor: "white"
                                },
                                container: {
                                    backgroundColor: COLOR.WHITE,
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                }
                            }}

                        >
                            <View
                                keyboardShouldPersistTaps='handled'
                                style={{ flex: 1 }}>
                                <Text style={{ alignSelf: 'center', 
                                    marginBottom: 20, 
                                    fontFamily: FONTS.FAMILY_SEMIBOLD,
                                    fontSize: 18 }}>Add
                                    Travelers</Text>
                                <TouchableOpacity
                                    onPress={() => this.RBSheet.close()}
                                    style={{ position: 'absolute', end: 20, top: 0 }}
                                >
                                    <Image style={{
                                        width: 15,
                                        height: 15,
                                        alignSelf: 'center',
                                        tintColor: COLOR.BLACK,
                                    }}
                                        source={require('../../../res/assets/close.png')} />
                                </TouchableOpacity>
                                <SearchBar
                                    placeholder="Search a Friend"
                                    onChangeText={text => {
                                        this.setState({
                                            friendsSearch: text
                                        })
                                        this.updateSearch(text);
                                    }}
                                    value={friendsSearch}
                                    containerStyle={{
                                        backgroundColor: COLOR.WHITE, borderBottomColor: 'transparent',
                                        borderTopColor: 'transparent'
                                    }}
                                    inputContainerStyle={{ backgroundColor: COLOR.GREY }}
                                    round

                                />
                                <FlatList
                                    style={{ marginTop: 10, flex: 1 }}
                                    data={this.state.friends}
                                    keyboardShouldPersistTaps='handled'
                                    renderItem={({ item }) => (
                                        <SelectFriendsList
                                            item={item}
                                            username={item.user2.firstName + " " + item.user2.lastName}
                                            image={require("../../../res/assets/user-image.png")}
                                            addParticipant={this.addParticipant}
                                            removeParticipant={this.removeParticipant}
                                            selected={
                                                this.state.selectedTravelers
                                                    .map(function (x) {
                                                        return x.user2.phone;
                                                    })
                                                    .indexOf(item.user2.phone) != -1
                                                    ? true
                                                    : false
                                            }
                                        />
                                    )}
                                    ListEmptyComponent={() => {
                                        return (
                                            <Text style={style.placeholderText}>Select Travellers</Text>
                                        );
                                    }}
                                />

                                <TouchableOpacity
                                    onPress={() => this.RBSheet.close()}
                                    style={style.addButton}>
                                    <Text style={{ alignSelf: 'center', color: COLOR.WHITE }}>Add</Text>
                                </TouchableOpacity>
                            </View>
                        </RBSheet>
                        <RBSheet
                            ref={
                                this.Address
                            }
                            height={heightPercentageToDP(55)}
                            openDuration={100}
                            closeDuration={100}
                            closeOnDragDown="true"
                            dragFromTopOnly="true"
                            animationType='slide'
                            keyboardShouldPersistTaps="handled"
                            keyboardAvoidingViewEnabled={true}
                            customStyles={{
                                draggableIcon: {
                                    backgroundColor: "white"
                                },
                                container: {
                                    backgroundColor: COLOR.WHITE,
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                }
                            }}

                        >
                            <GooglePlacesAutocomplete
                                ref={this.searchRef}
                                fetchDetails={true}
                                styles={{
                                    textInputContainer: {
                                        borderWidth: 1,
                                        borderColor: '#E8E8E8',
                                        marginTop: 20,
                                        marginHorizontal: 15,
                                        borderRadius: 10,
                                        height: 40,
                                        borderRadius: 20,
                                        backgroundColor: '#F6F6F6'
                                    },
                                    textInput: {
                                        marginHorizontal: 5,
                                        height: 38,
                                        borderRadius: 20,
                                        backgroundColor: '#F6F6F6'
                                    },
                                    listView: {
                                        width: widthPercentageToDP(90),
                                        alignSelf: 'center',
                                    }
                                }}
                                placeholder='Search'
                                onPress={(data, details = null) => {

                                    let newPlace = {
                                        place: data.structured_formatting.main_text,
                                        placeId: details.reference
                                    };
                                    let state = this.state.places;
                                    state.unshift(newPlace);
                                    state.length == 4 ? state.pop() : null;
                                    this.setState({
                                        places: state,
                                    }, () => this.Address.current.close());

                                }}

                                onFail={(error) => console.error(error)}
                                query={{
                                    key: 'AIzaSyAKhtBwQhmYRoAobEj7lx3jGztFyVzFF8s',
                                    language: 'en',
                                }}
                            />

                        </RBSheet>
                        <FlatList
                            style={{ marginTop: 10 }}
                            data={this.state.selectedTravelers}
                            renderItem={({ item }) => (
                                <UserList
                                    username={item.user2.firstName + " " + item.user2.lastName}
                                    image={require("../../../res/assets/user-image.png")}
                                    removeParticipant={() => this.removeParticipant(item)}
                                />
                            )}
                            ListEmptyComponent={() => {
                                return (
                                    <Text style={style.placeholderText}>Select Travellers</Text>
                                );
                            }}
                        />
                    </View>


                    <Calendar
                        updateMasterState={this.updateMasterState}
                        RBSheet={this.calRef}

                    />

                </SafeAreaView>
            </TouchableWithoutFeedback>
        );
    }
}

const UserList = (props) => {
    return (
        <View>
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <View
                    style={{
                        flexDirection: "row",
                        marginVertical: 10,
                    }}
                >
                    <Image
                        style={{
                            borderRadius: 25,
                            width: 50,
                            height: 50,
                            marginHorizontal: 15,
                        }}
                        source={props.image}
                    />
                    <View style={{ alignSelf: "center" }}>
                        <Text style={{ fontSize: 14, fontFamily: FONTS.FAMILY_MEDIUM }}>
                            {props.username}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    onPress={props.removeParticipant}
                    style={{
                        alignSelf: "center",
                        marginRight: 25,
                        width: 20,
                        height: 20,
                    }}
                >
                    <Icon name="times-circle" color={COLOR.SHADOWCOLOR} size={20} />
                </TouchableOpacity>
            </View>
            <View style={Styles.hr} />
        </View>
    );
};

const style = StyleSheet.create({
    text_input_card: {
        width: widthPercentageToDP(90),
        alignSelf: "center",
        backgroundColor: COLOR.WHITE,
        elevation: 5,
        padding: 12,
        marginVertical: 8,
        shadowColor: COLOR.BLACK,
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 2 },
    },
    plusIcon: {
        alignSelf: "center",
        justifyContent: "flex-end",
        width: 25,
        height: 25,
    },
    travellerText: {
        width: widthPercentageToDP(80),
        color: COLOR.WHITE,
        alignSelf: "center",
        fontSize: 16,
        marginLeft: 25,
        fontFamily: FONTS.FAMILY_MEDIUM,
    },
    inputText: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        marginVertical: 5,
    },
    inputLabel: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        color: COLOR.PRIMARYGREY,
    },
    saveText: {
        color: COLOR.PRIMARYBLUE,
        fontFamily: FONTS.FAMILY_MEDIUM,
        fontSize: 14,
        textAlign: "right",
    },
    placeholderText: {
        alignSelf: "center",
        fontSize: 15,
        color: COLOR.SHADOWCOLOR,
        marginVertical: 50,
    },
    topView: {
        flexDirection: "row",
        paddingHorizontal: 15,
        justifyContent: "space-between",
        marginVertical: 10,
    },
    addUserBackground: {
        borderRadius: 25,
        width: 45,
        height: 45,
        alignSelf: "center",
        justifyContent: "center",
        backgroundColor: COLOR.PRIMARYBLUE,
    },
    addParticipantsText: {
        marginLeft: 15,
        fontSize: 16,
        fontFamily: FONTS.FAMILY_SEMIBOLD,
        alignSelf: "center",
    },
    backButton: {
        marginHorizontal: 20,
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    addButton: {
        width: widthPercentageToDP(90),
        height: 50,
        marginTop: 15,
        marginBottom: 25,
        alignSelf: 'center',
        backgroundColor: COLOR.PRIMARYBLUE,
        borderRadius: 10,
        justifyContent: 'center'
    }
});

const mapStateToProps = (state, ownProps) => {
    return {
        tripData: state.tripReducer.tripData,
        friends: state.chatReducer.friends,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        addTrip: (data) => {
            dispatch(TripAction.addTrip(data));
        },
        getFriends: (val) => {
            dispatch(ChatAction.getFriends(val));
        },
        getAllTrips: (val) => {
            dispatch(TripAction.getAllTrips(val));
        },

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTripScreen);
