import React from 'react';
import {
    Text,
    View,
    Image,
    SafeAreaView,
    StyleSheet,
    TextInput, Button,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
} from 'react-native';
import { FlatList, } from 'react-native-gesture-handler';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Styles from '../../../res/styles/Styles';
import moment from 'moment';
import Icon from "react-native-vector-icons/FontAwesome5";
import TripAction from "../../../redux/actions/TripAction";
import SimpleToast from "react-native-simple-toast";
import { connect } from "react-redux";
import RBSheet from "react-native-raw-bottom-sheet";
import { SearchBar } from "react-native-elements";
import { Dialog, Paragraph } from "react-native-paper";
import ChatAction from "../../../redux/actions/ChatAction";
import ActionSheet from 'react-native-actionsheet';
import Auth from '../../../auth';
import { AS_USER_DETAIL } from '../../../api/Constants';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import SelectFriendsList from '../../../commonView/listView/SelectFriendsList';
import Calendar from '../../../commonView/globalComponent/Calendar';
import { Alert } from 'react-native';
import { Chip } from 'react-native-elements';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';


const auth = new Auth();
class EditTripScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: '',
            startText: '',
            endDate: '',
            endText: '',
            item: '',
            tripName: '',
            travellers: '',
            guideDailog: true,
            isGuide: false,
            selectedTraveller: null,
            exitDailog: false,
            userId: null,
            removeDailog: false,
            friendsSearch: '',
            friends: [],
            startDatePicker: false,
            endDatePicker: false,
            travellersToAdd: [],
            places: [{ placeId: null, place: 'add' }],
        };

        this.calRef = React.createRef();
        this.props.getFriends();
        this.ActionSheet = React.createRef();
        this.Address = React.createRef();

    }

    async componentDidMount() {
        if (this.props.singleTrip) {
            let tripData = this.props.singleTrip;
            this.setState({
                startDate: new Date(tripData.startDate).getTime(),
                startText: moment.unix(tripData.startDate / 1000).format('DD-MMM-YYYY'),
                endDate: new Date(tripData.endDate).getTime(),
                endText: moment.unix(tripData.endDate / 1000).format('DD-MMM-YYYY'),
                item: tripData,
                tripName: tripData.tripName,
                travellers: tripData.participants,
                isGuide: tripData.isGuide,
                places: tripData.places.length < 3 ? [...tripData.places, ...this.state.places] : tripData.places,

            })
        }

        const userDetails = JSON.parse(await auth.getValue(AS_USER_DETAIL));
        this.setState({ userId: userDetails.user });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.singleTrip != this.props.singleTrip) {
            if (this.props.singleTrip.conflict == false || this.props.singleTrip.conflict == null) {
                let tripData = this.props.singleTrip;
                this.setState({
                    startDate: tripData.startDate,
                    startText: moment.unix(tripData.startDate / 1000).format('DD-MMM-YYYY'),
                    endDate: new Date(tripData.endDate),
                    endText: moment.unix(tripData.endDate / 1000).format('DD-MMM-YYYY'),
                    item: tripData,
                    tripName: tripData.tripName,
                    travellers: tripData.participants,
                    isGuide: tripData.isGuide,

                });
            } else {
                let newData = this.props.singleTrip;
                Alert.alert("Conflict",
                    "Do you want to delete plans which are not in this date range?",
                    [{
                        text: "Yes",
                        onPress: () => {
                            this.props.deleteConflictedPlans(newData.id, newData.data);
                            this.props.getTripItinerary(this.state.item.id);
                        },
                        style: "ok"
                    }, { text: "No", onPress: () => this.props.getSingleTrip(this.state.item.id) }],
                )
            }
        }

        if (prevProps.friends != this.props.friends) {
            this.setState({
                friends: this.props.friends,
            })
        }
    }
    dismissAsGuide = () => {
        let data = JSON.stringify({
            trip: this.state.item.id,
            traveler: this.state.selectedTraveller.id
        });
        this.props.dismissAsGuide(data);
        setTimeout(() => {
            this.props.getSingleTrip(this.state.item.id);
        }, 250);
    }

    makeGuide = () => {
        let data = JSON.stringify({
            trip: this.state.item.id,
            traveler: this.state.selectedTraveller.id
        });
        this.props.makeGuide(data);
        setTimeout(() => {
            this.props.getSingleTrip(this.state.item.id);
        }, 250);
    }

    onStartDatePicked = (date) => {
        this.setState({
            startDate: date.getTime(),
            startText: moment(date).format('DD-MMM-YYYY'),
            startDatePicker: false,
        });
    }

    onEndDatePicked = (date) => {
        this.setState({
            endDate: date.getTime(),
            endText: moment(date).format('DD-MMM-YYYY'),
            endDatePicker: false,
        });
    }

    handleTripUpdate = () => {
        let places = this.state.places.filter(value => {
            if (value.placeId != null) {
                return value;
            }
        })
        if (this.state.tripName == null) {
            SimpleToast.show('Invalid Trip Name');
        } else if (this.state.startDate == null) {
            SimpleToast.show('Invalid Start Date');
        } else if (this.state.endDate == null) {
            SimpleToast.show('Invalid End Date');
        } else if (places.length < 1) {
            SimpleToast.show('Select places');
        } else {
            let data = {
                tripName: this.state.tripName,
                startDate: this.state.startDate,
                endDate: this.state.endDate,
                places: places
            }
            this.props.updateTrip(this.state.item.id, data);
        }

    }

    addTraveller = () => {
        if (this.state.travellersToAdd.length > 0) {
            let travelers = this.state.travellersToAdd.map(function (value) {
                return value.user2.id;
            })
            let data = JSON.stringify({
                trip: this.state.item.id,
                travelers: travelers,
            });
            this.props.addTripTraveller(data);
            this.setState({
                travellersToAdd: [],
            });
        }

    }

    removeTraveller = (item) => {
        let data = JSON.stringify({
            trip: this.state.item.id,
            traveler: item.id ? item.id : item,
        });
        this.props.removeTripTraveller(data);

        if (this.state.travellers.length <= 1 || !item.id) {
            setTimeout(() => {
                this.props.getAllTrips(2);
                this.props.getAllTrips(1);
            }, 500)

            this.props.navigation.pop(2);
        }
    }

    updateSearch = (friendsSearch) => {
        if (this.props.friends != null) {
            let text = friendsSearch.toLowerCase();
            let friendsTrunk = this.state.friends;
            let friendsData = [];
            friendsTrunk.filter((item) => {
                if (
                    (item.user2.firstName + " " + item.user2.lastName)
                        .toLowerCase()
                        .match(text)
                ) {
                    friendsData.push(item);
                }
            });
            if (friendsData.length == 0 || friendsSearch.length == 0) {
                this.setState({
                    friends: this.props.friends,
                });
            } else {
                this.setState({
                    friends: friendsData,
                });
            }
        }
    };
    updateMasterState = (attrName, value) => {
        this.setState({ [attrName]: value });
    };


    renderChip = ({ item }) => {

        return (
            <View style={{ marginRight: 5, marginVertical: 5 }}>
                <Chip
                    onPress={() => { item.placeId ? this.removeChip(item) : this.Address.current.open() }}
                    title={item.place}
                    icon={{
                        name: item.placeId ? "close" : "plus",
                        type: "font-awesome",
                        size: 15,
                        color: "white",
                    }}
                    iconRight
                />
            </View>
        )
    }

    removeChip = (item) => {
        let state = this.state.places;
        let index = state.map(value => value.placeId).indexOf(item.placeId);
        if (index != -1) {
            state.splice(index, 1);
            state.length == 2 && state[state.length - 1].placeId != null ? state.push({ placeId: null, place: 'add' }) : null;
            this.setState({ places: state });
        }
    }


    render() {
        return (
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                <SafeAreaView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
                    <View
                        style={style.topView}
                    >
                        <TouchableOpacity
                            style={{ marginHorizontal: 20 }}
                            onPress={() => {
                                this.props.navigation.goBack();
                            }}
                        >
                            <Icon name="chevron-left" size={18} />
                        </TouchableOpacity>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Text style={Styles.screen_heading}>
                                Edit Trip
                            </Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => {
                                this.handleTripUpdate();
                            }}
                        >
                            <Text
                                style={style.saveText}
                            >
                                Save
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={style.text_input_card}>
                        <Text
                            style={style.inputLabel}
                        >
                            Trip Name
                        </Text>
                        <TextInput
                            style={{
                                fontSize: 12,
                                fontFamily: FONTS.FAMILY_MEDIUM,
                                padding: 0,
                            }}
                            value={this.state.tripName}
                            onChangeText={text => this.setState({ tripName: text })}
                        />
                    </View>
                    <View style={style.text_input_card}>
                        <Text
                            style={style.inputLabel}
                        >
                            Dates
                        </Text>
                        <TouchableOpacity onPress={() => this.calRef.current.open()}>
                            <Text
                                style={style.inputText}
                            >
                                {this.state.startText + ' - ' + this.state.endText}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <FlatList
                            scrollEnabled={false}
                            columnWrapperStyle={{ flexWrap: 'wrap', flex: 1 }}
                            data={this.state.places}
                            renderItem={this.renderChip}
                            numColumns={3}
                            style={{
                                width: widthPercentageToDP(90),
                                alignSelf: 'center',
                                marginTop: 10,
                            }}
                        />
                    </View>

                    <View style={[style.topView, { marginTop: 40 }]}>
                        <View style={{ flexDirection: "row" }}>
                            {this.state.isGuide ?
                                <TouchableOpacity
                                    onPress={() => this.RBSheet.open()}
                                    style={style.addUserBackground}>
                                    <Image
                                        style={{ alignSelf: "center", width: 25, height: 25 }}
                                        source={require("../../../res/assets/addUser.png")}
                                        resizeMode={"contain"}
                                    />
                                </TouchableOpacity> : null}

                            <Text style={style.addParticipantsText}>{this.state.isGuide ? 'Add Travellers' : 'Travellers'}</Text>
                        </View>
                        <View
                            style={{
                                backgroundColor: COLOR.PRIMARYBLUE,
                                borderRadius: 5,
                                padding: 5,
                                alignSelf: "center",
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: 14,
                                    color: COLOR.WHITE,
                                    width: 100,
                                    textAlign: "center",
                                    fontFamily: FONTS.FAMILY_MEDIUM,
                                }}
                            >
                                {this.state.travellers &&
                                    this.state.travellers.length
                                    ? this.state.travellers.length + " Travellers"
                                    : " 0 Travellers"}
                            </Text>
                        </View>
                    </View>

                    <FlatList
                        style={{ marginTop: 10 }}
                        data={this.state.travellers}
                        keyboardShouldPersistTaps='handled'
                        renderItem={({ item }) => (
                            <UserList
                                item={item}
                                dataLength={this.state.travellers.length}
                                isUserGuide={this.state.isGuide}
                                username={item.firstName + ' ' + item.lastName}
                                userId={item.phone}
                                isSameUser={item.isSameUser}
                                image={require('../../../res/assets/user-image.png')}
                                removeTraveller={() => {
                                    this.setState({
                                        selectedTraveller: item,
                                        removeDailog: true,
                                    })
                                }}
                                isGuide={item.isGuide}
                                onPress={(item) => {
                                    if (!item.isSameUser) {
                                        this.setState({
                                            selectedTraveller: item
                                        }, () => this.ActionSheet.show());
                                    }
                                }}
                            />
                        )}
                    />
                    <TouchableOpacity
                        onPress={() => this.setState({ exitDailog: true })}
                        style={style.clearChatView}>
                        <View style={style.cardView}>
                            <Image
                                style={style.exitIcon}
                                source={require('../../../res/assets/exit.png')}
                                resizeMode={'contain'}
                            />
                        </View>
                        <Text
                            style={style.exitText}
                        >
                            Exit Trip
                        </Text>
                    </TouchableOpacity>

                    <DateTimePickerModal
                        isVisible={this.state.startDatePicker}
                        mode="date"
                        minimumDate={new Date()}
                        onConfirm={this.onStartDatePicked.bind(this)}
                        onCancel={() => this.setState({ startDatePicker: false })}
                    />
                    <DateTimePickerModal
                        isVisible={this.state.endDatePicker}
                        mode="date"
                        minimumDate={new Date()}
                        onConfirm={this.onEndDatePicked.bind(this)}
                        onCancel={() => this.setState({ endDatePicker: false })}
                    />
                    <ActionSheet
                        ref={o => this.ActionSheet = o}
                        title={this.state.selectedTraveller ? this.state.selectedTraveller.firstName + ' ' + this.state.selectedTraveller.lastName : ''}
                        options={
                            this.state.selectedTraveller ? this.state.selectedTraveller.isGuide ?
                                ['Dismiss as guide', 'Remove traveller', 'cancel'] :
                                ['Make guide', 'Remove traveller', 'cancel'] : [""]
                        }
                        cancelButtonIndex={2}
                        destructiveButtonIndex={1}
                        onPress={(index) => {
                            switch (index) {
                                case 0:
                                    this.state.selectedTraveller ? this.state.selectedTraveller.isGuide ?
                                        this.dismissAsGuide() : this.makeGuide() : console.log('null');
                                    break;
                                case 1:
                                    this.setState({
                                        removeDailog: true,
                                    })
                                    break;
                                case 2:
                                    this.setState({
                                        selectedTraveller: null,
                                    })
                                    break;
                                default:
                                    break;

                            }
                        }}
                    />
                    <Calendar
                        updateMasterState={this.updateMasterState}
                        RBSheet={this.calRef}

                    />
                    <Dialog visible={this.state.exitDailog}>
                        <Dialog.Title>Exit Trip</Dialog.Title>
                        <Dialog.Content>
                            <Paragraph>Do you want to exit trip?</Paragraph>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button onPress={() => {
                                this.setState({
                                    exitDailog: false
                                })
                                this.removeTraveller(this.state.userId)
                            }} title={"Yes"} />
                            <Button
                                onPress={() => {
                                    this.setState({
                                        exitDailog: false
                                    })
                                }}
                                title={"No"} />
                        </Dialog.Actions>
                    </Dialog>
                    <Dialog visible={this.state.removeDailog}>
                        <Dialog.Title>Remove traveller</Dialog.Title>
                        <Dialog.Content>
                            <Paragraph>Do you want to remove this traveller?</Paragraph>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button onPress={() => {
                                this.setState({
                                    removeDailog: false
                                })
                                this.removeTraveller(this.state.selectedTraveller)
                            }} title={"Yes"} />
                            <Button
                                onPress={() => {
                                    this.setState({
                                        removeDailog: false
                                    })
                                }}
                                title={"No"} />
                        </Dialog.Actions>
                    </Dialog>

                    <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        height={heightPercentageToDP(55)}
                        openDuration={100}
                        closeDuration={100}
                        closeOnDragDown="true"
                        dragFromTopOnly="true"
                        customStyles={{
                            draggableIcon: {
                                backgroundColor: "white"
                            },
                            container: {
                                backgroundColor: COLOR.WHITE,
                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                            }
                        }}
                    >
                        <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', marginBottom: 20, font: FONTS.FAMILY_SEMIBOLD }}>Add
                                Travellers</Text>
                            <TouchableOpacity
                                onPress={() => this.RBSheet.close()}
                                style={{ position: 'absolute', end: 20, top: 0 }}
                            >
                                <Image style={{
                                    width: 15,
                                    height: 15,
                                    alignSelf: 'center',
                                    tintColor: COLOR.BLACK,
                                }}
                                    source={require('../../../res/assets/close.png')} />
                            </TouchableOpacity>
                            <SearchBar
                                placeholder="Search a Friend"
                                onChangeText={text => {
                                    this.setState({
                                        friendsSearch: text
                                    })
                                    this.updateSearch(text);
                                }}
                                value={this.state.friendsSearch}
                                containerStyle={{
                                    backgroundColor: COLOR.WHITE, borderBottomColor: 'transparent',
                                    borderTopColor: 'transparent'
                                }}
                                inputContainerStyle={{ backgroundColor: COLOR.GREY }}
                                inputStyle={{ color: COLOR.BLACK }}
                                round
                            />
                            <FlatList
                                style={{ marginTop: 10 }}
                                data={this.state.friends}
                                keyboardShouldPersistTaps='handled'
                                renderItem={({ item }) => (

                                    <SelectFriendsList
                                        item={item}
                                        username={item.user2.firstName + " " + item.user2.lastName}
                                        image={require("../../../res/assets/user-image.png")}
                                        addParticipant={() => {
                                            let data = this.state.travellersToAdd;
                                            data.push(item);
                                            this.setState({
                                                travellersToAdd: data
                                            })
                                        }}
                                        removeParticipant={() => {
                                            let data = this.state.travellersToAdd;
                                            let index = data.map(function (value) {
                                                return value.user2.phone
                                            }).indexOf(item.phone);
                                            data.splice(index, 1);
                                            this.setState({
                                                travellersToAdd: data
                                            })
                                        }}
                                        selected={this.state.travellersToAdd.map(function (x) {
                                            return x.user2.phone;
                                        })
                                            .indexOf(item.user2.phone) != -1
                                            ? true
                                            : false}
                                        preselected={
                                            this.state.travellers
                                                .map(function (x) {
                                                    return x.phone;
                                                })
                                                .indexOf(item.user2.phone) != -1
                                                ? true
                                                : false
                                        }
                                    />
                                )}
                                ListEmptyComponent={() => {
                                    return (
                                        <Text style={style.placeholderText}>Select Participants</Text>
                                    );
                                }}
                            />
                            <TouchableOpacity
                                onPress={() => {
                                    this.RBSheet.close();
                                    this.addTraveller();
                                }}
                                style={style.addButton}>
                                <Text style={{ alignSelf: 'center', color: COLOR.WHITE }}>Add</Text>
                            </TouchableOpacity>

                        </View>
                    </RBSheet>
                    <RBSheet
                        ref={
                            this.Address
                        }
                        height={heightPercentageToDP(55)}
                        openDuration={100}
                        closeDuration={100}
                        closeOnDragDown="true"
                        dragFromTopOnly="true"
                        animationType='slide'
                        keyboardShouldPersistTaps="handled"
                        keyboardAvoidingViewEnabled={true}
                        customStyles={{
                            draggableIcon: {
                                backgroundColor: "white"
                            },
                            container: {
                                backgroundColor: COLOR.WHITE,
                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                            }
                        }}

                    >
                        <GooglePlacesAutocomplete
                            ref={this.searchRef}
                            fetchDetails={true}
                            styles={{
                                textInputContainer: {
                                    borderWidth: 1,
                                    borderColor: '#E8E8E8',
                                    marginTop: 20,
                                    marginHorizontal: 15,
                                    borderRadius: 10,
                                    height: 40,
                                    borderRadius: 20,
                                    backgroundColor: '#F6F6F6'
                                },
                                textInput: {
                                    marginHorizontal: 5,
                                    height: 38,
                                    borderRadius: 20,
                                    backgroundColor: '#F6F6F6'
                                },
                                listView: {
                                    width: widthPercentageToDP(90),
                                    alignSelf: 'center',
                                }
                            }}
                            placeholder='Search'
                            onPress={(data, details = null) => {

                                let newPlace = {
                                    place: data.structured_formatting.main_text,
                                    placeId: details.reference
                                };
                                let state = this.state.places;
                                state.unshift(newPlace);
                                state.length == 4 ? state.pop() : null;
                                this.setState({
                                    places: state,
                                }, () => this.Address.current.close());

                            }}

                            onFail={(error) => console.error(error)}
                            query={{
                                key: 'AIzaSyAKhtBwQhmYRoAobEj7lx3jGztFyVzFF8s',
                                language: 'en',
                            }}
                        />

                    </RBSheet>


                </SafeAreaView>
            </TouchableWithoutFeedback>
        );
    }
}

const UserList = props => {
    return (
        <TouchableOpacity
            onPress={props.isUserGuide && props.dataLength > 1 ? () => props.onPress(props.item) : null}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View
                    style={{
                        flexDirection: 'row',
                        marginVertical: 10,
                    }}
                >
                    <Image
                        style={{
                            borderRadius: 25,
                            width: 50,
                            height: 50,
                            marginHorizontal: 15,
                        }}
                        source={props.image}
                    />
                    <View style={{ alignSelf: 'center' }}>
                        <Text style={{ fontSize: 14, fontFamily: FONTS.FAMILY_MEDIUM }}>
                            {props.username}
                        </Text>

                    </View>

                </View>
                {props.isGuide ?
                    <View
                        style={{
                            marginHorizontal: 15,
                            alignSelf: 'center',
                            justifyContent: 'center',
                            width: 80,
                            height: 30,
                            borderRadius: 5,
                            borderWidth: 1,
                            borderColor: COLOR.PRIMARYBLUE,
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 12,
                                fontFamily: FONTS.FAMILY_MEDIUM,
                                textAlign: 'center',
                                alignSelf: 'center',
                                color: COLOR.PRIMARYBLUE,
                            }}>Guide</Text>
                    </View>
                    : null
                }
            </View>
            <View style={Styles.hr} />
        </TouchableOpacity>
    );
};

const FriendsList = props => {
    return (
        <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View
                    style={{
                        flexDirection: 'row',
                        marginVertical: 10,
                    }}
                >
                    <Image
                        style={{
                            borderRadius: 25,
                            width: 50,
                            height: 50,
                            marginHorizontal: 15,
                        }}
                        source={props.image}
                    />
                    <View style={{ alignSelf: 'center' }}>
                        <Text style={{ fontSize: 14, fontFamily: FONTS.FAMILY_MEDIUM }}>
                            {props.username}
                        </Text>
                    </View>
                </View>
                <View style={{ marginRight: 10, alignSelf: 'center' }}>
                    {props.selected ? null :

                        <Button
                            onPress={props.addParticipant}
                            title='Add'></Button>
                    }
                </View>

            </View>
            <View style={Styles.hr} />
        </View>
    )
}

const style = StyleSheet.create({
    text_input_card: {
        width: widthPercentageToDP(90),
        alignSelf: 'center',
        borderRadius: 5,
        backgroundColor: COLOR.WHITE,
        elevation: 5,
        padding: 12,
        marginVertical: 8,
        shadowColor: COLOR.BLACK,
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 2 },
    },
    topView: {
        flexDirection: 'row',
        marginVertical: 12,
        alignItems: 'center',
    },
    plusIcon: {
        alignSelf: 'center',
        justifyContent: 'flex-end',
        width: 24,
        height: 24,
    },
    travellerText: {
        width: widthPercentageToDP(80),
        color: COLOR.WHITE,
        alignSelf: 'center',
        fontSize: 16,
        marginLeft: 25,
        fontFamily: FONTS.FAMILY_MEDIUM,
    },
    inputText: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        marginVertical: 5,
    },
    inputLabel: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        color: COLOR.PRIMARYGREY,
    },
    saveText: {
        marginRight: 20,
        color: COLOR.PRIMARYBLUE,
        fontFamily: FONTS.FAMILY_MEDIUM,
        fontSize: 14,
        textAlign: 'right',
    },
    addUserBackground: {
        borderRadius: 25,
        width: 45,
        height: 45,
        alignSelf: "center",
        justifyContent: "center",
        backgroundColor: COLOR.PRIMARYBLUE,
    },
    addParticipantsText: {
        marginLeft: 15,
        fontSize: 16,
        fontFamily: FONTS.FAMILY_SEMIBOLD,
        alignSelf: "center",
    },
    topView: {
        flexDirection: "row",
        paddingHorizontal: 15,
        justifyContent: "space-between",
        marginVertical: 10,
    },
    placeholderText: {
        alignSelf: "center",
        fontSize: 15,
        color: COLOR.SHADOWCOLOR,
        marginVertical: 50,
    },
    clearChatView: {
        width: '90%',
        flexDirection: 'row',
        alignSelf: 'center',
        alignContent: 'center',
        bottom: 5,
        marginTop: 15,
    },
    exitIcon: {
        alignSelf: 'center',
        width: 20,
        height: 20,
        tintColor: COLOR.RED,
    },
    exitText: {
        fontFamily: FONTS.FAMILY_REGULAR,
        fontSize: 14,
        alignSelf: 'center',
        color: COLOR.RED,
    },
    cardView: {
        alignSelf: 'center', justifyContent: 'center', shadowColor: COLOR.BLACK,
        width: 40,
        height: 40,
        marginRight: 15,
        borderRadius: 25,
        shadowOpacity: 0.2,
        backgroundColor: COLOR.WHITE,
        shadowOffset: { width: 2, height: 2 },
    },
    addButton: {
        width: widthPercentageToDP(90),
        height: 50,
        marginTop: 15,
        marginBottom: 25,
        alignSelf: 'center',
        backgroundColor: COLOR.PRIMARYBLUE,
        borderRadius: 10,
        justifyContent: 'center'
    }
});



const mapStateToProps = (state, ownProps) => {
    return {
        friends: state.chatReducer.friends,
        singleTrip: state.tripReducer.singleTrip,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        updateTrip: (id, data) => {
            dispatch(TripAction.updateTrip(id, data))
        },
        getFriends: (val) => {
            dispatch(ChatAction.getFriends(val));
        },
        getAllTrips: (val) => {
            dispatch(TripAction.getAllTrips(val));
        },
        getSingleTrip: (id) => {
            dispatch(TripAction.getSingleTrip(id))
        },
        addTripTraveller: (data) => {
            dispatch(TripAction.addTripTraveller(data))
        },
        removeTripTraveller: (data) => {
            dispatch(TripAction.removeTripTraveller(data))
        },
        makeGuide: (data) => {
            dispatch(TripAction.makeGuide(data))
        },
        dismissAsGuide: (data) => {
            dispatch(TripAction.dismissAsGuide(data))
        },
        deleteConflictedPlans: (id, data) => {
            dispatch(TripAction.deleteConflictedPlans(id, data))
        },
        getTripItinerary: (val) => {
            dispatch(TripAction.getTripItinerary(val))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTripScreen);
