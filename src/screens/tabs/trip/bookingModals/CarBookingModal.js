import React from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLOR from '../../../../res/styles/Color';
import { FloatingTitleTextInputField } from '../../../../commonView/globalComponent/FloatingTitleTextInputField';
import FONTS from '../../../../res/styles/Fonts';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import SimpleToast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import TripAction from '../../../../redux/actions/TripAction';
import ActivityIndicator from '../../../../commonView/globalComponent/ActivityIndicator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class CarBookingModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pickupLocation: '',
            dropoffLocation: '',
            confirmationNumber: '',
            datePicker: false,
            date: null,
            dateText: 'Select Date',
            endDate: null,
            endDateText: 'Select Time',
            endDatePicker: false,
            dropoffTimePicker: false,
            pickUpTimePicker: false,
            dropoffTime: null,
            pickupTime: null,
            dropoffTimeText: 'Select Time',
            pickupTimeText: 'Select Time',
            loading: false,

        }
    }

    updateMasterState = (attrName, value) => {
        this.setState({ [attrName]: value });
    };

    componentDidUpdate(prevProps) {
        if (prevProps.bookCarData != this.props.bookCarData) {
            this.props.getBooking(this.props.trip);
            this.setState({ loading: false });
            this.props.carRBSheet.current.close();
        }
    }

    bookCar = () => {
        if (this.state.date == null) {
            SimpleToast.show('Select start date')
        } else if (this.state.pickupTime == null) {
            SimpleToast.show('Select pickup time')
        } else if (this.state.endDate == null) {
            SimpleToast.show('Select end date')
        } else if (this.state.dropoffTime == null) {
            SimpleToast.show('Select drop-off time')
        } else if (this.state.pickupLocation == '') {
            SimpleToast.show('Enter pick-up location')
        } else if (this.state.dropoffLocation == '') {
            SimpleToast.show('Enter drop-off location')
        } else if (this.state.confirmationNumber == '') {
            SimpleToast.show('Enter Confirmation number')
        } else {
            this.setState({ loading: true });
            let data = JSON.stringify({
                trip: this.props.trip,
                startDate: this.state.date,
                startTime: this.state.pickupTime,
                endDate: this.state.endDate,
                endTime: this.state.dropoffTime,
                pickupLocation: this.state.pickupLocation,
                dropLocation: this.state.dropoffLocation,
                confirmationNumber: parseInt(this.state.confirmationNumber)
            });
            this.props.bookCar(data);
        }
    }

    onDatePicked = (date) => {
        this.setState({
            date: date.getTime(),
            dateText: moment(date).format("DD-MMM-YYYY"),
            datePicker: false,
        });
    };

    onEndDatePicked = (date) => {
        this.setState({
            endDate: date.getTime(),
            endDateText: moment(date).format("DD-MMM-YYYY"),
            endDatePicker: false,
        })
    }

    onPickupTimePick = (time) => {
        if (this.state.date) {
            this.setState({
                pickupTime: time.getTime(),
                pickupTimeText: moment(time).format("h:mm a"),
                pickUpTimePicker: false,
            })
        } else {
            SimpleToast.show('Please select Pick-up date first');
        }

    }

    onDropoffTimePick = (time) => {
        if (this.state.endDate) {
            this.setState({
                dropoffTime: time.getTime(),
                dropoffTimeText: moment(time).format("h:mm a"),
                dropoffTimePicker: false,
            })
        } else {
            SimpleToast.show('Please select Drop-off date first');
        }

    }

    render() {
        return (
            <SafeAreaView>
                <RBSheet
                    ref={this.props.carRBSheet}
                    animationType={'slide'}
                    // closeOnDragDown={true}
                    closeOnPressMask={true}
                    height={heightPercentageToDP(100)}
                    keyboardAvoidingViewEnabled={true}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "rgba(52, 52, 52, 0.5)",
                        },
                        draggableIcon: {
                            backgroundColor: "#000",
                        },
                        container: {
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            height: heightPercentageToDP(100),
                        },
                    }}>
                    <KeyboardAwareScrollView>
                        <View style={styles.navigationView}>
                            <View style={styles.topView}>
                                <TouchableOpacity onPress={() => this.props.carRBSheet.current.close()}>
                                    <Text>Cancel</Text>
                                </TouchableOpacity>
                                <Text style={{ fontSize: 20 }}>Add Booking</Text>
                                <TouchableOpacity onPress={() => this.bookCar()}>
                                    <Text>Save</Text>
                                </TouchableOpacity>
                            </View>
                            <Image
                                resizeMode={'contain'}
                                style={{ height: 140, width: 140, alignSelf: 'center', marginTop: 10, marginBottom: -30 }}
                                source={require('../../../../res/assets/car_icon.png')}
                            />
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="pickupLocation"
                                title="Pick-up Location"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.pickupLocation}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="dropoffLocation"
                                title="Drop-off Location"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.dropoffLocation}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>

                        <View style={{ width: widthPercentageToDP(90), flexDirection: 'row', alignSelf: 'center' }}>


                            <View style={[styles.text_input_card, { width: null, flex: 1, marginRight: 5 }]}>
                                <Text style={styles.inputLabel}>Pickup date</Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({ datePicker: true })}>
                                    <Text style={styles.inputText}>{this.state.dateText}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.text_input_card, { width: null, flex: 1, marginLeft: 5 }]}>
                                <Text style={styles.inputLabel}>Pickup Time</Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({ pickUpTimePicker: true })}>
                                    <Text style={styles.inputText}>{this.state.pickupTimeText}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ width: widthPercentageToDP(90), flexDirection: 'row', alignSelf: 'center' }}>


                            <View style={[styles.text_input_card, { width: null, flex: 1, marginRight: 5 }]}>
                                <Text style={styles.inputLabel}>Drop-Off date</Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({ endDatePicker: true })}>
                                    <Text style={styles.inputText}>{this.state.endDateText}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.text_input_card, { width: null, flex: 1, marginLeft: 5 }]}>
                                <Text style={styles.inputLabel}>Drop-Off Time</Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({ dropoffTimePicker: true })}>
                                    <Text style={styles.inputText}>{this.state.dropoffTimeText}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="confirmationNumber"
                                title="Confirmation Number"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.confirmationNumber}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                    </KeyboardAwareScrollView>

                    <DateTimePicker
                        isVisible={this.state.datePicker}
                        mode="date"
                        minimumDate={new Date()}
                        onConfirm={this.onDatePicked.bind(this)}

                        onCancel={() => this.setState({ datePicker: false })}
                    />
                    <DateTimePicker
                        isVisible={this.state.endDatePicker}
                        mode="date"
                        minimumDate={new Date()}
                        onConfirm={this.onEndDatePicked.bind(this)}
                        onCancel={() => this.setState({ endDatePicker: false })}
                    />
                    <DateTimePicker
                        isVisible={this.state.pickUpTimePicker}
                        mode="time"
                        date={new Date(this.state.date)}
                        minimumDate={new Date()}
                        onConfirm={this.onPickupTimePick.bind(this)}
                        onCancel={() => this.setState({ pickUpTimePicker: false })}
                    />
                    <DateTimePicker
                        isVisible={this.state.dropoffTimePicker}
                        mode="time"
                        date={new Date(this.state.endDate)}
                        onConfirm={this.onDropoffTimePick.bind(this)}

                        minimumDate={new Date()}
                        onCancel={() => this.setState({ dropoffTimePicker: false })}
                    />
                    <ActivityIndicator loading={this.state.loading} />


                </RBSheet>


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    navigationView: {
        backgroundColor: '#D0EFDA',
        paddingTop: Platform.OS == 'ios' ? 45 : 15,

    },
    topView: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: widthPercentageToDP(90),
        alignSelf: 'center',
        alignItems: 'center'
    },
    text_input_card: {
        width: widthPercentageToDP(90),
        alignSelf: "center",
        backgroundColor: COLOR.WHITE,
        elevation: 5,
        padding: 12,
        marginVertical: 8,
        shadowColor: COLOR.BLACK,
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 2 },
    },
    inputText: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        marginVertical: 5,
    },
    inputLabel: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        color: COLOR.PRIMARYGREY,
    },
})

const mapStateToProps = (state, ownProps) => {
    return {
        bookCarData: state.tripReducer.bookCar
    }
}

const mapDispatchToProps = dispatch => {
    return {
        bookCar: (data) => {
            dispatch(TripAction.bookCar(data))
        },
        getBooking: (val) => {
            dispatch(TripAction.getBookings(val));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CarBookingModal);