import React from 'react';
import { Dimensions, Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLOR from '../../../../res/styles/Color';
import { FloatingTitleTextInputField } from '../../../../commonView/globalComponent/FloatingTitleTextInputField';
import FONTS from '../../../../res/styles/Fonts';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import { parse } from '@babel/core';
import SimpleToast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import TripAction from '../../../../redux/actions/TripAction';
import ActivityIndicator from '../../../../commonView/globalComponent/ActivityIndicator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FlightBookingModal from './FlightBookingModal';
import AddressModal from './AddressModal';

class HotelBookingModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hotelName: '',
            departureAirport: '',
            arrivalAirport: '',
            confirmationNumber: '',
            datePicker: false,
            date: null,
            dateText: 'Select Date',
            endDate: null,
            endDateText: 'Select Time',
            endDatePicker: false,
            noOfRooms: '',
            hotelAddress: '',
            loading: false,
        }
        this.addressModal = React.createRef();
    }

    updateMasterState = (attrName, value) => {
        this.setState({ [attrName]: value });
    };

    bookHotel = () => {
        if (this.state.hotelName == '') {
            SimpleToast.show('Enter hotel name')
        } else if (this.state.date == null) {
            SimpleToast.show('Select Check-in date')
        } else if (this.state.endDate == null) {
            SimpleToast.show('Select Check-out date')
        } else if (this.state.confirmationNumber == '') {
            SimpleToast.show('Enter confirmation #')
        } else {
            this.setState({ loading: true });
            let data = JSON.stringify({
                trip: this.props.trip,
                name: this.state.hotelName,
                address: this.state.hotelAddress,
                checkIn: this.state.date,
                checkOut: this.state.endDate,
                confirmationNumber: this.state.confirmationNumber,
                roomCount: this.state.noOfRooms,
            });
            this.props.bookHotel(data);
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.bookHotelData != this.props.bookHotelData) {
            this.props.getBooking(this.props.trip);
            this.setState({ loading: false });
            this.props.hotelRBSheet.current.close();
        }
    }

    onDatePicked = (date) => {
        this.setState({
            date: date.getTime(),
            dateText: moment(date).format("DD-MMM-YYYY"),
            datePicker: false,
        });
    };

    onEndDatePicked = (date) => {
        this.setState({
            endDate: date.getTime(),
            endDateText: moment(date).format("DD-MMM-YYYY"),
            endDatePicker: false,
        })
    }

    render() {
        return (
            <SafeAreaView>
                <RBSheet
                    ref={this.props.hotelRBSheet}
                    animationType={'slide'}
                    // closeOnDragDown={true}
                    closeOnPressMask={true}
                    height={heightPercentageToDP(100)}
                    keyboardAvoidingViewEnabled={true}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "rgba(52, 52, 52, 0.5)",
                        },
                        draggableIcon: {
                            backgroundColor: "#000",
                        },
                        container: {
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            height: heightPercentageToDP(100),
                        },
                    }}>
                    <KeyboardAwareScrollView>
                        <View style={styles.navigationView}>
                            <View style={styles.topView}>
                                <TouchableOpacity onPress={() => this.props.hotelRBSheet.current.close()}>
                                    <Text>Cancel</Text>
                                </TouchableOpacity>
                                <Text style={{ fontSize: 20 }}>Add Booking</Text>
                                <TouchableOpacity onPress={() => this.bookHotel()}>
                                    <Text>Save</Text>
                                </TouchableOpacity>
                            </View>
                            <Image
                                resizeMode={'contain'}
                                style={{ height: 120, width: 120, alignSelf: 'center', marginTop: 10 }}
                                source={require('../../../../res/assets/hotel_icon.png')}
                            />
                        </View>
                        <View style ={{}}>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="hotelName"
                                title="Hotel Name"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.hotelName}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="hotelAddress"
                                title="Hotel Address"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                updateMasterState={this.updateMasterState}
                                onFocus={() => this.addressModal.current.open()}
                                color={'#FFB100'}
                                value={this.state.hotelAddress}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>

                        <View style={{ width: widthPercentageToDP(90), flexDirection: 'row', alignSelf: 'center' }}>
                            <View style={[styles.text_input_card, { width: null, flex: 1, marginRight: 5 }]}>
                                <Text style={styles.inputLabel}>Check in</Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({ datePicker: true })}>
                                    <Text style={styles.inputText}>{this.state.dateText}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.text_input_card, { width: null, flex: 1, marginLeft: 5 }]}>
                                <Text style={styles.inputLabel}>Check out</Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({ endDatePicker: true })}>
                                    <Text style={styles.inputText}>{this.state.endDateText}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="confirmationNumber"
                                title="Confirmation Number"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.confirmationNumber}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="noOfRooms"
                                title="Number of rooms"
                                isWhite={false}
                                keyboardType={'number-pad'}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.noOfRooms}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                        </View>
                    </KeyboardAwareScrollView>
                    <DateTimePicker
                        isVisible={this.state.datePicker}
                        mode="date"
                        minimumDate={new Date()}
                        onConfirm={this.onDatePicked.bind(this)}
                        onCancel={() => this.setState({ datePicker: false })}
                    />
                    <DateTimePicker
                        isVisible={this.state.endDatePicker}
                        mode="date"
                        minimumDate={new Date()}
                        onConfirm={this.onEndDatePicked.bind(this)}
                        onCancel={() => this.setState({ endDatePicker: false })}
                    />
                    <AddressModal
                        trip={this.props.trip}
                        attrName="hotelAddress"
                        addressModal={this.addressModal}
                        updateMasterState={this.updateMasterState}
                    />
                    <ActivityIndicator loading={this.state.loading} />

                </RBSheet>


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    navigationView: {
        backgroundColor: '#D0EFDA',
        paddingTop: Platform.OS == 'ios' ? 45 : 15,

    },
    topView: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: widthPercentageToDP(90),
        alignSelf: 'center',
        alignItems: 'center'
    },
    text_input_card: {
        width: widthPercentageToDP(90),
        alignSelf: "center",
        backgroundColor: COLOR.WHITE,
        elevation: 5,
        padding: 12,
        marginVertical: 8,
        shadowColor: COLOR.BLACK,
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 2 },
    },
    inputText: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        marginVertical: 5,
    },
    inputLabel: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        color: COLOR.PRIMARYGREY,
    },
})

const mapStateToProps = (state, ownProps) => {
    return {
        bookHotelData: state.tripReducer.bookHotel,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        bookHotel: (data) => {
            dispatch(TripAction.bookHotel(data));
        },
        getBooking: (val) => {
            dispatch(TripAction.getBookings(val));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HotelBookingModal);