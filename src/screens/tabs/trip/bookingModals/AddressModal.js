import React from 'react';
import { Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLOR from '../../../../res/styles/Color';
import FONTS from '../../../../res/styles/Fonts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

class AddressModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            address: '',
            place: '',
        }
        this.searchRef = React.createRef();
    }


    render() {
        return (
            <SafeAreaView>
                <RBSheet
                    ref={this.props.addressModal}
                    animationType={'slide'}
                    // closeOnDragDown={true}
                    closeOnPressMask={true}
                    height={heightPercentageToDP(100)}
                    keyboardAvoidingViewEnabled={true}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "rgba(52, 52, 52, 0.5)",
                        },
                        draggableIcon: {
                            backgroundColor: "#000",
                        },
                        container: {
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            height: heightPercentageToDP(100),
                        },
                    }}>
                    <KeyboardAwareScrollView keyboardShouldPersistTaps='handled'>
                        <View style={styles.navigationView}>
                            <View style={styles.topView}>
                                <TouchableOpacity onPress={() => this.props.addressModal.current.close()}>
                                    <Text>Cancel</Text>
                                </TouchableOpacity>
                                <Text style={{ fontSize: 20 }}>Address</Text>
                                <TouchableOpacity onPress={() => {
                                    this.props.updateMasterState(this.props.attrName, this.props.place ? this.state.place != '' ? this.state.place : this.searchRef.current.getAddressText() : this.searchRef.current.getAddressText());
                                    this.props.addressModal.current.close();
                                }}>
                                    <Text>Save</Text>
                                </TouchableOpacity>
                            </View>
                            <GooglePlacesAutocomplete
                                ref={this.searchRef}
                                styles={{
                                    textInputContainer: {
                                        borderWidth: 1,
                                        borderColor: '#E8E8E8',
                                        marginTop: 20,
                                        marginHorizontal: 15,
                                        borderRadius: 10,
                                        height: 40,
                                        borderRadius: 20,
                                        backgroundColor: '#F6F6F6'
                                    },
                                    textInput: {
                                        marginHorizontal: 5,
                                        height: 38,
                                        borderRadius: 20,
                                        backgroundColor: '#F6F6F6'
                                    },
                                    listView: {
                                        width: widthPercentageToDP(90),
                                        alignSelf: 'center'
                                    }
                                }}
                                placeholder='Search'
                                onPress={(data, details = null) => {
                                    this.setState({
                                        place: data.structured_formatting.main_text,
                                    })
                                }}

                                onFail={(error) => console.error(error)}
                                query={{
                                    key: 'AIzaSyAKhtBwQhmYRoAobEj7lx3jGztFyVzFF8s',
                                    language: 'en',
                                }}
                            />

                        </View>
                    </KeyboardAwareScrollView>


                </RBSheet>


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    navigationView: {
        backgroundColor: COLOR.WHITE,
        paddingTop: Platform.OS == 'ios' ? 45 : 15,
        paddingBottom: 15,
        marginTop: 10,
    },
    topView: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: widthPercentageToDP(90),
        alignSelf: 'center',
        alignItems: 'center'
    },
    text_input_card: {
        width: widthPercentageToDP(90),
        alignSelf: "center",
        backgroundColor: COLOR.WHITE,
        elevation: 5,
        padding: 12,
        marginVertical: 8,
        shadowColor: COLOR.BLACK,
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 2 },
    },
    inputText: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        marginVertical: 5,
    },
    inputLabel: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        color: COLOR.PRIMARYGREY,
    },
})





export default AddressModal;