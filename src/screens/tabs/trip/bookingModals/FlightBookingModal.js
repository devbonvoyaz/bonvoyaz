import React from 'react';
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLOR from '../../../../res/styles/Color';
import { FloatingTitleTextInputField } from '../../../../commonView/globalComponent/FloatingTitleTextInputField';
import FONTS from '../../../../res/styles/Fonts';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import { connect } from 'react-redux';
import TripAction from '../../../../redux/actions/TripAction';
import SimpleToast from 'react-native-simple-toast';
import ActivityIndicator from '../../../../commonView/globalComponent/ActivityIndicator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class FlightBookingModal extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            airlineName: '',
            departureAirport: '',
            arrivalAirport: '',
            confirmationNumber: '',
            datePicker: false,
            date: null,
            dateText: 'Select Date',
            time: null,
            timeText: 'Select Time',
            flightNumber: '',
            timePicker: false,
            loading: false,
            arrivalDate: null,
            arrivalTime: null,
            arrivalDateText: 'Select Date',
            arrivalTimeText: 'Select Time',
            datePickerIndex: null,
            timePickerIndex: null,
        }
    }

    updateMasterState = (attrName, value) => {
        this.setState({ [attrName]: value });
    };

    onDatePicked = (date) => {
        if (this.state.datePickerIndex == 1) {
            this.setState({
                date: date.getTime(),
                dateText: moment(date).format("DD-MMM-YYYY"),
                datePicker: false,
                time: null,
                timeText: 'Select Time',
            });
        } else {
            this.setState({
                arrivalDate: date.getTime(),
                arrivalDateText: moment(date).format("DD-MMM-YYYY"),
                datePicker: false,
                arrivalTime: null,
                arrivalTimeText: 'Select Time'
            });
        }
        console.log('Date --', date.getTime());
    };

    onTimePicked = (time) => {
        if (this.state.timePickerIndex == 1) {
            if (this.state.date) {
                this.setState({
                    time: time.getTime(),
                    timeText: moment(time).format("h:mm a"),
                    timePicker: false,
                })
                console.log('Time --', time.getTime());
            } else {
                SimpleToast.show('Please select date first');
            }
        } else {
            if (this.state.arrivalDate) {
                this.setState({
                    arrivalTime: time.getTime(),
                    arrivalTimeText: moment(time).format("h:mm a"),
                    timePicker: false,
                })
                console.log('Time --', time.getTime());
            } else {
                SimpleToast.show('Please select Arrival date first');

            }
        }

    }

    componentDidUpdate(prevProps) {
        if (prevProps.bookFlightData != this.props.bookFlightData) {
            this.props.getBooking(this.props.trip);
            this.setState({ loading: false });
            this.props.flightRBSheet.current.close();
        }

    }

    bookFlight = () => {
        if (this.state.airlineName == '') {
            SimpleToast.show('Enter the airline name');
        } else if (this.state.date == null) {
            SimpleToast.show('Select date')
        } else if (this.state.time == null) {
            SimpleToast.show('Select Time')
        } else if (this.state.arrivalAirport == '') {
            SimpleToast.show('Enter Arrival Airport')
        } else if (this.state.departureAirport == '') {
            SimpleToast.show('Enter Departure Airport')
        } else if (this.state.confirmationNumber == '') {
            SimpleToast.show('Enter confirmation number')
        } else {
            this.setState({ loading: true });
            let data = JSON.stringify({
                trip: this.props.trip,
                name: this.state.airlineName,
                arrivalDate: this.state.arrivalDate,
                arrivalTime: this.state.arrivalTime,
                departureDate: this.state.date,
                departureTime: this.state.time,
                flightNumber: this.state.flightNumber,
                arrival: this.state.arrivalAirport,
                destination: this.state.departureAirport,
                confirmationNumber: parseInt(this.state.confirmationNumber)
            });
            this.props.bookFlight(data);
        }
    }

    render() {
        return (
            <SafeAreaView>
                <RBSheet
                    ref={this.props.flightRBSheet}
                    animationType={'slide'}
                    // closeOnDragDown={true}
                    closeOnPressMask={true}
                    height={heightPercentageToDP(100)}
                    keyboardAvoidingViewEnabled={true}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "rgba(52, 52, 52, 0.5)",
                        },
                        draggableIcon: {
                            backgroundColor: "#000",
                        },
                        container: {
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            height: heightPercentageToDP(100),
                        },
                    }}>
                    <KeyboardAwareScrollView>
                        <View style={styles.navigationView}>
                            <View style={styles.topView}>
                                <TouchableOpacity onPress={() => this.props.flightRBSheet.current.close()}>
                                    <Text>Cancel</Text>
                                </TouchableOpacity>
                                <Text style={{ fontSize: 20 }}>Add Booking</Text>
                                <TouchableOpacity onPress={() => this.bookFlight()}>
                                    <Text>Save</Text>
                                </TouchableOpacity>
                            </View>
                            <Image

                                style={{ height: 120, width: widthPercentageToDP(100) }}
                                source={require('../../../../res/assets/book_flight_icon.png')}
                            />
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="airlineName"
                                title="Airline Name"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.airlineName}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="departureAirport"
                                title="Departure Airport"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.departureAirport}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="arrivalAirport"
                                title="Arrival Airport"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.arrivalAirport}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                        <View style={{ width: widthPercentageToDP(90), flexDirection: 'row', alignSelf: 'center' }}>


                            <View style={[styles.text_input_card, { width: null, flex: 1, marginRight: 5 }]}>
                                <Text style={styles.inputLabel}>Date</Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({
                                        datePicker: true,
                                        datePickerIndex: 1
                                    })}>
                                    <Text style={styles.inputText}>{this.state.dateText}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.text_input_card, { width: null, flex: 1, marginLeft: 5 }]}>
                                <Text style={styles.inputLabel}>Time</Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({
                                        timePicker: true,
                                        timePickerIndex: 1
                                    })}>
                                    <Text style={styles.inputText}>{this.state.timeText}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ width: widthPercentageToDP(90), flexDirection: 'row', alignSelf: 'center' }}>


                            <View style={[styles.text_input_card, { width: null, flex: 1, marginRight: 5 }]}>
                                <Text style={styles.inputLabel}>Arrival Date</Text>
                                <TouchableOpacity
                                    onPress={() =>
                                        this.setState({
                                            datePicker: true,
                                            datePickerIndex: 2
                                        })}>
                                    <Text style={styles.inputText}>{this.state.arrivalDateText}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.text_input_card, { width: null, flex: 1, marginLeft: 5 }]}>
                                <Text style={styles.inputLabel}>Arrival Time</Text>
                                <TouchableOpacity
                                    onPress={() => this.setState({
                                        timePicker: true,
                                        timePickerIndex: 2,
                                    })}>
                                    <Text style={styles.inputText}>{this.state.arrivalTimeText}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="flightNumber"
                                title="FLight Number"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.flightNumber}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                        <View style={styles.text_input_card}>
                            <FloatingTitleTextInputField
                                attrName="confirmationNumber"
                                title="Confirmation Number"
                                isWhite={false}
                                textInputStyles={{
                                    padding: 0
                                }}
                                color={'#FFB100'}
                                value={this.state.confirmationNumber}
                                updateMasterState={this.updateMasterState}
                            />
                        </View>
                    </KeyboardAwareScrollView>
                    <DateTimePicker
                        isVisible={this.state.datePicker}
                        mode="date"
                        minimumDate={new Date()}
                        onConfirm={this.onDatePicked.bind(this)}

                        onCancel={() => this.setState({ datePicker: false })}
                    />
                    <DateTimePicker
                        isVisible={this.state.timePicker}
                        mode="time"
                        date={new Date(this.state.date)}
                        minimumDate={new Date()}
                        onConfirm={this.onTimePicked.bind(this)}
                        onCancel={() => this.setState({ timePicker: false })}
                    />
                    <ActivityIndicator loading={this.state.loading} />

                </RBSheet>


            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    navigationView: {
        backgroundColor: '#D0EFDA',
        paddingTop: Platform.OS == 'ios' ? 45 : 15,
        paddingBottom: 15,
    },
    topView: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: widthPercentageToDP(90),
        alignSelf: 'center',
        alignItems: 'center'
    },
    text_input_card: {
        width: widthPercentageToDP(90),
        alignSelf: "center",
        backgroundColor: COLOR.WHITE,
        elevation: 5,
        padding: 12,
        marginVertical: 8,
        shadowColor: COLOR.BLACK,
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 2 },
    },
    inputText: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        marginVertical: 5,
    },
    inputLabel: {
        fontSize: 12,
        fontFamily: FONTS.FAMILY_MEDIUM,
        color: COLOR.PRIMARYGREY,
    },
})

const mapStateToProps = (state, ownProps) => {
    return {
        bookFlightData: state.tripReducer.bookFlight,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        bookFlight: (data) => {
            dispatch(TripAction.bookFlight(data))
        },
        getBooking: (val) => {
            dispatch(TripAction.getBookings(val));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FlightBookingModal);