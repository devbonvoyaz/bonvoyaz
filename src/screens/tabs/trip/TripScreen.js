import React from 'react';
import { Text, View, Image, SafeAreaView, StyleSheet, ScrollView } from 'react-native';
import COLOR from '../../../res/styles/Color';
import { FlatList } from 'react-native-gesture-handler';
import Styles from '../../../res/styles/Styles';
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import ActionButton from 'react-native-action-button';
import TripObject from '../../../commonView/TripView/TripObject';
import { connect } from "react-redux";
import TripAction from "../../../redux/actions/TripAction";
import moment from "moment";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Upcoming from './Upcoming';
import { GOOGLE_IMAGE, GOOGLE_PLACES_API } from '../../../api/Constants';

const TripTabs = createMaterialTopTabNavigator();

class TripScreen extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            tabIndex: 0,
            tripsData: [],
            prevTripData: [],
        }
        this.props.getAllTrips(1);
        this.props.getAllTrips(2);
    }

    componentDidMount() {
        if (this.props.tripsData) {
            this.setState({
                tripsData: this.props.tripsData,
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.tripsData != this.props.tripsData) {
            this.setState({
                tripsData: this.props.tripsData,
            })
        }

        if (prevProps.pastTripData != this.props.pastTripData) {
            this.setState({
                prevTripData: this.props.pastTripData,
            })
        }

    }


    handleTabsChange = index => {
        this.setState({
            tabIndex: index,
        })
        index == 0 ? this.props.getAllTrips(2) : this.props.getAllTrips(1);

    };

    getImageUrl = (val) => {

        return GOOGLE_IMAGE + val + '&key=' + GOOGLE_PLACES_API;
    }

    render() {
        return (
            <SafeAreaView style={styles.containerView}>
                <View style={styles.toolbar} >
                    <View style={styles.titleView} >
                        <Text style={Styles.screen_heading}>
                            Trips
                        </Text>
                    </View>
                </View>
                <View style={Styles.hr} />

                <TripTabs.Navigator
                    initialRouteName="upcoming"
                    style={{
                        flex: 1,
                        backgroundColor: COLOR.WHITE
                    }}
                    swipeEnabled={true}
                    tabBarOptions={{
                        activeTintColor: COLOR.PRIMARTGREEN,
                        inactiveTintColor: COLOR.BLACK,
                        indicatorStyle: {
                            backgroundColor: COLOR.WHITE,
                            height: "100%",
                            width: "50%",
                            borderRadius: 40,
                            borderWidth: 1.5,
                            borderColor: COLOR.GREY
                        },
                        labelStyle: {
                            textTransform: 'none'
                        },
                        style: {
                            width: widthPercentageToDP(80),
                            backgroundColor: COLOR.GREY,
                            alignSelf: 'center',
                            borderRadius: 25,
                            marginVertical: 10,
                        },

                    }}
                >

                    <TripTabs.Screen
                        name="Upcoming"
                        children={() => <this.upcoming />}
                        options={{ tabBarLabel: "Upcoming", gestureEnabled: true }}
                    />
                    <TripTabs.Screen
                        name="Past"
                        children={() => <this.past />}
                        options={{ tabBarLabel: "Past" }}
                    />

                </TripTabs.Navigator>

            </SafeAreaView>
        );
    }

    upcoming = props => {
        return (

            <View
                style={{ backgroundColor: COLOR.WHITE, flexGrow: 1, flex: 1 }}>
                <Upcoming navigation={this.props.navigation} tripsData={this.state.tripsData} />



                <ActionButton
                    buttonColor={COLOR.PRIMARTGREEN}
                    onPress={() => {
                        this.props.navigation.navigate('AddTripScreen');
                    }}
                />
            </View>
        )
    }

    past = props => {
        return (
            <View style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
                <FlatList
                    style={{ width: widthPercentageToDP(95), alignSelf: 'center', flex: 1 }}
                    data={this.state.prevTripData}
                    renderItem={({ item }) => (
                        <TripObject
                            onPress={() => {
                                this.props.navigation.navigate('ViewTripScreen', { id: item.id });
                            }}
                            name={item.tripName}
                            image={item.image != "" ? { uri: this.getImageUrl(item.image) } : require('../../../res/assets/city.png')}
                            duration={moment.unix(item.startDate / 1000).format("ddd, DD MMM YYYY ") + ' - ' + moment.unix(item.endDate / 1000).format("ddd, DD MMM YYYY")}
                            createdAt={item.time}
                        />
                    )}
                    ListEmptyComponent={() => (
                        <Text style={{
                            alignSelf: 'center',
                            marginVertical: 50,
                        }}>{'No Past trips'}</Text>
                    )}
                />

            </View>
        )
    }
}

const styles = StyleSheet.create({
    toolbar: {
        flexDirection: 'row',
        marginVertical: 12,
        alignItems: 'center',
    },
    containerView: {
        flex: 1,
        backgroundColor: COLOR.WHITE
    },
    titleView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    }
})

const mapStateToProps = (state, ownProps) => {
    return {
        tripsData: state.tripReducer.tripsData,
        pastTripData: state.tripReducer.pastTripData,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAllTrips: (val) => {
            dispatch(TripAction.getAllTrips(val));
        }
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(TripScreen);