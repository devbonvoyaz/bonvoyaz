import React from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect } from 'react-redux';

import ActionButton from 'react-native-action-button';
import LinearGradient from 'react-native-linear-gradient';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Styles from '../../../res/styles/Styles';
import Icon from "react-native-vector-icons/FontAwesome";
import Ionicon from 'react-native-vector-icons/Ionicons';
import Itinerary from './TripDetails/Itinerary';
import Bookings from './TripDetails/Bookings';
import Spending from './TripDetails/Spending';
import moment from "moment";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import TripAction from "../../../redux/actions/TripAction";
import TripTabBar from './TripTabBar';

const TripTabs = createMaterialTopTabNavigator();

class ViewTripScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0,
      id: this.props.route.params.id,
      tripName: '',
      startDate: '',
      endDate: '',
    }
    this.props.getSingleTrip(this.props.route.params.id)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.singleTrip != this.props.singleTrip && this.props.singleTrip.tripName != null) {
      let data = this.props.singleTrip;
      this.setState({
        tripName: data.tripName,
        startDate: moment.unix(data.startDate / 1000).format("ddd, DD MMM YYYY"),
        endDate: moment.unix(data.endDate / 1000).format("ddd, DD MMM YYYY"),
      })
    }
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
        <View style={style.toolbar}>

          <TouchableOpacity style={style.backButton} onPress={() => {
            this.props.navigation.goBack();
          }}>
            <Icon name="chevron-left" style={{ alignSelf: 'center' }} size={18} />
          </TouchableOpacity>

          <Text style={[Styles.screen_heading, { alignSelf: 'center' }]}>
            {this.state.tripName}
          </Text>

          <TouchableOpacity style={style.backButton} onPress={() => {
            this.props.navigation.navigate('EditTripScreen', { item: this.state.item });
          }}>
            <Icon name="pencil" size={18} />
          </TouchableOpacity>
        </View>

        <Text
          style={{
            fontFamily: FONTS.FAMILY_REGULAR,
            alignSelf: 'center',
            marginBottom: 0,
          }}
        >
          {this.state.startDate + ' - ' + this.state.endDate}
        </Text>

        <TripTabs.Navigator
          initialRouteName="itinerary"
          tabBar={props => <TripTabBar {...props} />}
          tabBarOptions={{
            activeTintColor: COLOR.WHITE,
            inactiveTintColor: COLOR.PRIMARTGREEN,
            tabStyle: {
              width: widthPercentageToDP(90)
            }
          }}
        >
          <TripTabs.Screen
            name="itinerary"
            children={() => <Itinerary trip={this.props.route.params.id} navigation={this.props.navigation} />}
            options={{ tabBarLabel: "Itinerary" }}
          />
          <TripTabs.Screen
            name="booking"
            children={() => <Bookings trip={this.props.route.params.id} navigation={this.props.navigation} />}
            options={{ tabBarLabel: "Reservations" }}
          />
          <TripTabs.Screen
            name="spending"
            children={() => <Spending {...this.props} />}
            options={{ tabBarLabel: "Spending" }}
          />
        </TripTabs.Navigator>
      </SafeAreaView >
    );
  }
}

const NoPlans = props => {
  const navigation = useNavigation();
  return (
    <View
      style={style.noPlanView}
    >
      <View>
        <Text
          style={{
            fontFamily: FONTS.FAMILY_BOLD,
            fontSize: 18,
            alignSelf: 'center',
            marginTop: 15,
          }}
        >
          {this.state.tripName}
        </Text>
        <Text
          style={{
            fontFamily: FONTS.FAMILY_REGULAR,
            fontSize: 14,
            alignSelf: 'center',
            marginTop: 10,
          }}
        >
          {this.state.startDate + ' - ' + this.state.endDate}
        </Text>
      </View>
      <Text
        style={{
          fontFamily: FONTS.FAMILY_REGULAR,
          fontSize: 14,
          marginTop: 20,
          marginBottom: 10,
          color: COLOR.TEXTCOLOR,
          alignSelf: 'center',
        }}
      >
        No Plans
      </Text>
      <LinearGradient
        style={{
          margin: 25,
          borderRadius: 8,
        }}
        colors={[COLOR.PRIMARTGREEN, 'rgba(40, 195, 109, 0.8)']}
      >
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('AddPlan', { trip: props.trip });
          }}
        >
          <Text
            style={{
              color: COLOR.WHITE,
              fontFamily: FONTS.FAMILY_MEDIUM,
              fontSize: 16,
              alignSelf: 'center',
              paddingVertical: 12,
            }}
          >
            Add Plan
          </Text>
        </TouchableOpacity>
      </LinearGradient>
    </View>
  );
};

const style = StyleSheet.create({
  menuIcon: {
    width: 15,
    height: 15,
    tintColor: COLOR.BLACK,
    alignSelf: 'center',
  },
  backButton: {
    marginHorizontal: 20,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  toolbar: {
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  selectionView: {
    width: widthPercentageToDP(90),
    flexDirection: 'row',
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    shadowOffset: { width: 2, height: 2 },
    borderRadius: 10,
    padding: 15,
    marginVertical: 10,
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
    justifyContent: 'space-between'
  },
  itineraryView: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginHorizontal: 5,
    justifyContent: 'center',
    backgroundColor: '#01CAE4',

  },
  bookingView: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginHorizontal: 5,
    justifyContent: 'center',
    backgroundColor: '#8149FA'
  },
  image_background: {
    backgroundColor: COLOR.PRIMARYBLUE,
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
    height: 100,
  },
  border_Text: {
    fontSize: 6,
    padding: 5,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: COLOR.PRIMARYBLACK,
    color: COLOR.PRIMARYBLACK,
  },
  noPlanView: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',

    elevation: 5,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    shadowOffset: { width: 3, height: 3 },
    justifyContent: 'space-between',
    borderRadius: 8,
    backgroundColor: COLOR.WHITE,
    marginVertical: 8,
  },
  bookingText: {
    fontSize: 12,
    alignSelf: 'center',
    marginVertical: 5,
  }
});


//Objects
const mapStateToProps = (state, ownProps) => {
  return {
    singleTrip: state.tripReducer.singleTrip,
  }
}
const mapDispatchToProps = dispatch => {
  return {
    getSingleTrip: (id) => {
      dispatch(TripAction.getSingleTrip(id))
    },
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ViewTripScreen);