import React from 'react';
import { FlatList, Text, View } from 'react-native';
import moment from 'moment';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import TripObject from '../../../commonView/TripView/TripObject';
import { ScrollView } from 'react-native';
import { GOOGLE_IMAGE, GOOGLE_PLACES_API } from '../../../api/Constants';


export default function Upcoming(props) {
    const getImageUrl = (val) => {
        console.log(GOOGLE_IMAGE + val + '&key=' + GOOGLE_PLACES_API);
        return GOOGLE_IMAGE + val + '&key=' + GOOGLE_PLACES_API;
    }
    return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <FlatList
                style={{ width: widthPercentageToDP(95), alignSelf: 'center', flex: 1 }}
                data={props.tripsData}
                scrollEnabled={false}
                renderItem={({ item }) => (
                    <TripObject
                        onPress={() => {
                            props.navigation.navigate('ViewTripScreen', { id: item.id });
                        }}
                        name={item.tripName}
                        image={item.image != "" ? { uri: getImageUrl(item.image) } : require('../../../res/assets/city.png')}
                        duration={moment.unix(item.startDate / 1000).format("ddd, DD MMM YYYY") + ' - ' + moment.unix(item.endDate / 1000).format("ddd, DD MMM YYYY")}
                        createdAt={item.time}
                    />
                )}
                ListEmptyComponent={() => (
                    <Text style={{
                        alignSelf: 'center',
                        marginVertical: 50,
                    }}>{'No upcoming trips'}</Text>
                )}

            />
        </ScrollView>
    )
}