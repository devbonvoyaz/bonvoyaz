import * as React from "react";
import {
  Button,
  View,
  Text,
  Dimensions,
  TextInput,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
  Image
} from "react-native";
import RBSheet from "react-native-raw-bottom-sheet";
import COLOR from "../../../../../res/styles/Color";
import Icon from "react-native-vector-icons/Ionicons";
import KeyboardArrowDown from "react-native-vector-icons/MaterialIcons";
import Currencies from "../../../../../mockData/currency.json";
import CurrencyPicker from "../../../../../reusable/CurrencyPicker";

const deviceheight = Dimensions.get("window").height;
const devicewidth = Dimensions.get("window").width;

const CategoryModal = (props) => {
  const refCurrencyModal = React.useRef();
  const [currencies, setCurrencies] = React.useState(Currencies);
  const [category, setCategory] = React.useState('')
  const [amount, setAmount] = React.useState('')
  const [selectedCurrency, setSelectedCurrency] = React.useState({
    "code": "USD",
    "name": "Dollar",
    "symbol": "$"
  })

  return (
    <View>
      {/* To close RBSheet we need to use
      categoryRBSheet.current.close() */}
      <RBSheet
        ref={props.categoryRBSheet}
        animationType={'slide'}
        // closeOnDragDown={true}
        closeOnPressMask={true}
        height={deviceheight}
        keyboardAvoidingViewEnabled={true}
        customStyles={{
          wrapper: {
            backgroundColor: "rgba(52, 52, 52, 0.5)",
          },
          draggableIcon: {
            backgroundColor: "#000",
          },
          container: {
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            height: 400,
          },
        }}
      >
        <View
          style={{
            flex: 1,
            height: "100%",
            backgroundColor: "white",
            alignItems: "center",
          }}
        >
          <Icon
            name={"close"}
            style={{ fontSize: 24, alignSelf: "flex-end", margin: 10 }}
            onPress={() => props.categoryRBSheet.current.close()}
          />
          <Text style={styles.title}>Add a Category</Text>
          <TextInput
            placeholder={"Enter Category"}
            value={category}
            onChangeText={(val)=>setCategory(val)}
            placeholderTextColor={COLOR.PRIMARYGREY}
            keyboardType={"default"}
            style={styles.category}
          />
          <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
            <TextInput
              placeholder={"Enter Amount"}
              value={amount}
              onChangeText={(val)=>setAmount(val)}
              placeholderTextColor={COLOR.PRIMARYGREY}
              keyboardType={"number-pad"}
              style={styles.category}
            />
            <TouchableOpacity
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginLeft: -100,
                backgroundColor: "rgba(45, 156, 219, 0.1)",
                height: 40,
                width: 100,
                justifyContent: "center",
              }}
              onPress={() => {
                refCurrencyModal.current.open();
              }}
            >
              <Text style={{ color: COLOR.PRIMARYBLUE }}>{selectedCurrency.code} {selectedCurrency.symbol}</Text>
              <KeyboardArrowDown
                name="keyboard-arrow-down"
                style={{ fontSize: 24, color: COLOR.PRIMARYBLUE }}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.addButton}
            onPress={()=>{
              props.categoryRBSheet.current.close()
              props.addCategoryList({
                category,
                amount,
                selectedCurrency
              })
              setCategory('')
              setAmount('')
            }}
          >
            <Text style={styles.addButtonText}>Add</Text>
          </TouchableOpacity>
        </View>
        <CurrencyPicker
          refCurrencyModal={refCurrencyModal}
          // closeOnDragDown={true}
          closeOnPressMask={true}
          height={deviceheight}
          keyboardAvoidingViewEnabled={true}
          customStyles={{
            wrapper: {
              backgroundColor: "rgba(52, 52, 52, 0.5)",
            },
            draggableIcon: {
              backgroundColor: "#000",
            },
            container: {},
          }}
          setSelectedCurrency={setSelectedCurrency}
          setCurrencies={setCurrencies}
          currencies={currencies}
        />
      </RBSheet>
    </View>
  );
};

const styles = StyleSheet.create({
  category: {
    height: 40,
    width: "80%",
    fontSize: 14,
    borderColor: COLOR.PRIMARYGREY,
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    marginTop: 20,
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
  },
  addButton: {
    height: 40,
    width: "80%",
    backgroundColor: COLOR.PRIMARYBLUE,
    borderRadius: 5,
    padding: 10,
    alignItems: "center",
    marginTop: 20,
  },
  addButtonText: {
    fontSize: 14,
    color: COLOR.WHITE,
  },
});

export default CategoryModal;
