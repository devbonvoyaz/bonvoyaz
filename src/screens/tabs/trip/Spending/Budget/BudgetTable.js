import * as React from "react";
import { Text, View, StyleSheet } from "react-native";
import COLOR from "../../../../../res/styles/Color";

const BudgetTable = (props) => {

  return (
    <View style={{ margin: 10, marginTop: 40 }}>
      <View style={styles.headerStyle}>
        <Text style={[styles.title, {flex: 3}]}>Category</Text>
        <Text style={[styles.title, {flex: 1}]}>Amount</Text>
        <Text style={[styles.title, {flex: 1}]}>Spent</Text>
      </View>
      {
          props.categoryList.map(entry=>{
              return (
                <View style={styles.bodyRowStyle}>
                    <Text style={[styles.categoryText, {flex: 3}]}>{entry.category}</Text>
                    <Text style={[styles.categoryText, {flex: 1, color: COLOR.PRIMARYBLUE}]}>{entry.selectedCurrency?.currency?.symbol} {entry.amount}</Text>
                    <Text style={[styles.categoryText, {flex: 1, color: COLOR.PRIMARTGREEN}]}>{entry.selectedCurrency?.currency?.symbol} {entry.spent || 0}</Text>
                </View>
              )
          })
      }
    </View>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    flexDirection: "row",
    backgroundColor: COLOR.PRIMARTGREEN,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    height: 30,
    alignItems: "center"
  },
  title: {
    color: COLOR.WHITE,
    textAlign: 'left',
    paddingHorizontal: 10
  },
  bodyRowStyle: {
    borderBottomWidth: 1,
    borderBottomColor: COLOR.GREY,
    flexDirection: "row",
    paddingVertical: 10
  },
  categoryText: {
    color: COLOR.BLACK,
    textAlign: 'left',
    paddingHorizontal: 10
  }
});

export default BudgetTable;
