import React from "react";
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  View,
  TextInput,
} from "react-native";
import COLOR from "../../../../../res/styles/Color";

export default function MembersList(props) {
  const {
    members,
    selectedMember,
    selectUser,
    clickDisabled,
    hasInput,
    selectedSplitTab,
    changeUserSplit,
    memberSplits,
    selectedCurrency,
  } = props;

  return (
    <FlatList
      style={{ width: "100%" }}
      data={members}
      renderItem={({ item: user }) => {
        // console.log(user, memberSplits[user.id]);
        return (
          <TouchableOpacity
            key={user.id}
            style={[
              styles.flatButton,
              {
                backgroundColor:
                  selectedMember && selectedMember.id === user.id
                    ? "rgba(45, 156, 219, 0.1)"
                    : COLOR.WHITE,
              },
            ]}
            onPress={() => {
              selectUser && selectUser(user);
            }}
            disabled={clickDisabled}
          >
            <View style={styles.leftContainer}>
              <Image style={styles.userImage} source={user.profilePic} />
              <Text style={{ color: COLOR.BLACK }}>{user.name}</Text>
            </View>
            {hasInput && (
              <View
                style={{
                  backgroundColor: COLOR.LIGHTBLUE02,
                  flexDirection: "row",
                  alignItems: "center",
                  paddingHorizontal: 5,
                  borderRadius: 5,
                }}
              >
                <Text style={{ color: COLOR.PRIMARYBLUE }}>
                {selectedSplitTab.key!=='percent' && selectedCurrency.symbol}
                </Text>
                <TextInput
                  value={memberSplits[user.id]}
                  onChangeText={(val) => {
                    changeUserSplit(selectedSplitTab, user, val);
                  }}
                  keyboardType={"number-pad"}
                  style={styles.input}
                />
                <Text style={{ color: COLOR.PRIMARYBLUE }}>
                  {selectedSplitTab.key==='percent' && selectedSplitTab.label}
                </Text>
              </View>
            )}
          </TouchableOpacity>
        );
      }}
    />
  );
}

const styles = StyleSheet.create({
  flatButton: {
    flexDirection: "row",
    paddingVertical: 10,
    paddingHorizontal: 30,
    alignItems: "center",
    width: "100%",
    justifyContent: "space-between",
  },
  leftContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  userImage: {
    height: 40,
    width: 40,
    marginRight: 10,
    borderRadius: 50,
    resizeMode: "cover",
  },
  input: {
    height: 30,
    width: 50,
    fontSize: 14,
    textAlign: "left",
    color: COLOR.PRIMARYBLUE,
    padding: 5,
  },
});
