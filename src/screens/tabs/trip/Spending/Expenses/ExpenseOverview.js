import React from "react";
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from "react-native";
import COLOR from "../../../../../res/styles/Color";
import KeyboardArrowDown from "react-native-vector-icons/MaterialIcons";
import RnVectorIcon from "../../../../../reusable/RnVectorIcon";

const ExpenseOverview = (props) => {
  const {
    category,
    amount,
    selectedCurrency,
    description,
    refCurrencyModal,
    setField
  } = props;
  return (
    <>
      <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
        <RnVectorIcon
          iconFamily="MaterialIcons"
          name="category"
          style={{ position: "absolute", bottom: 8 }}
          size={24}
        />
        <TextInput
          placeholder={"Enter Category"}
          value={category}
          onChangeText={(val) => setField('category', val)}
          placeholderTextColor={COLOR.PRIMARYGREY}
          keyboardType={"default"}
          style={styles.category}
        />
      </View>
      <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
        <RnVectorIcon
          iconFamily="Ionicons"
          name="cash-outline"
          style={{ position: "absolute", bottom: 8 }}
          size={22}
        />
        <TextInput
          placeholder={"Enter Amount"}
          value={amount}
          onChangeText={(val) => setField('amount', val)}
          placeholderTextColor={COLOR.PRIMARYGREY}
          keyboardType={"number-pad"}
          style={[styles.category, { paddingLeft: 28 }]}
        />
        <TouchableOpacity
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginLeft: -100,
            borderRadius: 5,
            marginBottom: 5,
            backgroundColor: "rgba(45, 156, 219, 0.1)",
            height: 40,
            width: 100,
            justifyContent: "center",
          }}
          onPress={() => {
            refCurrencyModal.current.open();
          }}
        >
          <Text style={{ color: COLOR.PRIMARYBLUE }}>
            {selectedCurrency.code} {selectedCurrency.symbol}
          </Text>
          <KeyboardArrowDown
            name="keyboard-arrow-down"
            style={{ fontSize: 24, color: COLOR.PRIMARYBLUE }}
          />
        </TouchableOpacity>
      </View>
      <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
        <RnVectorIcon
          iconFamily="MaterialIcons"
          name="description"
          style={{ position: "absolute", bottom: 8 }}
          size={24}
        />
        <TextInput
          placeholder={"Enter Description"}
          value={description}
          onChangeText={(val) => setField('description', val)}
          placeholderTextColor={COLOR.PRIMARYGREY}
          keyboardType={"number-pad"}
          style={styles.category}
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  category: {
    height: 40,
    width: "80%",
    fontSize: 14,
    borderBottomColor: COLOR.LIGHTGREY,
    borderBottomWidth: 1,
    borderRadius: 5,
    padding: 10,
    paddingLeft: 30,
    marginTop: 15,
  },
});


export default ExpenseOverview