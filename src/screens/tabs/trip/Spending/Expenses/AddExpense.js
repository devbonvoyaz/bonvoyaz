import React from "react";
import {
  Button,
  View,
  Text,
  Dimensions,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
  Image,
  Platform,
  KeyboardAvoidingView,
} from "react-native";
import Styles from "../../../../../res/styles/Styles";
import COLOR from "../../../../../res/styles/Color";
import Icon from "react-native-vector-icons/FontAwesome";
import KeyboardArrowDown from "react-native-vector-icons/MaterialIcons";
import Currencies from "../../../../../mockData/currency.json";
import RnVectorIcon from "../../../../../reusable/RnVectorIcon";
import CurrencyPicker from "../../../../../reusable/CurrencyPicker";
import ExpenseOverview from "./ExpenseOverview";
import MembersList from "./MembersList";
import SplitBetweenView from "./SplitBetweenView";
import { ScrollView } from "react-native-gesture-handler";

const deviceheight = Dimensions.get("window").height;

class AddExpense extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "New Expense",
      category: "",
      amount: "0",
      description: "",
      totalSplitAmount: "0",
      selectedCurrency: {
        code: "USD",
        name: "Dollar",
        symbol: "$",
      },
      memberSplits: {},
      currencies: Currencies,
      refCurrencyModal: React.createRef(),
      tabs: [
        {
          key: "paid_by",
          label: "Paid By",
        },
        {
          key: "split_between",
          label: "Split Between",
        },
      ],
      activeState: "paid_by",
      splitTabs: [
        {
          label: "=",
          key: "equally",
        },
        {
          label: "/",
          key: "fraction",
        },
        {
          label: "%",
          key: "percent",
        },
      ],
      selectedSplitTab: {
        label: "=",
        key: "equally",
      },
      paidUser: {
        id: "1sdxacc",
        name: "You",
        profilePic: require("../../../../../res/assets/dummy_user_him.jpeg"),
        gender: "MALE",
      },
      tripMembers: [
        {
          id: "1sdxacc",
          name: "You",
          profilePic: require("../../../../../res/assets/dummy_user_him.jpeg"),
          gender: "MALE",
        },
        {
          id: "2ewdxacc",
          name: "Jenny Johns",
          profilePic: require("../../../../../res/assets/dummy_her.png"),
          gender: "FEMALE",
        },
        {
          id: "3r4efesdxacc",
          name: "Maddy",
          profilePic: require("../../../../../res/assets/dummy_user_her.jpeg"),
          gender: "FEMALE",
        },
        {
          id: "87vdxacc",
          name: "Jack Hinders",
          profilePic: require("../../../../../res/assets/dummy_him.png"),
          gender: "MALE",
        },
        {
          id: "1sdxafsccc",
          name: "You",
          profilePic: require("../../../../../res/assets/dummy_user_him.jpeg"),
          gender: "MALE",
        },
        {
          id: "vywbewdxacc",
          name: "Jenny Johns",
          profilePic: require("../../../../../res/assets/dummy_her.png"),
          gender: "FEMALE",
        },
        {
          id: "3r48sdxacc",
          name: "Maddy",
          profilePic: require("../../../../../res/assets/dummy_user_her.jpeg"),
          gender: "FEMALE",
        },
        {
          id: "87vdc",
          name: "Jack Hinders",
          profilePic: require("../../../../../res/assets/dummy_him.png"),
          gender: "MALE",
        },
      ],
    };
  }

  componentDidMount() {
    const { splitTabs, tripMembers } = this.state;
    let memberSplits = {};
    splitTabs.forEach((tab) => {
      memberSplits[tab.key] =
        tripMembers &&
        tripMembers.reduce((res, user) => {
          res[user.id] = "0";
          return res;
        }, {});
    });
    this.setState({
      memberSplits,
    });
  }

  setSelectedCurrency = (val) => {
    this.setState({
      selectedCurrency: val,
    });
  };

  setCurrencies = (val) => {
    this.setState({
      currencies: val,
    });
  };

  selectPaidUser = (user) => {
    /* For selecting multiple paid users */
    // const { tripMembers } = this.state;
    // let updatedList = tripMembers.map((member) => {
    //   if (member.id === user.id) {
    //     member.paid = member.paid ? !member.paid : true;
    //   }
    //   return member;
    // });

    this.setState({
      paidUser: user,
    });
  };

  updateTotalSplitAmount = (type) => {
    const {amount} = this.state
    let totalSplitAmount = Object.values(this.state.memberSplits[type]).reduce((res, curr)=>{
      res += Number(curr)
      return res
    }, 0)
    this.setState({
      totalSplitAmount: totalSplitAmount
    })
  }

  calculateSplitPercent = (splitTotal, totalAmount, split) => {
    let res = Number(splitTotal)/Number(totalAmount)
    if(split){
      if(Number(totalAmount)=="0"){
        return splitTotal
      }else{
        return 100*res
      }
    }else{
      if(!Number(totalAmount)){
        return 0
      }else{
        return 100
      }
    }
  }

  setField = (field, value) => {
    if(field==='amount'){
      value = value.replace(/^0+/, "")
      value = value || "0"
    }
    this.setState(
      {
        [field]: value,
      },
      (newVal) => {
        const {tripMembers, amount, memberSplits, selectedSplitTab} = this.state
        if (field === "amount" && selectedSplitTab.key==="equally") {
          let updatedMemberSplits = {};
            memberSplits["equally"] =
              tripMembers &&
              tripMembers.reduce((res, user) => {
                res[user.id] = `${amount/tripMembers.length}`;
                return res;
              }, {});
          this.setState({
            memberSplits: {
              ...memberSplits,
              updatedMemberSplits
            }
          }, ()=>{
            this.updateTotalSplitAmount("equally")
          });
        }else if(field==="amount"){
          this.updateTotalSplitAmount(selectedSplitTab.key)
        }
      }
    );
  };


  changeSplit = (val) => {
    this.setState({
      selectedSplitTab: val,
    }, ()=>{
      this.updateTotalSplitAmount(val.key)
    });
  };

  changeUserSplit = (type, user, amount) => {
    const { memberSplits } = this.state;
    const updatedMemberSplits = memberSplits;
    let updatedAmount = amount.replace(/^0+/, "");
    updatedMemberSplits[type.key][user.id] = updatedAmount || "0";
    this.setState({
      memberSplits: updatedMemberSplits,
    }, ()=>{
      this.updateTotalSplitAmount(type.key)
    });
  };

  render() {
    const {
      title,
      category,
      amount,
      selectedCurrency,
      currencies,
      description,
      refCurrencyModal,
      activeState,
      tripMembers,
      tabs,
      paidUser,
      splitTabs,
      selectedSplitTab,
      memberSplits,
      totalSplitAmount
    } = this.state;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
        <View style={styles.toolbar}>
          <TouchableOpacity
            style={styles.backButton}
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <RnVectorIcon
              iconFamily="FontAwesome"
              name="chevron-left"
              style={{ alignSelf: "center" }}
              size={18}
            />
          </TouchableOpacity>

          <Text style={[Styles.screen_heading, { alignSelf: "center" }]}>
            {title}
          </Text>
          {/* dummy view for making title align center */}
          <TouchableOpacity style={styles.backButton}></TouchableOpacity>
        </View>
        <KeyboardAvoidingView behavior="padding">
          <ScrollView>
            <View
              style={{
                height: 200,
                backgroundColor: "white",
                // marginTop: 10,
                alignItems: "center",
              }}
            >
              <ExpenseOverview
                category={category}
                amount={amount}
                selectedCurrency={selectedCurrency}
                description={description}
                refCurrencyModal={refCurrencyModal}
                setField={this.setField}
              />
            </View>

            <View
              style={{
                // height: 800,
                flex: 1,
                marginTop: -25,
                marginBottom: 40,
              }}
            >
              <View style={styles.tabButtonContainer}>
                {tabs.map((tab) => (
                  <TouchableOpacity
                    style={[
                      styles.tabButtonStyle,
                      activeState === tab.key && {
                        backgroundColor: COLOR.PRIMARTGREEN,
                      },
                    ]}
                    onPress={() => this.setState({ activeState: tab.key })}
                  >
                    <Text
                      style={[
                        styles.tabButtonText,
                        activeState === tab.key && { color: COLOR.WHITE },
                      ]}
                    >
                      {tab.label}
                    </Text>
                  </TouchableOpacity>
                ))}
              </View>
              {activeState === "paid_by" && (
                <View
                  style={{
                    marginBottom: Platform.OS === "ios" ? "35%" : "40%",
                  }}
                >
                  <MembersList
                    members={tripMembers}
                    selectedMember={paidUser}
                    selectUser={this.selectPaidUser}
                  />
                </View>
              )}
              {activeState === "split_between" && (
                <View style={{ flex: 1 }}>
                  <SplitBetweenView
                    members={tripMembers}
                    selectedMember={paidUser}
                    splitTabs={splitTabs}
                    selectedSplitTab={selectedSplitTab}
                    changeSplit={this.changeSplit}
                    memberSplits={memberSplits}
                    clickDisabled={true}
                    changeUserSplit={this.changeUserSplit}
                    selectedCurrency={selectedCurrency}
                  />
                </View>
              )}
            </View>

            {/* For Currency picker initialization */}
            <CurrencyPicker
              refCurrencyModal={refCurrencyModal}
              // closeOnDragDown={true}
              closeOnPressMask={true}
              height={deviceheight}
              keyboardAvoidingViewEnabled={true}
              customStyles={{
                wrapper: {
                  backgroundColor: "rgba(52, 52, 52, 0.5)",
                },
                draggableIcon: {
                  backgroundColor: "#000",
                },
                container: {},
              }}
              setSelectedCurrency={this.setSelectedCurrency}
              setCurrencies={this.setCurrencies}
              currencies={currencies}
            />
          </ScrollView>
        </KeyboardAvoidingView>
        <View style={styles.footer}>
          <View style={styles.footerHeader}>
            <Text style={styles.footerHeaderText}>Amount</Text>
            <Text style={styles.footerHeaderText}>$25000</Text>
          </View>
          <TouchableOpacity style={styles.footerButton}>
            <Text style={{ color: COLOR.WHITE }}>Split now</Text>
          </TouchableOpacity>
          <View style={styles.footerTitleText}>
            <Text style={{fontWeight: '500'}}>
              {
                selectedSplitTab.key==='percent' ? (
                  `${Number(this.calculateSplitPercent(totalSplitAmount, amount, true)).toFixed(2)}${selectedSplitTab.label} of ${Number(this.calculateSplitPercent(totalSplitAmount, amount, false)).toFixed(2)}${selectedSplitTab.label}`
                ) : (
                  `${selectedCurrency.symbol}${Number(totalSplitAmount).toFixed(2)} of ${selectedCurrency.symbol}${Number(amount).toFixed(2)}`
                )
              }
            </Text>
            <Text style={{fontSize: 12, color: 'grey'}}>
              ({
                selectedSplitTab.key==='percent' ? (
                  `${(this.calculateSplitPercent(totalSplitAmount, amount, false)-this.calculateSplitPercent(totalSplitAmount, amount, true)).toFixed(2)}${selectedSplitTab.label} Left`
                ) : (
                  `${selectedCurrency.symbol}${(amount-totalSplitAmount).toFixed(2)} Left`
                )
              })
            </Text>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  footerTitleText: {
    alignItems: 'center',
    padding: 5
  },
  backButton: {
    marginHorizontal: 20,
    width: 40,
    height: 40,
    justifyContent: "center",
    alignSelf: "center",
  },
  nextButton: {
    marginHorizontal: 20,
    width: 60,
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderRadius: 5,
    backgroundColor: COLOR.PRIMARYBLUE,
  },
  toolbar: {
    marginVertical: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
  },
  addButton: {
    height: 40,
    width: "80%",
    backgroundColor: COLOR.PRIMARYBLUE,
    borderRadius: 5,
    padding: 10,
    alignItems: "center",
    marginTop: 20,
  },
  addButtonText: {
    fontSize: 14,
    color: COLOR.WHITE,
  },
  tabButtonStyle: {
    backgroundColor: COLOR.WHITE,
    height: 40,
    width: "30%",
    alignItems: "center",
    justifyContent: "center",
    margin: 10,
    borderWidth: 1,
    borderColor: COLOR.PRIMARTGREEN,
    borderRadius: 5,
  },
  tabButtonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    padding: 5,
  },
  tabButtonText: {
    color: COLOR.PRIMARTGREEN,
    fontWeight: "500",
  },
  footer: {
    position: "absolute",
    height: 150,
    width: "100%",
    bottom: 0,
    backgroundColor: "white",
  },
  footerHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "rgba(196,196,196, 0.2)",
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: COLOR.LIGHTGREY,
    marginBottom: 10,
  },
  footerHeaderText: {
    paddingHorizontal: 30,
    color: "black",
    fontWeight: "bold",
  },
  footerButton: {
    width: "90%",
    backgroundColor: COLOR.PRIMARYBLUE,
    alignSelf: "center",
    padding: 10,
    alignItems: "center",
    borderRadius: 5,
  },
});

export default AddExpense;
