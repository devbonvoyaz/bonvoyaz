import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import COLOR from "../../../../../res/styles/Color";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Chip } from "react-native-paper";
import FONTS from "../../../../../res/styles/Fonts";
import ActionButton from "react-native-action-button";

const moneyIcons = ["dollar-sign", "rupee-sign"];
const overViewSection = [
  {
    key: "total_expense",
    label: "Total Expense",
    tintColor: COLOR.PRIMARYBLUE,
    backgroundColor: COLOR.PRIMARYBLUE,
    iconImage: require("../../../../../res/assets/budget.png"),
  },
  {
    key: "you_owe",
    label: "You Owe",
    tintColor: '#E46DD6',
    backgroundColor: '#E46DD6',
    iconImage: require("../../../../../res/assets/down-arrow.png"),
  },
  {
    key: "you_get",
    label: "You Get",
    tintColor: COLOR.PRIMARTGREEN,
    backgroundColor: COLOR.PRIMARTGREEN,
    iconImage: require("../../../../../res/assets/up-arrow.png"),
  },
];

const Expenses = (props) => {
  return (
    <>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          flexDirection: "column",
          backgroundColor: COLOR.WHITE,
        }}
        style={{ backgroundColor: COLOR.WHITE }}
      >
        <View style={styles.container}>
          <View style={styles.budgetOverview}>
            {overViewSection.map((section) => {
              return (
                <View style={styles.overViewListStyle}>
                  <View style={styles.overViewLeft}>
                    <View style={[styles.iconBoxStyle]} disabled>
                      <Image
                        source={section.iconImage}
                        style={[
                          styles.imageIconStyle,
                          {
                            tintColor: section.tintColor,
                          },
                        ]}
                      />
                    </View>
                    <Text style={styles.subtitle}>{section.label}</Text>
                  </View>
                  <View style={styles.overViewRight}>
                    {moneyIcons.map((e, i, ele) => {
                      return (
                        <>
                          <Chip
                            icon={() => (
                              <View
                                style={[
                                  styles.chipIconStyle,
                                  {
                                    backgroundColor: section.backgroundColor,
                                  },
                                ]}
                              >
                                <Icon name={e} color={COLOR.WHITE} />
                              </View>
                            )}
                            style={styles.chipStyle}
                            textStyle={styles.chipTextStyle}
                          >
                            {(i + 1) * 100}
                          </Chip>
                          {i !== ele.length - 1 && (
                            <Icon
                              name={"plus"}
                              style={{ marginHorizontal: 5 }}
                            />
                          )}
                        </>
                      );
                    })}
                  </View>
                </View>
              );
            })}
          </View>
        </View>
      </ScrollView>
      <ActionButton
        buttonColor={COLOR.PRIMARYBLUE}
        onPress={() => props.navigation.navigate('AddExpense')}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.WHITE,
  },
  budgetOverview: {
    marginTop: 5,
    alignSelf: "center",
    justifyContent: "space-around",
    padding: 10,
    paddingBottom: 0,
    backgroundColor: "white",
    borderRadius: 10,
    minHeight: 100,
    width: "95%",
    backgroundColor: COLOR.WHITE,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    elevation: 5,
    shadowOffset: { width: 1, height: 1 },
  },
  iconBoxStyle: {
    width: 40,
    height: 40,
    marginRight: 10,
    backgroundColor: COLOR.WHITE,
    borderRadius: 50,
    shadowColor: COLOR.BLACK,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
  },
  imageIconStyle: {
    flex: 1,
    width: "60%",
    resizeMode: "contain",
    alignSelf: "center"
  },
  overViewListStyle: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginBottom: 10,
  },
  overViewLeft: {
    flex: 0.5,
    flexDirection: "row",
    flexWrap: "nowrap",
    alignItems: "center",
  },
  overViewRight: {
    flex: 0.5,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  chipIconStyle: {
    width: 25,
    height: 25,
    borderRadius: 50,
    paddingLeft: -20,
    marginLeft: -7,
    justifyContent: "center",
    alignItems: "center",
  },
  chipStyle: {
    marginVertical: 5,
    height: 25,
    alignItems: "center",
    shadowColor: COLOR.BLACK,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.15,
    shadowRadius: 3.84,
    elevation: 4,
    backgroundColor: COLOR.WHITE,
  },
  subtitle: { fontFamily: FONTS.FAMILY_REGULAR },
  chipTextStyle: {
    paddingLeft: 0,
    fontFamily: FONTS.FAMILY_REGULAR,
  },
});

export default Expenses;
