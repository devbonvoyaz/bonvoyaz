import React from "react";
import { View, Text, Platform, TouchableOpacity } from "react-native";
import COLOR from "../../../../../res/styles/Color";
import MembersList from "./MembersList";

export default function SplitBetweenView(props) {
  const {
    splitTabs,
    selectedSplitTab,
    changeSplit,
    memberSplits,
    changeUserSplit,
    selectedCurrency,
  } = props;
  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          justifyContent: "space-around",
          height: 40,
          marginTop: 20,
          flexDirection: "row",
        }}
      >
        {splitTabs &&
          splitTabs.map((tab) => (
            <TouchableOpacity
              style={{
                backgroundColor:
                  selectedSplitTab.key === tab.key
                    ? COLOR.PRIMARYBLUE
                    : "rgba(45, 156, 219, 0.2)",
                height: 30,
                width: 80,
                borderRadius: 5,
                alignItems: "center",
                justifyContent: "center",
              }}
              onPress={() => changeSplit(tab)}
            >
              <Text
                style={{
                  color:
                    selectedSplitTab.key === tab.key
                      ? COLOR.WHITE
                      : "rgb(45, 156, 219)",
                }}
              >
                {tab.label}
              </Text>
            </TouchableOpacity>
          ))}
      </View>
      <View style={{ marginBottom: Platform.OS === "ios" ? "35%" : "45%" }}>
        <MembersList
          members={props.members}
          clickDisabled={false}
          memberSplits={memberSplits[selectedSplitTab.key]}
          hasInput={true}
          selectedSplitTab={selectedSplitTab}
          changeUserSplit={changeUserSplit}
          selectedCurrency={selectedCurrency}
        />
      </View>
    </View>
  );
}
