import { useNavigation } from '@react-navigation/core';
import React, { useEffect } from 'react';
import { Image, SafeAreaView, View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import COLOR from '../../../res/styles/Color';
import FONTS from '../../../res/styles/Fonts';
import Toolbar from '../../../commonView/navView/Toolbar';
import { useRoute } from '@react-navigation/native';


export default function () {
  const navigation = useNavigation();
  const route = useRoute();



  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
      <Toolbar title="Add Plan" />

      <View style={styles.cardView} >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <TouchableOpacity onPress={() => { navigation.navigate('AddPlace', { trip: route.params.trip }); }}>
            <View style={[styles.option_view, { backgroundColor: '#cc33ff' }]}>
              <Image
                source={require('../../../res/assets/train.png')}
                style={{ alignSelf: 'center', width: 24, height: 24 }}
              />
            </View>
            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center' }}  >
              Place
            </Text>
          </TouchableOpacity>        
          <TouchableOpacity onPress={() => { navigation.navigate('AddHotel', { trip: route.params.trip }); }}  >
            <View style={[styles.option_view, { backgroundColor: '#FF585D' }]}>
              <Image
                source={require('../../../res/assets/hotel.png')}
                style={{ alignSelf: 'center', width: 24, height: 24 }}
              />
            </View>
            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center' }} >
              Hotel
            </Text>
          </TouchableOpacity>
          <TouchableOpacity >
            <View style={[styles.option_view, { backgroundColor: '#2C7873' }]}>
              <Image
                source={require('../../../res/assets/flight.png')}
                style={{ alignSelf: 'center', width: 24, height: 24 }}
              />
            </View>
            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center' }} >
              Activity
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 15,
          }}
        >
          <TouchableOpacity onPress={() => { navigation.navigate('AddRestaurant', { trip: route.params.trip }); }}  >
            <View style={[styles.option_view, { backgroundColor: COLOR.PRIMARTGREEN }]}>
              <Image
                source={require('../../../res/assets/restaurant.png')}
                style={{ alignSelf: 'center', width: 24, height: 24 }}
              />
            </View>
            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center' }} >
              Restaurant
            </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => { navigation.navigate('AddDrive', { trip: route.params.trip }); }} >
            <View style={[styles.option_view, { backgroundColor: '#2998EA' }]}>
              <Image
                source={require('../../../res/assets/drive.png')}
                style={{ alignSelf: 'center', width: 24, height: 24 }}
              />
            </View>
            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center' }}  >
              Drive
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => { navigation.navigate('AddTrain', { trip: route.params.trip }); }}
          >
            <View style={[styles.option_view, { backgroundColor: '#FFBA1E' }]}>
              <Image
                source={require('../../../res/assets/train.png')}
                style={{ alignSelf: 'center', width: 24, height: 24 }}
              />
            </View>
            <Text style={{ fontFamily: FONTS.FAMILY_REGULAR, alignSelf: 'center' }} >
              Train
            </Text>
          </TouchableOpacity>

        </View>

      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  option_view: {
    width: 60,
    height: 60,
    borderRadius: 35,
    alignContent: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  toolbar: {
    flexDirection: 'row',
    marginVertical: 12,
    alignItems: 'center',
    justifyContent: 'space-between',

  },
  titleView: {
    width: '60%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardView: {
    width: widthPercentageToDP(85),
    alignSelf: 'center',
    padding: 20,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    elevation: 3,
    shadowOffset: { width: 2, height: 2 },
    justifyContent: 'space-between',
    borderRadius: 8,
    backgroundColor: COLOR.WHITE,
    marginVertical: 25,
  }
});
