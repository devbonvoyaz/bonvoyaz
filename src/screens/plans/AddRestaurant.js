import React from 'react';
import { SafeAreaView, StyleSheet, Text, FlatList, View, Image, } from 'react-native';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import DialogInputView from '../../commonView/globalComponent/DialogInputView';
import Toolbar2 from '../../commonView/navView/Toolbar2';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import { FloatingTitleTextInputField } from "../../commonView/globalComponent/FloatingTitleTextInputField";
import SimpleToast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import PlanAction from '../../redux/actions/PlanAction';
import AddressModal from '../tabs/trip/bookingModals/AddressModal';
import SelectFriendsList from '../../commonView/listView/SelectFriendsList';
import TripAction from '../../redux/actions/TripAction';
import { ScrollView } from 'react-native-gesture-handler';

class AddRestaurant extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isTimePickerVisible: false,
      isDatePickerVisible: false,
      timePickerIndex: 0,
      mDate: null,
      mTime: null,
      time: '',
      date: '',
      restaurantName: '',
      address: '',
      travellers: [],
      selected: [],
      item: null,
    };
    this.addressModal = React.createRef();
  }


  componentDidMount() {
    if (this.props.route.params.item) {
      let data = this.props.route.params.item;
      this.setState({
        item: data,
        restaurantName: data.name,
        address: data.address,
        mDate: data.date,
        mTime: data.arrivalTime,
        date: moment(data.date).format('DD-MMM-YYYY'),
        time: moment(data.arrivalTime).format('hh:mm A'),
        selected: data.participants ? data.participants.map(value => value.id) : [],
      })
    }
    if (this.props.singleTrip) {
      this.setState({
        travellers: this.props.singleTrip.participants,
      });
    }
  }


  addRestaurant = () => {
    if (this.state.restaurantName == '') {
      SimpleToast.show('Enter Restaurant Name');
    } else if (this.state.address == '') {
      SimpleToast.show('Enter Address');
    } else if (this.state.mDate == null) {
      SimpleToast.show('Select Date');
    } else if (this.state.mTime == null) {
      SimpleToast.show('Select Time');
    } else {
      let data = JSON.stringify({
        trip: this.props.route.params.trip,
        name: this.state.restaurantName,
        address: this.state.address,
        date: this.state.mDate,
        arrivalTime: this.state.mTime,
        participants: this.state.selected,
      })
      this.props.addRestaurant(data);
    }
  }

  updateRestaurantPlan = () => {
    if (this.state.restaurantName == '') {
      SimpleToast.show('Enter Restaurant Name');
    } else if (this.state.address == '') {
      SimpleToast.show('Enter Address');
    } else if (this.state.mDate == null) {
      SimpleToast.show('Select Date');
    } else if (this.state.mTime == null) {
      SimpleToast.show('Select Time');
    } else {
      let data = JSON.stringify({
        name: this.state.restaurantName,
        address: this.state.address,
        date: this.state.mDate,
        arrivalTime: this.state.mTime,
        participants: this.state.selected,
      })
      this.props.updateRestaurantPlan(this.state.item.id, data);
    }
  }

  hideTimePicker = () => {
    this.setState({
      isTimePickerVisible: false,
    });
  };

  handleTimeConfirm = (date) => {
    this.setState({
      isTimePickerVisible: false,
    });

    switch (this.state.timePickerIndex) {
      case 0: {
        this.setState({
          time: moment(date).format('hh:mm A'),
          mTime: new Date(date).getTime(),
        });
        break;
      };
      case 1: {
        this.setState({
          departureTime: moment(date).format('hh:mm A'),
          mDepartureTime: new Date(date).getTime(),
        });
        break;
      };

    }
    console.log(date);
  };


  hideDatePicker = () => {
    this.setState({
      isDatePickerVisible: false,
    });
  };

  handleDateConfirm = (date) => {
    this.setState({
      isDatePickerVisible: false,
    });
    this.setState({
      date: moment(date).format('DD-MMM-YYYY'),
      mDate: date.getTime(),
      mTime: null,
      time: 'Enter',
    });

  };

  updateMasterState = (attrName, value) => {
    this.setState({ [attrName]: value });
  };


  render() {
    return (
      <SafeAreaView style={{ backgroundColor: COLOR.WHITE, flex: 1 }}>
        <ScrollView>
          <Toolbar2 title={this.state.item ? 'Edit Restaurant' : 'Add Restaurant'} optionPress={this.state.item ? this.updateRestaurantPlan : this.addRestaurant} />

          <View
            style={{
              alignSelf: 'center',
              marginHorizontal: 15,
            }}
          >
            <View style={styles.text_input_card}>
              <FloatingTitleTextInputField
                attrName="restaurantName"
                title="Restaurant Name"
                isWhite={false}
                textInputStyles={{
                  padding: 0
                }}
                color={'#FFB100'}
                value={this.state.restaurantName}
                updateMasterState={this.updateMasterState}
              />
            </View>
            <View style={styles.text_input_card}>
              <FloatingTitleTextInputField
                attrName="address"
                title="Address"
                isWhite={false}
                textInputStyles={{
                  padding: 0
                }}
                onFocus={() => {
                  this.addressModal.current.open();
                }}
                color={'#FFB100'}
                value={this.state.address}
                updateMasterState={this.updateMasterState}
              />
            </View>


            <DialogInputView
              width={widthPercentageToDP(90)}
              title={'Date'}
              placeholder={this.state.date}
              onPress={() => {
                this.setState({
                  isDatePickerVisible: true,
                });
              }}
            />

            <DialogInputView
              width={widthPercentageToDP(90)}
              title={'Time'}
              placeholder={this.state.time}
              onPress={() => {
                if (this.state.mDate) {
                  this.setState({
                    isTimePickerVisible: true,
                    timePickerIndex: 0,
                  });
                } else {
                  SimpleToast.show('Select date First');
                }
              }}
            />



          </View>

          <View style={styles.selectPeopleView} >
            <Text style={styles.selectPeopleText} >
              Select People
            </Text>
            <Image
              style={styles.addUserIcon}
              source={require('../../res/assets/addusericon.png')}
            />
          </View>
          <FlatList
            style={{ marginTop: 10 }}
            data={this.state.travellers}
            keyboardShouldPersistTaps='handled'
            renderItem={({ item }) => (
              <SelectFriendsList
                item={item}
                username={item.firstName + " " + item.lastName}
                image={require("../../res/assets/user-image.png")}
                addParticipant={() => {
                  let data = this.state.selected;
                  data.push(item.id);
                  this.setState({
                    selected: data,
                  })
                }}
                removeParticipant={() => {
                  let index = this.state.selected.map(function (value) {
                    return value;
                  }).indexOf(item.id);
                  if (index != -1) {
                    let value = this.state.selected;
                    value.splice(index, 1);
                    this.setState({
                      selected: value
                    })
                  }
                }}
                preselected={null}
                selected={this.state.selected.map(x => { return x }).indexOf(item.id) == -1 ? false : true}
              />
            )}
          />

        </ScrollView>
        <DateTimePickerModal
          isVisible={this.state.isTimePickerVisible}
          mode="time"
          date={this.state.mDate}
          headerTextIOS={"Pick a date"}
          onConfirm={this.handleTimeConfirm.bind(this)}
          onCancel={this.hideTimePicker.bind(this)}
        />
        <DateTimePickerModal
          isVisible={this.state.isDatePickerVisible}
          mode="date"
          onConfirm={this.handleDateConfirm.bind(this)}
          onCancel={this.hideDatePicker.bind(this)}
        />
        <AddressModal
          trip={this.props.trip}
          attrName={"address"}
          addressModal={this.addressModal}
          updateMasterState={this.updateMasterState}
        />

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  text_input_card: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    borderRadius: 5,
    backgroundColor: COLOR.WHITE,
    elevation: 5,
    shadowColor: COLOR.BLACK,
    paddingRight: 40,
    shadowOpacity: 0.2,
    shadowOffset: { width: 2, height: 2 },
    padding: 12,
    marginVertical: 5,
  },
  text_input: {
    fontSize: 14,
    fontFamily: FONTS.FAMILY_MEDIUM,
    padding: 2,
  },
  text: {
    fontSize: 14,
    fontFamily: FONTS.FAMILY_MEDIUM,
    color: COLOR.PRIMARYGREY,
    marginTop: 5,
  },
  selectPeapleText: {
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.PRIMARYGREY,
    marginLeft: 15,
    marginVertical: 15,
    alignSelf: 'center',
  },
  addUserIcon: {
    alignSelf: 'center',
    width: 25,
    height: 25,
    marginRight: 15,
  },
  selectPeopleText: {
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.PRIMARYGREY,
    marginLeft: 15,
    marginVertical: 15,
    alignSelf: 'center',
  },
  selectPeopleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
  },
  addUserIcon: {
    alignSelf: 'center',
    width: 25,
    height: 25,
    marginRight: 15,
  },

});

const mapDispatchToProps = (dispatch) => {
  return {
    addRestaurant: (data) => {
      dispatch(PlanAction.addRestaurant(data));
    },
    updateRestaurantPlan: (val, data) => {
      dispatch(PlanAction.updateRestaurantPlan(val, data))
    }

  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    singleTrip: state.tripReducer.singleTrip,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRestaurant);
