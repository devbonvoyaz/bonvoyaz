import React from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import DialogInputView from '../../commonView/globalComponent/DialogInputView';
import Toolbar2 from '../../commonView/navView/Toolbar2';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import { connect } from 'react-redux';
import TripAction from '../../redux/actions/TripAction';
import SimpleToast from 'react-native-simple-toast';
import { FloatingTitleTextInputField } from "../../commonView/globalComponent/FloatingTitleTextInputField";
import PlanAction from '../../redux/actions/PlanAction';
import SelectFriendsList from '../../commonView/listView/SelectFriendsList';
import { object } from 'prop-types';


class AddHotel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isTimePickerVisible: false,
      isDatePickerVisible: false,
      timePickerIndex: 0,
      datePickerIndex: 0,
      arrivalDate: '',
      mArrivalDate: null,
      arrivalTime: '',
      mArrivalTime: null,
      departureDate: '',
      mDepartureDate: null,
      departureTime: '',
      mDepartureTime: null,
      hotelData: [],
      selectedBooking: null,
      selected: [],
      travellers: [],
      item: null,
    };
    this.props.getHotelBookingByTrip(this.props.route.params.item ? this.props.route.params.item.trip : this.props.route.params.trip);
  }

  componentDidMount() {
    if (this.props.route.params.item) {
      let data = this.props.route.params.item;
      this.setState({
        item: data,
        mArrivalDate: data.arrivalDate,
        mArrivalTime: data.arrivalTime,
        mDepartureDate: data.departureDate,
        mDepartureTime: data.departureTime,
        arrivalDate: moment(data.arrivalDate).format('DD-MMM-YYYY'),
        arrivalTime: moment(data.arrivalTime).format('DD-MMM-YYYY'),
        departureDate: data.departureDate ? moment(data.departureDate).format('DD-MMM-YYYY') : 'Enter',
        departureTime: data.departureTime ? moment(data.departureTime).format('hh:mm A') : 'Enter',
        selected: data.participants ? data.participants.map(value => value.id) : [],
      })
    }

    if (this.props.hotelBookings) {
      this.setState({
        hotelData: this.props.hotelBookings
      })
    }
    if (this.props.singleTrip) {
      this.setState({
        travellers: this.props.singleTrip.participants,
      });

    }

  }

  componentDidUpdate(prevProps) {
    if (prevProps.hotelBookings != this.props.hotelBookings) {
      this.setState({
        hotelData: this.props.hotelBookings
      }, () => {

        if (this.state.item && this.state.item.hotelBooking) {
          let object = this.state.item;
          let index = this.state.hotelData.map(value => value.id).indexOf(object.hotelBooking);
          if (index != -1) {
            this.setState({
              selectedBooking: index,
            })
          }
        }

      });

    }
  }

  AddHotel = () => {
    if (this.state.mArrivalDate == null) {
      SimpleToast.show('Select Arrival Date');
    } else if (this.state.mArrivalTime == null) {
      SimpleToast.show('Select Arrival Time');
    } else {
      let data = JSON.stringify({
        trip: this.props.route.params.trip,
        hotelName: this.state.selectedBooking != null ? this.state.hotelData[this.state.selectedBooking].name : null,
        hotelBooking: this.state.selectedBooking != null ? this.state.hotelData[this.state.selectedBooking].id : null,
        arrivalDate: this.state.mArrivalDate,
        arrivalTime: this.state.mArrivalTime,
        departureDate: this.state.mDepartureDate,
        departureTime: this.state.mDepartureTime,
        participants: this.state.selected,
      })
      this.props.addHotel(data);
    }
  }

  updateHotelPlan = () => {
    if (this.state.mArrivalDate == null) {
      SimpleToast.show('Select Arrival Date');
    } else if (this.state.mArrivalTime == null) {
      SimpleToast.show('Select Arrival Time');
    } else {
      let data = JSON.stringify({
        hotelName: this.state.selectedBooking != null ? this.state.hotelData[this.state.selectedBooking].name : null,
        hotelBooking: this.state.selectedBooking != null ? this.state.hotelData[this.state.selectedBooking].id : null,
        arrivalDate: this.state.mArrivalDate,
        arrivalTime: this.state.mArrivalTime,
        departureDate: this.state.mDepartureDate,
        departureTime: this.state.mDepartureTime,
        participants: this.state.selected,
      })
      this.props.updateHotelPlan(this.state.item.id, data);
    }
  }

  hideTimePicker = () => {
    this.setState({
      isTimePickerVisible: false,
    });
  };

  handleTimeConfirm = (date) => {
    this.setState({
      isTimePickerVisible: false,
    });

    switch (this.state.timePickerIndex) {
      case 0: {
        {
          this.setState({
            arrivalTime: moment(date).format('hh:mm A'),
            mArrivalTime: new Date(date).getTime(),
          });
        }
        break;
      };
      case 1: {
        this.setState({
          departureTime: moment(date).format('hh:mm A'),
          mDepartureTime: new Date(date).getTime(),
        });
        break;
      };

    }
  };


  hideDatePicker = () => {
    this.setState({
      isDatePickerVisible: false,
    });
  };

  handleDateConfirm = (date) => {
    this.setState({
      isDatePickerVisible: false,
    });

    switch (this.state.datePickerIndex) {
      case 0: {
        this.setState({
          arrivalDate: moment(date).format('DD-MMM-YYYY'),
          mArrivalDate: date.getTime(),
          arrivalTime: 'Enter',
          mArrivalTime: null,
        });
        break;
      }
      case 1: {
        this.setState({
          departureDate: moment(date).format('DD-MMM-YYYY'),
          mDepartureDate: date.getTime(),
          departureTime: 'Enter',
          mDepartureTime: null,
        });
        break;
      };

    }
  };

  updateMasterState = (attrName, value) => {
    this.setState({ [attrName]: value });
  };


  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
        <ScrollView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
          <Toolbar2 title={this.state.item ? 'Edit Hotel' : 'Add Hotel'} optionPress={this.state.item ? this.updateHotelPlan : this.AddHotel} />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 15,
            }}
          >

            <DialogInputView
              width={widthPercentageToDP(44)}
              title={'Arrival Date'}
              placeholder={this.state.arrivalDate}
              onPress={() => {
                this.setState({
                  isDatePickerVisible: true,
                  datePickerIndex: 0,
                });
              }}
            />
            <DialogInputView
              width={widthPercentageToDP(44)}
              title={'Arrival Time'}
              placeholder={this.state.arrivalTime}
              onPress={() => {
                if (this.state.mArrivalDate) {
                  this.setState({
                    timePickerIndex: 0,
                  }, () => this.setState({ isTimePickerVisible: true, }));
                } else {
                  SimpleToast.show('Select Arrival Date')
                }
              }}
            />
          </View>
          <View
            style={styles.horizontalView}
          >
            <DialogInputView
              width={widthPercentageToDP(44)}
              title={'Departure  Date'}
              placeholder={this.state.departureDate}
              onPress={() => {
                this.setState({
                  isDatePickerVisible: true,
                  datePickerIndex: 1,
                });
              }}
            />
            <DialogInputView
              width={widthPercentageToDP(44)}
              title={'Departure Time'}
              placeholder={this.state.departureTime}
              onPress={() => {
                if (this.state.mDepartureDate) {
                  this.setState({
                    timePickerIndex: 1,
                  }, () => this.setState({ isTimePickerVisible: true, }));
                } else {
                  SimpleToast.show('Select Departure date First');
                }
              }}
            />
          </View>
          <View
            style={styles.linearGradient}

          >
            <Text
              style={styles.title}
            >
              Choose a booking
            </Text>
          </View>
          <FlatList
            style={{ flex: 1 }}
            data={this.state.hotelData}
            ListEmptyComponent={() => (
              <View>
                <Image
                  resizeMode={'contain'}
                  style={styles.hotelIcon}
                  source={require('../../res/assets/hotel_icon.png')}
                />
                <Text style={styles.emptyListText}>
                  You have no bookings to show right now. You can add them by tapping on ‘Add’
                </Text>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('AddBooking', { trip: this.props.route.params.trip })}
                  style={styles.addBookingButton}>
                  <Text style={{ alignSelf: 'center', color: COLOR.WHITE }}>Add a booking</Text>
                </TouchableOpacity>
              </View>
            )}
            renderItem={({ item, index }) => {

              return (
                <TouchableOpacity
                  onPress={() => this.setState({
                    selectedBooking: this.state.selectedBooking == index ? null : index,
                  })}
                  style={[styles.text_input_card, { flexDirection: 'row', paddingHorizontal: 5, paddingVertical: 10, borderWidth: 1, borderColor: this.state.selectedBooking == index ? COLOR.PRIMARTGREEN : COLOR.WHITE }]}>
                  <Image
                    source={require('../../res/assets/hotelimage.png')}
                    style={{
                      width: 120,
                      height: 100,
                      alignSelf: 'center'
                    }}
                    resizeMode={'contain'}
                  />
                  <View style={{ marginHorizontal: 5, alignItems: 'flex-start', flex: 1, }}>
                    <Text>{item.name}</Text>
                    <Text style={styles.textLite}>Check-In {'&'} Out :</Text>
                    <Text>{moment.unix(item.checkIn / 1000).format("MMMM DD") + '  -  ' + moment.unix(item.checkOut / 1000).format("MMMM DD")}</Text>
                    <Text style={{ marginVertical: 5 }}><Text style={styles.textLite}>Confirmation No : </Text>{item.confirmationNumber}</Text>
                    <Text ><Text style={styles.textLite}>No.of Rooms : </Text>{item.roomCount}</Text>

                  </View>
                </TouchableOpacity>)
            }}
          />

          <View style={styles.selectPeopleView} >
            <Text style={styles.selectPeopleText} >
              Select People
            </Text>
            <Image
              style={styles.addUserIcon}
              source={require('../../res/assets/addusericon.png')}
            />
          </View>


          <FlatList
            style={{ marginTop: 10, flex: 1 }}
            data={this.state.travellers}
            keyboardShouldPersistTaps='handled'
            renderItem={({ item }) => (
              <SelectFriendsList
                item={item}
                username={item.firstName + " " + item.lastName}
                image={require("../../res/assets/user-image.png")}
                addParticipant={() => {
                  let data = this.state.selected;
                  data.push(item.id);
                  this.setState({
                    selected: data,
                  })
                }}
                removeParticipant={() => {
                  let index = this.state.selected.map(function (value) {
                    return value;
                  }).indexOf(item.id);
                  if (index != -1) {
                    let value = this.state.selected;
                    value.splice(index, 1);
                    this.setState({
                      selected: value
                    })
                  }
                }}
                preselected={null}
                selected={this.state.selected.map(x => { return x }).indexOf(item.id) == -1 ? false : true}
              />)}
          />

        </ScrollView>
        <DateTimePickerModal
          isVisible={this.state.isTimePickerVisible}
          mode="time"
          date={this.state.timePickerIndex == 0 ? this.state.mArrivalDate : this.state.mDepartureDate}
          headerTextIOS={"Pick a date"}
          onConfirm={this.handleTimeConfirm.bind(this)}
          onCancel={this.hideTimePicker.bind(this)}
        />
        <DateTimePickerModal
          isVisible={this.state.isDatePickerVisible}
          mode="date"
          onConfirm={this.handleDateConfirm.bind(this)}
          onCancel={this.hideDatePicker.bind(this)}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  text_input_card: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    borderRadius: 5,
    backgroundColor: COLOR.WHITE,
    elevation: 5,
    paddingRight: 40,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    shadowOffset: { width: 2, height: 2 },
    padding: 12,
    marginVertical: 5,
  },
  text_input: {
    fontSize: 14,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    padding: 2,
  },
  text: {
    fontSize: 14,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    color: COLOR.PRIMARYGREY,
    marginTop: 5,
  },
  horizontalView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 15,
  },
  linearGradient: {
    height: 40,
    flexDirection: 'row',
    marginTop: 25,
    backgroundColor: 'rgba(45, 156, 219, 0.1)'
  },
  title: {
    width: widthPercentageToDP(80),
    color: COLOR.PRIMARYBLUE,
    alignSelf: 'center',
    fontSize: 16,
    marginLeft: 25,
    fontFamily: FONTS.FAMILY_MEDIUM,
  },
  selectPeopleText: {
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.PRIMARYGREY,
    marginLeft: 15,
    marginVertical: 15,
    alignSelf: 'center',
  },
  addUserIcon: {
    alignSelf: 'center',
    width: 25,
    height: 25,
    marginRight: 15,
  },
  textLite: {
    fontSize: 12,
    marginVertical: 5,
    fontWeight: '200',
  },
  hotelIcon: {
    height: 120,
    width: 120,
    alignSelf: 'center',
    marginTop: 25
  },
  emptyListText: {
    width: widthPercentageToDP(80),
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 15,
    marginTop: 20
  },
  addBookingButton: {
    width: widthPercentageToDP(70),
    marginVertical: 10,
    height: 40,
    alignSelf: 'center',
    backgroundColor: COLOR.PRIMARYBLUE,
    justifyContent: 'center',
    borderRadius: 5
  },
  selectPeopleText: {
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.PRIMARYGREY,
    marginLeft: 15,
    marginVertical: 15,
    alignSelf: 'center',
  },
  selectPeopleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
  },
  addUserIcon: {
    alignSelf: 'center',
    width: 25,
    height: 25,
    marginRight: 15,
  },

});

const mapStateToProps = (state, ownProps) => {
  return {
    hotelBookings: state.tripReducer.hotelBookings,
    singleTrip: state.tripReducer.singleTrip,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getHotelBookingByTrip: (val) => {
      dispatch(TripAction.getHotelBookingByTrip(val));
    },
    addHotel: (data) => {
      dispatch(PlanAction.addHotel(data));
    },
    updateHotelPlan: (val, data) => {
      dispatch(PlanAction.updateHotelPlan(val, data))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddHotel);
