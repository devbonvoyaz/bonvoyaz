import React from 'react';
import { SafeAreaView, StyleSheet, Text, FlatList, Image, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import DialogInputView from '../../commonView/globalComponent/DialogInputView';
import Toolbar2 from '../../commonView/navView/Toolbar2';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { FloatingTitleTextInputField } from "../../commonView/globalComponent/FloatingTitleTextInputField";
import { connect } from 'react-redux';
import PlanAction from '../../redux/actions/PlanAction';
import AddressModal from '../tabs/trip/bookingModals/AddressModal';
import SelectFriendsList from '../../commonView/listView/SelectFriendsList';

class AddDrive extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isTimePickerVisible: false,
      isDatePickerVisible: false,
      timePickerIndex: 0,
      datePickerIndex: 0,
      startPoint: '',
      endPoint: '',
      time: '',
      date: '',
      mTime: null,
      mDate: null,
      pickUpDate: 'Enter',
      pickUpTime: 'Enter',
      dropOffDate: 'Enter',
      dropOffTime: 'Enter',
      attrName: 'startPoint',
      travellers: [],
      selected: [],
      item: null,

    };
    this.addressModal = React.createRef();
  }

  componentDidMount() {
    if (this.props.route.params.item) {
      let data = this.props.route.params.item;
      this.setState({
        item: this.props.route.params.item,
        startPoint: data.startPoint,
        endPoint: data.endPoint,
        mTime: data.startTime,
        mDate: data.startDate,
        date: moment(data.startDate).format('DD-MMM-YYYY'),
        time: moment(data.startTime).format('hh:mm A'),
        selected: data.participants ? data.participants.map(value => value.id) : [],
      })
    }
    if (this.props.singleTrip) {
      this.setState({
        travellers: this.props.singleTrip.participants,
      });
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.singleTrip != this.props.singleTrip) {
      let tripData = this.props.singleTrip;
      this.setState({
        travellers: tripData.participants,
      })
    }
  }

  addDrivePlan = () => {
    if (this.state.startPoint == '') {
      Toast.show('Enter Start Point');
    } else if (this.state.endPoint == '') {
      Toast.show('Enter End Point');
    } else if (this.state.mDate == null) {
      Toast.show('Select Date');
    } else if (this.state.mTime == null) {
      Toast.show('Select Time');
    } else {
      let data = JSON.stringify({
        trip: this.props.route.params.trip,
        startPoint: this.state.startPoint,
        endPoint: this.state.endPoint,
        startDate: this.state.mDate,
        startTime: this.state.mTime,
        participants: this.state.selected
      })
      this.props.addDrivePlan(data);
    }
  }

  updateDrivePlan = () => {
    if (this.state.startPoint == '') {
      Toast.show('Enter Start Point');
    } else if (this.state.endPoint == '') {
      Toast.show('Enter End Point');
    } else if (this.state.mDate == null) {
      Toast.show('Select Date');
    } else if (this.state.mTime == null) {
      Toast.show('Select Time');
    } else {
      let data = JSON.stringify({
        trip: this.props.route.params.trip,
        startPoint: this.state.startPoint,
        endPoint: this.state.endPoint,
        startDate: this.state.mDate,
        startTime: this.state.mTime,
        participants: this.state.selected
      })
      this.props.updateDrivePlan(this.state.item.id, data);
    }
  }



  hideTimePicker = () => {
    this.setState({
      isTimePickerVisible: false,
    });
  };

  handleTimeConfirm = (date) => {
    this.setState({
      isTimePickerVisible: false,
    });

    switch (this.state.timePickerIndex) {
      case 0: {
        if (this.state.mDate) {
          this.setState({
            time: moment(date).format('hh:mm A'),
            mTime: date.getTime(),
          });
        } else {
          Toast.show('Select Date first');
        }
        break;
      };

      case 1: {
        this.setState({ pickUpTime: moment(date).format('hh:mm A') });
        break;
      };
      case 2: {
        this.setState({ dropOffTime: moment(date).format('hh:mm A') });
        break;
      }
    }
  };


  hideDatePicker = () => {
    this.setState({
      isDatePickerVisible: false,
    });
  };

  handleDateConfirm = (date) => {
    this.setState({
      isDatePickerVisible: false,
    });

    switch (this.state.datePickerIndex) {
      case 0: {
        this.setState({
          date: moment(date).format('DD-MMM-YYYY'),
          mDate: date.getTime(),
          time: 'Enter',
          mTime: null,
        });
        break;
      }
      case 1: {
        this.setState({ pickUpDate: moment(date).format('DD-MMM-YYYY') });
        break;
      };
      case 2: {
        this.setState({ dropOffDate: moment(date).format('DD-MMM-YYYY') });
        break;
      }
    }
  };

  updateMasterState = (attrName, value) => {
    this.setState({ [attrName]: value });
  };

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: COLOR.WHITE, flex: 1 }}>
        <ScrollView>
          <Toolbar2 title={this.state.item ? 'Edit Drive' : 'Drive'} optionPress={() => this.state.item ? this.updateDrivePlan() : this.addDrivePlan()} />

          <View
            style={{
              alignSelf: 'center',
              marginHorizontal: 15,
            }}
          >

            <View style={styles.text_input_card}>
              <FloatingTitleTextInputField
                attrName="startPoint"
                title="Start Point"
                isWhite={false}
                textInputStyles={{
                  padding: 0
                }}
                onFocus={() => {
                  this.setState({
                    attrName: "startPoint",
                  }, () => this.addressModal.current.open());
                }}
                color={'#FFB100'}
                value={this.state.startPoint}
                updateMasterState={this.updateMasterState}
              />
            </View>
            <View style={styles.text_input_card}>
              <FloatingTitleTextInputField
                attrName="endPoint"
                title="End Point"
                isWhite={false}
                textInputStyles={{
                  padding: 0
                }}
                onFocus={() => {
                  this.setState({
                    attrName: "endPoint",
                  }, () => this.addressModal.current.open());
                }}
                color={'#FFB100'}
                value={this.state.endPoint}
                updateMasterState={this.updateMasterState}
              />
            </View>


            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >

              <DialogInputView
                width={widthPercentageToDP(43)}
                title={'Start Date'}
                placeholder={this.state.date}
                onPress={() => {
                  this.setState({
                    isDatePickerVisible: true,
                    datePickerIndex: 0,
                  });
                }}
              />
              <DialogInputView
                width={widthPercentageToDP(43)}
                title={'Start Time'}
                placeholder={this.state.time}
                onPress={() => {
                  if (this.state.mDate) {
                    this.setState({
                      isTimePickerVisible: true,
                      timePickerIndex: 0,
                    });
                  } else {
                    Toast.show('Select date first');
                  }
                }}
              />

            </View>

          </View>

          <View style={styles.selectPeopleView} >
            <Text style={styles.selectPeopleText} >
              Select People
            </Text>
            <Image
              style={styles.addUserIcon}
              source={require('../../res/assets/addusericon.png')}
            />
          </View>
          <FlatList
            style={{ marginTop: 10 }}
            data={this.state.travellers}
            keyboardShouldPersistTaps='handled'
            renderItem={({ item }) => (
              <SelectFriendsList
                item={item}
                username={item.firstName + " " + item.lastName}
                image={require("../../res/assets/user-image.png")}
                addParticipant={() => {
                  let data = this.state.selected;
                  data.push(item.id);
                  this.setState({
                    selected: data,
                  })
                }}
                removeParticipant={() => {
                  let index = this.state.selected.map(function (value) {
                    return value;
                  }).indexOf(item.id);
                  if (index != -1) {
                    let value = this.state.selected;
                    value.splice(index, 1);
                    this.setState({
                      selected: value
                    })
                  }
                }}
                preselected={null}
                selected={this.state.selected ? this.state.selected.map(x => { return x }).indexOf(item.id) == -1 ? false : true : false}
              />)}
          />

        </ScrollView>

        <DateTimePickerModal
          isVisible={this.state.isTimePickerVisible}
          mode="time"
          date={new Date(this.state.mDate)}
          headerTextIOS={"Pick a date"}
          onConfirm={this.handleTimeConfirm.bind(this)}
          onCancel={this.hideTimePicker.bind(this)}
        />
        <DateTimePickerModal
          isVisible={this.state.isDatePickerVisible}
          mode="date"
          onConfirm={this.handleDateConfirm.bind(this)}
          onCancel={this.hideDatePicker.bind(this)}
        />
        <AddressModal
          trip={this.props.trip}
          attrName={this.state.attrName}
          addressModal={this.addressModal}
          place={true}
          updateMasterState={this.updateMasterState}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  text_input_card: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    borderRadius: 8,
    backgroundColor: COLOR.WHITE,
    elevation: 5,
    shadowColor: COLOR.BLACK,
    shadowOpacity: 0.2,
    shadowOffset: { width: 2, height: 2 },
    padding: 12,
    marginVertical: 8,
    paddingRight: 40,
  },
  text_input: {
    fontSize: 14,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    padding: 5,
  },
  text: {
    fontSize: 14,
    fontFamily: FONTS.FAMILY_SEMIBOLD,
    color: COLOR.PRIMARYGREY,
    marginTop: 5,
  },
  selectPeopleText: {
    fontFamily: FONTS.FAMILY_REGULAR,
    color: COLOR.PRIMARYGREY,
    marginLeft: 15,
    marginVertical: 15,
    alignSelf: 'center',
  },
  selectPeopleView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
  },
  addUserIcon: {
    alignSelf: 'center',
    width: 25,
    height: 25,
    marginRight: 15,
  },
});

const mapDispatchToProps = (dispatch) => {
  return {
    addDrivePlan: (data) => {
      dispatch(PlanAction.addDrivePlan(data))
    },
    updateDrivePlan: (val, data) => {
      dispatch(PlanAction.updateDrivePlan(val, data))
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    singleTrip: state.tripReducer.singleTrip,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddDrive);
