import React from 'react';
import {
    FlatList,
    SafeAreaView,
    StyleSheet,
    View,
    Image,
    Text
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import DialogInputView from '../../commonView/globalComponent/DialogInputView';
import Toolbar2 from '../../commonView/navView/Toolbar2';
import COLOR from '../../res/styles/Color';
import FONTS from '../../res/styles/Fonts';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import { FloatingTitleTextInputField } from "../../commonView/globalComponent/FloatingTitleTextInputField";
import AddressModal from '../tabs/trip/bookingModals/AddressModal';
import SimpleToast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import SelectFriendsList from '../../commonView/listView/SelectFriendsList';
import PlanAction from '../../redux/actions/PlanAction';
import Toast from 'react-native-simple-toast';



class AddPlace extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isTimePickerVisible: false,
            isDatePickerVisible: false,
            place: '',
            attrName: '',
            date: null,
            time: null,
            mDate: null,
            mTime: null,
            selected: [],
            travellers: [],
            item: null,
        };

        this.addressModal = React.createRef();

    }

    addPlace = () => {
        if (this.state.place == '') {
            Toast.show('Enter Place');
        } else if (this.state.mDate == null) {
            Toast.show('Select Date');
        } else if (this.state.mTime == null) {
            Toast.show('Select Time');
        } else {
            let data = JSON.stringify({
                trip: this.props.route.params.trip,
                placeName: this.state.place,
                date: this.state.mDate,
                time: this.state.mTime,
                participants: this.state.selected
            });
            this.props.addPlace(data);
        }
    }

    updatePlace = () => {
        if (this.state.place == '') {
            Toast.show('Enter Place');
        } else if (this.state.mDate == null) {
            Toast.show('Select Date');
        } else if (this.state.mTime == null) {
            Toast.show('Select Time');
        } else {
            let data = JSON.stringify({
                placeName: this.state.place,
                date: this.state.mDate,
                time: this.state.mTime,
                participants: this.state.selected
            });
            this.props.updatePlacePlan(this.state.item.id, data);
        }
    }



    componentDidMount() {
        if (this.props.route.params.item) {
            let data = this.props.route.params.item;
            this.setState({
                item: data,
                place: data.placeName,
                mDate: data.date,
                mTime: data.time,
                date: moment(data.date).format('DD-MMM-YYYY'),
                time: moment(data.time).format('hh:mm A'),
                selected: data.participants ? data.participants.map(value => value.id) : [],
            })
        }
        if (this.props.singleTrip) {
            this.setState({
                travellers: this.props.singleTrip.participants,
            });
        }
    }

    hideTimePicker = () => {
        this.setState({
            isTimePickerVisible: false,
        });
    };

    handleTimeConfirm = (date) => {
        this.setState({
            isTimePickerVisible: false,
        });

        this.setState({
            time: moment(date).format('hh:mm A'),
            mTime: new Date(date).getTime(),
        });


    };


    hideDatePicker = () => {
        this.setState({
            isDatePickerVisible: false,
        });
    };

    handleDateConfirm = (date) => {
        this.setState({
            isDatePickerVisible: false,
        });
        this.setState({
            date: moment(date).format('DD-MMM-YYYY'),
            mDate: date.getTime(),
            time: 'Enter',
            mTime: null,
        });
    };

    updateMasterState = (attrName, value) => {
        this.setState({ [attrName]: value });
    };


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
                <ScrollView style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
                    <Toolbar2 title={this.state.item ? 'Add Place' : 'Edit Place'} optionPress={this.state.item ? this.updatePlace : this.addPlace} />

                    <View style={styles.text_input_card}>
                        <FloatingTitleTextInputField
                            attrName="place"
                            title="Place"
                            isWhite={false}
                            textInputStyles={{
                                padding: 0
                            }}
                            onFocus={() => {
                                this.setState({
                                    attrName: "place",
                                }, () => this.addressModal.current.open());
                            }}
                            color={'#FFB100'}
                            value={this.state.place}
                            updateMasterState={this.updateMasterState}
                        />
                    </View>
                    <View
                        style={styles.rowView}
                    >

                        <DialogInputView
                            width={widthPercentageToDP(43)}
                            title={'Start Date'}
                            placeholder={this.state.date}
                            onPress={() => {
                                this.setState({
                                    isDatePickerVisible: true,
                                    datePickerIndex: 0,
                                });
                            }}
                        />
                        <DialogInputView
                            width={widthPercentageToDP(43)}
                            title={'Start Time'}
                            placeholder={this.state.time}
                            onPress={() => {
                                if (this.state.mDate) {
                                    this.setState({
                                        isTimePickerVisible: true,
                                        timePickerIndex: 0,
                                    });
                                } else {
                                    SimpleToast.show('Select date first');
                                }
                            }}
                        />

                    </View>
                    <View style={styles.selectPeopleView} >
                        <Text style={styles.selectPeopleText} >
                            Select People
                        </Text>
                        <Image
                            style={styles.addUserIcon}
                            source={require('../../res/assets/addusericon.png')}
                        />
                    </View>
                    <FlatList
                        style={{ marginTop: 10 }}
                        data={this.state.travellers}
                        keyboardShouldPersistTaps='handled'
                        renderItem={({ item }) => (
                            <SelectFriendsList
                                item={item}
                                username={item.firstName + " " + item.lastName}
                                image={require("../../res/assets/user-image.png")}
                                addParticipant={() => {
                                    let data = this.state.selected;
                                    data.push(item.id);
                                    this.setState({
                                        selected: data,
                                    })
                                }}
                                removeParticipant={() => {
                                    let index = this.state.selected.map(function (value) {
                                        return value;
                                    }).indexOf(item.id);
                                    if (index != -1) {
                                        let value = this.state.selected;
                                        value.splice(index, 1);
                                        this.setState({
                                            selected: value
                                        })
                                    }
                                }}
                                preselected={null}
                                selected={this.state.selected.map(x => { return x }).indexOf(item.id) == -1 ? false : true}
                            />
                        )}
                    />


                </ScrollView>
                <DateTimePickerModal
                    isVisible={this.state.isTimePickerVisible}
                    mode="time"
                    date={this.state.mDate}
                    headerTextIOS={"Pick a date"}
                    onConfirm={this.handleTimeConfirm.bind(this)}
                    onCancel={this.hideTimePicker.bind(this)}
                />
                <DateTimePickerModal
                    isVisible={this.state.isDatePickerVisible}
                    mode="date"
                    onConfirm={this.handleDateConfirm.bind(this)}
                    onCancel={this.hideDatePicker.bind(this)}
                />
                <AddressModal
                    trip={this.props.trip}
                    attrName={this.state.attrName}
                    addressModal={this.addressModal}
                    updateMasterState={this.updateMasterState}
                />
            </SafeAreaView>
        );
    }
}



const styles = StyleSheet.create({
    text_input_card: {
        width: widthPercentageToDP(90),
        alignSelf: 'center',
        borderRadius: 5,
        backgroundColor: COLOR.WHITE,
        elevation: 5,
        paddingRight: 40,
        shadowColor: COLOR.BLACK,
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 2 },
        padding: 12,
        marginVertical: 5,
    },

    rowView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: widthPercentageToDP(90),
        alignSelf: 'center'
    },
    text_input: {
        fontSize: 14,
        fontFamily: FONTS.FAMILY_SEMIBOLD,
        padding: 2,
    },
    text: {
        fontSize: 14,
        fontFamily: FONTS.FAMILY_SEMIBOLD,
        color: COLOR.PRIMARYGREY,
        marginTop: 5,
    },
    horizontalView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 15,
    },
    linearGradient: {
        height: 40,
        flexDirection: 'row',
        marginTop: 25,
        backgroundColor: 'rgba(45, 156, 219, 0.1)'
    },
    title: {
        width: widthPercentageToDP(80),
        color: COLOR.PRIMARYBLUE,
        alignSelf: 'center',
        fontSize: 16,
        marginLeft: 25,
        fontFamily: FONTS.FAMILY_MEDIUM,
    },
    selectPeopleText: {
        fontFamily: FONTS.FAMILY_REGULAR,
        color: COLOR.PRIMARYGREY,
        marginLeft: 15,
        marginVertical: 15,
        alignSelf: 'center',
    },
    addUserIcon: {
        alignSelf: 'center',
        width: 25,
        height: 25,
        marginRight: 15,
    },
    textLite: {
        fontSize: 12,
        marginVertical: 5,
        fontWeight: '200',
    },
    hotelIcon: {
        height: 120,
        width: 120,
        alignSelf: 'center',
        marginTop: 25
    },
    emptyListText: {
        width: widthPercentageToDP(80),
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 15,
        marginTop: 20
    },
    addBookingButton: {
        width: widthPercentageToDP(70),
        marginVertical: 10,
        height: 40,
        alignSelf: 'center',
        backgroundColor: COLOR.PRIMARYBLUE,
        justifyContent: 'center',
        borderRadius: 5
    },
    selectPeopleText: {
        fontFamily: FONTS.FAMILY_REGULAR,
        color: COLOR.PRIMARYGREY,
        marginLeft: 15,
        marginVertical: 15,
        alignSelf: 'center',
    },
    selectPeopleView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15,
    },
    addUserIcon: {
        alignSelf: 'center',
        width: 25,
        height: 25,
        marginRight: 15,
    },

});

const mapStateToProps = (state, ownProps) => {
    return {
        singleTrip: state.tripReducer.singleTrip,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addPlace: (data) => {
            dispatch(PlanAction.addPlace(data))
        },
        updatePlacePlan: (val, data) => {
            dispatch(PlanAction.updatePlacePlan(val, data))
        }
    }
}




export default connect(mapStateToProps, mapDispatchToProps)(AddPlace);
