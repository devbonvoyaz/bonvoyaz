import React, { useRef, useState, useEffect } from 'react';
import RootStack from './src/routes/Routes';
import { AppState } from "react-native";
import API from './src/api/Api';
import { LogBox } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import Auth from './src/auth';
import { AS_FCM_TOKEN } from './src/api/Constants';


const api = new API();
const auth = new Auth();
const App = () => {
  console.disableYellowBox = true;
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  useEffect(() => {
    AppState.addEventListener("change", _handleAppStateChange);

    return () => {
      AppState.removeEventListener("change", _handleAppStateChange);
    };
  }, []);

  useEffect(() => {
    requestUserPermission();
    addFirebaseListner();
    getFcmToken();
  })


  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    await messaging().requestPermission({
      sound: true,
      alert: true,
      provisional: true,
      announcement: true,
    });

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  }

  async function addFirebaseListner() {

    let that = this;
    messaging().onMessage(async remoteMessage => {
      console.log(remoteMessage.notification.title)
      /**this.refreshData(remoteMessage);
      this.popup.show({
        onPress: function () {
          that.handleRedirection(remoteMessage);
        },
        appTitle: 'Goalprize',
        timeText: 'Now',
        val: 'called',
        title: remoteMessage
          ? remoteMessage.notification
            ? remoteMessage.notification.title
            : ''
          : '',
        body: remoteMessage
          ? remoteMessage.notification
            ? remoteMessage.notification.body
            : ''
          : '',
        slideOutTime: 3000,
      });**/
    });

    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
    });

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
      //Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
      // navigation.navigate(remoteMessage.data.type);
    });
    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
          that.refreshData(remoteMessage);
          that.handleRedirection(remoteMessage);
        }
      });
  };

  async function getFcmToken() {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      auth.setValue(AS_FCM_TOKEN, fcmToken);
      console.log('Your Firebase Token is:', fcmToken);
    } else { 
      console.log('Failed', 'No token received');
    }
  };



  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      // TODO SET USERS ONLINE STATUS TO TRUE
      setUserOnline();

    } else {
      // TODO SET USERS ONLINE STATUS TO FALSE
      setUserOffline();
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
    console.log("AppState", appState.current);
  };
  LogBox.ignoreAllLogs();

  return (

    <RootStack />
  );
};

function setUserOnline() {
  try {
    api.setUserOnline().then((json) => {
      if (json.status == 200) {
        console.log(json.data.response);


      } else {
        setTimeout(() => {
          Toast.show(json.data.response);
          console.log(json.data.response)
        }, 0);

      }
    }).catch(function (error) {
      Toast.show(error.response);
      console.log(error);
    });
  } catch (error) {
    console.log(error)
  }
}

function setUserOffline() {
  try {
    api.setUserOffline().then((json) => {
      if (json.status == 200) {
        console.log(json.data.response);


      } else {
        setTimeout(() => {
          Toast.show(json.data.response);
          console.log(json.data.response)
        }, 0);

      }
    }).catch(function (error) {
      Toast.show(error.response);
      console.log(error);
    });
  } catch (error) {
    console.log(error)
  }
}

export default App;
